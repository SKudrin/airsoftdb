<?php

namespace App\Console\Commands;

use Validator;
use DB;

use Illuminate\Console\Command;
use App\Models\Region as RegionModel;
use App\Models\Point as PointModel;

class AirsoftInfoParser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'airsoftinfo:parse';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Парсер сайта airsoftinfo.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(RegionModel $regionModel, PointModel $pointModel)
    {
        $points = json_decode(file_get_contents('http://www.airsoftinfo.ru/teams/ds.php?search_teams=1'));
        $regions = [];

        $mappedRegions = 0;
        $unAddedPoints = 0;

        $unMappedRegions = [];

        $addedPoints = 0;
        $unAddedPoints = 0;

        $stopWords = ['область', ' и ', 'обл.', 'край', 'АО', 'Республика', 'Республка', 'автономный округ'];

        foreach ($points as $point) {
            $data = [];
            //regions mapping
            if (!array_key_exists($point->region->id, $regions)) {
                $regionName = trim(str_replace($stopWords, '', $point->region->name));

                $regions[$point->region->id] = false;
                $existingRegion = DB::select('select * from regions where UPPER(name) LIKE ?', ['%' . mb_strtoupper($regionName) . '%']);

                if (!empty($existingRegion)) {
                    $this->comment($regionName . ' founded');
                    $existingRegion = $existingRegion[0];
                    $regions[$point->region->id] = $existingRegion->id;
                    $mappedRegions++;
                } else {
                    $this->comment($regionName . ' not found');
                    $unMappedRegions[] = $regionName;
                    $unAddedPoints++;
                    continue;
                }
            }

            $data['region_id'] = $regions[$point->region->id];
            $data['type_id'] = 4;
            $data['name'] = $point->name;
            $data['airsoftinfo_id'] = $point->id;
            $data['address'] = $point->region->name;

            $pointGeocodeJson = \Geocoder::geocode('json', ['address' => $point->region->name]);
            $pointGeocode = json_decode($pointGeocodeJson);

            if (isset($pointGeocode->results[0])) {
                $data['lat'] = $pointGeocode->results[0]->geometry->location->lat;
                $data['lng'] = $pointGeocode->results[0]->geometry->location->lng;
                $data['lat'] += rand(-99, 99) / 100;
                $data['lng'] += rand(-99, 99) / 100;
            }

            $validator = $this->validator($data);

            if ($validator->fails()) {
                $this->comment('point not valid');
                var_dump($validator->fails());
                $unAddedPoints++;
                continue;
            } else {
                if (empty($pointModel->where('airsoftinfo_id', '=', $point->id)->first())) {
                    $this->comment('point added');
                    $point = $pointModel->create($data);
                    $addedPoints++;
                } else {
                    $this->comment('point dublicate');
                }
            }
        }

        $totalRegions = count($regions);
        $this->comment('Total Regions: ' . $totalRegions);
        $this->comment('Mapped Regions: ' . $mappedRegions);
        $this->comment('Not Mapped Regions: ' . ($totalRegions - $mappedRegions));

        $totalPoints = count($points);
        $this->comment('Total Points: ' . $totalPoints);
        $this->comment('Added Points: ' . $addedPoints);
        $this->comment('Not Added Points: ' . ($totalPoints - $addedPoints));
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'type_id' => 'required|exists:points_types,id',
            'region_id' => 'required|exists:regions,id',
            'user_id' => 'sometimes|exists:users,id|unique:points',
            'lat' => 'required|numeric',
            'lng' => 'required|numeric',
            'phone' => 'regex:/^\d{11}$/i'
        ]);
    }
}
