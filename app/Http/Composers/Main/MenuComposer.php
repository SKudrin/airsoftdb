<?php
namespace App\Http\Composers\Main;

use Illuminate\Contracts\View\View;
use App\Models\PointsType as PointsTypeModel;
use App\Models\Region as RegionModel;

class MenuComposer {

    /**
    * PointsType model
    *
    * @var App\Models\PointsType
    */
    protected $pointsTypeModel;

    /**
    *
    * @param  App\Models\PointsType $pointsTypeModel
    * @return void
    */
    public function __construct(PointsTypeModel $pointsTypeModel, RegionModel $regionModel)
    {
        $this->pointsTypeModel = $pointsTypeModel;
        $this->regionModel = $regionModel;
    }

    /**
    * Add nothifications stats in admin view
    *
    * @param  View  $view
    * @return void
    */
    public function compose(View $view)
    {
        $pointsTypes = $this->pointsTypeModel->notEmpty()->get();
        $regions = $this->regionModel->notEmpty()->get();

        $view->with('pointsTypes', $pointsTypes)
             ->with('regions', $regions);
    }

}