<?php
namespace App\Http\Composers\Admin;

use Illuminate\Contracts\View\View;
use App\Models\User as UserModel;

class NotificationComposer {

    /**
    * User model
    *
    * @var App\Models\User
    */
    protected $UserModel;

    /**
    *
    * @param  App\Models\User $UserModel
    * @return void
    */
    public function __construct(UserModel $UserModel)
    {
        $this->UserModel = $UserModel;
    }

    /**
    * Add nothifications stats in admin view
    *
    * @param  View  $view
    * @return void
    */
    public function compose(View $view)
    {
        $totalNotifications = 0;

        $notCheckedUsers = $this->UserModel->where('is_checked', '=', false)->count();
        $totalNotifications += $notCheckedUsers;

        $view->with('notCheckedUsers', $notCheckedUsers);
        $view->with('totalNotifications', $totalNotifications);
    }

}