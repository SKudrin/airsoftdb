<?php

namespace App\Http\Controllers\Main;

use Validator;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Region as RegionModel;

class RegionController extends Controller
{
    protected $regionModel;

    public function __construct(RegionModel $regionModel)
    {
        $this->regionModel = $regionModel;
    }

    /**
     * Listing of the points as json.
     *
     * @return Response
     */
    public function indexJson(Request $request)
    {
        $regions = $this->regionModel->get();
        $activeRegionsIds = array_flatten($this->regionModel->notEmpty()->select(['regions.id'])->get()->toArray());

        foreach ($regions as $region) {
            $region->active = (false !== array_search($region->id, $activeRegionsIds)) ? true : false;
        }

        return response()->json($regions);
    }
}
