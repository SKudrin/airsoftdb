<?php

namespace App\Http\Controllers\Main;

use Validator;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Exceptions\Admin\AdminNotFoundHttpException as AdminNotFoundHttpException;

use App\Models\Point as PointModel;
use App\Models\PointsType as PointsTypeModel;
use App\Models\Country as CountryModel;
use App\Models\Region as RegionModel;
use App\Models\Photo as PhotoModel;

class ProfileController extends Controller
{
    CONST USER_AVATARS_DIR = '../public/img/uavatars/';
    CONST POINT_AVATARS_DIR = '../public/img/pavatars/';

    protected $pointsTypeModel;
    protected $pointModel;
    protected $countryModel;
    protected $regionModel;
    protected $photoModel;

    public function __construct(PointsTypeModel $pointsTypeModel, PointModel $pointModel, CountryModel $countryModel, RegionModel $regionModel, PhotoModel $photoModel)
    {
        $this->pointsTypeModel = $pointsTypeModel;
        $this->pointModel = $pointModel;
        $this->countryModel = $countryModel;
        $this->regionModel = $regionModel;
        $this->photoModel = $photoModel;
    }

    public function showProfile()
    {
        $pointsTypes = $this->pointsTypeModel->all();
        $regions = $this->regionModel->all();
        $user = Auth::user();

        return view('main.user.profile', ['pointsTypes' => $pointsTypes, 'regions' => $regions, 'user' => $user]);
    }

    public function updateUser(Request $request)
    {
        $user = Auth::user();

        if ($request->has('email'))
            $request->merge(['email' => mb_strtolower($request->get('email'))]);

        if ($request->has('email') && $request->get('email') == $user->email)
            $request->offsetUnset('email');

        $validator = $this->userValidator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        if ($request->hasFile('avatar')) {
            \Image::make($request->file('avatar'))->resize(200, 200)->save(self::USER_AVATARS_DIR . 'u_' . $user->id . '.png');
            $request->merge(['has_avatar' => true]);
        }

        $user->fill($request->all());

        $user->save();

        $process = ['success' => true, 'message' => 'User updated!', 'errors' => []];

        return redirect(route('user.profile'))->with('userUpdate', $process);
    }

    public function updatePoint(Request $request)
    {
        $id = $request->get('id');

        if (empty($id))
            abort(403, 'hacking attention');

        $validator = $this->pointValidator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $point = $this->pointModel->findOrFail($id);
        $data = $request->get('point')[$id];

        if ($request->hasFile('point.' . $id . '.avatar')) {
            \Image::make($request->file('point.' . $id . '.avatar'))->resize(200, 200)->save(self::POINT_AVATARS_DIR . 'p_' . $id . '.png');
            $data['has_avatar'] = true;
        }

        if ($request->hasFile('point.' . $id . '.photos')) {
            $photos = $request->file('point.' . $id . '.photos');
            $i = 0;

            foreach ($photos as $photo) {
                $i++;
                $name = hash("md5", $i . $photo->getClientOriginalName() . time());
                $extension = $photo->getClientOriginalExtension();

                $uploadedPhoto = \Image::make($photo);
                $uploadedPhoto->save(PhotoModel::ORIGINAL_DIR . $name . '.' . $extension);

                if ($uploadedPhoto->width() > 200) {
                    $uploadedPhoto->fit(200);
                }

                $uploadedPhoto->save(PhotoModel::SMALL_DIR . $name . '.' . $extension);

                $user = PhotoModel::create(['point_id' => $id, 'name' => $name, 'ext' => $extension]);
            }
        }

        $point->fill($data);
        $point->save();

        $process = ['success' => true, 'message' => 'Point updated!', 'id' => $id];

        return redirect(route('user.profile'))->with('pointUpdate', $process);
    }

    public function deletePoint(Request $request)
    {
        # code...
    }

    public function createPoint(Request $request)
    {
        # code...
    }

    public function changePassword(Request $request)
    {
        # code...
    }

    protected function userValidator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'sometimes|required|email|max:255|unique:users',
            'avatar' => 'sometimes|image'
        ]);
    }

    protected function pointValidator(array $data)
    {
        return Validator::make($data, [
            'point.' . $data['id'] . '.name' => 'required|max:255',
            'point.' . $data['id'] . '.type_id' => 'required|exists:points_types,id',
            'point.' . $data['id'] . '.region_id' => 'required|exists:regions,id',
            'point.' . $data['id'] . '.avatar' => 'sometimes|image',
            'point.' . $data['id'] . 'avatar' => 'sometimes|image',
            'id' => 'required|exists:points,id,user_id,' . Auth::user()->id
        ]);
    }
}
