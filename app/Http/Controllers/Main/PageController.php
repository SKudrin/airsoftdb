<?php

namespace App\Http\Controllers\Main;

use Validator;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Exceptions\Admin\AdminNotFoundHttpException as AdminNotFoundHttpException;

use App\Models\Point as PointModel;
use App\Models\PointsType as PointsTypeModel;
use App\Models\Country as CountryModel;
use App\Models\Region as RegionModel;

class PageController extends Controller
{
    protected $pointsTypeModel;
    protected $pointModel;
    protected $countryModel;
    protected $regionModel;

    public function __construct(PointsTypeModel $pointsTypeModel, PointModel $pointModel, CountryModel $countryModel, RegionModel $regionModel)
    {
        $this->pointsTypeModel = $pointsTypeModel;
        $this->pointModel = $pointModel;
        $this->countryModel = $countryModel;
        $this->regionModel = $regionModel;
    }
    /**
     * Display a main page.
     *
     * @return Response
     */
    public function main()
    {
        $pointsTypes = $this->pointsTypeModel->notEmpty()->get();
        $country = $this->countryModel->first();

        if (!empty($country)) {
            $regions = $country->regions()->notEmpty()->get();
        } else {
            $regions = [];
        }

        return view('main.index', ['pointsTypes' => $pointsTypes, 'country' => $country, 'regions' => $regions]);
    }

    public function finder(Request $request, $region = null)
    {
        $appliedFilters = [];

        if (!empty($region)) {
            $appliedFilters['region'] = $region->id;
            $points = $region->points()->active()->with('type', 'region');
        } else {
            $appliedFilters['region'] = 'nil';
            $points = $this->pointModel->whereRaw('1 != 1');
        }

        if ($request->has('ftype')) {
            if ($request->get('ftype')[0] !== 'nil') {

                $validator = $this->validator($request->all());

                if (!$validator->fails()) {
                    $appliedFilters['ftype'] = $request->get('ftype');
                    $points = $points->inTypes($appliedFilters['ftype']);
                }
            } else {
                $appliedFilters['ftype'] = ['nil'];
                $points = $points->whereRaw('1 != 1');
            }
        }

        $points = $points->get();

        $pointsTypes = $this->pointsTypeModel->notEmpty()->get();
        $regions = $this->regionModel->notEmpty()->get();

        return view('main.finder', ['pointsTypes' => $pointsTypes,
                                    'regions' => $regions,
                                    'currentRegion' => $region,
                                    'points' => $points,
                                    'pointsJson' => $points->toJson(),
                                    'appliedFilters' => $appliedFilters
                                    ]);
    }

    public function profile()
    {
        $pointsTypes = $this->pointsTypeModel->all();
        $regions = $this->regionModel->all();
        $user = Auth::user();

        return view('main.user.profile', ['pointsTypes' => $pointsTypes, 'regions' => $regions, 'user' => $user]);
    }

    // MOVE TO ADMIN PAGE CONT
    public function adminDashboard()
    {
        return view('admin.dashboard');
    }

    // AND IT
    public function adminNotFound()
    {
        throw new AdminNotFoundHttpException();
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'ftype' => 'sometimes|arrayWithNums|exists:points_types,id'
        ]);
    }
}
