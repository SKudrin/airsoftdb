<?php

namespace App\Http\Controllers\Main;

use Validator;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Point as PointModel;
use App\Models\PointsType as PointsTypeModel;
use App\Models\Country as CountryModel;
use App\Models\Region as RegionModel;

class PointController extends Controller
{
    protected $pointsTypeModel;
    protected $pointModel;
    protected $countryModel;
    protected $regionModel;

    public function __construct(PointModel $pointModel, PointsTypeModel $pointsTypeModel, CountryModel $countryModel, RegionModel $regionModel)
    {
        $this->pointsTypeModel = $pointsTypeModel;
        $this->pointModel = $pointModel;
        $this->countryModel = $countryModel;
        $this->regionModel = $regionModel;
    }

    /**
     * Display a listing of the points.
     *
     * @return Response
     */
    public function index($pointsType, $region = null)
    {
        $pointsCount = $this->pointModel->active()->ofType($pointsType)->ofRegion($region)->count();

        if ($pointsCount === 0)
            \App::abort(404);

        return view('main.point.index', ['pointsCount' => $pointsCount, 'pointsType' => $pointsType, 'region' => $region]);
    }

    /**
     * Display the specified point.
     *
     * @param  App\Models\Point  $point
     * @return Response
     */
    public function show($point)
    {
        if (!$point->active)
            \App::abort(404);

        return view('main.point.show', ['point' => $point]);
    }

    /**
     * Listing of the points as json.
     *
     * @return Response
     */
    public function indexJson(Request $request)
    {
        $validator = $this->validator($request->all());

        if (!$validator->fails()) {
            $pointsTypes = $request->get('type');
            $region = $request->get('region');
            $points = $this->pointModel->active()
                                        ->inTypes($pointsTypes)
                                        ->ofRegion($this->regionModel->whereId($region)->first())
                                        ->with('type', 'region')
                                        ->get();

        } else {
            $points = [];
        }

        return response()->json($points);
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'type' => 'sometimes|required|arrayWithNums|exists:points_types,id',
            'region' => 'sometimes|required|numeric|exists:regions,id'
        ]);
    }
}
