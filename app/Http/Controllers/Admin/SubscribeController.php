<?php

namespace App\Http\Controllers\Admin;

use Validator;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Commands\SendEmail;

use App\Models\Point as PointModel;
use App\Models\User as UserModel;
use App\Models\PointsType as PointsTypeModel;
use App\Models\Region as RegionModel;

class SubscribeController extends Controller
{
    /**
     * Display a listing of the subscribe.
     *
     * @return Response
     */
    public function index(PointModel $pointModel)
    {
        $points = $pointModel->paginate(20);

        return view('admin.subscribe.index', ['points' => $points]);
    }

    /**
     * Show the form for creating a new subscribe.
     *
     * @return Response
     */
    public function create(PointsTypeModel $pointsTypeModel, UserModel $userModel, RegionModel $regionModel)
    {
        $pointsTypes = $pointsTypeModel->all();
        $regions = $regionModel->all();

        $users = $userModel->leftJoin('points', 'points.user_id', '=', 'users.id')
                            ->whereNull('points.user_id')
                            ->select('users.id', 'users.email')
                            ->get();

        return view('admin.subscribe.create', ['pointsTypes' => $pointsTypes, 'users' => $users, 'regions' => $regions]);
    }

    /**
     * Store a newly created subscribe in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request, UserModel $userModel)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $subject = $request->get('subject');
        $body = $request->get('body');

        if ($request->has('test_mail'))
        {
            $test = $request->get('test_mail');

            $this->dispatch(
                new SendEmail($test, $subject, $body)
            );

            $mailsSended = 1;
            $request->flash();
        }
        else
        {
            $users = $userModel->join('points', 'users.id', '=', 'points.user_id')
                           ->select('users.email')
                           ->where(function ($query) use ($request) {
                                $query->whereIn('points.type_id', $request->get('type_id'))
                                      ->where('users.email', 'LIKE', '%@%');
                           })->distinct()->get();

            foreach ($users as $user) {
                $this->dispatch(
                    new SendEmail($user->email, $subject, $body)
                );
            }

            $mailsSended = count($users);
        }

        $process = ['success' => true, 'message' => $mailsSended . ' mails sended'];
        return redirect(route('admin.subscribe.create'))->with('process', $process);
    }

    public function validator(array $data)
    {
        return Validator::make($data, [
            'type_id' => 'required_without:test_mail|arrayWithNums',
            'subject' => 'required|string',
            'body'    => 'required|string',
            'test_mail'    => 'sometimes|email'
        ]);
    }
}
