<?php

namespace App\Http\Controllers\Admin;

use Validator;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\Models\PointsType as PointsTypeModel;

class PointsTypeController extends Controller
{
    /**
     * Display a listing of the points types.
     *
     * @return Response
     */
    public function index(PointsTypeModel $pointsTypeModel)
    {
        $pointsTypes = $pointsTypeModel->paginate(20);

        return view('admin.pointstype.index', ['pointsTypes' => $pointsTypes]);
    }

    /**
     * Show the form for creating a new points type.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.pointstype.create');
    }

    /**
     * Store a newly created points type in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $pointsType = PointsTypeModel::create($request->all());

        $process = ['success' => true, 'message' => 'Points type created!'];

        return redirect(route('admin.pointsType.show', $pointsType))->with('create', $process);
    }

    /**
     * Display the specified points type.
     *
     * @param  App\Models\PointsType  $pointsType
     * @return Response
     */
    public function show($pointsType)
    {
        return view('admin.pointstype.show', ['pointsType' => $pointsType]);
    }

    /**
     * Show the form for editing the specified points type.
     *
     * @param  App\Models\PointsType  $pointsType
     * @return Response
     */
    public function edit($pointsType)
    {
        return view('admin.pointstype.edit', ['pointsType' => $pointsType]);
    }

    /**
     * Update the specified points type in storage.
     *
     * @param  Request  $request
     * @param  App\Models\PointsType  $pointsType
     * @return Response
     */
    public function update(Request $request, $pointsType)
    {
        if ($request->has('name') && $request->get('name') === $pointsType->name)
            $request->offsetUnset('name');

        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $pointsType->fill($request->all());
        $pointsType->save();

        $process = ['success' => true, 'message' => 'Points type updated!'];

        return redirect(route('admin.pointsType.edit', $pointsType))->with('update', $process);
    }

    /**
     * Remove the specified points type from storage.
     *
     * @param  App\Models\PointsType  $pointsType
     * @return Response
     */
    public function destroy($pointsType)
    {
        $pointsType->delete();
        $process = ['success' => true, 'message' => 'Points type removed!'];

        return redirect()->back()->with('delete', $process);
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'sometimes|unique:points_types|required|max:255'
        ]);
    }
}
