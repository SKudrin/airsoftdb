<?php

namespace App\Http\Controllers\Admin;

use Validator;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\Models\Country as CountryModel;

class CountryController extends Controller
{
    /**
     * Display a listing of the countries.
     *
     * @return Response
     */
    public function index(CountryModel $countryModel)
    {
        $countries = $countryModel->paginate(20);

        return view('admin.country.index', ['countries' => $countries]);
    }

    /**
     * Show the form for creating a new country.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.country.create');
    }

    /**
     * Store a newly created country in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $country = CountryModel::create($request->all());

        $process = ['success' => true, 'message' => 'Country created!'];

        return redirect(route('admin.country.show', $country))->with('create', $process);
    }

    /**
     * Display the specified country.
     *
     * @param  App\Models\Country  $country
     * @return Response
     */
    public function show($country)
    {
        return view('admin.country.show', ['country' => $country]);
    }

    /**
     * Show the form for editing the specified country.
     *
     * @param  App\Models\Country  $country
     * @return Response
     */
    public function edit($country)
    {
        return view('admin.country.edit', ['country' => $country]);
    }

    /**
     * Update the specified country in storage.
     *
     * @param  Request  $request
     * @param  App\Models\Country  $country
     * @return Response
     */
    public function update(Request $request, $country)
    {
        if ($request->has('name') && $request->get('name') === $country->name)
            $request->offsetUnset('name');

        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $country->fill($request->all());
        $country->save();

        $process = ['success' => true, 'message' => 'Country updated!'];

        return redirect(route('admin.country.edit', $country))->with('update', $process);
    }

    /**
     * Remove the specified country from storage.
     *
     * @param  App\Models\Country  $country
     * @return Response
     */
    public function destroy($country)
    {
        $country->delete();
        $process = ['success' => true, 'message' => 'Country removed!'];

        return redirect()->back()->with('delete', $process);
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'sometimes|unique:countries|required|max:255'
        ]);
    }
}
