<?php

namespace App\Http\Controllers\Admin;

use Validator;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\Models\Region as RegionModel;
use App\Models\Country as CountryModel;

class RegionController extends Controller
{
    /**
     * Display a listing of the regions.
     *
     * @return Response
     */
    public function index(RegionModel $regionModel)
    {
        $regions = $regionModel->paginate(20);

        return view('admin.region.index', ['regions' => $regions]);
    }

    /**
     * Show the form for creating a new region.
     *
     * @return Response
     */
    public function create(CountryModel $countryModel)
    {
        $countries = $countryModel->all();

        return view('admin.region.create', ['countries' => $countries]);
    }

    /**
     * Store a newly created region in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request, RegionModel $regionModel)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $region = $regionModel->create($request->all());

        $process = ['success' => true, 'message' => 'Region created!'];

        return redirect(route('admin.region.show', $region))->with('create', $process);
    }

    /**
     * Display the specified region.
     *
     * @param  App\Models\Region  $region
     * @return Response
     */
    public function show($region)
    {
        return view('admin.region.show', ['region' => $region]);
    }

    /**
     * Show the form for editing the specified region.
     *
     * @param  App\Models\Region  $region
     * @return Response
     */
    public function edit(CountryModel $countryModel, $region)
    {
        $countries = $countryModel->all();

        return view('admin.region.edit', ['region' => $region, 'countries' => $countries]);
    }

    /**
     * Update the specified region in storage.
     *
     * @param  Request  $request
     * @param  App\Models\Region  $region
     * @return Response
     */
    public function update(Request $request, $region)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $region->fill($request->all());
        $region->save();

        $process = ['success' => true, 'message' => 'Region updated!'];

        return redirect(route('admin.region.edit', $region))->with('update', $process);
    }

    /**
     * Remove the specified region from storage.
     *
     * @param  App\Models\Region  $region
     * @return Response
     */
    public function destroy($region)
    {
        $region->delete();
        $process = ['success' => true, 'message' => 'Region removed!'];

        return redirect()->back()->with('delete', $process);
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'country_id' => 'required|exists:countries,id'
        ]);
    }
}
