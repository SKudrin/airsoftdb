<?php

namespace App\Http\Controllers\Admin;

use Validator;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\Models\Point as PointModel;
use App\Models\User as UserModel;
use App\Models\PointsType as PointsTypeModel;
use App\Models\Region as RegionModel;

class PointController extends Controller
{
    /**
     * Display a listing of the points.
     *
     * @return Response
     */
    public function index(PointModel $pointModel)
    {
        $points = $pointModel->paginate(20);

        return view('admin.point.index', ['points' => $points]);
    }

    /**
     * Show the form for creating a new point.
     *
     * @return Response
     */
    public function create(PointsTypeModel $pointsTypeModel, UserModel $userModel, RegionModel $regionModel)
    {
        $pointsTypes = $pointsTypeModel->all();
        $regions = $regionModel->all();

        $users = $userModel->leftJoin('points', 'points.user_id', '=', 'users.id')
                            ->whereNull('points.user_id')
                            ->select('users.id', 'users.email')
                            ->get();

        return view('admin.point.create', ['pointsTypes' => $pointsTypes, 'users' => $users, 'regions' => $regions]);
    }

    /**
     * Store a newly created point in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request, PointModel $pointModel)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $point = $pointModel->create($request->all());

        $process = ['success' => true, 'message' => 'Point created!'];

        return redirect(route('admin.point.show', $point))->with('create', $process);
    }

    /**
     * Display the specified point.
     *
     * @param  App\Models\Point  $point
     * @return Response
     */
    public function show($point)
    {
        return view('admin.point.show', ['point' => $point]);
    }

    /**
     * Show the form for editing the specified point.
     *
     * @param  App\Models\Point  $point
     * @return Response
     */
    public function edit(PointsTypeModel $pointsTypeModel, UserModel $userModel, RegionModel $regionModel, $point)
    {
        $pointsTypes = $pointsTypeModel->all();
        $regions = $regionModel->all();

        $users = $userModel->leftJoin('points', 'points.user_id', '=', 'users.id')
                            ->whereNull('points.user_id')
                            ->orWhere('points.id', $point->id)
                            ->select('users.id', 'users.email')
                            ->get();

        return view('admin.point.edit', ['point' => $point, 'pointsTypes' => $pointsTypes, 'users' => $users, 'regions' => $regions]);
    }

    /**
     * Update the specified point in storage.
     *
     * @param  Request  $request
     * @param  App\Models\Point  $point
     * @return Response
     */
    public function update(Request $request, $point)
    {
        if ($request->has('user_id') && $request->get('user_id') == $point->userId)
            $request->offsetUnset('user_id');

        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $point->fill($request->all());
        $point->save();

        $process = ['success' => true, 'message' => 'Point updated!'];

        return redirect(route('admin.point.edit', $point))->with('update', $process);
    }

    /**
     * Remove the specified point from storage.
     *
     * @param  App\Models\Point  $point
     * @return Response
     */
    public function destroy($point)
    {
        $point->delete();
        $process = ['success' => true, 'message' => 'Point removed!'];

        return redirect()->back()->with('delete', $process);
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'type_id' => 'required|exists:points_types,id',
            'region_id' => 'required|exists:regions,id',
            'user_id' => 'sometimes|exists:users,id|unique:points',
            'lat' => 'required|numeric',
            'lng' => 'required|numeric'
        ]);
    }
}
