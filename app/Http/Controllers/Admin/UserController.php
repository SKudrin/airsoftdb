<?php

namespace App\Http\Controllers\Admin;

use Validator;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\Models\User as UserModel;
use App\Models\Point as PointModel;
use App\Models\PointsType as PointsTypeModel;
use App\Models\Region as RegionModel;

class UserController extends Controller
{
    /**
     * Display a listing of the users.
     *
     * @return Response
     */
    public function index(UserModel $userModel)
    {
        $users = $userModel->paginate(20);

        return view('admin.user.index', ['users' => $users]);
    }

    /**
     * Show the form for creating a new user.
     *
     * @return Response
     */
    public function create(PointModel $pointModel, PointsTypeModel $pointsTypeModel, RegionModel $regionModel)
    {
        $pointsTypes = $pointsTypeModel->all();
        $regions = $regionModel->all();
        $points = $pointModel->whereNull('user_id')->get();

        return view('admin.user.create', ['pointsTypes' => $pointsTypes, 'points' => $points, 'regions' => $regions]);
    }

    /**
     * Store a newly created user in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request, PointModel $pointModel)
    {
        if ($request->has('email'))
            $request->merge(['email' => mb_strtolower($request->get('email'))]);

        if ($request->has('password'))
            $request->merge(['password' => bcrypt($request->get('password'))]);

        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $user = UserModel::create($request->all());

        $user->isActive = $request->has('is_active');
        $user->isAdmin = $request->has('is_admin');
        $user->isChecked = true;

        $user->save();

        // creating/select point
        if ($request->has('create_point')) {
            $user->points()->create($request->get('point'));
        } elseif ($request->has('point_id')) {
            $points = $pointModel->find($request->get('point_id'));
            $user->points()->saveMany($points);
        }

        $process = ['success' => true, 'message' => 'User created!'];

        return redirect(route('admin.user.show', $user))->with('create', $process);
    }

    /**
     * Display the specified user.
     *
     * @param  App\Models\User  $user
     * @return Response
     */
    public function show($user)
    {
        return view('admin.user.show', ['user' => $user]);
    }

    /**
     * Show the form for editing the specified user.
     *
     * @param  App\Models\User  $user
     * @return Response
     */
    public function edit(PointModel $pointModel, PointsTypeModel $pointsTypeModel, $user)
    {
        $pointsTypes = $pointsTypeModel->all();
        $points = $pointModel->whereNull('user_id')->orWhere('user_id', $user->id)->get();

        return view('admin.user.edit', ['user' => $user, 'points' => $points, 'pointsTypes' => $pointsTypes]);
    }

    /**
     * Update the specified user in storage.
     *
     * @param  Request  $request
     * @param  App\Models\User  $user
     * @return Response
     */
    public function update(Request $request, PointModel $pointModel, $user)
    {
        if ($request->has('email'))
            $request->merge(['email' => mb_strtolower($request->get('email'))]);

        if ($request->has('email') && $request->get('email') == $user->email)
            $request->offsetUnset('email');

        if ($request->has('password') && $request->get('password') == $user->password)
            $request->offsetUnset('password');

        if ($request->has('password'))
            $request->merge(['password' => bcrypt($request->get('password'))]);

        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        $user->fill($request->all());

        $user->isActive = $request->has('is_active');
        $user->isAdmin = $request->has('is_admin');
        $user->isChecked = true;

        $user->save();

        if (!empty($request->get('point_id')))
            $points = $pointModel->find($request->get('point_id'));

        $user->points()->saveMany(isset($points) ? $points : []);

        $process = ['success' => true, 'message' => 'User updated!'];

        return redirect(route('admin.user.edit', $user))->with('update', $process);
    }

    /**
     * Remove the specified user from storage.
     *
     * @param  App\Models\User  $user
     * @return Response
     */
    public function destroy($user)
    {
        if ($user->id !== Auth::user()->id) {
            $user->delete();
            $process = ['success' => true, 'message' => 'User removed!'];
        } else {
            $process = ['success' => false, 'message' => 'You can\'t remove you self!'];;
        }

        return redirect()->back()->with('delete', $process);
    }

    /**
     * Active/Inactive the specified user.
     *
     * @param  App\Models\User  $user
     * @return Response
     */
    public function active(Request $request, $user)
    {
        $user->isActive = $request->has('is_active');
        $user->isChecked = true;
        $user->save();

        $message = 'User ' . ($user->isActive ? 'activated' : 'deactivated') . '!';
        $process = ['success' => true, 'active' => $user->isActive, 'message' => $message];

        return redirect(route('admin.user.show', $user))->with('active', $process);
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'sometimes|required|email|max:255|unique:users',
            'password' => 'sometimes|required|min:6',
            'point_id' => 'exists:points,id',
            'point.name' => 'required_if:create_point,on|max:255',
            'point.type_id' => 'required_if:create_point,on|exists:points_types,id',
            'point.lat' => 'required_if:create_point,on|numeric',
            'point.lng' => 'required_if:create_point,on|numeric',
            'point.region_id' => 'required_if:create_point,on|exists:regions,id'
        ]);
    }
}
