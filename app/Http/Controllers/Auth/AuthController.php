<?php

namespace App\Http\Controllers\Auth;

use App\Models\User as UserModel;
use App\Models\PointsType as PointsTypeModel;
use App\Models\Region as RegionModel;

use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => ['getLogout']]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            'password_confirmation' => 'required|same:password',
            'point.name' => 'required|max:255',
            'point.address' => 'required',
            'point.type_id' => 'required|exists:points_types,id',
            'point.region_id' => 'required|exists:regions,id'
        ]);
    }

    /**
     * Show the application login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogin()
    {
        if (view()->exists('main.auth.authenticate')) {
            return view('main.auth.authenticate');
        }

        return view('main.auth.login');
    }

    /**
     * Get the path to the admin login route.
     *
     * @return string
     */
    public function adminLoginPath()
    {
        return property_exists($this, 'adminLoginPath') ? $this->adminLoginPath : '/admin/login';
    }

    /**
     * Show the admin login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAdminLogin()
    {
        if (view()->exists('admin.auth.authenticate')) {
            return view('admin.auth.authenticate');
        }

        return view('admin.auth.login');
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getRegister(PointsTypeModel $pointsTypeModel, RegionModel $regionModel)
    {
        $pointsTypes = $pointsTypeModel->all();
        $regions = $regionModel->all();

        return view('main.auth.register', ['pointsTypes' => $pointsTypes, 'regions' => $regions]);
    }


    /**
     * Handle a admin login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postAdminLogin(Request $request)
    {
        $this->validate($request, [
            $this->loginUsername() => 'required', 'password' => 'required',
        ]);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        $throttles = $this->isUsingThrottlesLoginsTrait();

        if ($throttles && $this->hasTooManyLoginAttempts($request)) {
            return $this->sendLockoutResponse($request);
        }

        $credentials = $this->getCredentials($request);

        // must have admin and active flags
        $credentials['is_admin'] = true;
        $credentials['is_active'] = true;

        // case insensitive username
        $credentials[$this->loginUsername()] = mb_strtolower($credentials[$this->loginUsername()]);

        if (Auth::attempt($credentials, $request->has('remember'))) {
            return $this->handleAdminWasAuthenticated($request, $throttles);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        if ($throttles) {
            $this->incrementLoginAttempts($request);
        }

        return redirect($this->adminLoginPath())
            ->withInput($request->only($this->loginUsername(), 'remember'))
            ->withErrors([
                $this->loginUsername() => $this->getFailedLoginMessage(),
            ]);
    }

    /**
     * Handle a user login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postLogin(Request $request)
    {
        $this->validate($request, [
            $this->loginUsername() => 'required', 'password' => 'required',
        ]);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        $throttles = $this->isUsingThrottlesLoginsTrait();

        if ($throttles && $this->hasTooManyLoginAttempts($request)) {
            return $this->sendLockoutResponse($request);
        }

        $credentials = $this->getCredentials($request);

        // must have active flag
        $credentials['is_active'] = true;

        // case insensitive username
        $credentials[$this->loginUsername()] = mb_strtolower($credentials[$this->loginUsername()]);

        if (Auth::attempt($credentials, $request->has('remember'))) {
            return $this->handleUserWasAuthenticated($request, $throttles);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        if ($throttles) {
            $this->incrementLoginAttempts($request);
        }

        return redirect($this->loginPath())
            ->withInput($request->only($this->loginUsername(), 'remember'))
            ->withErrors([
                $this->loginUsername() => $this->getFailedLoginMessage(),
            ]);
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  bool  $throttles
     * @return \Illuminate\Http\Response
     */
    protected function handleAdminWasAuthenticated(Request $request, $throttles)
    {
        if ($throttles) {
            $this->clearLoginAttempts($request);
        }

        if (method_exists($this, 'authenticated')) {
            return $this->authenticated($request, Auth::user());
        }

        return redirect()->intended($this->adminRedirectPath());
    }

    /**
     * Get the post admin login redirect path.
     *
     * @return string
     */
    public function adminRedirectPath()
    {
        if (property_exists($this, 'redirectPath')) {
            return $this->redirectPath;
        }

        return property_exists($this, 'redirectTo') ? $this->redirectTo : '/admin';
    }

    /**
     * Get the post login redirect path.
     *
     * @return string
     */
    public function redirectPath()
    {
        if (property_exists($this, 'redirectPath')) {
            return $this->redirectPath;
        }

        return property_exists($this, 'redirectTo') ? $this->redirectTo : '/';
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postRegister(Request $request, RegionModel $regionModel)
    {
        if ($request->has('email'))
            $request->merge(['email' => mb_strtolower($request->get('email'))]);

        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        if ($request->has('password'))
            $request->merge(['password' => bcrypt($request->get('password'))]);

        $point = $request->get('point');
        $region = $regionModel->find($point['region_id']);

        $pointGeocodeJson = \Geocoder::geocode('json', ['address' => $region->name . ' ' . $request->get('point')['address']]);
        $pointGeocode = json_decode($pointGeocodeJson);

        if (isset($pointGeocode->results[0])) {
            $point['lat'] = $pointGeocode->results[0]->geometry->location->lat;
            $point['lng'] = $pointGeocode->results[0]->geometry->location->lng;
        } else {
            $point['lat'] = 0;
            $point['lng'] = 0;
        }

        $user = UserModel::create($request->all());
        $user->save();

        $user->points()->create($point);

        Auth::login($user);

        $this->redirectPath = route('register.success');

        return redirect($this->redirectPath());
    }

    public function registerSuccess()
    {
        return view('main.auth.registersuccess');
    }
}
