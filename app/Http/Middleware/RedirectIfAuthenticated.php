<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;

class RedirectIfAuthenticated
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Request::is('admin/*'))
        {
            if ($this->auth->check() && $this->auth->user()->isAdmin && $this->auth->user()->isActive) {
                return redirect(route('admin.home'));
            }
        } else {
            if ($this->auth->check() && $this->auth->user()->isActive) {
                return redirect(route('home'));
            }
        }

        return $next($request);
    }
}
