<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['uses' => 'Main\PageController@main', 'as' => 'home']);


Route::get('/finder/{region?}', ['uses' => 'Main\PageController@finder', 'as' => 'finder'])
        ->where('region', '[0-9]+');

// logout any user
Route::get('auth/logout', ['uses' => 'Auth\AuthController@getLogout', 'as' => 'logout']);

/* USER ROUTES */

// user login page
Route::get('auth/login', ['uses' => 'Auth\AuthController@getLogin', 'as' => 'login']);

// user register page
Route::get('auth/register', ['uses' => 'Auth\AuthController@getRegister', 'as' => 'register.page']);

// user register
Route::post('auth/register', ['uses' => 'Auth\AuthController@postRegister', 'as' => 'register']);

// user register success
Route::get('auth/register/success', ['uses' => 'Auth\AuthController@registerSuccess', 'as' => 'register.success']);

// user login page
Route::post('auth/login', ['uses' => 'Auth\AuthController@postLogin', 'as' => 'auth']);

/* PROFILE ROUTES */

// profile page
Route::get('/profile', ['uses' => 'Main\ProfileController@showProfile', 'as' => 'user.profile', 'middleware' => 'auth']);

// profile update user
Route::post('/profile/update/user', ['uses' => 'Main\ProfileController@updateUser', 'as' => 'profile.update_user', 'middleware' => 'auth']);

// profile update user
Route::post('/profile/update/point', ['uses' => 'Main\ProfileController@updatePoint', 'as' => 'profile.update_point', 'middleware' => 'auth']);

// profile change password
Route::post('/profile/change_password', ['uses' => 'Main\ProfileController@changePassword', 'as' => 'profile.change_password', 'middleware' => 'auth']);

// point show
Route::get('point/{point}/show', ['uses' => 'Main\PointController@show', 'as' => 'point.show'])->where('point', '[0-9]+');

// points list
Route::get('point/{pointsType}/{region?}/', ['uses' => 'Main\PointController@index', 'as' => 'point.index'])
        ->where('pointsType', '[0-9]+')
        ->where('region', '[0-9]+');

// points list json
Route::get('/api/point', ['uses' => 'Main\PointController@indexJson', 'as' => 'point.index_json']);

// regions list json
Route::get('/api/region', ['uses' => 'Main\RegionController@indexJson', 'as' => 'region.index_json']);


/* ADMIN ROUTES */

// admin login page
Route::get('admin/login', ['uses' => 'Auth\AuthController@getAdminLogin', 'as' => 'admin.login']);

// admin auth
Route::post('admin/login', ['uses' => 'Auth\AuthController@postAdminLogin', 'as' => 'admin.auth']);

Route::group(['prefix' => 'admin', 'middleware' => 'auth.admin'], function() {

    Route::get('/', ['uses' => 'Main\PageController@adminDashboard', 'as' => 'admin.home']);

    // admin users CRUD
    Route::resource('user', 'Admin\UserController');
    // admin users other
    Route::post('user/{user}/active', ['uses' => 'Admin\UserController@active', 'as' => 'admin.user.active'])->where('user', '[0-9]+');

    // admin points types CRUD
    Route::resource('pointsType', 'Admin\PointsTypeController');

    // admin country CRUD
    Route::resource('country', 'Admin\CountryController');

    // admin region CRUD
    Route::resource('region', 'Admin\RegionController');

    // admin points CRUD
    Route::resource('point', 'Admin\PointController');

    // subscribe
    Route::get('subscribe', ['uses' => 'Admin\SubscribeController@index', 'as' => 'admin.subscribe']);

    // subscribe
    Route::post('subscribe/store', ['uses' => 'Admin\SubscribeController@store', 'as' => 'admin.subscribe.store']);


    Route::get('subscribe/create', ['uses' => 'Admin\SubscribeController@create', 'as' => 'admin.subscribe.create']);

    /*
    * missing routes of admin group
    */
    Route::any('{_missing}', ['uses' => 'Main\PageController@adminNotFound'])
        ->where('_missing', '.*+');
});
