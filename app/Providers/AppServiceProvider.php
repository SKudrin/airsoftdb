<?php

namespace App\Providers;

use Blade;
use Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::extend(function($value) {
            return preg_replace('/\@define(.+)/', '<?php ${1}; ?>', $value);
        });

        Validator::extend('arrayWithNums', function($attribute, $value, $parameters) {
            if (!is_array($value)) {
                return false;
            } else {
                foreach ($value as $item) {
                    if (!is_numeric($item)) {
                        return false;
                    }
                }
            }

            return true;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
