<?php
namespace App\Providers;

use View;
use Illuminate\Support\ServiceProvider;
use App\Http\Composers\Admin\NotificationComposer;
use App\Http\Composers\Main\MenuComposer;
use App\Models\User;

class ComposerServiceProvider extends ServiceProvider {

    /**
    * Register composers
    *
    * @return void
    */
    public function boot()
    {
        View::composer('admin.partials.mainheader', 'App\Http\Composers\Admin\NotificationComposer');
        View::composer('main.partials.topmenu', 'App\Http\Composers\Main\MenuComposer');
    }

    /**
    * Register composer services
    *
    * @return void
    */
    public function register()
    {
        $this->app->singleton('App\Http\Composers\Admin\NotificationComposer', function($app)
        {
            return new NotificationComposer($app['App\Models\User']);
        });

        $this->app->singleton('App\Http\Composers\Main\MenuComposer', function($app)
        {
            return new MenuComposer($app['App\Models\PointsType'], $app['App\Models\Region']);
        });
    }

}