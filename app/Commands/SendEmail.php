<?php

namespace App\Commands;

use Symfony\Component\Console\Command\Command;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmail extends Command implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $from = 'admin@airsoftlands.com';
    protected $fromName = 'airsoftlands';
    protected $to;
    protected $subject;
    protected $body;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct($to, $subject, $body)
    {
        $this->to = $to;
        $this->subject = $subject;
        $this->body = $body;
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        $to = $this->to;
        $from = $this->from;
        $fromName = $this->fromName;
        $subject = $this->subject;

        \Mail::send(['email.html.blank', 'email.text.blank'], ['msg' => $this->body], function($message) use ($to, $from, $fromName, $subject)
        {
            $message->from($from, $fromName);
            $message->to($to);
            $message->subject($subject);
        });
    }
}
