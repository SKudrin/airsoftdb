<?php

/*
 * This is simple NotFoundHttpException for admin routes group, handler use admin template.
 */

namespace App\Exceptions\Admin;
use \Symfony\Component\HttpKernel\Exception\NotFoundHttpException as NotFoundHttpException;

/**
 * NotFoundHttpException.
 *
 * @author Sergey Kudrin <sergio.kudrin@gmail.com>
 */
class AdminNotFoundHttpException extends NotFoundHttpException
{

}
