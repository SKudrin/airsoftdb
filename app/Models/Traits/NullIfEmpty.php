<?php

namespace App\Models\Traits;

trait NullIfEmpty
{
    public static function boot()
    {
        parent::boot();

        static::saving(function($model) {
            $model->beforeSave();
        });
    }

    /**
     * Replace empty value for nullable fields to null.
     */
    public function beforeSave()
    {
        if (isset($this->nullable) and is_array($this->nullable) and !empty($this->nullable)) {
            foreach ($this->attributes as $key => &$value) {
                if (in_array($key, $this->nullable)) {
                    if ('' === $value) $value = null;
                }
            }
        }
    }
}
