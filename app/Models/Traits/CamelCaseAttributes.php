<?php

namespace App\Models\Traits;

trait CamelCaseAttributes
{
    /**
    * Override the default functionality so we may access attributes via camelCase along with snake case. E.g.,
    *
    * echo $object->first_name;
    * echo $object->firstName;
    *
    * @param string $key
    * @return mixed
    */
    public function getAttribute($key)
    {
        return parent::getAttribute(snake_case($key));
    }

    /**
    * Override the default functionality so we may set attributes via camelCase along with snake case. E.g.,
    *
    * $object->first_name = "Bob";
    * $object->firstName = "Bob";
    *
    * @param string $key
    * @param mixed $value
    * @return mixed
    */
    public function setAttribute($key, $value)
    {
        return parent::setAttribute(snake_case($key), $value);
    }
}