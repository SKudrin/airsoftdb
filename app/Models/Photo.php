<?php

namespace App\Models;

use App\Models\Traits\CamelCaseAttributes;
use App\Models\Traits\NullIfEmpty;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    CONST ORIGINAL_DIR = '../public/img/photos/o/';
    CONST SMALL_DIR = '../public/img/photos/s/';

    use CamelCaseAttributes;

    public static $snakeAttributes = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'photos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'ext', 'point_id'];

    public function point()
    {
        return $this->belongsTo('App\Models\Point', 'point_id');
    }
}
