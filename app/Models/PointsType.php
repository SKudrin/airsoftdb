<?php

namespace App\Models;

use App\Models\Traits\CamelCaseAttributes;
use Illuminate\Database\Eloquent\Model;

use App\Models\Region as RegionModel;

class PointsType extends Model
{
    use CamelCaseAttributes;

    public static $snakeAttributes = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'points_types';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description'];

    public function points()
    {
        return $this->hasMany('App\Models\Point', 'type_id')->orderBy('name', 'desc');
    }

    public function getRegionsAttribute()
    {
        return RegionModel::join('points', 'points.region_id', '=', 'regions.id')
                          ->join('points_types', 'points.type_id', '=', 'points_types.id')
                          ->leftJoin('users', 'points.user_id', '=', 'users.id')
                          ->select('regions.id', 'regions.name')
                          ->where('points_types.id', '=', $this->id)
                          ->where(function ($query) {
                              $query->where('users.is_active', '=', 'true')
                                    ->orWhereNull('users.id');
                              })
                          ->groupBy('regions.id', 'regions.name')
                          ->get();
    }

    public function getIconAttribute()
    {
      return '/img/icons/menu/field.svg';
    }

    public function scopeNotEmpty($query)
    {
        return $query->join('points', 'points.type_id', '=', 'points_types.id')
                     ->leftJoin('users', 'users.id', '=', 'points.user_id')
                     ->select('points_types.*')
                     ->where(function ($query) {
                        $query->where('users.is_active', '=', 'true')
                              ->orWhereNull('users.id');
                     })
                     ->groupBy('points_types.id', 'points_types.name');
    }
}
