<?php

namespace App\Models;

use App\Models\Traits\CamelCaseAttributes;
use App\Models\Traits\NullIfEmpty;

use Illuminate\Database\Eloquent\Model;

class Point extends Model
{
    use CamelCaseAttributes, NullIfEmpty;

    public static $snakeAttributes = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'points';

    /**
    * Nullable fields auto convert to null if empty
    */
    protected $nullable = ['user_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['user_id', 'created_at', 'updated_at', 'airsoftinfo_id'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'address', 'phone', 'description', 'lat', 'lng', 'type_id', 'user_id', 'region_id', 'airsoftinfo_id', 'has_avatar', 'is_vip', 'is_bad'];

    public function type()
    {
        return $this->belongsTo('App\Models\PointsType', 'type_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function region()
    {
        return $this->belongsTo('App\Models\Region');
    }

    public function photos()
    {
        return $this->hasMany('App\Models\Photo', 'point_id')->orderBy('id', 'desc');
    }


    public function getHasPhotoAttribute()
    {
      return false;
    }

    public function getAvatarAttribute()
    {
        return '/img/pika.png';
    }

    public function getActiveAttribute()
    {
        $count = $this->leftJoin('users', 'users.id', '=', 'points.user_id')
                    ->select('points.*')
                    ->where('points.id', '=', $this->id)
                    ->where(function ($query) {
                        $query->where('users.is_active', '=', 'true')
                              ->orWhereNull('users.id');
                    })
                    ->count();

        return $count > 0;
    }

    public function scopeActive($query)
    {
        return $query->leftJoin('users', 'users.id', '=', 'points.user_id')
                     ->select('points.*')
                     ->where(function ($query) {
                        $query->where('users.is_active', '=', 'true')
                              ->orWhereNull('users.id');
                     });
    }

    public function scopeOfType($query, \App\Models\pointsType $pointsType = null)
    {
        if (!empty($pointsType)) {
            $query = $query->whereTypeId($pointsType->id);
        }

        return $query;
    }

    public function scopeInTypes($query, array $pointsTypes = null)
    {
        if (!empty($pointsTypes)) {
            $query = $query->whereIn('type_id', $pointsTypes);
        }

        return $query;
    }

    public function scopeOfRegion($query, \App\Models\Region $region = null)
    {
        if (!empty($region)) {
            $query = $query->whereRegionId($region->id);
        }

        return $query;
    }
}
