<?php

namespace App\Models;

use App\Models\Traits\CamelCaseAttributes;
use App\Models\PointsType as PointsType;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    use CamelCaseAttributes;

    public static $snakeAttributes = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'regions';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['country_id', 'name'];

    public function country()
    {
        return $this->belongsTo('App\Models\Country', 'country_id');
    }

    public function points()
    {
        return $this->hasMany('App\Models\Point')->orderBy('name', 'desc');
    }

    public function getNameAttribute($name)
    {
        return mb_convert_case($name, MB_CASE_TITLE, "UTF-8");
    }

    public function scopeNotEmpty($query)
    {
        return $query->join('points', 'points.region_id', '=', 'regions.id')
                     ->leftJoin('users', 'users.id', '=', 'points.user_id')
                     ->select('regions.*')
                     ->where(function ($query) {
                        $query->where('users.is_active', '=', 'true')
                              ->orWhereNull('users.id');
                     })
                     ->groupBy('regions.id', 'regions.name');
    }
}
