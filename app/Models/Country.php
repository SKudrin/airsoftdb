<?php

namespace App\Models;

use App\Models\Traits\CamelCaseAttributes;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    use CamelCaseAttributes;

    public static $snakeAttributes = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'countries';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    public function regions()
    {
        return $this->hasMany('App\Models\Region', 'country_id')->orderBy('name', 'desc');
    }

    public function scopeActive($query)
    {
        return $query->join('regions', 'regions.country_id', '=', 'countries.id')
                     ->join('points', 'points.region_id', '=', 'regions.id')
                     ->leftJoin('users', 'users.id', '=', 'points.user_id')
                     ->select('countries.*')
                     ->where(function ($query) {
                        $query->where('users.is_active', '=', 'true')
                              ->orWhereNull('users.id');
                     })
                     ->groupBy('countries.id', 'countries.name');
    }
}
