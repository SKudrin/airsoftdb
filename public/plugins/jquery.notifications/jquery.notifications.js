
    

(function($) {

var settings_object_driven = {      
    verbose: false,
    onMouseUp: false,
    onMouseDown: false
};
  
$.fn.notifyMenu = function( options, callback ){
    var objects = [], return_data;
    var mmenu = {};

    this.each(function(){
    	// initialize the object here (add methods, event bindings, etc)
    if($(this).data('object') === undefined){
        var object = {};
        object.$this = $(this);
        object.settings = $.extend( settings_object_driven, options );

        if(object.settings.verbose) console.log("Initializing Notifications Menu");

        

        // create mmenu instance
        /*
        object.$menu = $('#'+$(this).attr('id')).mmenu(
            {
                position  : 'right',
                counters  : true,
                searchfield : true,                            
                dragOpen   : {
                    open       : true,
                    threshold  : 10            
                }
            },
            {hardwareAcceleration: false}
        );
        */
        
        object.$menu = $('#'+$(this).attr('id')).mmenu(
            {
                position  : 'right',
                counters  : true,
                searchfield : true                
            },
            {hardwareAcceleration: false}
        );


        // create a mapping between the menu items and the submenu pages
        mmenu.conf = object.$menu.mmenu.getConf();
        mmenu.conf.classes.removeme = 'mm-remove-me';        
        mmenu.conf.classes.notification = 'mm-notification';

        mmenu.map = {};
        object.$menu.find('a.'+mmenu.conf.classes.subopen).each(function(){
            submenu = $(this).attr('href');
            mmenu.map[ $(this).parents('li').attr('id') ] = submenu.replace('#', '');
        });

        // NOTE: Initialization continues after methods below.

// PUBLIC METHODS
        // returns the DOM element as a jquery object
        object.get = function()
        {
            return this.$this;
        }

        object.addNotification = function(o, callback)
        {
            if(typeof(callback)==='undefined') callback = false;   

            var count = 0;
            var $m = $('#'+o.menu);
            if ( $m.length == 0 ) return {error: "Menu DOM element not found. Provide the element id."};

            var $item = $('<li><a href="">'+o.text+'</a>'+object._menuItemNotifyHtml()+'</li>');
            $item.addClass(mmenu.conf.classes.notification);
            object._bindEvents($item);
            
            if ( o.link )   $item.find('a').attr('href', o.link);
            if ( o.id )     $item.attr('id', o.id);
            if ( o.label )
            {
                var $l = $('#'+o.label);               
                if ( $l.length > 0 )
                {                 
                    $item.insertAfter( $l );
                    $count = 1; 
                }
            } else {
                var $s = $( $m.find('a').attr('href') );                
                if ( $s.length > 0 )
                {
                    $t = $s.find('.'+mmenu.conf.classes.subtitle);
                    ( $t.length > 0 ) ? $item.insertAfter( $t ) : $item.prependTo( $s );
                    $count = 1;
                }               
            }

            if ( $count = 1 )  object._menuCountPlus($m, +1);

            var r = {menu: $m.attr('id'), notification: $item.attr('id'), menuCount:object.notificationsCount($m.attr('id')) , totalCount: object.notificationsCount() };

            if ( callback )  callback( r );
            
            return r;
            //return {element: $item, success:$count, count: object.notificationsCount() };
        }

        object.removeNotification = function(id, callback)
        {
            if(typeof(callback)==='undefined') callback = false;                     
            //(typeof o == 'string' || o instanceof String) ? id = o : id = o.id;

            var $m = object._getMenuBySubMenuItemId( id );            
            var $i = $('#'+id);

            if( $i.length>0 && $m )
            {                
                $i.remove();
                object._menuCountPlus($m, -1);
            }

            var r = {menu: $m.attr('id'), menuCount:object.notificationsCount($m.attr('id')) , totalCount: object.notificationsCount() };

            if ( callback ) callback(r);            

            return r;
        }

        object.notificationsCount = function(submenu)
        {
            if(typeof(submenu)==='undefined') submenu = false;

            if( submenu )
            {                
                $sub = $('#'+mmenu.map[submenu]);

                if( $sub.length> 0 )
                {
                    $notifications = $sub.find('.'+mmenu.conf.classes.notification);
                    return $notifications.length;    
                }                
            }
            else
            {
                $notifications = object.$menu.find('.'+mmenu.conf.classes.notification);
                return $notifications.length;
            }
            return false;
        }

    // "PRIVATE" METHODS
        object._bindEvents = function($items)
        {

            $items.on('mouseenter',
                function()
                {
                    $(this).find('.' + mmenu.conf.classes.removeme).stop().fadeIn(0);
                }
            )
            .on('mouseleave',
                function()
                {
                    $(this).find('.' + mmenu.conf.classes.removeme).stop().fadeOut(0);
                }
            ).on('swipeleft',
                function( e )
                {
                    $(this).find('.' + mmenu.conf.classes.removeme).stop().fadeIn(0);
                }
            ).on('swiperight',
                function( e )
                {
                    $(this).find('.' + mmenu.conf.classes.removeme).stop().fadeOut(0);
                }
            );

        }

        object._menuItemNotifyHtml = function()
        {
            return "<div class='"+mmenu.conf.classes.removeme+"'>X</div>";
        }          

        object._menuCountPlus = function($m, count)
        {
            var $c = $m.find('em.'+mmenu.conf.classes.counter);
            if( $c.length > 0 )  $c.html( parseInt($c.html())+count );  
        }

        object._getMenuBySubMenuItemId = function(id)
        {
            var $i = $('#'+id);
            if( $i.length == 0 ) return false;
            
            var $p = $i.parents('ul');  

            for ( menu in mmenu.map )
            {                
                if( mmenu.map[menu] == $p.attr('id') )
                {                    
                    var $m = $('#'+menu);    
                    if( $m.length>0 ) return $m;
                    break;
                }
            }

            return false;
        }

    // EVENT METHODS
        // Executed whenever the mouse moves
        object._removeMe = function(event)
        {
            console.log('remove self');
        }

// BINDING EVENTS           
        //object.$this.bind('mousemove', $.proxy(object._mouseMove, object)); 
        //object.$menu.find('.'+mmenu.conf.classes.removeme).bind('mousedown', $.proxy(object._removeMe, object));

// FINAL INITIALIZATION
        // For each submenu find all of the notification items and create a delete button for each.
        $.each(mmenu.map, function( menu, sub )
        {
            $items = $('#'+sub).find('li')
            .not( '.' + mmenu.conf.classes.subtitle )
            .not( '.' + mmenu.conf.classes.label )
            .not( '.' + mmenu.conf.classes.noresults );

            $items.each(function()
            {   
                $(this).addClass(mmenu.conf.classes.notification);
                $(object._menuItemNotifyHtml()).appendTo($(this));                
            });

            object._bindEvents($items);            
        });

        // define the object within the object
        object.$this.data('object', object);		      
      } else {                  
        object = $(this).data('object');
      }        
      objects.push(object);        
    });
    
    if(objects.length === 1){
      return_data = objects[0];
    }else{
      return_data = objects;        
    }
    
    return_data.all = function(callback){
      //console.log(callback);
      $.each( objects, function(){
        callback.apply(this);
      });        
    };    
    return return_data;
  };


})(jQuery);
