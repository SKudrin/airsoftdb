

var AC3_Map_settings =
{
  verbose: false,
  onMouseUp: false,		// callback
  onMouseDown: false,	// callback
  map: {},
  center_position: null,
  unit_of_measurement: "miles",
  animate_results_onload: false, // animate the
  search_radius_size: false, // int - how large the search radius should be.
  markerOnClick: false,
  markerOnClose: false,
  onFitMarkers: false,
  onZoomChange: false,
  initOnLoad: true
};


$.fn.AC3Map = function( options, map_options, callback )
{
	var objects = [], return_data;

		// console.log(this);

	this.each(function()
	{
		// initialize the object here (add methods, event bindings, etc)
  	if( $(this).data('object') === undefined )
  	{
		// ---------------------
		//   INITIALIZE
		// ---------------------
		var object = {};

		object.initialized = false;
		object.$this = $(this);
		//object.settings = $.extend( AC3_Map_settings, options );
		object.settings = $.extend(true, {}, AC3_Map_settings, options);

    		// Measurement constants (assume Miles unless otherwise indicated)
		object.earths_radius = 3959; 				  // Earths radius in miles.
		object.measurement_unit_multiplier = 1609.36; // Number of meters in a mile.

		object.map = null;
		object.selected_marker_id = null;
		object.markers = {};
		//object.isolated = [];
		object.stored_marker_ids = [];
		object.circles = {};
		object.bubbles = {};

		object.search_query_circle = null;

		// If the map-canvas exists then use it, otherwise create it
		if ( object.$this.find('.map-canvas').length == 0 ) {
			object.$canvas = $('<div class="map-canvas" id="'+object.$this.attr('id')+'-canvas"></div>').prependTo(object.$this);
		} else {
			object.$canvas = object.$this.find('.map-canvas');
		}

		// If the map-grid exists then use it, otherwise create it
		if ( object.$this.find('.map-grid').length == 0 ) {
			object.$grid = $('<div  class="map-grid" id="'+object.$this.attr('id')+'-grid"></div>').prependTo(object.$this);
		} else {
			object.$grid = object.$this.find('.map-grid');
		}

		if ( object.settings.verbose ) {
			console.log("Initializing AC3Map");
		}


  	// INIT happens at the bottom (in order to have access to all of the methods).


  	// ---------------------
		//   PUBLIC METHODS
		// ---------------------
		// returns the DOM element as a jquery object
		object.get = function()
		{
			return this.$this;
		}

		object.map = function()
		{
			return this.map;
		}

		object.gmap = function()
		{
			return this.map;
		}

		// Remove all markers from the map
		object.remove_markers = function()
		{
			if ( this.settings.fast_markers ) {
				for ( key in this.markers) {
					this.markers[key].setMap(null);
				}
				this.markers = {};
				return;
			}

			for ( key in this.markers) {
				this.gmarker(key).setMap(null);
			}

			this.markers = {};
		}

		/*
		object.close_marker_infoWindows = function(id) {
			id = typeof id !== 'undefined' ? id : false;

			if ( id ) {
				this.markers[id].infoWindow().close();
				return;
			}

			for ( key in this.markers) {
				this.markers[key].infoWindow().close();
			}
		}
		*/

		object.get_marker_icon = function(tile) {

			var tile_size = [40, 40]; // width and height of a single icon
			var icon_size = new google.maps.Size(tile_size[0], tile_size[1]);
			var icon_url  = '/img/icons/markers.png';
			var icon_tile_origin = function(tilenumber) {
				// The origin of the sprite on the x axis.
				var x = (tilenumber - 1) * tile_size[0];
				return new google.maps.Point(x, 0);
			}
			var icon_tile_anchor = function(tilenumber) {
				if ( tilenumber == 1 ) return new google.maps.Point(20,36);
				if ( tilenumber == 2 ) return new google.maps.Point(20,36);
				if ( tilenumber == 4 ) return new google.maps.Point(20,20);
			}
			var icons = {
				'field': {
					url: icon_url,
					size: icon_size,
					origin: icon_tile_origin(1),
					anchor: icon_tile_anchor(1)
				},
				'shop': {
					url: icon_url,
					size: icon_size,
					origin: icon_tile_origin(2),
					anchor: icon_tile_anchor(2)
				},
				'team': {
					url: icon_url,
					size: icon_size,
					origin: icon_tile_origin(3),
					anchor: icon_tile_anchor(3)
				},
				'dot': {
					url: icon_url,
					size: icon_size,
					origin: icon_tile_origin(4),
					anchor: icon_tile_anchor(4)
				},
				'organizer': {
					url: icon_url,
					size: icon_size,
					origin: icon_tile_origin(5),
					anchor: icon_tile_anchor(5)
				},
				'advocate': {
					url: icon_url,
					size: icon_size,
					origin: icon_tile_origin(6),
					anchor: icon_tile_anchor(6)
				},
			};
			return icons[tile];
		}


		object.set_marker_icon = function(ids, tile) {
			tile = typeof tile !== 'undefined' ? tile : 1;

			/*var tile_size = [40, 40]; // width and height of a single icon
			var icon_size = new google.maps.Size(tile_size[0], tile_size[1]);
			var icon_url  = '/resources/icons/map/markers.png';
			var icon_tile_origin = function(tilenumber) {
				// The origin of the sprite on the x axis.
				var x = (tilenumber - 1) * tile_size[0];
				return new google.maps.Point(x, 0);
			}
			var icon_tile_anchor = function(tilenumber) {
				if ( tilenumber == 1 ) return new google.maps.Point(20,36);
				if ( tilenumber == 2 ) return new google.maps.Point(20,36);
				if ( tilenumber == 4 ) return new google.maps.Point(20,20);
			}
			var icons = {
				'field':
				{
					url: icon_url,
					size: icon_size,
					origin: icon_tile_origin(tile),
					anchor: icon_tile_anchor(tile)
				}
			};

			var icon_settings = icons['field'];
			*/

			var icon = this.get_marker_icon(tile);
			//var icon = null;

			if( $.isArray(ids) ) {
				for ( i=0; i<ids.length; i++ ) {
					if ( tile === false ) {  // reset the icon
						icon = this.get_marker_icon(this.markers[ids[i]]['unit']);
					}
					this.gmarker(ids[i]).setIcon(icon);
				}
			} else {
				this.gmarker(ids).setIcon(icon);
			}
		}

		object.add_fast_marker = function(position, settings) {

			//this.markers[id]
			var self = this;
			var id = settings.id;

			var marker = new google.maps.Marker({
				id: settings.id,
				unit: settings.unit,
				visible: settings.visible,
				title: settings.title,
				icon: self.get_marker_icon(settings.unit),
				//icon: self.icon(settings.icon),
				// icon: {
				// 	url: '/resources/icons/map/markers.png',
				// 	size: new google.maps.Size(40, 40),
				// 	origin: new google.maps.Point(0, 0),
				// 	anchor: new google.maps.Point(20,36)
				// },
				position: new google.maps.LatLng(position[0], position[1]),
				map: this.map
			});

			this.markers[id] = marker;
			//this.markers[id].set('distance', parseFloat(distance));

			google.maps.event.addListener(
				marker,
				'click',
				function() {
					var r = true;
					if (typeof(self.settings.markerOnClick) == "function") {
						r = self.settings.markerOnClick( self.markers[this.id] );
					}
					if ( r === false ) {
						return;
					}
					self.select_marker( this.id );
				}
			);

			return this.markers[id];
		}

		// Add a marker to the map.
		// @param array position: [lat, lng]
		// @param string id: unique id assigned to the marker
		// @param string type: the "type" of marker (used for grouping)
		object.add_marker = function(position, options)
		{
			custom = typeof custom !== 'undefined' ? custom : {};

			var settings = {};
			settings = $.extend( settings,  options );

			// AC3 Map object redefined for use in event listeners.
			var self = this;
			var id = settings.id;

			// Create a new marker
			this.markers[id] = new AC3Marker(position, this.map, settings);

			// If the map is still being initialized then determine if the markers should be
			// visible or not. (They should be invisible if the radius is animating).
			if ( this.initialized === false && this.settings.animate_results_onload === true )
				this.markers[id].marker().set('visible', false);

			// Get the distance from the center to the new marker and store it away for later.
			var distance = this._distance_between_points(
														 this.getPosition(),
														 this.markers[id].marker().getPosition()
														);
			this.markers[id].set('distance', parseFloat(distance));

			// Get the actual google maps marker object
			var google_maps_marker = this.markers[id].marker();
			var google_maps_infoWindow = this.markers[id].infoWindow();

			// Marker Events
			google.maps.event.addListener(
				google_maps_marker,
				'click',
				function() {
					if (typeof(self.settings.markerOnClick) == "function") {
						self.settings.markerOnClick( self.markers[this.id] );
					}
					self.select_marker( this.id );
				}
			);
			google.maps.event.addListener(
				google_maps_infoWindow,
				'closeclick',
				function() {
					if (typeof(self.settings.markerOnClose) == "function") {
						self.settings.markerOnClose( id );
					}
				}
			);
			google.maps.event.addListener(
				google_maps_marker,
				'mouseover',
				function(){
					if ( self.settings.markerHUD ) {
						self.show_marker_hud( this );
					}
					//self.markers[this.id].circle().start_animation();
				}
			);
			google.maps.event.addListener(
				google_maps_marker,
				'mouseout',
				function(){
					if ( self.settings.markerHUD ) {
						self.clear_hud();
					}
				}
			);

			return this.markers[id];
		};

		object.center = function(lat, lng)
		{
			lat = typeof lat !== 'undefined' ? lat : false;
			lng = typeof lng !== 'undefined' ? lng : false;

			//this.center_position;
			if ( lat && lng ) {
				this.gmap().setCenter(new google.maps.LatLng(lat, lng));
			} else {
				this.gmap().setCenter(this.center_position);
			}
		}

		object.selected_marker = function()
		{
			if ( !this.selected_marker_id ) return null;
			return this.markers[this.selected_marker_id];
		};

		object.deselect_markers = function()
		{
			for( key in this.markers )
			{
				this.markers[key].bubble().close();
				this.markers[key].setIcon(1);
			}
			this.selected_marker_id = null;
		};

		object.size = function(obj) {
		    var size = 0, key;
		    for (key in obj) {
		        if (obj.hasOwnProperty(key)) size++;
		    }
		    return size;
		};

		object.store_markers_vis_state = function() {
			this.stored_marker_ids = this.visible_marker_ids();
		}

		object.restore_markers_vis_state = function(clear) {
			clear = typeof clear !== 'undefined' ? clear : true;

			for ( key in this.markers) {
				if ( $.inArray(key, this.stored_marker_ids) >= 0) {
					this.gmarker(key).setVisible(true);
				} else {
					this.gmarker(key).setVisible(false);
				}
			}

			if ( clear === true ) {
				this.stored_marker_ids.length = 0;
			}

			this.fit_markers(this.markers);
		}

		object.restore_marker_visibility = function(id) {
			id = typeof id !== 'undefined' ? id : false;
			if ( id != false ) {
				this.markers[id].marker().setVisible(true);
			} else {
				for ( key in this.markers) {
					this.gmarker(key).setVisible(true);
				}
			}
		}

		/*
		object.isolate_fast_markers = function(ids, isolate, frame) {
			var visible = [];
			isolate = typeof isolate !== 'undefined' ? isolate : true;
			frame = typeof frame !== 'undefined' ? frame : false;
			if ( isolate === true ) {
					for ( key in this.markers) {
						if ( $.inArray(key, ids) >= 0) {
							//this.isolated.push(key);
							visible.push(this.markers[key]);
							this.markers[key].setVisible(true);
						} else {
							this.markers[key].setVisible(false);
						}
					}
				}
				if ( isolate === false ) {
				for ( key in this.markers) {
					this.markers[key].marker().setVisible(true);
				}
				visible = this.markers;
			}
			if ( frame === true ) {
				this.fit_markers(visible);
			}
		}
		*/

		// Sets the visibility of just the markers for the given ids
		object.isolate_markers = function(ids, isolate, frame) {
			isolate = typeof isolate !== 'undefined' ? isolate : true;
			frame = typeof frame !== 'undefined' ? frame : false;
			var visible = [];

			if ( isolate === true ) {

				//this.isolated.length = 0;

				for ( key in this.markers) {
					if ( $.inArray(key, ids) >= 0) {
						//this.isolated.push(key);
						visible.push(this.markers[key]);
						this.gmarker(key).setVisible(true);
					} else {
						this.gmarker(key).setVisible(false);
					}
				}
			}

			// In this case then show all of the markers.
			// There may be a need to set the vis state of the markers before the isolate
			// function is called but for now I don't need that functionality so just vis them
			// all.
			if ( isolate === false ) {
				for ( key in this.markers) {
					this.gmarker(key).setVisible(true);
				}
				visible = this.markers;
			}

			if ( frame === true ) {
				this.fit_markers(visible);
			}


			/*
			if ( this.settings.fast_markers ) {
				this.isolate_fast_markers(ids, isolate, frame);
				return;
			}


			if ( isolate === true ) {

				//this.isolated.length = 0;

				for ( key in this.markers) {
					if ( $.inArray(key, ids) >= 0) {
						//this.isolated.push(key);
						visible.push(this.markers[key]);
						this.markers[key].marker().setVisible(true);
					} else {
						this.markers[key].marker().setVisible(false);
					}
				}
			}

			// In this case then show all of the markers.
			// There may be a need to set the vis state of the markers before the isolate
			// function is called but for now I don't need that functionality so just vis them
			// all.
			if ( isolate === false ) {
				for ( key in this.markers) {
					this.markers[key].marker().setVisible(true);
				}
				visible = this.markers;
			}

			if ( frame === true ) {
				this.fit_markers(visible);
			}
			*/
		}

		object.isolate_marker = function(id, state) {
			state = typeof state !== 'undefined' ? state : true;

			/*
			if ( state == true ) {

			}

			if ( state == false ) {
				if ( id in this.markers ) {
					this.markers[key].marker().setVisible(false);
					this.markers[key].marker().infoWindow().close();
				}
			}
			*/

			return;
			/*
			// We no longer want to change the icon for non isolated markers.
			visibility = false;
			if ( !id ) visibility = true;

			for ( key in this.markers)
			{
				if ( key == id ) continue;
				if ( visibility )
				{
					//this.markers[key].marker().setVisible(visibility);
					this.markers[key].setIcon(1);
				} else {
					this.markers[key].setIcon(2);
				}
			}
			*/
		};

		// Get all of the isolated markers by id or by object.
		/*
		object.isolated_markers = function(getID) {
			getID = typeof getID !== 'undefined' ? getID : false;
			if ( getID === true ) {
				return this.isolated;
			}

			if ( getID === false ) {
				var tmp = [];
				for ( key in this.isolated) {
					tmp.push(this.markers[key]);
				}
				return tmp;
			}
		};
		*/

		object.deselect_marker = function(id) {
			//this.markers[id].setIcon(2);
		}

		object.select_marker = function(id)
		{
			if (typeof(this.markers[id].infoWindow) == "function") {
				this.markers[id].infoWindow().open(this.map, this.markers[id].marker());
			}

			//this.markers[id].setIcon(2);
			return;
			/*
			for ( key in this.markers)
			{
				this.markers[key].bubble().close();
			}
			this.markers[id].bubble().open();
			this.selected_marker_id = id;
			*/
		};

		// Takes an array of objects like this:
		//    [{'position': [lng, lat], 'title':'', 'unit':'', 'url',''}]
		object.add_markers = function(markers)
		{

			if( $.isArray(markers) )
			{
				for( var i=0; i < markers.length; i++ )
				{
					this.add_marker( markers[i]['position'], false, markers[i] );
				}
				return;
			} else {
				/*
				// This is to support the old way of doing in as found in unit_dir_list.php
				// uniqueid: {'position': [lng, lat], 'title':'', 'unit':'', 'url',''}
				for( id in markers )
				{
					var custom = markers[id];
					custom['id'] = id;
					this.add_marker( markers[id]['position'], custom );
				}
				*/
			}
		};

		object.marker = function(id)
		{
			return this.markers[id];
		}

		// Get goolge map marker by id
		// @return object
		object.gmarker = function(id)
		{
			if ( this.settings.fast_markers ) {
				return this.markers[id];
			} else {
				return this.markers[id].marker();
			}
		}


		// Get all markers on the map
		// @return array
		object.get_markers = function()
		{
			var markers = new Array();
			for ( key in this.markers ) {
				markers.push( this.markers[key] );
			}
			return markers;
		};

		// Get markers by a certain type
		// @param string type (ex: "fields", "shops", "teams")
		// @return array
		object.markers_by_type = function(type)
		{
			var markers = new Array();
			for ( key in this.markers )
			{
				if ( this.markers[key].get('type') == type )
				{
					markers.push( this.markers[key] );
				}
			}
			return markers;
		};

		object.show_marker_hud = function(marker)
		{
			// Get marker as AC3Marker for convenience.
			marker = this.markers[marker.id];

			var type = '';
			var title = '';

			if ( marker.get('type') ) type = marker.get('type');
			if ( marker.get('title') ) title = marker.get('title');

			// If there is no title then don't continue.
			if ( !title ) return;

			// Append the marker type in front of the title.
			if ( type ) type = type + ": "
			title = type.toUpperCase() + title;

			// Clear the hud before updating it.
			this.clear_hud();

			//var distance = 'Driving: ';
			//distance = distance + $('#' + marker.custom.markerID).find('.distance').html();

			this._typewritter(
				$('#map_hud_row1'),
				title,
				10,
				function() {
					/*
					this._typewritter(
						$('#map_hud_row1'),
						distance,
						10
					);
					*/
				}
			);
		};

		object.clear_hud = function ()
		{
			$('#map_hud_row1').html('');
			$('#map_hud_row2').html('');
			$('#map_hud_row3').html('');
		};

		object.fit_LatLngs = function(points)
		{
			var bounds = new google.maps.LatLngBounds();

			for (var i=0; i < points.length; i++) {
				bounds.extend( points[i] );
			}

			this.map.fitBounds(bounds);

			if (typeof(this.settings.onFitMarkers) == "function") {
				this.settings.onFitMarkers();
			}
		}

		/*
		object.fit_fast_markers = function(markers) {
			if ( !markers ) markers = this.markers;

			var bounds = new google.maps.LatLngBounds();
			var count = 0;
			// Add all of the markers to the bounds object.
			for ( key in markers ) {
				bounds.extend( markers[key].position );
				count++;
			}

			// Include the position of the search center.
			// bounds.extend(searchCenter);

			//  Fit these bounds to the map.
			this.map.fitBounds(bounds);
			if ( count == 1 ) {
				this.map.setZoom(10);
			}
		}
		*/

		object.fit_markers = function(markers)
		{
			markers = typeof markers !== 'undefined' ? markers : false;

			// if ( this.settings.fast_markers ) {
			// 	this.fit_fast_markers(markers);
			// 	return;
			// }

			if ( markers === false ) {
				markers = this.markers;
			}

			//console.log(markers);

			var bounds = new google.maps.LatLngBounds();
			var count = 0;
			// Add all of the markers to the bounds object.
			for ( key in markers ) {
				bounds.extend( markers[key].position );
				count++;
			}
			// Include the position of the search center.
			// bounds.extend(searchCenter);

			//  Fit these bounds to the map.
			this.map.fitBounds(bounds);
			if ( count == 1 ) {
				this.map.setZoom(10);
			}

			if (typeof(this.settings.onFitMarkers) == "function") {
				this.settings.onFitMarkers();
			}
			// TODO: If there is only one marker then set the minimal zoom
		};

		object.fit_visible_markers = function() {
			this.fit_markers(this.visible_markers());
		}

		// The number of visible markers (all or by type)
		object.visible_markers_count = function(type)
		{
			// uses underscore script to get the size of the object
			// _.size(markers);
			return _.size(visible_markers(type));
		}

		object.marker_ids = function() {
			var markers = [];
			for ( key in this.markers ) {
				markers.push(key);
			}
			return markers;
		}

		// Returns an array of all visible markers that are visible on the map.
		object.visible_marker_ids = function() {
			var markers = [];
			for ( key in this.markers ) {
				if ( this.gmarker(key).get('visible') ) {
					markers.push(key);
				}
			}
			return markers;
		}

		// Returns an object list of all markers that are visible on the map.
		object.visible_markers = function(type)
		{
			type = typeof type !== 'undefined' ? type : false;
			var markers = {};
			for ( key in this.markers )
			{
				var visible = this.gmarker(key).get('visible');

				// If no type is given then add any marker that is visible
				if ( !type && visible )
				{
					markers[key] = this.markers[key];
					continue;
				}

				// If type is given add the marker if it's visible and it matches the type.
				if ( type == this.markers[key].get('type') && visible )
				{
					markers[key] = this.markers[key];
				}
			}

			return markers;
		}

		// Get the position of the center of the maps search query as a google LatLng object.
		object.getPosition = function()
		{
			return this.center_position;
		}

		// Get all of the markers that fall within a radius from the search origin.
		// @param float radius - size based on unit_of_measurement (default is Miles).
		object.get_markers_within_search_radius = function(radius)
		{
			var within = {};

			for ( key in this.markers )
			{
				var d = 0;
				// If the marker has the distance cached (which it should) then use it.
				if ( this.markers[key].get('distance') )
				{
					d = this.markers[key].get('distance');
				} else {
					// Distance was not cached on the marker so calculate it on the fly.
					d = this._distance_between_points( this.getPosition(),
													   this.gmarker(key).getPosition() );
				}

				// If the distance is within the radius the add it to the list.
				if ( d < radius )
					within[key] = this.markers[key];
			}
			return within;
		}

		// Animate the size of the circle geometry that originates at the position of the
		// maps search query.
		//
		// @param float targetRadius - The target size of the radius based on the
		//							   unit_of_measurement (default is Miles).
		// @param float startRadius  - Opt: The start size of the radius based on the
		//							   unit_of_measurement (default is Miles).  If this is
		//                             not set then use the search radius.
		// @param function callback  - Opt: Script to execute after the animation has completed.
		object.animate_search_radius = function(targetRadius, startRadius, callback)
		{
			// The default startRadius is the current size of the searchRadius.
			startRadius = typeof startRadius !== 'undefined' ? startRadius : true;
			if ( startRadius === true ) startRadius = this.settings.search_radius_size;

			if ( !startRadius ) startRadius = 0;

			var self = this;

			// Update the global searchRadius to the new targetRadius.
			searchRadius = targetRadius;
			// The size of the start radius.
			//startRadius *= this.measurement_unit_multiplier;
			// The size of the target radius.
			//targetRadius *= this.measurement_unit_multiplier;

			// The number of steps it takes to get to the target radius size
			// (which corresponds to the speed of the animation).
			var steps = 30;
			// The target radius size of the cirlce.
			var size = 10;
			// The current frame in the animation.
			var frame = 0;
			// The speed at which each frame is executed.
			var execSpeed = 80;
			// state of the radius size (growing or shrinking)
			var radius_is_growing = ( (targetRadius - startRadius) > 0 ) ? true : false;

			// List of markers that fall within the search radius boundries.
			var reached = [];

			// List of markers that are already visible.
			var visible = this.visible_markers();

			// Zoom the map out to fit all of the markers that will be shown.
			if ( radius_is_growing )
			{
				this.fit_markers( this.get_markers_within_search_radius( searchRadius ) );
			}

			// Loop through the animation of the circle's radius.
			// If the radius is growing then show the markers as the radius covers them.
			// If the radius is shrinknig then hide the markers that are no longer inside.
			var looper = setInterval(function()
			{
				frame++;

				if( frame <= steps )
				{
					// ease into the final radius size
					var newRadius = self._easein(frame, steps, startRadius, targetRadius);

					for ( key in self.markers )
					{
						//var marker_distance = this.markers[key].custom.fieldDistance * this.measurement_unit_multiplier;
						var marker_distance = self.markers[key].get('distance');



						// If the radius is growing then add makers if they aren't already
						// visible and they are within the radius.
						if ( radius_is_growing && newRadius > marker_distance && !visible[key] )
						{
							// If the marker has already been reached then skip.
							if($.inArray(key, reached)!==-1) continue;

							// Marked the marker as reached.
							reached.push(key);

							// Vis the marker on the map.
							self.markers[key].marker().set('visible', true);

							// Animate the markers circle when it is reached.
							self.markers[key].circle().start_animation();
						}

						// If the radius is shrinking then remove markers that are already
						// visible but are no longer in the radius.
						if ( !radius_is_growing && marker_distance > newRadius && visible[key] )
						{
							// Invis the marker on the map.
							self.markers[key].marker().set('visible', false);
						}
					}

					// Set the circles new radius (multiply by meters)
					self.search_query_circle.setRadius( newRadius
														* self.measurement_unit_multiplier
													  );
					//QueryCircle.setRadius(newRadius);

					// Set the circles new opacity
					var newOpacity = self._easein(frame, steps, .35, .075);
					self.search_query_circle.set('fillOpacity', newOpacity);
					return;
				} else {
					// Stop the animation by clearing the looper interval.
					clearInterval(looper);

					//if ( callback ) callback();

					// Zoom in to fit the markers in the radius.
					if ( !radius_is_growing )
					{
						var vm = self.visible_markers();
						// Only zoom in if there is more then one marker to frame.
						if ( _.size( vm ) > 0 )
						{
							self.fit_markers( vm );
						}
					}

			        return;
				}
			}, execSpeed);
		}

		// ---------------------
		//  "PRIVATE" METHODS
		// ---------------------
		// Returns the position of the mouse relative to the position of the element
		object._mouse = function(event)
		{
			var o = this.$this.offset();
			var x = event.pageX - o.left;
			var y = event.pageY - o.top;
			return {'x':x, 'y':y};
		}

		object._typewritter = function($element, text, speed, callback)
		{
			var speed = typeof speed !== 'undefined' ? speed : 40;
			var chars = text.split('');
			var frames = text.length-1;
			var counter = 0;

			var currentString = "";
			var looper = setInterval(function()
			{
				if( counter <= frames )
				{
					currentString = currentString+chars[counter];
					$element.html(currentString);
				} else {
					if(callback) callback();
					clearInterval(looper);
			        return;
				}
				counter++;
			}, speed);
		}

		// Round a number to the given decimal place
		object._round = function(num, places)
		{
			var multiplier = Math.pow(10, places);
			return Math.round(num * multiplier) / multiplier;
		}

		// Function for easing into a value with a specified number of frames
		// as well as facilitate value remapping.
		object._easein = function(frame, totalFrames, startValue, endValue)
		{
			startValue = typeof startValue !== 'undefined' ? startValue : 0;
			endValue = typeof endValue !== 'undefined' ? endValue : 1;

			var ease = 1-(Math.pow((1-(frame/totalFrames)),2));
			var mapping = (ease*(endValue-startValue)) + startValue;
			return mapping;
		}

		// Get the distance between two geographic points based on the set unit_of_measurement.
		// @param1 object  Google LatLng object
		// @param2 object  Google LatLng object
		// @return float   The distance between the points
		object._distance_between_points = function(point1, point2)
		{
			var self = this;
			rad = function(x) {return x*Math.PI/180;}
			distHaversine = function(p1, p2)
			{
			  var R = self.earths_radius;
			  //var R = 6371; // earth's mean radius in km
			  var dLat  = rad(p2.lat() - p1.lat());
			  var dLong = rad(p2.lng() - p1.lng());
			  var a = Math.sin(dLat/2) * Math.sin(dLat/2)
			  		+ Math.cos(rad(p1.lat())) * Math.cos(rad(p2.lat()))
			  		* Math.sin(dLong/2) * Math.sin(dLong/2);
			  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
			  var d = R * c;
			  return d.toFixed(3);
			}
			return distHaversine(point1, point2);
			/*
			// using google api
			// <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&v=3&libraries=geometry"></script>
			var d = google.maps.geometry.spherical.computeDistanceBetween(
							point1.getPosition(), // position of search query center.
							point2.getPosition() // position of the marker.
				);
			*/
		}

		object._zoom_style = function( zoom_level )
		{
			var style = [ { "stylers": [ { "invert_lightness": true }, { "saturation": -100 }, { "gamma": 1.34 }, { "visibility": "on" }, { "weight": 0.4 } ] },{ "elementType": "labels", "stylers": [ { "lightness": -25 }, { "weight": 2 } ] }, { "featureType": "water", "stylers": [ { "lightness": -55 } ] },{ "featureType": "administrative.neighborhood", "elementType": "labels", "stylers": [ { "visibility": "off" } ] }, { "featureType": "poi", "elementType": "labels", "stylers": [ { "visibility": "off" } ] }  ];

			// Adjust the lightnesss of the landscape features based on the zoom level
			if( zoom_level >= 5 && zoom_level <= 9 )
			{
				style.push({ "featureType": "landscape", "stylers": [ { "lightness": 15 } ] });

			}
			if( zoom_level == 10 )
			{
				style.push({ "featureType": "landscape", "stylers": [ { "lightness": 5 } ] });
			}

			var border_color = '#999999';//#b57b00
			var border_thickness = 1.5;
			if ( zoom_level >= 7) {
				border_thickness = 3;
			}
			if( zoom_level <= 5 ) {
				border_thickness = 1;
			}
			if ( zoom_level <= 3) {
				border_thickness = 0.5;
			}
			style.push({ "featureType": "administrative.province", "elementType": "geometry", "stylers": [ { "color": border_color }, { "weight": border_thickness } ] });

			return style;
		}

		object.markers_in_bounds = function(bounds) {
			bounds = typeof bounds !== 'undefined' ? bounds : false;

			if ( bounds === false ) {
				bounds = this.map.getBounds();
			}

			// If there are stil no bounds then the map has not been fully loaded.  Exit to
			// prevent an error
			if ( !bounds ) {
				return;
			}
              var bounded = [];

              for ( var key in this.markers ) {
              	var pos = this.gmarker(key).getPosition();
              	if ( bounds.contains(pos) ) {
                      bounded.push(pos);
                  }
              }
              return bounded;
		}

		// ----------
		//   INIT
		// ----------
		object.init_map = function()
		{
			var self = this;

			var center = new google.maps.LatLng(37.668821, -122.080796);  // Hayward, CA
			if ( this.settings['center'] )
			{
				center = new google.maps.LatLng( this.settings['center'][0],
												 this.settings['center'][1]
												);
			}

			// The center position of the map query as google LatLng object.
			this.center_position = center;

			// Update the measurement constants if the unit_of_measurement has been set.
			if ( this.settings['unit_of_measurement'] == 'kilometers' )
			{
				this.earths_radius = 6371; // Earths radius in kilometers.
				this.measurement_unit_multiplier = 1000; // Number of meters in a kilometer
			}

			var zoom_level = 9;
			if ( this.settings['map']['zoom'])
				zoom_level = this.settings['map']['zoom'];

			//var mapStyle = [ { "stylers": [ { "invert_lightness": true }, { "saturation": -100 }, { "gamma": 1.34 }, { "visibility": "on" }, { "weight": 0.4 } ] },{ "elementType": "labels", "stylers": [ { "lightness": -25 }, { "weight": 2 } ] }, { "featureType": "water", "stylers": [ { "lightness": -55 } ] },{ "featureType": "administrative.neighborhood", "elementType": "labels", "stylers": [ { "visibility": "off" } ] }, { "featureType": "poi", "elementType": "labels", "stylers": [ { "visibility": "off" } ] }  ];
			//var map_type = google.maps.MapTypeId.ROAD;ROADMAP
			var map_type = google.maps.MapTypeId.ROADMAP
			var map_default_options = {
				center: center,
				zoom: zoom_level,
				panControl:false,
				streetViewControl:false,
				zoomControl:true,
				scrollwheel: true,
			    zoomControlOptions: {
			      style:google.maps.ZoomControlStyle.SMALL
			    },
				mapTypeId: map_type,
				mapTypeControlOptions: {
		        	mapTypeIds: [google.maps.MapTypeId.ROADMAP, google.maps.MapTypeId.SATELLITE]
		    	},
		    	styles: self._zoom_style(zoom_level)
			};

    			var map_options = $.extend( map_default_options, this.settings['map'] );

    			object.map = new google.maps.Map(object.$canvas.get(0), map_options);
    			//object.$canvas
    			//object.map = new google.maps.Map(document.getElementById(object.settings.elementId), map_options);

			// Bake the measurement units into the map for use in other classes.
			object.map.earths_radius = self.earths_radius;
			object.map.measurement_unit_multiplier = self.measurement_unit_multiplier;

			// Add the search query circle
			this.search_query_circle = new google.maps.Circle({
		    	center: this.center_position,
			    strokeColor: '#b28a19', //'#a17d40',//'#FF0000',
				strokeOpacity: 0.1,
				strokeWeight: 2,
				fillColor: '#b28a19',//'#a17d40',//'#FF0000',
				fillOpacity: 0.1,
			    map: object.map,
			    radius: 0
			});

			// Add markers with init
			if ( this.settings['markers'] )
			{
				// Add the markers to the map.
				object.add_markers( this.settings['markers'] );

				if ( $.isArray( this.settings['markers'] ) )
				{
					// Fit the markers within the bounds of the map
					if ( this.settings['markers'].length > 1 )
					{
						object.fit_markers();
					}
				} else {
					object.fit_markers();
				}
			}

			if ( this.settings.animate_results_onload )
			{
				var search_radius_size = this.settings.search_radius_size;

				// If the search radius size has not been set then get then see if there are
				// markers loaded on the map.  If so, then make the radius size 10% bigger then
				// the furthest marker.
				if ( !this.settings.search_radius_size )
				{
					var s = 0;
					for ( key in object.markers )
					{
						var d = object.markers[key].get('distance');
						if ( d && d > s )
						{
							s = d;
						}
					}
					search_radius_size = s + (s * .10);
				}

				google.maps.event.addListenerOnce(object.map, 'tilesloaded', function(){
					//animate_sight();
					object.animate_search_radius( search_radius_size, 0);
				    // do something only the first time the map is loaded
				    google.maps.event.clearListeners(map, 'tilesloaded');
				});
			}

			google.maps.event.addListener(object.map, 'zoom_changed', function() {
				style = self._zoom_style( this.getZoom() );
				this.setOptions({styles: style});
				if (typeof(self.settings.onZoomChange) == "function") {
					self.settings.onZoomChange();
				}
			});

			// refit all of the currently bounded markers if the page has been resized
			$(window).resize(function() {
			    if(this.resizeTO) clearTimeout(this.resizeTO);
			    // Store the bounded markers when resize is initiated.
			    if ( !object.bounded ) {
			    	object.bounded = object.markers_in_bounds();
			    	object.bounded_center = object.map.getCenter();
			    }
			    this.resizeTO = setTimeout(function() {
			        $(this).trigger('mapResizeEnd');
			    }, 200);
			});
			// delay the marker fitting until the resize of the browser has come to a rest.
			$(window).bind('mapResizeEnd', function() {
				var bounded = object.bounded;
				if( bounded.length > 1 ) {
					// if there were markers within the bounds then fit them
                	object.fit_LatLngs(object.bounded);
				} else {
					// if there were no markers then recenter
					object.map.setCenter(object.bounded_center);
				}
                // clear the bounded markers array
                object.bounded = false;
			});

			object.initialized = true;
		} // END OF INIT

		// Initialize the map
		if ( object.settings.initOnLoad == true ) {
			google.maps.event.addDomListener(window, 'load', object.init_map() );
		}


		// ----------
		//   EVENTS
		// ----------
		// Executed whenever the mouse button pressed down
		object._mouseDown = function(event)
		{
			if( this.settings.verbose )
			{
		  		var pos = this._mouse(event);
			}

			// callback
			if( this.settings.onMouseDown )
			{
			  this.settings.onMouseDown.call(this.$this, event);
			}

			if ( this.selected_marker() )
				this.deselect_markers();

			event && event.preventDefault();
		}

		// Executed whenver the mouse button is let go
		object._mouseUp = function(event)
		{
			if( this.settings.verbose ) console.log("mouseup");

			// callback
			if( this.settings.onMouseUp )
			{
		  		this.settings.onMouseUp.call(this.$this, event);
			}

			event && event.preventDefault();
		}

		// Executed whenever the mouse moves
		object._mouseMove = function(event)
		{
			var pos = this._mouse(event);
		}

		// Bind events with methods
		object.$_mousedown = object.$this.bind('mousedown', $.proxy(object._mouseDown, object));
		object.$_mouseup = object.$this.bind('mouseup', $.proxy(object._mouseUp, object));
		//object.$_mousemove = object.$this.bind('mousemove', $.proxy(object._mouseMove, object));

		// define the object within the object
		object.$this.data('object', object);

		} else {
			//console.log("object exists");
			object = $(this).data('object');
		}
  	objects.push(object);
	}); // end of instance

	if( objects.length === 1 )
	{
  	return_data = objects[0];
	} else {
  	return_data = objects;
	}

	return_data.all = function(callback)
	{
  	//console.log(callback);
  	$.each( objects, function(){
    		callback.apply(this);
  	});
	};

	return return_data;
};


function AC3Marker(position, map, custom)
{
	// Custom is an object with key value pairs
	custom = typeof custom !== 'undefined' ? custom : false;
	if ( !custom ) custom = {};

	// If position in an array of [lat, lng] then run init.
	// Else we assume the position is actually an existing google marker.
	if ( $.isArray(position) )
	{
		this.init(position, map, custom)
	} else {
		var marker = position; // The position is actually a marker
		this.convert( marker );
	}
}
AC3Marker.prototype = {
	// Init happens at the bottom
	getID: function()
	{
	this.get('id');
	},

	marker: function()
	{
		return this.m;
	},

	get: function( name )
	{
		return this.m['custom'][name];
	},

	set: function( name, data )
	{
		if ( name == 'id' ) this.m['id'] = data;
	this.m['custom'][name] = data;
	},

	getPosition: function()
	{
		return this.m.getPosition();
	},

	setPosition: function(lat, lng)
	{
		var pos = new google.maps.LatLng(lat, lng);
		// Set the position of the marker
		this.m.set('position', pos);
		// Set the position of the circle
		this.AC3Circle().circle().set('center', pos);
		// Set the position of the infobubble.
	this.AC3Bubble().set('center', pos);
	},

	attr: function(name, data)
	{
		data = typeof data !== 'undefined' ? data : false;

		// Check to see if the attribute exists
		//if ( !this.marker['custom'].haseOwnProperty(name) ) return undefined;

		// Get the attribute data
		if ( !data ) return this[name];

		// Set the attribute data.  The attr data will still live on the google marker object.
		this[name] = data;
	},

	convert: function( marker )
	{
		this.m = marker;
	},

	circle: function()
	{
		return this.AC3circle;
	},

	AC3Circle: function()
	{
		return this.AC3circle;
	},

	getIcon: function() {
		return this.iconID;
	},

	setIcon: function(id)
	{
		this.iconID = id;
		this.m.setIcon(this.icon(id));
	},

	// The icons will be stored as a sprite (in a single file). This method
	icon: function(id)
	{
		id = typeof id !== 'undefined' ? id : 1;

		var tile_size = [40, 40]; // width and height of a single icon
		var icon_size = new google.maps.Size(tile_size[0], tile_size[1]);
		var icon_url  = '/img/icons/markers.png';

		var icon_tile_origin = function(tilenumber)
		{
			// The origin of the sprite on the x axis.
			var x = (tilenumber - 1) * tile_size[0];
			return new google.maps.Point(x, 0);
		}

		var icon_tile_anchor = function(tilenumber)
		{
			if ( tilenumber == 1 ) return new google.maps.Point(20,36);
			if ( tilenumber == 2 ) return new google.maps.Point(20,36);
			if ( tilenumber == 4 ) return new google.maps.Point(20,20);
		}

		var icons =
		{
			'field':
			{
				url: icon_url,
				size: icon_size,
				origin: icon_tile_origin(id),
				anchor: icon_tile_anchor(id)
			}
		};

		return icons['field'];
	},

	AC3Bubble: function()
	{
		return this.b;
	},

	bubble: function()
	{
		return this.b;
	},

	infoWindow: function()
	{
		return this.b;
	},

	_uniqueid: function()
	{
		return 'xxxx'.replace(/[xy]/g, function(c) {
		    var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
		    return v.toString(16);
		});

		// http://stackoverflow.com/a/2117523/377205
		// Here is a more complex uniqueid with some seeded chars
		/*
		return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
		    var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
		    return v.toString(16);
		});
		*/
	},

	init: function(position, map, options)
	{
		var self = this;

		var settings = {
			id: false,
			visible: true,
			//title: 'This is a test title',
			url: '#',
			circle: true, // include circle geometery with the marker
			icon: 1
		};

		settings = $.extend( settings,  options );

		if ( settings.id === false ) {
			settings.id = self._uniqueid();
		}

		var marker = new google.maps.Marker({
			id: settings.id,
			visible: settings.visible,
			title: settings.title,
			icon: self.icon(settings.icon),
			position: new google.maps.LatLng(position[0], position[1]),
			map: map
		});

		this.iconID = settings.icon;

		this.m = marker;
		this.m['custom'] = {};
		this.m['id'] = null;

		var self = this;

		this.set('id', settings.id);

		/* // No longer using this but it may break some things.
		for ( key in options ) {
			if ( key == 'position' ) continue;  // No need to store this here.
			if ( key == 'id' ) this.id = custom[key];
			this.set(key, custom[key]);
		}
		*/

		if ( settings.circle === true) {
			this.AC3circle = new AC3Circle(position, map);
		}

		var bubble_html =  "<div class='map_marker_bubble'> 									\
								<div class='map_marker_bubble_title'>							\
									<a href='"+settings.url+"'>"+settings.title+"</a>			\
								</div>															\
					 		</div>";

		/*
		// google's infowWindow
		this.b = new google.maps.InfoWindow({
		     content: bubble_html,
		     pixelOffset: new google.maps.Size(-1, 40)
		});
		this.b.set('id', settings.id);

		google.maps.event.addListener(marker, 'dragend', function (event)
		{
			var lat = this.getPosition().lat();
			var lng = this.getPosition().lng();
			var pos = new google.maps.LatLng(lat, lng);
			self.AC3Circle().circle().set('center', pos);
			self.AC3Bubble().set('center', pos);
		});
		*/


		 this.b = new InfoBubble({
		          map: map,
		          content: "<a style='font-size:14px;' href='"+settings.url+"'>"+settings.title+"</a>",
		          position: new google.maps.LatLng(position[0], position[1]),
		          shadowStyle: 1,
		          padding: 15,
		          backgroundColor: '#edeae7',//'#282726',
		          borderRadius: 4,
		          arrowSize: 15,
		          borderWidth: 1,
		          borderColor: '#444',//'#9a6901',
		          disableAutoPan: true,
		          hideCloseButton: false,
		          arrowPosition: 30,
		          backgroundClassName: 'map_marker_bubble_bg',
		          arrowStyle: 0
		        });

		google.maps.event.addListener(marker, 'dragend', function (event)
		{
			var lat = this.getPosition().lat();
			var lng = this.getPosition().lng();
			var pos = new google.maps.LatLng(lat, lng);
			self.AC3Circle().circle().set('center', pos);
			self.AC3Bubble().set('center', pos);
		});

		 // Add a class to the info bubble window
		 // This is a hack in order to set the width of the window to auto. For some reason
		 // the plugin tries to set the width of the window and it always screws it up.
		 $w = $(this.b.j).parent();
		 $w.addClass('map_marker_bubble_window');
		 $w.css({'padding': '15px 25px 15px 15px'});
		 $w.parent().find('img').attr('src', '/img/icons/close_bubble_dark.png')

		 /*
		 google.maps.event.addListener($w.parent().find('img'),'closeclick',function(){
	   		console.log('closed');
		 });
		*/


		 /*
		 $w.find('.map_marker_bubble_close').on('click', function() {
		 	console.log('closing');
		 });
		 */

		 /*
		 $offset = $w.parent().parent().parent();
		 $offset.addClass('test');
		 console.log($offset);
		 $offset.css({'top': '-20px'});
		 */
		/*
		this.b = new InfoBubble({
						map: map,
						content: bubble_html,
						position: self.getPosition(),
						shadowStyle: 1,
						padding: 0,
						backgroundColor: '#2a2a2a',
						borderRadius: 3,
						borderWidth: 1,
						borderColor: '#222222',
						disableAutoPan: true,
						//hideCloseButton: true,
						arrowSize: 15,
						arrowPosition: 25,
						//minWidth: 200,
						backgroundClassName: 'map_marker_bubble_bg'
		});
		*/


		//$('.map_marker_bubble_bg').parent().width('auto');
  	},
}

// Function for easing into a value with a specified number of frames
// as well as facilitate value remapping.
function easein(frame, totalFrames, startValue, endValue)
{
	startValue = typeof startValue !== 'undefined' ? startValue : 0;
	endValue = typeof endValue !== 'undefined' ? endValue : 1;

	var ease = 1-(Math.pow((1-(frame/totalFrames)),2));
	var mapping = (ease*(endValue-startValue)) + startValue;
	return mapping;
}


function AC3Circle(position, map, custom)
{
	// Custom is an object with key value pairs
	custom = typeof custom !== 'undefined' ? custom : false;
	if ( !custom ) custom = {};

	// If position in an array of [lat, lng] then run init.
	// Else we assume the position is actually an existing google marker.
	if ( $.isArray(position) )
	{
		this.init(position, map, custom)
	} else {
		var marker = position; // The position is actually a marker
		this.convert( marker );
	}
}
AC3Circle.prototype = {
	// Init happens at the bottom

	// Get the actual google api circle object.
	circle: function()
	{
		return this.google_circle;
	},

	// Set the visibility of the circle
	visible: function ( state )
	{
		this.circle().set('visible', state);
	},

	stop_animation: function()
	{
		delete this['anim_loop'];
		// id is removed frome the looping_marker_circles array.  default is false.
	},

	start_animation: function(loop, settings)
	{
		// loop (true) means to continue to loop the radius animation until the marker
		loop = typeof loop !== 'undefined' ? loop : false;
		speed = typeof speed !== 'undefined' ? speed : 'fast';

		var self = this;

		// If the circle is already animating then return (don't interrupt it).
		if ( self['animating'] ) return;

		// Add a flag to the circle indicating that it is currently animating.
		self['animating'] = 1;

		// Begining radius size and opacity for the circle.
		self.circle().setRadius(0);
		self.circle().set('strokeOpacity', 1);
		self.circle().set('fillOpacity', 1);
		self.circle().set('visible', true);

		// The target radius size of the cirlce.
		var size = 10;
		// The current frame in the animation.
		var frame = 0;
		// The number of steps it takes to get to the target radius size
		// (which corresponds to the speed of the animation).
		var steps = 30;
		// The speed at which each frame is executed.
		var execSpeed = 35;

		if ( settings )
		{
			if ( settings.speed == 'fast' )
			{
				steps = 20;
				execSpeed = 40;
			}
			if ( settings.speed == 'medium' )
			{
				steps = 30;
				execSpeed = 35;
			}
			if ( settings.speed == 'slow' )
			{
				steps = 40;
				execSpeed = 30;
			}

			if ( settings.steps )
				steps = settings.steps;

			if ( settings.execSpeed )
				execSpeed = settings.execSpeed;

			if ( settings.size ) {
				// Set the size of the radius to be 15% the width of the viewport.
				if ( settings.size === 'smart') {
					var map = self.circle().map;
					var bounds = map.getBounds();
					var ne = bounds.getNorthEast();
					var sw = bounds.getSouthWest();
					var left = new google.maps.LatLng(ne.lat(), sw.lng());
					var right = new google.maps.LatLng(sw.lat(), ne.lng());
					var boundDist = MapUtils.distance_between_points(left, right);
					//size = boundDist*.15;
					size = boundDist*.05;
				} else {
					size = settings.size;
				}
			}
		}

		// Calculating all of the animation frames.
		var looper = setInterval(function()
		{
			frame++;

			// IF looping then check to see if looping should continue or end.
			if ( loop && frame == (steps+1) )
			{
				// IF this circle is set to loop the animation then continue
				// the loop by reseting the frame to 0.
				// ELSE end the loop by setting the frame to greater then the steps.
				if ( self['anim_loop'] )
				{
					frame = 0;
				} else {
					frame == steps+1;
				}
			}

			// IF the frame is less then the number of steps then continue the animation.
			// ELSE end the animation and remove the animating flag.
			if( frame <= steps )
			{
				// Determine the new radius size and opacity.
				var newRadius = easein( frame, steps, 0, size );
				var newOpacity = 1-(frame/steps);

				// Set the new radius size and opacity.
				self.circle().setRadius(newRadius * self.measurement_unit_multiplier);
				self.circle().set('fillOpacity', newOpacity);
				self.circle().set('strokeOpacity', newOpacity);
			} else {
				// Remove the animating flag from the circle.
				delete self['animating'];

				// Stop the animation by clearing the looper interval.
				clearInterval(looper);
		        return;
			}
		}, execSpeed);

		// If the animation is looping then add the marker id to the array.
		if ( loop ) self['anim_loop'] = 1; //looping_marker_circles[id] = true;
	},

	init: function(position, map, custom)
	{
		var self = this;
		this['custom'] = {};

		for ( key in custom )
		{
			if ( key == 'position' ) continue;  // No need to store this here.
			this.set(key, custom[key]);
		}

		this.earths_radius = map.earths_radius;
		this.measurement_unit_multiplier = map.measurement_unit_multiplier;


		// Include circle geometry for every marker... this can't be effecient.
		this.google_circle = new google.maps.Circle({
									visible: false,
									center: new google.maps.LatLng(position[0], position[1]),
									strokeColor:'#b28a19',
									fillColor:'#b28a19',
									strokeOpacity: .6,
									fillOpacity: .4,
									strokeWeight: 2,
									map: map,
									radius: 20000,
							  });
  	},
};


// Namespace Template
// http://stackoverflow.com/a/5947280/377205
(function( MapUtils, $, undefined ) {
    //Public Method
	MapUtils.distance_between_points = function(point1, point2)
	{
		var earths_radius = 3959; 				  	// Earths radius in miles.
		var	measurement_unit_multiplier = 1609.36; 	// Number of meters in a mile.

		rad = function(x) {return x*Math.PI/180;}

		distHaversine = function(p1, p2)
		{
		  var R = earths_radius;
		  //var R = 6371; // earth's mean radius in km
		  var dLat  = rad(p2.lat() - p1.lat());
		  var dLong = rad(p2.lng() - p1.lng());
		  var a = Math.sin(dLat/2) * Math.sin(dLat/2)
		  		+ Math.cos(rad(p1.lat())) * Math.cos(rad(p2.lat()))
		  		* Math.sin(dLong/2) * Math.sin(dLong/2);
		  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		  var d = R * c;
		  return d.toFixed(3);
		}
		return distHaversine(point1, point2);
	}

}( window.MapUtils = window.MapUtils || {}, jQuery ));




