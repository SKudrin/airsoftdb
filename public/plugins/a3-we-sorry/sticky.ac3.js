

var AC3StickySettings = {
  sticky: true,
  stickyID: 'sticky',
  unstickyID: 'unsticky',
  standinID: 'sticker_standin',
  enabled: true
};

$.fn.AC3Sticky = function( options, callback )
{
  var objects = [], return_data;
  this.each(function()
  {

    // initialize the object here (add methods, event bindings, etc)
      if( $(this).data('object') === undefined )
      {
          var object = {};
          object.$this = $(this);          
          object.$element = $(this);
          object.$window = $(window);
          object.settings = $.extend( AC3StickySettings, options );
          object.offset = object.$element.offset();
          object.$sticky = $('#'+object.settings.stickyID);
          object.$unsticky = $('#'+object.settings.unstickyID);
          object.$standin = $('#'+object.settings.standinID);          

          //var $window    = $(window);
          //var Sticker = object;  // The sticker element (needs to be a property so that the vars are constant)

          //$(this).prev().prop('tagName');

    // ---------------------
    //   PUBLIC METHODS
    // ---------------------
      // returns the DOM element as a jquery object
      object.get = function() {
        return this.$this;
      }      

      object.Sticky = function(stick) {
        stick = typeof stick !== 'undefined' ? stick : true;        
        if ( stick === true ) {                       
            this.$element.addClass("stick");//.appendTo("#"+this.settings.stickyID);
            this._stick();
        } else {          
          this.$element.removeClass("stick");
          this._unstick();
        }        
      }

      // Adjust the height of the standin element (useful when the height of the sticky element is dynamic).
      object.StandinHeight = function( height, speed ) {
        speed = typeof speed !== 'undefined' ? speed : 300;
        if ( speed ) {
          this.$standin.animate({'height':height+'px'}, 300, function(){
            $(this).css({'overflow': 'hidden'});
          });  
        } else {          
          this.$standin.height( height );
        }        
      }

      object.disable = function() {
        this.settings.enabled = false;
      }

      object.enable = function() {
        this.settings.enabled = true;
      }

      object._stick = function() {
        // set the sticker_standin height so there is no pop during scroll.
        this.$standin.height( this.$element.height() );
        // move the sticker element into the sticky element
        this.$element.addClass("is_sticky").appendTo("#"+this.settings.stickyID);             
      }

      object._unstick = function() {        
        // reset the stick_standin height to zero
        this.$standin.height(0);
        // move the sticker element back to it's original location in the dom.
        this.$element.appendTo("#"+this.settings.unstickyID).removeClass("is_sticky");
      }

      // ---------------------
    //  "PRIVATE" METHODS
    // ---------------------
      // Returns the position of the mouse relative to the position of the element          
      object._onScroll = function(event)
      {

        if ( this.settings.enabled === false ) {
          return;
        }

        if ( !this.$element.hasClass('stick') ) {
          return;
        }

        if (this.$window.scrollTop() > this.offset.top) {
          // is_sticky class is used to query whether the sticker is already sticky.  If so then
          // no need to continue.
          if ( this.$element.hasClass("is_sticky") ) return;
          this._stick();
        } else {
          //if ( this.$element.hasClass("is_sticky") ) return;
          this._unstick();          
        } 
      }

      

      // ---------------------
      //  INIT
      // ---------------------
      object.$window.scroll(function(e) {         
        object._onScroll(e);
      });

      if ( object.settings.sticky ) {       
        object.Sticky(true);
      }      

      // define the object within the object
      object.$this.data('object', object);
        
      } else {
        //console.log("object exists");          
        object = $(this).data('object');
      }        
      objects.push(object);        
    }); // end of instance
  
    if( objects.length === 1 )
    {
      return_data = objects[0];
    } else {
      return_data = objects;        
    }
  
    return_data.all = function(callback)
    {
      //console.log(callback);
      $.each( objects, function(){
          callback.apply(this);
      });   
    };    

    return return_data;
};

