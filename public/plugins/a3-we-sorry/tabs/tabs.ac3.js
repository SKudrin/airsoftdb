// Requires
//   global.js
//   tabs.tpl
//   tabs.ac3.css

var AC3Tabs_default_settings =
{
	displayStyle: 'auto', // auto, horizontal, vertical
	onTabClick: null
};

$.fn.AC3Tabs = function( options, init_callback )
{
	var objects = [], return_data;

	this.each(function()
	{
		// initialize the object here (add methods, event bindings, etc)
    	if( $(this).data('object') === undefined )
    	{
  		// ---------------------
		//   SETUP
		// ---------------------
      		//var self = this;
      		var object = {};

      		object.initialized = false;
      		object.$this = $(this);
      		object.$tabs = $(this).find('.ac3tabs');
      		object.$mobile = $(this).find('.ac3tabs_mobile');
      		object.$label = $(this).find('.ac3tabs_menu_label');
      		object.settings = $.extend(true, {}, AC3Tabs_default_settings, options);

      		object.settings.menu_label = object.$label.text();


      	// NOTE: init happens at the bottom so that it has access to the methods.

  		// ---------------------
		//   PUBLIC METHODS
		// ---------------------
			// returns the DOM element as a jquery object
			object.get = function()
			{
				return this.$this;
			}

			object.Open = function() {
				Global.animate_height_auto(object.$tabs, 200);
				object.$tabs.addClass('open');
				object.$mobile.addClass('open');
			}

			object.Close = function(speed) {
				speed = typeof speed !== 'undefined' ? speed : 200;

				object.$tabs.animate({height: 0}, speed, function(){

	           	});
	           	object.$tabs.removeClass('open');
	           	object.$mobile.removeClass('open');
			}

			/* Set the display style
			 * @access public
			 * @param string style ('auto', 'vertical', 'horizontal')
			 */
			object.DisplayStyle = function(style) {
				style = typeof style !== 'undefined' ? style : 'auto';

				object.settings.displayStyle = style;

				if ( style == 'auto' ) {
					if ( object._tabs_fit_in_row() === true ) {
						object._vertical(false);
					} else {
						object._vertical();
					}
				} else if ( style == 'vertical' ) {
					object._vertical(true);
				} else if ( style == 'horizontal' ) {
					object._vertical(false);
				} else {
					return;
				}
			}

			object.SelectTab = function(id, callback) {
				callback = typeof callback !== 'undefined' ? callback : false;
				if ( $('#'+id).length ) {
					object._select_tab($('#'+id), callback);
				}
			}

			object.GetTabCount = function(id) {
				var current = $('#'+id).find('.ac3tab_count').text();
				return parseInt(current);
			}

			// id === true will update the count on the selected tab
			object.TabCountPlus = function(id, change) {
				if ( id === true ) {
					id = object.GetSelectedTabID();
				}
				var current = object.GetTabCount(id);
				$('#'+id).find('.ac3tab_count').text(current+change);
			}

			object.GetSelectedTabID = function() {
				$tab = object.GetSelectedTab();
				if ( $tab ) {
					return $tab.attr('id');
				}
				return false;
				/*
				var selected = false;
				object.$tabs.find('.ac3tab').each(function() {
					if ( $(this).hasClass('selected') ) {
						selected = $(this).attr('id');
						return false; // break the each loop
					}
				});
				return selected;
				*/
			}

			object.GetSelectedTab = function() {
				return object.$tabs.find('.ac3tab.selected');
			}

			object.isVertical = function() {
				if ( object.$this.hasClass('vertical') ) {
					return true;
				} else {
					return false;
				}
			}


  		// ---------------------
		//  "PRIVATE" METHODS
		// ---------------------
			object._hello = function() {
				return "hello";
			}

			object._vertical = function(state) {
				state = typeof state !== 'undefined' ? state : true;
				if ( state == true ) {
					object.$this.addClass('vertical');
				} else {
					object.$this.removeClass('vertical');
				}
			}

			object._select_tab = function($tab, callback) {
				callback = typeof callback !== 'undefined' ? callback : false;

				object.$tabs.find('.ac3tab').removeClass('selected');
				$tab.addClass('selected');

				if ( callback ) {
					callback($tab);
				}

				object._update_menu_label();

				if ( object.isVertical() ) {
					setTimeout(function() {
						object.Close();
					}, 400);
				}
			}

			object._update_menu_label = function() {
				$tab = object.GetSelectedTab();
				object.$label.html(object.settings.menu_label + "<span class='ac3tabs_menu_sub_label' > | " + $tab.text().toUpperCase() + "</span>" );
			}

			// Determine if all of the tabs fit in a single row.
			object._tabs_fit_in_row = function() {
				if ( object.initial_cumulative_width < object.$this.width() ) {
					return true;
				} else {
					return false;
				}
			}

			// Get the cumulative width of all of the tabs.
			object._get_cumulative_width = function() {
				var cumulative = 0;
				object.$tabs.find('.ac3tab').each(function() {
					cumulative += $(this).outerWidth(true);
				});
				return cumulative;
			}



		// ----------
		//   INIT
		// ----------
			object.initialize = function() {
				// Set the initial_cumulative_width of all of the children elements before the style
				// is set.
				object.initial_cumulative_width = object._get_cumulative_width();

				// If on a mobile device then set this as vertical
				if ( Browser.ScreenSize() <= 2 ) {
					object._vertical();
				} else {
					object.DisplayStyle(object.settings.displayStyle);
				}

				if ( object.$this.hasClass('vertical') ) {
					object.$tabs.height(0);
				}

				// Update the mobile menu label to reflect the selected tab
				object._update_menu_label();

				// The menu collapses to vertical when all of the tabs no longer fit in a single row
				$(window).resize(function() {
					if ( object.settings.displayStyle == 'auto') {

						if ( Browser.ScreenSize() <= 2 ) {
							if ( !object.$this.hasClass('vertical') ) {
								object._vertical();
								object.$tabs.height(0);
							}
							return;
						}

						if ( object._tabs_fit_in_row() === true ) {
							object._vertical(false);
							object.$tabs.removeAttr('style');
						} else {
							if ( !object.$this.hasClass('vertical') ) {
								object._vertical();
								// Close it too
								object.$tabs.height(0);
							}
						}
					}
			    });


				// on click
				object.$tabs.find('.ac3tab').each(function() {
					$(this).on('click', function() {
						// If the tab_label is an anchor then return.
						if ( $(this).find('.ac3tab_label').prop('tagName') == 'A' ) {
							return;
						}
						if ( object.settings.onTabClick && typeof(object.settings.onTabClick) === 'function') {
							object._select_tab($(this), object.settings.onTabClick);
						} else {
							object._select_tab($(this));
						}
					});
				});

				object.$mobile.on('click', function() {
			        if ( object.$tabs.height() == 0) {
			        	object.Open();
			        } else {
			        	object.Close();
			        }
			      });

				if ( init_callback ) {
					init_callback();
				}
				object.initialized = true;

			}

			object.initialize();

			// Define the object within the object
			object.$this.data('object', object);
		} else {
			object = $(this).data('object');
		}
    	objects.push(object);
  	}); // End of instance

  	// Setting up the plugin.  Don't do anything below this line.
  	if( objects.length === 1 ) {
    	return_data = objects[0];
  	} else {
    	return_data = objects;
  	}
  	return_data.all = function(callback) {
    	$.each( objects, function(){
      		callback.apply(this);
    	});
  	};
  	return return_data;
};

/*
$( document ).ready(function(){
	$('.ac3tabs_wrap.autoload').SingleTextareaForm();
});
*/