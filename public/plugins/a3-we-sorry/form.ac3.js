var AC3FormSettings = 
{ 
  onChange: false,  		// Callback for whenever the form detects a change.
  onReset: false, 	// Callback for whenever the form had changes but no longer does.
  changedClass: 'input_has_change', // class name added to any input that has a changed value
  firstBlurClass: 'first_blur', // class name added to an input when it is blurred for the first time.
  validate: true, 			// Use the built in validator on the fields  
  ajax: true, 				// Use ajax for submits
  onSubmit: false, 			// Callback that fires before submitting the form
  onSubmitAfter: false, 	// Callback that fires after submitting the form  
};



$.fn.AC3Form = function( options, init_callback )
{
	var objects = [], return_data;	

	this.each(function()
	{
		// initialize the object here (add methods, event bindings, etc)
    	if( $(this).data('object') === undefined )
    	{    
  		// ---------------------
		//   SETUP
		// ---------------------    			
      		var object = {};  

			//object.settings = $.extend( AC3FormSettings, options );   
			object.settings = $.extend(true, {}, AC3FormSettings, options);    		
      		object.$this = $(this);
      		object.$form = $(this);
      		object.initialized = false;      		      		
      		object.hasChanges = false;

      		if(object.settings.verbose)
      			console.log("Initializing");
      	// NOTE: init happens at the bottom so that it has access to the methods.			

  		// ---------------------
		//   PUBLIC METHODS
		// ---------------------
			/** 
  			* @desc Get the form dom jquery element  			  			
  			* @return jQuery element
			*/ 
			object.get = function()
			{	
				return object.$this;
			}

			/** 
  			* @desc Set the value of an input.
  			* @param string input - The name of the input you wish to set a value on.
  			* @param string value - The value of the input you wish to set.
  			* @return bool - success or failure 
			*/ 
			object.set = function(input, value)
			{
				if ( value === false ) {
					value = "";
				}
				var $input = object.input(input);
				if ( !$input ) {
					return false;
				}

				// NOTE: The state input is currently a hack because it depends on the selectpicker
				//       object.  Should come up with something better here.
				if ( $input.attr('name') == 'state' && $input.hasClass('selectpicker')) {					
					$input.selectpicker('val', value);					
				} else {
					$input.val(value);	
				}
				// var attr = $input.attr('val');
				// if (typeof attr !== typeof undefined && attr !== false) {
				// 	$input.val(value);	
				// }

				object.refreshState(input);
				return true;
			}
			
			/** 
  			* @desc Get input by name.
  			* @param string name - The name of the input.  			
  			* @return jQuery element
			*/			
			object.input = function(name)
			{
				var $input = null;
				// If name is a string then see if the input exists by name.
				if ( jQuery.type(name) === 'string' ) {
					if ( object.inputs[name] ) {
						$input = object.inputs[name]
					}		
				}
				// If name is a jQuery object then see if the object is an input in the form.
				if ( name instanceof jQuery) {
					if ( object.inputs[$(name).attr('name')] ) {
						$input = object.inputs[name.attr('name')];
					}
				}
				return $input;
			}			
				
			/** 
  			* @desc Reset a specific input or all of the inputs
  			* @param string input (optional) - The name of the input you would like to reset.
  			* @return None
			*/
			object.reset = function(input)
			{
				input = typeof input !== 'undefined' ? input : false;

				if ( input === false ) {
					for( input in object.inputs ) {
						object.reset(input);
					}
					/*
					// don't need to to do this here because this will happen when an input is set.
					if ( object.settings.onReset ) {
						console.log('three');
						object.settings.onReset(object.defaults);
					}
					*/
				} else {	
					var $input = object.input(input);
					if ( !$input ) {
						return false;
					}

					/*
					// Might be best to handle this with the onReset callback
					// If the input is redactor and the element is a div then update the html
					if ( $input.hasClass('redactor') && !$input.is('textarea') ) {						
						$input.html(object.defaults[$input.attr('name')]);
						return;
					}
					*/
					
					object.set($input, object.defaults[$input.attr('name')]);
				}
			}

			/** 
  			* @desc Update the state of the form with the current values of the inputs.  			
  			* @return None
			*/
			object.update = function()
			{
				for( input in object.inputs ) {
					// Reset what the default value is for the input
					object.defaults[input] = object.inputs[input].val();
					// Refresh the state on the input.
					object.refreshState(input);
				}

				// Remove anything else that might have the changedClass on it (redactor elements).
				object.$this.find("."+object.settings.changedClass).removeClass(object.settings.changedClass);

				if ( object.hasChanges && object.settings.onReset ) {						
					object.settings.onReset(object.defaults);
				}
				object.hasChanges = false;
			}

			/** 
  			* @desc Execute this code whenever a key is pressed.
  			* @return bool - success or failure 
			*/
			object.onKeyPressed = function(input, e, callback)
			{				
				if(e.keyCode === 27) {					
					object.reset(input);					        
				}

				var $input = object.input(input);
				if ( !$input ) {
					return false;
				}
				
				if ( object.hasChange(input) ) {
					object._set_input_changed(input);
				} else {
					object._set_input_nochange(input);
				}

				if ( callback ) {
					callback();
				}
				return true;			
			}


			/** 
  			* @desc Execute this code whenever an input loses focus.
  			* @return bool - success or failure 
			*/
			object.onFocusOut = function(input, e, callback)
			{				
				var $input = object.input(input);				

				if ( object.focusValue ) {
					if ( object.focusValue === $input.val() ) {
						object.focusValue = null;
						return;
					}
				}
								
				if ( !$input ) {
					return false;
				}										
				
				// Update the state of the input based on whether it has changed or not.
				object.refreshState(input);				
				
				if ( callback ) {
	            	callback();		            	
				}
			}						

			/** 
  			* @desc Determine if a specific input has changes or if the entire form has changes.
  			* @param string input (optional) - The input name you wish to query.  			
  			* @return bool
			*/
			object.hasChange = function(input, callback)
			{
				input = typeof input !== 'undefined' ? input : false;

				// If input is not specified then return whether the form has changes.
				if ( !input ) {
					if ( object.changes().length > 0 ) {
						return true;
					} 
					return false;
				}

				// If an input is specified then check to see if the input has changes.
				var $input = object.input(input);
				if ( !$input ) {
					return null;
				}

				if ( $input.val() !== object.defaults[$input.attr('name')] ) {		
					return true;
				}
				return false;
			}

			object.changes = function()
			{			
				var changes = new Array();
				for ( input in object.inputs ) {
					if ( object.inputs[input].hasClass(object.settings.changedClass)) {						
						changes.push(object.inputs[input]);
					}
				}
				return changes;

				// Loop through all of the inputs manually (above) instead of as below in order
				// to assure we're only looking at the specific inputs that the form has loaded
				// into the plugin.
				//return object.$this.find("."+object.settings.changedClass);
			}

			/** 
  			* @desc Refresh the state of the input by determining if there is a change or not.
  			* @param string input - The input name you wish to refresh.
  			* @return bool True if the input has changed, False if the input is the same.
			*/
			object.refreshState = function(input)
			{				
				var $input = object.input(input);
				if ( !$input ) {
					return null;
				}	

				if ( object.hasChange(input) ) {					
					object._set_input_changed(input);					
				} else {
					object._set_input_nochange(input);	
				}				
				
				if ( object.hasChange() ) {
					object.hasChanges = true;
					if (object.settings.onChange ) {
						object.settings.onChange(object.changes());	
					}					
				} else {									
					//if ( object.hasChanges && object.settings.onReset ) {
					if ( object.settings.onReset ) {					
						object.settings.onReset(object.defaults);
					}
					object.hasChanges = false;
				}				
				
				return object.hasChanges;
			}

			object.loadRedactor = function($element, options) {
				optons = typeof optons !== 'undefined' ? optons : false;
				object._init_redactor($element, options);
			}

		// ---------------------
		//   "PRIVATE" METHODS
		// ---------------------
			/** 
  			* @desc Set the state of the input as having no change.
  			* @param string input - The input name
  			* @return bool success or failure
			*/
			object._set_input_nochange = function(input)
			{
				var $input = object.input(input);
				if ( !$input ) {
					return false;
				}
				$input.removeClass(object.settings.changedClass);
				return true;
			}

			/** 
  			* @desc Set the state of the input as having a change.
  			* @param string input - The input name
  			* @return bool success or failure
			*/
			object._set_input_changed = function(input)
			{
				var $input = object.input(input);
				if ( !$input ) {
					return false;
				}
				$input.addClass(object.settings.changedClass);
		        $input.addClass(object.settings.firstBlurClass);
		        return true;
			}

			/** 
  			* @desc Run at initialization only - This sets up limiters on the inputs so that only
  			* 		certain keys can be used for certain fields.  			
  			* @return None
			*/
			object._key_limiters = function()
			{
				if ( object.input('latitude') )	{
					object.input('latitude').numeric({
						allowMinus : true,
						max: 90,
						min: -90		
					});
				}
				if ( object.input('longitude') ) {
					object.input('longitude').numeric({
						allowMinus : true,
						max: 180,
						min: -180
					});
				}
				if ( object.input('phone') ) {
					object.input('phone').alphanum({						
						allowNumeric: true,						
						allowLatin: false,
						allowOtherCharSets: false,
						allowSpace: false,
						allow: "-",
						maxLength: 12
					});
				}
			}

			object._init_redactor = function($element, custom_optons) {
				custom_optons = typeof custom_optons !== 'undefined' ? custom_optons : false;


				if ( !object.settings.redactor.plugins ) {
					var buttons = ['html', 'formatting', 'bold', 'italic', 'deleted', 'unorderedlist', 'orderedlist', 'outdent', 'indent', 'link', 'alignment', 'horizontalrule'];
					if ( $element.hasClass('media') ) {					
						buttons = ['html', 'formatting', 'bold', 'italic', 'deleted', 'unorderedlist', 'orderedlist', 'outdent', 'indent', 'image', 'file', 'link', 'alignment', 'horizontalrule', '|', 'image', 'video'];
					}
				}

				if ( custom_optons && custom_optons.uploadDir ) {
					var upload = object.settings.uploader + "&dir=" + custom_optons.uploadDir;
				} else {
					var upload = object.settings.uploader + "&dir=" + $element.attr('name');	
				}				

				var default_settings = {										
					minHeight: 200,					
					imageUpload: upload,
					buttons: buttons,
					buttonsHideOnMobile: ['image', 'video'],				
					initCallback: function()
					{	
						var r = this;						
						// Not sure why, but I have to put this in setTimeout for it to work.						
						setTimeout(function() {							
							var name = r.core.getElement().attr('name');
							object.defaults[name] = r.code.get();
						}, 10);						
					},
					blurCallback: function(html)
					{	
						/*
						// Since redactor doesn't use the original textarea then update the textarea
						// manually in order to get the callbacks to fire correctly.
						object.set(this.$source[0].name, this.get());

						// For display purposes only - add a border to the redactor editor if there is
						// a change, remove it otherwise.
						if ( this.get() !== object.defaults[this.$source[0].name] ) {						
							this.$box.addClass(object.settings.changedClass);
						} else {						
							this.$box.removeClass(object.settings.changedClass);
						}	
						*/				
					},
					imageUploadCallback: function(image, json) 
					{	
						
					},
					imageUploadErrorCallback: function(json) 
					{				
						var text = JSON.stringify(json);				
						alert("Error on upload: "+json);
					}
				}

				if ( !object.settings.redactor.plugins ) {
					if ( $element.hasClass('media') ) {
						default_settings.plugins = ['imagemanager', 'video', 'fontsize', 'fontcolor'];
					} else {
						default_settings.plugins = ['fontsize', 'fontcolor'];
					}					
				}				

				var settings = $.extend( default_settings, object.settings.redactor );

				if ( custom_optons !== false ) {					
					settings =  $.extend( settings, custom_optons );						
				}		

				var redactor = $element.redactor(settings);				
				object.inputs[$element.attr('name')] = $element;						
				
				return redactor;
			}		
    			

			object.submit = function() {
				var $self = this.$this;
				$self.submit();
				//$("form:first").submit();
			}	
  		
		// ----------
		//   INIT
		// ----------		
			object.initialize = function()
			{
				var $self = this.$this;				

				// All of the form inputs
				object.inputs = {};
				object.defaults = {};
				object.focusValue = null;

				// Store away every input and its value when the page was loaded.
				$.each($self.find(':input'), function() {
					object.inputs[$(this).attr('name')] = $(this);
					object.defaults[$(this).attr('name')] = $(this).val();
					// Store the value of the focused element before any changes are made.
					$(this).on('focus', function() {												
						object.focusValue = $(this).val();						
					});
				});

				// If not using the validator then bind the blur and keyup to the inputs.
				// Otherwise the validator will do this work.
				if ( !object.settings.validate ) {					
					$self.on('blur', ':input', function(e) {												
						object.onFocusOut($(this));
					});
					$self.on('keydown', ':input', function(e) {
						object.onKeyPressed($(this), e);
					});
				}

				// Set the key limiters on known input types.
				object._key_limiters();

				// Initialize all redactor instances
				if ( object.settings.redactor.waitToLoad !== true ) {
					object.$form.find('.redactor').each(function() {				
						object._init_redactor($(this));
					});	
				}
				

				// Disable submit with the enter button
				// used to have keyup too.
				$self.bind("keypress", function(e) {									

					if(e.which == 13 && e.target.nodeName != "TEXTAREA") {
						var $el = $(e.target);										
						// We need to allow enter on redactor elements as well (which are divs that
						// are using contenteditable attribute.  I'm not sure if this is very robust
						// because we have hardcoded the class name here.
						if ( !$el.hasClass('redactor') ){
							e.preventDefault();
							return false;
						}
						/*
						// Check to see if the div is content editable... because in the case
						// of redactor when need to allow
						if ( !$el.is("[contenteditable='true']") ) {
							e.preventDefault();
							return false;
						}
						*/
					}
				});				

				// Setup the generic validator
				if ( object.settings.validate ) {					
					object.validator = AC3FormValidator(object);
				}				

				if ( init_callback ) {
					init_callback();
				}
				object.initialized = true;	
			} // END OF INIT

			object.initialize();		

			// Define the object within the object
			object.$this.data('object', object);			  
		} else {				         
			object = $(this).data('object');
		}        
    	objects.push(object);        
  	}); // End of instance  
  	
  	// Setting up the plugin.  Don't do anything below this line.
  	if( objects.length === 1 ) {
    	return_data = objects[0];
  	} else {
    	return_data = objects;
  	}  
  	return_data.all = function(callback) {    	
    	$.each( objects, function(){
      		callback.apply(this);
    	});   
  	};
  	return return_data;
};


function AC3FormValidator(AC3Form) {
	//var settings = AC3Form.settings.validationSettings;	

	var defaults = {
		ignore: ".ignore, :hidden", // Skips validation on fields with ignore class
		rules: {
			title: {
				required: true,
			},
			email: {
				required: false,
				email: true,				
			},			
			password: "required",
			hours: {
				required: false,
				rangelength: [0, 300]
			},
			url: {
				required: false,
				url: true
			},
			phone: {
				required: false,
				phoneUS: true
			},
			facebook: {
				// remote: {url:settings.remote, type:"post", data:{validate:1}}
				facebook: true
			},
			googleplus: {
				googleplus: true
			},
			twitter: {
				twitter: true
			},			
			youtube: {
				youtube: true
			},
			vimeo: {
			},
			instagram: {
				instagram: true
			},
			pinterest: {
				pinterest: true
			},
			tumblr: {
				tumblr: true
			},
			flickr: {
				flickr: true				
			},
			myspace: {
				myspace: true				
			},
			organization: {
				valueNotEquals: "default"
			},
			state: {
				valueNotEquals: "default"
			},
			url2: {
				url2: true
			}
		},
		errorPlacement: function(error, element) {
			if (element.attr("name") == "accept_agreement") {				
				element.parents('.customCheckboxGrp').prepend(error);				
			} else {
				error.insertAfter(element);
			}
		},

		/*
		messages: {
			facebook: 	{remote: jQuery.format("Invalid Facebook url.")},
			googleplus: {remote: jQuery.format("Invalid Google Plus url.")},			
			twitter: 	{remote: jQuery.format("Invalid Twitter url.")},
			youtube: 	{remote: jQuery.format("Invalid YouTube url.")},
			vimeo: 		{remote: jQuery.format("Invalid Vimeo url.")},
			instagram:  {remote: jQuery.format("Invalid Instagram url.")},
			pinterest: 	{remote: jQuery.format("Invalid Pinterest url.")},
			tumblr: 	{remote: jQuery.format("Invalid Tumblr url.")},
			flickr: 	{remote: jQuery.format("Invalid Flickr url.")},
			myspace: 	{remote: jQuery.format("Invalid MySpace url.")}
		},
		*/		
		onkeyup: function(input, event) {			
			AC3Form.onKeyPressed($(input), event);
		},

		//focusCleanup: true, //removes the errorClass from the invalid elements and hides all error messages whenever the element is focused		

		onfocusout: function(input, event)
		{			
			// Since I'm using onfocusout I need to call .valid() myself.			
			var valid = $(input).valid();

			if ( !valid ) {
				return;
			}
			AC3Form.onFocusOut($(input), event);
		},

		submitHandler: function(form) {			
			if ( AC3Form.settings.onSubmit ) {
				// Callback before submitting the form.					
				if (typeof(AC3Form.settings.onSubmit) === "function") {     				
    				var proceed = AC3Form.settings.onSubmit($(form));
    				if ( proceed === false ) {
    					return;
    				}
				}
			}			

			if ( AC3Form.settings.ajax === true ) {		

				var $form = $(form);		

				AC3Form.$form.find(":submit").addClass('pressed');			
			    
			    //var url = AC3Form.$form.attr('action');			    
			    var url = $form.attr('action');

			    //var data = AC3Form.$form.serializeArray();
			    var data = $form.serialize();			    

			    // Add the ajax key which will tell the submit handler that this is 
			    // being executed with an ajax call.
			    data = data+"&ajax=1"; // data.push({name:'ajax', value:1}); // If using serializeArray			   

			    $.ajax({
			           type: "POST",
			           url: url,
			           data: data, // serializes the form's elements.
			           dataType: 'json',
			           success: function(data)
			           {	
			           		AC3Form.$form.find(":submit").removeClass('pressed');

			           		if ( typeof(AC3Form.settings.onSubmitAfter) == "function"  ) {
								var after = AC3Form.settings.onSubmitAfter(data);
							} 		

							if ( after === false ) {
								return;
							}

			           		if ( data.updated ) {
								AC3Form.$form.find(':submit').removeClass('pressed');
								AC3Form.update();															
			           		}
			           }

			    });
			    return false; // avoid to execute the actual submit of the form.
			} else {				
				form.submit();
			}			
			return false;		
		}
	}

	var settings = $.extend( defaults, AC3Form.settings.validationSettings );

	//$FormElement = AC3Form.$this;
	var V = AC3Form.$form.validate(settings);

	return V;	
}

function isFunction(functionToCheck) {
	var getType = {};
	return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
}

/*! jQuery Validation Plugin - v1.11.1 - 3/22/2013  https://github.com/jzaefferer/jquery-validation Copyright (c) 2013 Jörn Zaefferer; Licensed MIT */
(function(t){t.extend(t.fn,{validate:function(e){if(!this.length)return e&&e.debug&&window.console&&console.warn("Nothing selected, can't validate, returning nothing."),void 0;var i=t.data(this[0],"validator");return i?i:(this.attr("novalidate","novalidate"),i=new t.validator(e,this[0]),t.data(this[0],"validator",i),i.settings.onsubmit&&(this.validateDelegate(":submit","click",function(e){i.settings.submitHandler&&(i.submitButton=e.target),t(e.target).hasClass("cancel")&&(i.cancelSubmit=!0),void 0!==t(e.target).attr("formnovalidate")&&(i.cancelSubmit=!0)}),this.submit(function(e){function s(){var s;return i.settings.submitHandler?(i.submitButton&&(s=t("<input type='hidden'/>").attr("name",i.submitButton.name).val(t(i.submitButton).val()).appendTo(i.currentForm)),i.settings.submitHandler.call(i,i.currentForm,e),i.submitButton&&s.remove(),!1):!0}return i.settings.debug&&e.preventDefault(),i.cancelSubmit?(i.cancelSubmit=!1,s()):i.form()?i.pendingRequest?(i.formSubmitted=!0,!1):s():(i.focusInvalid(),!1)})),i)},valid:function(){if(t(this[0]).is("form"))return this.validate().form();var e=!0,i=t(this[0].form).validate();return this.each(function(){e=e&&i.element(this)}),e},removeAttrs:function(e){var i={},s=this;return t.each(e.split(/\s/),function(t,e){i[e]=s.attr(e),s.removeAttr(e)}),i},rules:function(e,i){var s=this[0];if(e){var r=t.data(s.form,"validator").settings,n=r.rules,a=t.validator.staticRules(s);switch(e){case"add":t.extend(a,t.validator.normalizeRule(i)),delete a.messages,n[s.name]=a,i.messages&&(r.messages[s.name]=t.extend(r.messages[s.name],i.messages));break;case"remove":if(!i)return delete n[s.name],a;var u={};return t.each(i.split(/\s/),function(t,e){u[e]=a[e],delete a[e]}),u}}var o=t.validator.normalizeRules(t.extend({},t.validator.classRules(s),t.validator.attributeRules(s),t.validator.dataRules(s),t.validator.staticRules(s)),s);if(o.required){var l=o.required;delete o.required,o=t.extend({required:l},o)}return o}}),t.extend(t.expr[":"],{blank:function(e){return!t.trim(""+t(e).val())},filled:function(e){return!!t.trim(""+t(e).val())},unchecked:function(e){return!t(e).prop("checked")}}),t.validator=function(e,i){this.settings=t.extend(!0,{},t.validator.defaults,e),this.currentForm=i,this.init()},t.validator.format=function(e,i){return 1===arguments.length?function(){var i=t.makeArray(arguments);return i.unshift(e),t.validator.format.apply(this,i)}:(arguments.length>2&&i.constructor!==Array&&(i=t.makeArray(arguments).slice(1)),i.constructor!==Array&&(i=[i]),t.each(i,function(t,i){e=e.replace(RegExp("\\{"+t+"\\}","g"),function(){return i})}),e)},t.extend(t.validator,{defaults:{messages:{},groups:{},rules:{},errorClass:"error",validClass:"valid",errorElement:"label",focusInvalid:!0,errorContainer:t([]),errorLabelContainer:t([]),onsubmit:!0,ignore:":hidden",ignoreTitle:!1,onfocusin:function(t){this.lastActive=t,this.settings.focusCleanup&&!this.blockFocusCleanup&&(this.settings.unhighlight&&this.settings.unhighlight.call(this,t,this.settings.errorClass,this.settings.validClass),this.addWrapper(this.errorsFor(t)).hide())},onfocusout:function(t){this.checkable(t)||!(t.name in this.submitted)&&this.optional(t)||this.element(t)},onkeyup:function(t,e){(9!==e.which||""!==this.elementValue(t))&&(t.name in this.submitted||t===this.lastElement)&&this.element(t)},onclick:function(t){t.name in this.submitted?this.element(t):t.parentNode.name in this.submitted&&this.element(t.parentNode)},highlight:function(e,i,s){"radio"===e.type?this.findByName(e.name).addClass(i).removeClass(s):t(e).addClass(i).removeClass(s)},unhighlight:function(e,i,s){"radio"===e.type?this.findByName(e.name).removeClass(i).addClass(s):t(e).removeClass(i).addClass(s)}},setDefaults:function(e){t.extend(t.validator.defaults,e)},messages:{required:"This field is required.",remote:"Please fix this field.",email:"Please enter a valid email address.",url:"Please enter a valid URL.",date:"Please enter a valid date.",dateISO:"Please enter a valid date (ISO).",number:"Please enter a valid number.",digits:"Please enter only digits.",creditcard:"Please enter a valid credit card number.",equalTo:"Please enter the same value again.",maxlength:t.validator.format("Please enter no more than {0} characters."),minlength:t.validator.format("Please enter at least {0} characters."),rangelength:t.validator.format("Please enter a value between {0} and {1} characters long."),range:t.validator.format("Please enter a value between {0} and {1}."),max:t.validator.format("Please enter a value less than or equal to {0}."),min:t.validator.format("Please enter a value greater than or equal to {0}.")},autoCreateRanges:!1,prototype:{init:function(){function e(e){var i=t.data(this[0].form,"validator"),s="on"+e.type.replace(/^validate/,"");i.settings[s]&&i.settings[s].call(i,this[0],e)}this.labelContainer=t(this.settings.errorLabelContainer),this.errorContext=this.labelContainer.length&&this.labelContainer||t(this.currentForm),this.containers=t(this.settings.errorContainer).add(this.settings.errorLabelContainer),this.submitted={},this.valueCache={},this.pendingRequest=0,this.pending={},this.invalid={},this.reset();var i=this.groups={};t.each(this.settings.groups,function(e,s){"string"==typeof s&&(s=s.split(/\s/)),t.each(s,function(t,s){i[s]=e})});var s=this.settings.rules;t.each(s,function(e,i){s[e]=t.validator.normalizeRule(i)}),t(this.currentForm).validateDelegate(":text, [type='password'], [type='file'], select, textarea, [type='number'], [type='search'] ,[type='tel'], [type='url'], [type='email'], [type='datetime'], [type='date'], [type='month'], [type='week'], [type='time'], [type='datetime-local'], [type='range'], [type='color'] ","focusin focusout keyup",e).validateDelegate("[type='radio'], [type='checkbox'], select, option","click",e),this.settings.invalidHandler&&t(this.currentForm).bind("invalid-form.validate",this.settings.invalidHandler)},form:function(){return this.checkForm(),t.extend(this.submitted,this.errorMap),this.invalid=t.extend({},this.errorMap),this.valid()||t(this.currentForm).triggerHandler("invalid-form",[this]),this.showErrors(),this.valid()},checkForm:function(){this.prepareForm();for(var t=0,e=this.currentElements=this.elements();e[t];t++)this.check(e[t]);return this.valid()},element:function(e){e=this.validationTargetFor(this.clean(e)),this.lastElement=e,this.prepareElement(e),this.currentElements=t(e);var i=this.check(e)!==!1;return i?delete this.invalid[e.name]:this.invalid[e.name]=!0,this.numberOfInvalids()||(this.toHide=this.toHide.add(this.containers)),this.showErrors(),i},showErrors:function(e){if(e){t.extend(this.errorMap,e),this.errorList=[];for(var i in e)this.errorList.push({message:e[i],element:this.findByName(i)[0]});this.successList=t.grep(this.successList,function(t){return!(t.name in e)})}this.settings.showErrors?this.settings.showErrors.call(this,this.errorMap,this.errorList):this.defaultShowErrors()},resetForm:function(){t.fn.resetForm&&t(this.currentForm).resetForm(),this.submitted={},this.lastElement=null,this.prepareForm(),this.hideErrors(),this.elements().removeClass(this.settings.errorClass).removeData("previousValue")},numberOfInvalids:function(){return this.objectLength(this.invalid)},objectLength:function(t){var e=0;for(var i in t)e++;return e},hideErrors:function(){this.addWrapper(this.toHide).hide()},valid:function(){return 0===this.size()},size:function(){return this.errorList.length},focusInvalid:function(){if(this.settings.focusInvalid)try{t(this.findLastActive()||this.errorList.length&&this.errorList[0].element||[]).filter(":visible").focus().trigger("focusin")}catch(e){}},findLastActive:function(){var e=this.lastActive;return e&&1===t.grep(this.errorList,function(t){return t.element.name===e.name}).length&&e},elements:function(){var e=this,i={};return t(this.currentForm).find("input, select, textarea").not(":submit, :reset, :image, [disabled]").not(this.settings.ignore).filter(function(){return!this.name&&e.settings.debug&&window.console&&console.error("%o has no name assigned",this),this.name in i||!e.objectLength(t(this).rules())?!1:(i[this.name]=!0,!0)})},clean:function(e){return t(e)[0]},errors:function(){var e=this.settings.errorClass.replace(" ",".");return t(this.settings.errorElement+"."+e,this.errorContext)},reset:function(){this.successList=[],this.errorList=[],this.errorMap={},this.toShow=t([]),this.toHide=t([]),this.currentElements=t([])},prepareForm:function(){this.reset(),this.toHide=this.errors().add(this.containers)},prepareElement:function(t){this.reset(),this.toHide=this.errorsFor(t)},elementValue:function(e){var i=t(e).attr("type"),s=t(e).val();return"radio"===i||"checkbox"===i?t("input[name='"+t(e).attr("name")+"']:checked").val():"string"==typeof s?s.replace(/\r/g,""):s},check:function(e){e=this.validationTargetFor(this.clean(e));var i,s=t(e).rules(),r=!1,n=this.elementValue(e);for(var a in s){var u={method:a,parameters:s[a]};try{if(i=t.validator.methods[a].call(this,n,e,u.parameters),"dependency-mismatch"===i){r=!0;continue}if(r=!1,"pending"===i)return this.toHide=this.toHide.not(this.errorsFor(e)),void 0;if(!i)return this.formatAndAdd(e,u),!1}catch(o){throw this.settings.debug&&window.console&&console.log("Exception occurred when checking element "+e.id+", check the '"+u.method+"' method.",o),o}}return r?void 0:(this.objectLength(s)&&this.successList.push(e),!0)},customDataMessage:function(e,i){return t(e).data("msg-"+i.toLowerCase())||e.attributes&&t(e).attr("data-msg-"+i.toLowerCase())},customMessage:function(t,e){var i=this.settings.messages[t];return i&&(i.constructor===String?i:i[e])},findDefined:function(){for(var t=0;arguments.length>t;t++)if(void 0!==arguments[t])return arguments[t];return void 0},defaultMessage:function(e,i){return this.findDefined(this.customMessage(e.name,i),this.customDataMessage(e,i),!this.settings.ignoreTitle&&e.title||void 0,t.validator.messages[i],"<strong>Warning: No message defined for "+e.name+"</strong>")},formatAndAdd:function(e,i){var s=this.defaultMessage(e,i.method),r=/\$?\{(\d+)\}/g;"function"==typeof s?s=s.call(this,i.parameters,e):r.test(s)&&(s=t.validator.format(s.replace(r,"{$1}"),i.parameters)),this.errorList.push({message:s,element:e}),this.errorMap[e.name]=s,this.submitted[e.name]=s},addWrapper:function(t){return this.settings.wrapper&&(t=t.add(t.parent(this.settings.wrapper))),t},defaultShowErrors:function(){var t,e;for(t=0;this.errorList[t];t++){var i=this.errorList[t];this.settings.highlight&&this.settings.highlight.call(this,i.element,this.settings.errorClass,this.settings.validClass),this.showLabel(i.element,i.message)}if(this.errorList.length&&(this.toShow=this.toShow.add(this.containers)),this.settings.success)for(t=0;this.successList[t];t++)this.showLabel(this.successList[t]);if(this.settings.unhighlight)for(t=0,e=this.validElements();e[t];t++)this.settings.unhighlight.call(this,e[t],this.settings.errorClass,this.settings.validClass);this.toHide=this.toHide.not(this.toShow),this.hideErrors(),this.addWrapper(this.toShow).show()},validElements:function(){return this.currentElements.not(this.invalidElements())},invalidElements:function(){return t(this.errorList).map(function(){return this.element})},showLabel:function(e,i){var s=this.errorsFor(e);s.length?(s.removeClass(this.settings.validClass).addClass(this.settings.errorClass),s.html(i)):(s=t("<"+this.settings.errorElement+">").attr("for",this.idOrName(e)).addClass(this.settings.errorClass).html(i||""),this.settings.wrapper&&(s=s.hide().show().wrap("<"+this.settings.wrapper+"/>").parent()),this.labelContainer.append(s).length||(this.settings.errorPlacement?this.settings.errorPlacement(s,t(e)):s.insertAfter(e))),!i&&this.settings.success&&(s.text(""),"string"==typeof this.settings.success?s.addClass(this.settings.success):this.settings.success(s,e)),this.toShow=this.toShow.add(s)},errorsFor:function(e){var i=this.idOrName(e);return this.errors().filter(function(){return t(this).attr("for")===i})},idOrName:function(t){return this.groups[t.name]||(this.checkable(t)?t.name:t.id||t.name)},validationTargetFor:function(t){return this.checkable(t)&&(t=this.findByName(t.name).not(this.settings.ignore)[0]),t},checkable:function(t){return/radio|checkbox/i.test(t.type)},findByName:function(e){return t(this.currentForm).find("[name='"+e+"']")},getLength:function(e,i){switch(i.nodeName.toLowerCase()){case"select":return t("option:selected",i).length;case"input":if(this.checkable(i))return this.findByName(i.name).filter(":checked").length}return e.length},depend:function(t,e){return this.dependTypes[typeof t]?this.dependTypes[typeof t](t,e):!0},dependTypes:{"boolean":function(t){return t},string:function(e,i){return!!t(e,i.form).length},"function":function(t,e){return t(e)}},optional:function(e){var i=this.elementValue(e);return!t.validator.methods.required.call(this,i,e)&&"dependency-mismatch"},startRequest:function(t){this.pending[t.name]||(this.pendingRequest++,this.pending[t.name]=!0)},stopRequest:function(e,i){this.pendingRequest--,0>this.pendingRequest&&(this.pendingRequest=0),delete this.pending[e.name],i&&0===this.pendingRequest&&this.formSubmitted&&this.form()?(t(this.currentForm).submit(),this.formSubmitted=!1):!i&&0===this.pendingRequest&&this.formSubmitted&&(t(this.currentForm).triggerHandler("invalid-form",[this]),this.formSubmitted=!1)},previousValue:function(e){return t.data(e,"previousValue")||t.data(e,"previousValue",{old:null,valid:!0,message:this.defaultMessage(e,"remote")})}},classRuleSettings:{required:{required:!0},email:{email:!0},url:{url:!0},date:{date:!0},dateISO:{dateISO:!0},number:{number:!0},digits:{digits:!0},creditcard:{creditcard:!0}},addClassRules:function(e,i){e.constructor===String?this.classRuleSettings[e]=i:t.extend(this.classRuleSettings,e)},classRules:function(e){var i={},s=t(e).attr("class");return s&&t.each(s.split(" "),function(){this in t.validator.classRuleSettings&&t.extend(i,t.validator.classRuleSettings[this])}),i},attributeRules:function(e){var i={},s=t(e),r=s[0].getAttribute("type");for(var n in t.validator.methods){var a;"required"===n?(a=s.get(0).getAttribute(n),""===a&&(a=!0),a=!!a):a=s.attr(n),/min|max/.test(n)&&(null===r||/number|range|text/.test(r))&&(a=Number(a)),a?i[n]=a:r===n&&"range"!==r&&(i[n]=!0)}return i.maxlength&&/-1|2147483647|524288/.test(i.maxlength)&&delete i.maxlength,i},dataRules:function(e){var i,s,r={},n=t(e);for(i in t.validator.methods)s=n.data("rule-"+i.toLowerCase()),void 0!==s&&(r[i]=s);return r},staticRules:function(e){var i={},s=t.data(e.form,"validator");return s.settings.rules&&(i=t.validator.normalizeRule(s.settings.rules[e.name])||{}),i},normalizeRules:function(e,i){return t.each(e,function(s,r){if(r===!1)return delete e[s],void 0;if(r.param||r.depends){var n=!0;switch(typeof r.depends){case"string":n=!!t(r.depends,i.form).length;break;case"function":n=r.depends.call(i,i)}n?e[s]=void 0!==r.param?r.param:!0:delete e[s]}}),t.each(e,function(s,r){e[s]=t.isFunction(r)?r(i):r}),t.each(["minlength","maxlength"],function(){e[this]&&(e[this]=Number(e[this]))}),t.each(["rangelength","range"],function(){var i;e[this]&&(t.isArray(e[this])?e[this]=[Number(e[this][0]),Number(e[this][1])]:"string"==typeof e[this]&&(i=e[this].split(/[\s,]+/),e[this]=[Number(i[0]),Number(i[1])]))}),t.validator.autoCreateRanges&&(e.min&&e.max&&(e.range=[e.min,e.max],delete e.min,delete e.max),e.minlength&&e.maxlength&&(e.rangelength=[e.minlength,e.maxlength],delete e.minlength,delete e.maxlength)),e},normalizeRule:function(e){if("string"==typeof e){var i={};t.each(e.split(/\s/),function(){i[this]=!0}),e=i}return e},addMethod:function(e,i,s){t.validator.methods[e]=i,t.validator.messages[e]=void 0!==s?s:t.validator.messages[e],3>i.length&&t.validator.addClassRules(e,t.validator.normalizeRule(e))},methods:{required:function(e,i,s){if(!this.depend(s,i))return"dependency-mismatch";if("select"===i.nodeName.toLowerCase()){var r=t(i).val();return r&&r.length>0}return this.checkable(i)?this.getLength(e,i)>0:t.trim(e).length>0},email:function(t,e){return this.optional(e)||/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i.test(t)},url:function(t,e){return this.optional(e)||/^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(t)},date:function(t,e){return this.optional(e)||!/Invalid|NaN/.test(""+new Date(t))},dateISO:function(t,e){return this.optional(e)||/^\d{4}[\/\-]\d{1,2}[\/\-]\d{1,2}$/.test(t)},number:function(t,e){return this.optional(e)||/^-?(?:\d+|\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test(t)},digits:function(t,e){return this.optional(e)||/^\d+$/.test(t)},creditcard:function(t,e){if(this.optional(e))return"dependency-mismatch";if(/[^0-9 \-]+/.test(t))return!1;var i=0,s=0,r=!1;t=t.replace(/\D/g,"");for(var n=t.length-1;n>=0;n--){var a=t.charAt(n);s=parseInt(a,10),r&&(s*=2)>9&&(s-=9),i+=s,r=!r}return 0===i%10},minlength:function(e,i,s){var r=t.isArray(e)?e.length:this.getLength(t.trim(e),i);return this.optional(i)||r>=s},maxlength:function(e,i,s){var r=t.isArray(e)?e.length:this.getLength(t.trim(e),i);return this.optional(i)||s>=r},rangelength:function(e,i,s){var r=t.isArray(e)?e.length:this.getLength(t.trim(e),i);return this.optional(i)||r>=s[0]&&s[1]>=r},min:function(t,e,i){return this.optional(e)||t>=i},max:function(t,e,i){return this.optional(e)||i>=t},range:function(t,e,i){return this.optional(e)||t>=i[0]&&i[1]>=t},equalTo:function(e,i,s){var r=t(s);return this.settings.onfocusout&&r.unbind(".validate-equalTo").bind("blur.validate-equalTo",function(){t(i).valid()}),e===r.val()},remote:function(e,i,s){if(this.optional(i))return"dependency-mismatch";var r=this.previousValue(i);if(this.settings.messages[i.name]||(this.settings.messages[i.name]={}),r.originalMessage=this.settings.messages[i.name].remote,this.settings.messages[i.name].remote=r.message,s="string"==typeof s&&{url:s}||s,r.old===e)return r.valid;r.old=e;var n=this;this.startRequest(i);var a={};return a[i.name]=e,t.ajax(t.extend(!0,{url:s,mode:"abort",port:"validate"+i.name,dataType:"json",data:a,success:function(s){n.settings.messages[i.name].remote=r.originalMessage;var a=s===!0||"true"===s;if(a){var u=n.formSubmitted;n.prepareElement(i),n.formSubmitted=u,n.successList.push(i),delete n.invalid[i.name],n.showErrors()}else{var o={},l=s||n.defaultMessage(i,"remote");o[i.name]=r.message=t.isFunction(l)?l(e):l,n.invalid[i.name]=!0,n.showErrors(o)}r.valid=a,n.stopRequest(i,a)}},s)),"pending"}}}),t.format=t.validator.format})(jQuery),function(t){var e={};if(t.ajaxPrefilter)t.ajaxPrefilter(function(t,i,s){var r=t.port;"abort"===t.mode&&(e[r]&&e[r].abort(),e[r]=s)});else{var i=t.ajax;t.ajax=function(s){var r=("mode"in s?s:t.ajaxSettings).mode,n=("port"in s?s:t.ajaxSettings).port;return"abort"===r?(e[n]&&e[n].abort(),e[n]=i.apply(this,arguments),e[n]):i.apply(this,arguments)}}}(jQuery),function(t){t.extend(t.fn,{validateDelegate:function(e,i,s){return this.bind(i,function(i){var r=t(i.target);return r.is(e)?s.apply(r,arguments):void 0})}})}(jQuery);

jQuery.validator.addMethod("url2", function(value, element, param) {
	return this.optional(element) || /^(www\.)(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)*(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(value);
}, "Please enter a valid URL");

jQuery.validator.addMethod("facebook", function(value, element) {
	return this.optional(element) || /^(https?:\/\/)?((w{3}\.)?)facebook.com\/?.*/i.test(value);
}, "Must be a Facebook URL");
jQuery.validator.addMethod("twitter", function(value, element) {
	return this.optional(element) || /^(https?:\/\/)?((w{3}\.)?)twitter\.com\/(#!\/)?[a-z0-9_]+$/i.test(value);
}, "Must be a Twitter URL");
jQuery.validator.addMethod("googleplus", function(value, element) {
	return this.optional(element) || /^(https?:\/\/)?((w{3}\.)?)plus.google.com\/?.*/i.test(value);
}, "Must be a Google Plus URL");
jQuery.validator.addMethod("youtube", function(value, element) {
	return this.optional(element) || /^(https?:\/\/)?((w{3}\.)?)youtube.com\/?.*/i.test(value);
}, "Must be a Youtube URL.  ex: http://www.youtube.com");
jQuery.validator.addMethod("vimeo", function(value, element) {
	return this.optional(element) || /^(https?:\/\/)?((w{3}\.)?)vimeo.com\/?.*/i.test(value);
}, "Must be a Vimeo URL");
jQuery.validator.addMethod("instagram", function(value, element) {
	return this.optional(element) || /^(https?:\/\/)?((w{3}\.)?)instagram.com\/?.*/i.test(value);
}, "Must be an Instagram URL");
jQuery.validator.addMethod("pinterest", function(value, element) {
	return this.optional(element) || /^(https?:\/\/)?((w{3}\.)?)pinterest.com\/?.*/i.test(value);
}, "Must be a Pinterest URL");
jQuery.validator.addMethod("tumblr", function(value, element) {
	return this.optional(element) || /^(https?:\/\/)?((w{3}\.)?)tumblr.com\/?.*/i.test(value);
}, "Must be a Tumblr URL");
jQuery.validator.addMethod("flickr", function(value, element) {
	return this.optional(element) || /^(https?:\/\/)?((w{3}\.)?)flickr.com\/?.*/i.test(value);
}, "Must be a Flickr URL");
jQuery.validator.addMethod("myspace", function(value, element) {
	return this.optional(element) || /^(https?:\/\/)?((w{3}\.)?)myspace.com\/?.*/i.test(value);
}, "Must be a MySpace URL");

jQuery.validator.addMethod("valueNotEquals", function(value, element, arg) {
	return arg != value;
}, "Value must not equal arg.");


jQuery.validator.addMethod("zipcodeUS", function(value, element) {
	return this.optional(element) || /\d{5}-\d{4}$|^\d{5}$/.test(value);
}, "The specified US ZIP Code is invalid");
jQuery.validator.addMethod("phoneUS", function(phone_number, element) {
	phone_number = phone_number.replace(/\s+/g, "");
	return this.optional(element) || phone_number.length > 9 &&
		phone_number.match(/^(\+?1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/);
}, "Please specify a valid phone number");

/*
jQuery.validator.addMethod("full_url", function(val, elem) {
    // if no url, don't do anything
    if (val.length == 0) { return true; }
    // if user has not entered http:// https:// or ftp:// assume they mean http://
    if(!/^(https?|ftp):\/\//i.test(val)) {
        val = 'http://'+val; // set both the value
        $(elem).val(val); // also update the form element

    }
 return /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(val);    
 // return /^(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test(val);
}, "Must be a valid URL");
*/

jQuery.validator.addMethod("httpCheck", function(value, element, arg) {
	return /^(https?:\/\/)/i.test(value);
}, "Must begin with http:// or https://");

/********************************************************************
* Limit the characters that may be entered in a text field
* Common options: alphanumeric, alphabetic or numeric
* Kevin Sheedy, 2012
* http://github.com/KevinSheedy/jquery.alphanum
*********************************************************************/
(function(e){function l(){var e="!@#$%^&*()+=[]\\';,/{}|\":<>?~`.-_";e+=" ";return e}function c(){var e="¬"+"€"+"£"+"¦";return e}function h(t,n,r){t.each(function(){var t=e(this);t.bind("keyup change paste",function(e){var i="";if(e.originalEvent&&e.originalEvent.clipboardData&&e.originalEvent.clipboardData.getData)i=e.originalEvent.clipboardData.getData("text/plain");setTimeout(function(){m(t,n,r,i)},0)});t.bind("keypress",function(e){var i=!e.charCode?e.which:e.charCode;if(v(i)||e.ctrlKey||e.metaKey)return;var s=String.fromCharCode(i);var o=t.selection();var u=o.start;var a=o.end;var f=t.val();var l=f.substring(0,u)+s+f.substring(a);var c=n(l,r);if(c!=l)e.preventDefault()})})}function p(t,n){var r=parseFloat(e(t).val());var i=e(t);if(isNaN(r)){i.val("");return}if(d(n.min)&&r<n.min)i.val("");if(d(n.max)&&r>n.max)i.val("")}function d(e){return!isNaN(e)}function v(e){if(e>=32)return false;if(e==10)return false;if(e==13)return false;return true}function m(e,t,n,r){var i=e.val();if(i==""&&r.length>0)i=r;var s=t(i,n);if(i==s)return;var o=e.alphanum_caret();e.val(s);if(i.length==s.length+1)e.alphanum_caret(o-1);else e.alphanum_caret(o)}function g(n,i){if(typeof i=="undefined")i=t;var s,o={};if(typeof n==="string")s=r[n];else if(typeof n=="undefined")s={};else s=n;e.extend(o,i,s);if(typeof o.blacklist=="undefined")o.blacklistSet=P(o.allow,o.disallow);return o}function y(t){var r,s={};if(typeof t==="string")r=i[t];else if(typeof t=="undefined")r={};else r=t;e.extend(s,n,r);return s}function b(e,t,n){if(n.maxLength&&e.length>=n.maxLength)return false;if(n.allow.indexOf(t)>=0)return true;if(n.allowSpace&&t==" ")return true;if(n.blacklistSet.contains(t))return false;if(!n.allowNumeric&&a[t])return false;if(!n.allowUpper&&M(t))return false;if(!n.allowLower&&_(t))return false;if(!n.allowCaseless&&D(t))return false;if(!n.allowLatin&&f.contains(t))return false;if(!n.allowOtherCharSets){if(a[t]||f.contains(t))return true;else return false}return true}function w(e,t,n){if(a[t]){if(S(e,n))return false;if(T(e,n))return false;if(x(e,n))return false;if(N(e+t,n))return false;if(C(e+t,n))return false;return true}if(n.allowPlus&&t=="+"&&e=="")return true;if(n.allowMinus&&t=="-"&&e=="")return true;if(t==o&&n.allowThouSep&&j(e,t))return true;if(t==u){if(e.indexOf(u)>=0)return false;if(n.allowDecSep)return true}return false}function E(e){e=e+"";return e.replace(/[^0-9]/g,"").length}function S(e,t){var n=t.maxDigits;if(n==""||isNaN(n))return false;var r=E(e);if(r>=n)return true;return false}function x(e,t){var n=t.maxDecimalPlaces;if(n==""||isNaN(n))return false;var r=e.indexOf(u);if(r==-1)return false;var i=e.substring(r);var s=E(i);if(s>=n)return true;return false}function T(e,t){var n=t.maxPreDecimalPlaces;if(n==""||isNaN(n))return false;var r=e.indexOf(u);if(r>=0)return false;var i=E(e);if(i>=n)return true;return false}function N(e,t){if(!t.max||t.max<0)return false;var n=parseFloat(e);if(n>t.max)return true;return false}function C(e,t){if(!t.min||t.min>0)return false;var n=parseFloat(e);if(n<t.min)return true;return false}function k(e,t){if(typeof e!="string")return e;var n=e.split("");var r=[];var i=0;var s;for(i=0;i<n.length;i++){s=n[i];var o=r.join("");if(b(o,s,t))r.push(s)}var u=r.join("");if(t.forceLower)u=u.toLowerCase();else if(t.forceUpper)u=u.toUpperCase();return u}function L(e,t){if(typeof e!="string")return e;var n=e.split("");var r=[];var i=0;var s;for(i=0;i<n.length;i++){s=n[i];var o=r.join("");if(w(o,s,t))r.push(s)}return r.join("")}function A(e){var t=e.split("");var n=0;var r=[];var i;for(n=0;n<t.length;n++){i=t[n]}}function O(e){}function M(e){var t=e.toUpperCase();var n=e.toLowerCase();if(e==t&&t!=n)return true;else return false}function _(e){var t=e.toUpperCase();var n=e.toLowerCase();if(e==n&&t!=n)return true;else return false}function D(e){if(e.toUpperCase()==e.toLowerCase())return true;else return false}function P(e,t){var n=new F(s+t);var r=new F(e);var i=n.subtract(r);return i}function H(){var e="0123456789".split("");var t={};var n=0;var r;for(n=0;n<e.length;n++){r=e[n];t[r]=true}return t}function B(){var e="abcdefghijklmnopqrstuvwxyz";var t=e.toUpperCase();var n=new F(e+t);return n}function j(e,t){if(e.length==0)return false;var n=e.indexOf(u);if(n>=0)return false;var r=e.indexOf(o);if(r<0)return true;var i=e.lastIndexOf(o);var s=e.length-i-1;if(s<3)return false;var a=E(e.substring(r));if(a%3>0)return false;return true}function F(e){if(typeof e=="string")this.map=I(e);else this.map={}}function I(e){var t={};var n=e.split("");var r=0;var i;for(r=0;r<n.length;r++){i=n[r];t[i]=true}return t}e.fn.alphanum=function(e){var t=g(e);var n=this;h(n,k,t);return this};e.fn.alpha=function(e){var t=g("alpha");var n=g(e,t);var r=this;h(r,k,n);return this};e.fn.numeric=function(e){var t=y(e);var n=this;h(n,L,t);n.blur(function(){p(this,e)});return this};var t={allow:"",disallow:"",allowSpace:true,allowNumeric:true,allowUpper:true,allowLower:true,allowCaseless:true,allowLatin:true,allowOtherCharSets:true,forceUpper:false,forceLower:false,maxLength:NaN};var n={allowPlus:false,allowMinus:true,allowThouSep:true,allowDecSep:true,allowLeadingSpaces:false,maxDigits:NaN,maxDecimalPlaces:NaN,maxPreDecimalPlaces:NaN,max:NaN,min:NaN};var r={alpha:{allowNumeric:false},upper:{allowNumeric:false,allowUpper:true,allowLower:false,allowCaseless:true},lower:{allowNumeric:false,allowUpper:false,allowLower:true,allowCaseless:true}};var i={integer:{allowPlus:false,allowMinus:true,allowThouSep:false,allowDecSep:false},positiveInteger:{allowPlus:false,allowMinus:false,allowThouSep:false,allowDecSep:false}};var s=l()+c();var o=",";var u=".";var a=H();var f=B();F.prototype.add=function(e){var t=this.clone();for(var n in e.map)t.map[n]=true;return t};F.prototype.subtract=function(e){var t=this.clone();for(var n in e.map)delete t.map[n];return t};F.prototype.contains=function(e){if(this.map[e])return true;else return false};F.prototype.clone=function(){var e=new F;for(var t in this.map)e.map[t]=true;return e};e.fn.alphanum.backdoorAlphaNum=function(e,t){var n=g(t);return k(e,n)};e.fn.alphanum.backdoorNumeric=function(e,t){var n=y(t);return L(e,n)};e.fn.alphanum.setNumericSeparators=function(e){if(e.thousandsSeparator.length!=1)return;if(e.decimalSeparator.length!=1)return;o=e.thousandsSeparator;u=e.decimalSeparator}})(jQuery);(function(e){function t(e,t){if(e.createTextRange){var n=e.createTextRange();n.move("character",t);n.select()}else if(e.selectionStart!=null){e.focus();e.setSelectionRange(t,t)}}function n(e){if("selection"in document){var t=e.createTextRange();try{t.setEndPoint("EndToStart",document.selection.createRange())}catch(n){return 0}return t.text.length}else if(e.selectionStart!=null){return e.selectionStart}}e.fn.alphanum_caret=function(r,i){if(typeof r==="undefined"){return n(this.get(0))}return this.queue(function(n){if(isNaN(r)){var s=e(this).val().indexOf(r);if(i===true){s+=r.length}else if(typeof i!=="undefined"){s+=i}t(this,s)}else{t(this,r)}n()})}})(jQuery);(function(e){var t=function(e){return e.replace(/([a-z])([a-z]+)/gi,function(e,t,n){return t+n.toLowerCase()}).replace(/_/g,"")},n=function(e){return e.replace(/^([a-z]+)_TO_([a-z]+)/i,function(e,t,n){return n+"_TO_"+t})},r=function(e){return e?e.ownerDocument.defaultView||e.ownerDocument.parentWindow:window},i=function(t,n){var r=e.Range.current(t).clone(),i=e.Range(t).select(t);if(!r.overlaps(i)){return null}if(r.compare("START_TO_START",i)<1){startPos=0;r.move("START_TO_START",i)}else{fromElementToCurrent=i.clone();fromElementToCurrent.move("END_TO_START",r);startPos=fromElementToCurrent.toString().length}if(r.compare("END_TO_END",i)>=0){endPos=i.toString().length}else{endPos=startPos+r.toString().length}return{start:startPos,end:endPos}},s=function(t){var n=r(t);if(t.selectionStart!==undefined){if(document.activeElement&&document.activeElement!=t&&t.selectionStart==t.selectionEnd&&t.selectionStart==0){return{start:t.value.length,end:t.value.length}}return{start:t.selectionStart,end:t.selectionEnd}}else if(n.getSelection){return i(t,n)}else{try{if(t.nodeName.toLowerCase()=="input"){var s=r(t).document.selection.createRange(),o=t.createTextRange();o.setEndPoint("EndToStart",s);var u=o.text.length;return{start:u,end:u+s.text.length}}else{var a=i(t,n);if(!a){return a}var f=e.Range.current().clone(),l=f.clone().collapse().range,c=f.clone().collapse(false).range;l.moveStart("character",-1);c.moveStart("character",-1);if(a.startPos!=0&&l.text==""){a.startPos+=2}if(a.endPos!=0&&c.text==""){a.endPos+=2}return a}}catch(h){return{start:t.value.length,end:t.value.length}}}},o=function(e,t,n){var i=r(e);if(e.setSelectionRange){if(n===undefined){e.focus();e.setSelectionRange(t,t)}else{e.select();e.selectionStart=t;e.selectionEnd=n}}else if(e.createTextRange){var s=e.createTextRange();s.moveStart("character",t);n=n||t;s.moveEnd("character",n-e.value.length);s.select()}else if(i.getSelection){var o=i.document,u=i.getSelection(),f=o.createRange(),l=[t,n!==undefined?n:t];a([e],l);f.setStart(l[0].el,l[0].count);f.setEnd(l[1].el,l[1].count);u.removeAllRanges();u.addRange(f)}else if(i.document.body.createTextRange){var f=document.body.createTextRange();f.moveToElementText(e);f.collapse();f.moveStart("character",t);f.moveEnd("character",n!==undefined?n:t);f.select()}},u=function(e,t,n,r){if(typeof n[0]==="number"&&n[0]<t){n[0]={el:r,count:n[0]-e}}if(typeof n[1]==="number"&&n[1]<=t){n[1]={el:r,count:n[1]-e}}},a=function(e,t,n){var r,i;n=n||0;for(var s=0;e[s];s++){r=e[s];if(r.nodeType===3||r.nodeType===4){i=n;n+=r.nodeValue.length;u(i,n,t,r)}else if(r.nodeType!==8){n=a(r.childNodes,t,n)}}return n};jQuery.fn.selection=function(e,t){if(e!==undefined){return this.each(function(){o(this,e,t)})}else{return s(this[0])}};e.fn.selection.getCharElement=a})(jQuery)