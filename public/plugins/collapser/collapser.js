// Requires global.js

var collapser_default_settings =
{
  openSpeed: 200,
  beforeExpand: false,
  afterExpand: false,
  beforeCollapse: false,
  afterCollapse: false,
  afterChange: false
};

$.fn.Collapser = function( options, init_callback )
{
	var objects = [], return_data;

	this.each(function()
	{
		// initialize the object here (add methods, event bindings, etc)
    	if( $(this).data('object') === undefined )
    	{
  		// ---------------------
		//   SETUP
		// ---------------------
      		//var self = this;
      		var object = {};

      		object.initialized = false;
      		object.$this = $(this);
      		object.settings = $.extend(true, {}, collapser_default_settings, options);

      		object.$content = object.$this.find('.content');
      		object.$widget = object.$this.find('.header .widget');
      		object.$view = object.$this.find('.header .view');

      	// NOTE: init happens at the bottom so that it has access to the methods.

  		// ---------------------
		//   PUBLIC METHODS
		// ---------------------
			// returns the DOM element as a jquery object
			object.get = function() {
				return this.$this;
			}

			object.expand = function() {
				if ( object.settings.beforeExpand ) {
	            	object.settings.beforeExpand(object.$this);
	            }

				if ( Browser.isMobile() ) {
	                object.$content.css('height', 'auto');
	                if ( object.settings.afterExpand ) {
		            	object.settings.afterExpand(object.$this);
		            }
		            if ( object.settings.afterChange ) {
		            	object.settings.afterChange(object.$this);
		            }
	            } else {
	                Global.animate_height_auto(object.$content, object.settings.openSpeed, true, function() {
	                	if ( object.settings.afterExpand ) {
			            	object.settings.afterExpand(object.$this);
			            }
			            if ( object.settings.afterChange ) {
			            	object.settings.afterChange(object.$this);
			            }
	                });
	            }
	            object.$this.addClass('open');
			}

			object.collapse = function() {

				if ( object.settings.beforeCollapse ) {
					var before = object.settings.beforeCollapse(object.$this);
					if ( before === false ) {
						return;
					}
				}

				object.$this.removeClass('open');

				if ( Browser.isMobile() ) {
	                object.$content.css('height', '0px');
	                if ( object.settings.afterCollapse ) {
		            	object.settings.afterCollapse(object.$this);
		            }
	                if ( object.settings.afterChange ) {
		            	object.settings.afterChange(object.$this);
		            }
	            } else {
	                object.$content.animate({'height':'0px'}, object.settings.openSpeed, function() {
	                	if ( object.settings.afterCollapse ) {
			            	object.settings.afterCollapse(object.$this);
			            }
			            if ( object.settings.afterChange ) {
			            	object.settings.afterChange(object.$this);
			            }
	                });
	            }
			}

			object.toggle = function() {
				if ( object.$this.hasClass('open') ) {
					object.collapse();
				} else {
					object.expand();
				}
			}

  		// ---------------------
		//  "PRIVATE" METHODS
		// ---------------------
			object._hello = function() {
				return "hello";
			}

		// ----------
		//   INIT
		// ----------
			object.initialize = function() {
				if ( init_callback ) {
					init_callback();
				}

				object.$widget.on('click', function(e) {
		        	object.toggle();
		        });

		        object.$view.on('click', function(e) {
		            // If there's a link then don't expand/collapse.
		        	if(e.target.nodeName == 'A') return;
		        	object.toggle();
		        });

				object.initialized = true;
			}

			object.initialize();

			// Define the object within the object
			object.$this.data('object', object);
		} else {
			object = $(this).data('object');
		}
    	objects.push(object);
  	}); // End of instance

  	// Setting up the plugin.  Don't do anything below this line.
  	if( objects.length === 1 ) {
    	return_data = objects[0];
  	} else {
    	return_data = objects;
  	}
  	return_data.all = function(callback) {
    	$.each( objects, function(){
      		callback.apply(this);
    	});
  	};
  	return return_data;
};

$(document).ready(function() {
	$('.collapser.autoload').Collapser();
});


// Requires global.js

var collapser_sizer_default_settings =
{
	beforeExpand: false,
	afterExpand: false,
	beforeCollapse: false,
	afterCollapse: false,
	afterChange: false
};

$.fn.CollapserSizers = function( options, init_callback )
{
	var objects = [], return_data;

	this.each(function()
	{
		// initialize the object here (add methods, event bindings, etc)
    	if( $(this).data('object') === undefined )
    	{
  		// ---------------------
		//   SETUP
		// ---------------------
      		//var self = this;
      		var object = {};
      		var namespace = 'collapsersizer';


      		object.initialized = false;
      		object.$this = $(this);
      		object.settings = $.extend(true, {}, collapser_default_settings, options);
      		object.$small = $(this).find('.collapser_small');
      		object.$medium = $(this).find('.collapser_medium');

      	// NOTE: init happens at the bottom so that it has access to the methods.

  		// ---------------------
		//   PUBLIC METHODS
		// ---------------------
			// returns the DOM element as a jquery object
			object.get = function() {
				return this.$this;
			}

			object.expand = function() {
				if ( object.settings.beforeExpand ) {
	            	object.settings.beforeExpand(object.$this);
	            }

				$(document).find(object.$this.data('selector')).each(function() {
					$(this).find('.content').css('height', 'auto');
				    $(this).addClass('open');
				});

				if ( object.settings.afterExpand ) {
	            	object.settings.afterExpand(object.$this);
	            }
	            if ( object.settings.afterChange ) {
	            	object.settings.afterChange(object.$this);
	            }
			}

			object.collapse = function() {
				if ( object.settings.beforeCollapse ) {
	            	object.settings.beforeCollapse(object.$this);
	            }

				$(document).find(object.$this.data('selector')).each(function() {
					$(this).find('.content').css('height', '0px');
				    $(this).removeClass('open');
				});

				if ( object.settings.afterCollapse ) {
	            	object.settings.afterCollapse(object.$this);
	            }
	            if ( object.settings.afterChange ) {
	            	object.settings.afterChange(object.$this);
	            }
			}

			object.destroy = function() {
				object.$this.removeData(namespace).off('.'+namespace);
				self.removeData();
			}

		// ----------
		//   INIT
		// ----------
			object.initialize = function() {
				if ( init_callback ) {
					init_callback();
				}

				object.$this.on('click.'+ namespace, '.collapser_small', function() {
					object.collapse();
				});

				object.$this.on('click.'+ namespace, '.collapser_medium', function() {
					object.expand();
				});

				object.initialized = true;
			}

			object.initialize();

			// Define the object within the object
			object.$this.data('object', object);
		} else {
			object = $(this).data('object');
		}
    	objects.push(object);
  	}); // End of instance

  	// Setting up the plugin.  Don't do anything below this line.
  	if( objects.length === 1 ) {
    	return_data = objects[0];
  	} else {
    	return_data = objects;
  	}
  	return_data.all = function(callback) {
    	$.each( objects, function(){
      		callback.apply(this);
    	});
  	};
  	return return_data;
};

$(document).ready(function() {
	$('.collapser_sizers.autoload').CollapserSizers();
});