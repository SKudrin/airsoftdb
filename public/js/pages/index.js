(function( Page, $, undefined ) {
    //Private Property
    var Settings = {};
    //Public Property
    Page.Map = null;
    Page.Paper = null;
    Page.Points = null;
    Page.Counts = null;


    //Public Method
    Page.init = function(args) {
        Settings = $.extend( Settings, args );

        $('#promo_banner_close').on('click', function() {
            $('#promo_banner_wrap').animate({
                height: 0}, 200, function() {
                // Animation complete.
            });
        });

        var width = 970,
            height = 540;

        var pointsData,
            showedByRegions = {},
            maxPointsInRegion = 10;

        var svg = d3.select("#plot_map").append("svg")
            .attr("width", width)
            .attr("height", height)
            .style("margin", "10px auto");

        var projection = d3.geo.albers()
                            .rotate([-105, 0])
                            .center([-10, 65])
                            .parallels([52, 64])
                            .scale(750)
                            .translate([width / 2, height / 2]);

        var path = d3.geo.path().projection(projection);

        //Reading map file and data
        queue()
            .defer(d3.json, "/map/russia_1e-7sr.json")
            .defer(d3.json, "/api/region")
            .await(ready);

        //Start of Choropleth drawing
        function ready(error, map, data) {
            var idByCode = {};

            data.forEach(function(d) {
                idByCode[d.code] = {'id' : d.id, 'name' : d.name, 'active' : d.active};
            });

            //Drawing Choropleth
            svg.append("g")
                .attr("class", "region")
                .selectAll("path")
                .data(topojson.object(map, map.objects.russia).geometries)
                //.data(topojson.feature(map, map.objects.russia).features) <-- in case topojson.v1.js
                .enter().append("path")
                .attr("d", path)
                .style("fill", "#D3900D")
                .style("opacity", 0.8)
                //Adding mouseevents
                .on("mouseover", function(d) {
                    $('#state_label').html(idByCode[d.properties.region].name);
                    d3.select(this).transition().style("opacity", 1);
                })
                .on("mouseout", function() {
                    $('#state_label').html('Россия');
                    d3.select(this)
                        .transition()
                        .style("opacity", 0.8);
                })
                .on("click", function(d) {
                    if (typeof idByCode[d.properties.region] == 'object' && idByCode[d.properties.region].active) {
                        window.location.href = '/finder/' + idByCode[d.properties.region].id;
                    } else {
                        alert('В этом регионе пока пусто :(');
                    };
                })

            // Adding points on the map
            d3.json("/api/point", function(error, data) {
                pointsData = data;
                showPoints(data, 'all');
                $('#plot_map').show();
                $('#map_loading').hide();
            });

        }; // <-- End of Choropleth drawing

        function showPoints(data, type) {
            var points = svg.selectAll("g.point" + type)
                        .data(data)
                        .enter()
                        .append("g")
                        .attr("class", "point")
                        .attr("visibility", function(d,i){
                            if (showedByRegions[d.region_id] === undefined || showedByRegions[d.region_id].length < maxPointsInRegion) {
                                showedByRegions[d.region_id] = showedByRegions[d.region_id] === undefined ? [] : showedByRegions[d.region_id];
                                showedByRegions[d.region_id].push(d.type_id);
                                return "true";
                            } else if (typeToReplace = replacePoint(d)) {
                                if ($.inArray(typeToReplace, showedByRegions[d.region_id]) !== -1) {
                                    svg.select("[data-type=\"" + typeToReplace + "\"]").filter("[visibility=true]").attr("visibility", function(d,i){ return 'hidden'; }).remove();
                                    showedByRegions[d.region_id].splice($.inArray(typeToReplace, showedByRegions[d.region_id]), 1);
                                };
                                showedByRegions[d.region_id].push(d.type_id);
                                return "true";
                            } else {
                                return "hidden";
                            }
                        })
                        .attr('data-type', function(d) { return d.type_id; })
                        .attr("transform", function(d) { return "translate(" + projection([d.lng, d.lat]) + ")"; });

            points.append("circle")
                .attr("r", 2)
                .style("fill", "#e4a317")
                .style("stroke", "#555")
                .style("opacity", 1);

            points.selectAll("circle[visibility=hidden]").remove();

            return points;
        }

        function replacePoint(d) {
            var percentInTypes = {},
                length = showedByRegions[d.region_id].length,
                percent = 100 / length,
                type;

            for (var i = 0; i < length; i++) {
                type = showedByRegions[d.region_id][i];
                if (percentInTypes[type] === undefined) {
                    percentInTypes[type] = percent;
                } else {
                    percentInTypes[type] += percent;
                }
            };

            var arr = Object.keys( percentInTypes ).map(function ( key ) { return key; });
            var max = Math.max.apply( null, arr );

            if (percentInTypes[d.type_id] == undefined || max != d.type_id) {
                return true;
            };
        }

        function switchPointVisibleTo(action, type) {
            if (action === 'off') {
                svg.selectAll("[data-type=\"" + type + "\"]").filter("[visibility=true]").each(function (d) {
                    if (showedByRegions[d.region_id].length > 0) {
                        if ($.inArray(d.type_id, showedByRegions[d.region_id]) !== -1) {
                            showedByRegions[d.region_id].splice($.inArray(d.type_id, showedByRegions[d.region_id]), 1);
                        };
                    };
                }).attr("visibility", function(d,i){ return 'hidden'; }).remove();
            } else {
                var data = $.grep(pointsData, function(e){ return e.type_id == type; });
                showPoints(data, type);
            };
        }

        $( document ).ready(function() {

            $('#menubar-mobile-wrap').insertAfter('#header-main-push');

            // Toggle the visibility of the dots on the map based on the unit type
            $('div.unit .switcher').on('click', function() {
              var id = $(this).parents('div.unit').attr('id');
              if ( $(this).hasClass('off') ) {
                $(this).removeClass('off');
                    $(this).parents('div.unit').removeClass('off');
                $(this).html('СКРЫТЬ');
                    switchPointVisibleTo('on', $(this).attr('data-type'));
                $('.'+id).css('display', '');
              } else {
                $(this).addClass('off');
                    $(this).parents('div.unit').addClass('off');
                $(this).html('ПОКАЗАТЬ');
                    switchPointVisibleTo('off', $(this).attr('data-type'));
                $('.'+id).css('display', 'none');
              }
            });

            $('#hide_plot_map').on('click', function() {
                if ( $('#plot_map_wrap').hasClass('closed')) {
                    Global.animate_height_auto( $('#plot_map_wrap'), 300);
                    $('#plot_map_wrap').removeClass('closed');
                    $('#hide_plot_map').html('<span class="open"><nobr>КАРТА</nobr></span>');
                } else {
                    $('#plot_map_wrap').animate({'height':'0px'}, 300);
                    $('#plot_map_wrap').addClass('closed');
                    $('#hide_plot_map').html('<span class="closed"><nobr>КАРТА</nobr></span>');
                }
            });

            // Expand or Collapse the list of states
            $('#byname span').on('click', function() {
              if ( $(this).hasClass('open') ) {
                $(this).removeClass('open');
                $('#state_picker').animate({'height': '0px'}, 400);
              } else {
                $(this).addClass('open');
                Global.animate_height_auto($('#state_picker'), 400, true);
              }
            });
        });
    };

}( window.Page = window.Page || {}, jQuery ));