(function( Finder, $, undefined ) {
  var settings = {'loadPointsFromServer' : true,
                  'points' : {},
                  'region' : 'nil',
                  'ftypeApplied' : undefined,
                  'fwordApplied' : undefined,
                  'sort' : {'param':'name','order':'asc', 'sorted':true},
                  'map' : null
                  };
  var map = null;
  Finder.Map = null;

  Finder.getSettings = function () {
    return settings;
  };

  Finder.setSettings = function (key, val) {
    settings[key] = val;
  };

  Finder.getPoints = function () {
    var allPoints = settings.points,
        points = {},
        pointScore,
        scoreToShow = 0;

    if (settings.ftypeApplied !== undefined)
      scoreToShow++;

    if (settings.fwordApplied !== undefined)
      scoreToShow++;

    // sort
    if (settings.sort.sorted !== true) {
      allPoints = sortPoints(allPoints);
    }

    for (point in allPoints) {
      score = 0;
      //filters
      // type
      if (settings.ftypeApplied !== undefined && typeFilter(allPoints[point]))
        score++;

      // word
      if (settings.fwordApplied !== undefined && wordFilter(allPoints[point]))
        score++;

      if (score === scoreToShow) {
        points[point] = allPoints[point];
      };
    }

    return points;
  };

  function sortPoints(points) {
    var sortSettings = Finder.getSettings().sort;
    var sortFunc;
    switch (sortSettings.param) {
      case 'type':
        sortFunc = function (a,b) {
          if (a.type_id < b.type_id)
            return sortSettings.order === 'asc' ? -1 : 1;
          if (a.type_id > b.type_id)
            return sortSettings.order === 'asc' ? 1 : -1;
          return 0;
        };
        break;
      default:
        sortFunc = function (a,b) {
          if (a.name < b.name)
            return sortSettings.order === 'asc' ? -1 : 1;
          if (a.name > b.name)
            return sortSettings.order === 'asc' ? 1 : -1;
          return 0;
        };
        break;
    }

    var sortedPoints = points.sort(sortFunc);

    $.extend(sortSettings, {'sorted' : true});
    Finder.setSettings('sort', sortSettings);
    Finder.setSettings('points', sortedPoints);

    return sortedPoints;
  };

  Finder.init = function(args) {
    settings = $.extend(settings,args);
  };

  Finder.refresh = function(ajax) {
    var ajax = ajax || settings.loadPointsFromServer,
        d = $.Deferred();

    if (ajax === true) {
      var req = $.ajax({
        url: "/api/point",
        method: "GET",
        data: { 'region' : settings.region }
      }).done(function(points) {
        settings.points = points;
        d.resolve();
      });
    } else {
      d.resolve();
    };

    settings.loadPointsFromServer = false;

    return d.promise();
  };

  Finder.show = function(points) {
    $('#myAppView').find('div.unit').each(function () {
      $(this).remove();
    });
    var i = 0,
      html = '<div class="container"><div class="sixteen columns" style="position:relative;">';

    for (point in points) {
      i++;
      html += '<div class="point unit" id="point_' + points[point].id + '">\
                <table class="header">\
                  <tbody>\
                    <tr>\
                      <td class="widget">\
                        <div class="norgie">\
                          <div class="icon">\
                            <div class="pointerRight"></div>\
                          </div>\
                        </div>\
                      </td>\
                      <td class="view">\
                        <div class="unitname ng-scope">\
                            <img class="unit_icon" src="/img/icons/menu/field.svg" width="16" style="opacity:.4;">\
                          <a class="name" target="_self" href="/point/' + points[point].id + '/show">' + points[point].name + '</a>\
                        </div>\
                        <div class="citystate">\
                          <span class="location">' + points[point].region.name + '</span>\
                        </div>\
                      </td>\
                      <td class="options">\
                        <div class="option">\
                          <img class="marker iconbutton tooltipstered" src="/img/icons/marker.svg" width="14">\
                        </div>\
                        <img class="mobile_unit_icon ng-scope" src="/img/icons/menu/field.svg" width="16" style="opacity:.4;">\
                      </td>\
                    </tr>\
                  </tbody>\
                </table>\
                <div class="content" style="height: 0px;">\
                  <hr>\
                  <div class="contentpad">\
                    <div class="ng-scope">\
                        <div class="coverwrap ng-scope">\
                            <a target="_self" href="/point/' + points[point].id + '/show">\
                                ' + ((points[point].has_avatar) ? '<img class="cover" src="/img/pavatars/p_' + points[point].id + '.png">' : '') + '\
                            </a>\
                        </div>\
                      <div class="citystate">' + points[point].region.name + '</div>\
                      <div class="directions">\
                        <a target="_self" href="/point/' + points[point].id + '/show">Map &amp; Directions</a>\
                      </div>\
                      <div class="description">' + (points[point].description ? points[point].description : '') + '</div>\
                    </div>\
                  </div>\
                </div>\
              </div>';
    };

    html += '</div></div>';

    $('#results').html(i + ' ' + Global.declOfNum(i, ['Результат', 'Результата', 'Результатов']))

    $('#myAppView').html(html);
  };

  Finder.initAddress = function() {
      Global.Google_Places_Autocomplete('address', function(address, comp) {
        MapManager.showLocation(comp.geometry.location);
        if ( address == '' ) {
          return false;
        }
        // Finder.getDirections();
        //return false;
      });
  }

  function typeFilter(point) {
    return ((typeof settings.ftypeApplied === 'undefined') || jQuery.inArray("" + point.type.id, settings.ftypeApplied) !== -1);
  };

  function wordFilter(point) {
    return -1 !== point.name.toLowerCase().indexOf(settings.fwordApplied);
  }
}( window.Finder = window.Finder || {}, jQuery ));

(function( Page, $, undefined ) {
    var Settings = {};

    Page.init = function(args) {
        Settings = $.extend( Settings, args );

        function updateQueryStringRegion(region) {
          baseUrl = [location.protocol, '//', location.host, location.pathname].join('');
          urlQueryString = document.location.search;
          keyRegex = new RegExp('\/finder(\/\\d+|\/|)');

          if (baseUrl.match(keyRegex) !== null) {
            baseUrl = baseUrl.replace(keyRegex, "/finder/" + region);
          }

          window.history.replaceState({}, "", baseUrl + urlQueryString);
        }

        $(document).ready(function(e) {
          MapManager.showMap().done(function (map) {
            var markers = MapManager.syncMarkers(Finder.getPoints());
            MapManager.showBounds(markers);
            Finder.initAddress();
          });

          //select bs
          $('.selectpicker').selectpicker();

          //show or hide additional filters
          $('body').on('click', '#gear', function () {
            if ($('#morefilters').height() == 0) {
              $('#morefilters').height("auto").addClass('open');
            } else {
              $('#morefilters').height(0).removeClass('open');
            }

            $('#mapside_results_wrap').css('top', $('#finder').height() + 25);
          });

          // collapse or open row
          $('body').on('click', 'table.header', function () {
            var unit = $(this).parent('div.unit');

            if (unit.hasClass('open')) {
              collapseRow(unit);
            } else {
              openRow(unit);
            }
          });

          // type filters
          $('body').on('change', '.unit_picker_cbox', function () {
            var useFilter = false,
                filterVal = [];
            $('.unit_picker_cbox').each(function () {
                if ($(this).prop('checked') == false) {
                  useFilter = true;
                } else {
                  filterVal.push($(this).attr('data-type-id'))
                }
            });

            if (useFilter) {
              Global.RemoveParamFromQueryString('ftype[]');
              if (filterVal.length > 0) {
                for (var val in filterVal) {
                  Global.AddURLQueryStringParam('ftype[]', filterVal[val]);
                }
              }else{
                Global.AddURLQueryStringParam('ftype[]', 'nil');
              };
            }else{
              filterVal = undefined;
              Global.RemoveParamFromQueryString('ftype[]');
            };
            Finder.setSettings('ftypeApplied', filterVal);
            Finder.refresh().done(function () {
              var points = Finder.getPoints();
              var markers = MapManager.syncMarkers(points);
              MapManager.showBounds(markers);
              Finder.show(points);
            });
          });

          // region select
          $('body').on('change', 'select[name="region"]', function () {
            var region = $('select[name="region"] option:selected').val(),
                finderSettings = Finder.getSettings();

            region = region === 'nil' ? '' : region;

            if (finderSettings.region == "undefined" || finderSettings.region !== region) {
              Finder.setSettings('region', region);
              Finder.setSettings('loadPointsFromServer', true);
            };

            updateQueryStringRegion(region);
            Finder.refresh().done(function () {
              var points = Finder.getPoints();
              var markers = MapManager.syncMarkers(points);
              MapManager.showBounds(markers);
              Finder.show(points);
            });
          });

          // collapse all rows
          $('#sizers #small').on('click', function () {
            $('#myAppView').find('div.unit').each(function () {
              collapseRow($(this));
            });
          });

          // open all rows
          $('#sizers #medium').on('click', function () {
            $('#myAppView').find('div.unit').each(function () {
                openRow($(this));
            });
          });

          // name filter
          $('#filter').on('keyup', function () {
            var word = $(this).val().toLowerCase() === "" ? undefined : $(this).val().toLowerCase();
            Finder.setSettings('fwordApplied', word);
            Finder.refresh().done(function () {
              Finder.show(Finder.getPoints());
            });
          })

          // sort by
          $('body').on('change', 'select[name="sort"]', function () {
            updateSortData();
            Finder.refresh().done(function () {
              Finder.show(Finder.getPoints());
            });
          });

          // sort dir
          $('body').on('change', 'input[name="sortdir"]', function () {
            updateSortData();
            Finder.refresh().done(function () {
              Finder.show(Finder.getPoints());
            });
          });

          function updateSortData () {
            sortBy = $('select[name="sort"] option:selected').val();
            sortDir = $('input[name="sortdir"]:checked').val();
            Finder.setSettings('sort', {'param':sortBy,'order':sortDir, 'sorted':false});
          }

          function openRow(row) {
            row.addClass('open');
            row.find('.norgie').addClass('open');
            row.children('.content').height("auto");
          };

          function collapseRow(row) {
            row.removeClass('open');
            row.find('.norgie').removeClass('open');
            row.children('.content').height(0);
          };
        });
    };
}( window.Page = window.Page || {}, jQuery ));

