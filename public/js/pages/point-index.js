(function( Page, $, undefined ) {

    var Settings = {};

    Page.init = function(args) {
        Settings = $.extend( Settings, args );

        $('h3').pin({containerSelector: ".state",
                     offsetTop: function() {
                        if ( Browser.ScreenSize() <= 2 ) {
                            return $('#menubar-mobile').height();
                        } else {
                            return 0;
                        }
                    }
        });

        $('.collapser').Collapser({
            beforeExpand: function($el) {
                load_unit_logo($el);
            },
          afterChange: function() {
            $(window).trigger('resize'); // refresh pin
          }
        });

        $('.collapser_sizers').CollapserSizers({
            beforeExpand: function() {
                $('.collapser').each(function() {
                    load_unit_logo($(this));
                });
            },
          afterChange: function() {
            $(window).trigger('resize'); // refresh pin
          }
        });

        // поиск, хардкодом идут типы точек, можно перепилить
        // var Search = $('#search_by_location').SiteSearch({
        //   filter: ['places', Settings.unit+'s'], // this should be removed to include places
        //   placeholder: "Enter "+Settings.unit.capitalizeFirstLetter()+" Name or Address",
        //   sectionDisplayLimit: 5,
        //   templates: {
        //         places: function(data) {
        //             var query = Global.encodeURIComponent(data.address);
        //             if ( data.filter ) {
        //                 query = query + data.filter;
        //             }

        //             if ( Settings.unit == 'field' ) {
        //                 unit = '&f=1';
        //             }
        //             if ( Settings.unit == 'shop' ) {
        //                 unit = '&s=1';
        //             }
        //             if ( Settings.unit == 'team' ) {
        //                 unit = '&t=1';
        //             }

        //             return "<a href='/finder?q="+query+unit+"'><img class='ss_icon' src='/resources/icons/marker.png'>"+data.address+"</a>";
        //         }
        //     }
        // });

        $(document).ready(function(e) {
        // prevent a specific anchor tag with id
           $('a#show_results_on_map').click(function(e){
                // stop its defaut behaviour
                e.preventDefault();

                // Make sure that the map is visible when they arrive
                $.cookie('show_map', 1, { expires: 365, path: '/finder' });

                window.location.href = $(this).prop('href');
            });
        });

        $.scrollUp({
            animation: 'fade',
            scrollDistance: '150',
            scrollImg: {
                active: true,
                type: 'background',
                src: '/resources/icons/top.png'
            }
        });
    };

    function load_unit_logo($el) {
        $logo = $el.find('.logo');
        if ( $logo.attr('src') == '') {
            $logo.attr('src', $logo.data('src'));
        }
    }

}( window.Page = window.Page || {}, jQuery ));