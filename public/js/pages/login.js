(function( Page, $, undefined ) {
    // Private Property
    var data = {};
    var Settings = {};

    Page.init = function(args) {

        Settings = $.extend( Settings, args );

        if ( !Global.MobileDevice() ) {
            $('#email').focus();
        }

        $('.user_input').one('focus', function() {
            $('#login_error').fadeOut();
        });
    };

}( window.Page = window.Page || {}, jQuery ));
