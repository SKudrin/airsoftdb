(function( Page, $, undefined ) {
    var Settings = {};

    Page.init = function(args) {
        Settings = $.extend( Settings, args );

        $('#tabs').AC3Tabs({
            onTabClick: function($element) {
                Page.SetTab($element.data('page'));
            }
        });

        $('#slideshow').cycle({
            speed: 500,
            loader: true,
            prev: '#feature_cycle_prev',
            next: '#feature_cycle_next',
            swipe: true,
            log: false,
            pauseOnHover: "#slideshow_wrap",
            fx: 'scrollHorz',
            swipefx: 'scrollHorz',
            slides: '> div',
            pause: true,
            centerVert: true,
            centerHorz: true
        });

        // Show the map when you click on the address
        $('#address, .show_location').on('click', function() {
            Page.SetTab('location');
            $('#tabs').AC3Tabs().SelectTab('location_tab');
        });


        $('#about .show_feature').on('click', function() {
            var $feature = $(this).parents('.feature');
            $feature.find('.show_feature').fadeOut(0);
            var $content = $feature.find('.feature_content');
            var height = 350;
            if ( Browser.ScreenSize() == 2 ) {
                height = 275;
            }
            if ( Browser.ScreenSize() == 1 ) {
                height = 235;
            }
            $content.animate({'height':height+'px'}, 300, function() {
                $feature.find('.hide_feature').fadeIn(300);
                $content.css('height', 'auto');
            });
        });

        $('#about .hide_feature').on('click', function() {
            var $feature = $(this).parents('.feature');
            $feature.find('.hide_feature').fadeOut(0);
            var $content = $feature.find('.feature_content');
            $content.animate({'height':'0px'}, 300, function() {
                $feature.find('.show_feature').fadeIn(300);
            });
        });

        $('#location .show_feature').on('click', function() {
            Page.ShowFeature(true, $(this).parents('.feature'));
        });

        $('#location .hide_feature').on('click', function(){
            Page.ShowFeature(false, $(this).parents('.feature'));
        });

        if ( $('#slideshow').length != 0) {
            $('#slideshow').on('cycle-initialized', function( event, opts ) {
                $(window).trigger('resize');
            });
        }

        $('#slideshow').on('mouseleave', function() {
            $('#feature_cycle_prev').fadeTo(200, 0);
            $('#feature_cycle_next').fadeTo(200, 0);
        });

        $('#feature_cycle_prev').on('mouseenter', function() {
            $('#feature_cycle_prev').fadeTo(200, .7);
            $('#feature_cycle_next').fadeTo(200, .3);
        });
        $('#feature_cycle_next').on('mouseenter', function() {
            $('#feature_cycle_prev').fadeTo(200, .3);
            $('#feature_cycle_next').fadeTo(200, .7);
        });

        //$('#header-main div').last().before($('#edit_page_wrap'));

        $('a.fresco').on('click', function() {
            setTimeout(function() {
                var text = $('h1').text();
                var $win = $('.fr-window');
                if ( !$win.find('h1.fresco_header').length ) {
                    $('<h1 class="fresco_header">'+text+'</h1>').prependTo($win);
                }
            }, 200);
        });

        if ( typeof Report != 'undefined' ) {
            Report.init();
            $('#inappropriate').on('click', function(){
                Report.Open();
            });
        }

        $(window).load(function() {
            Page.PromoteThumbs();
        });
    };

    // Promote all of the thumbnails to full res after the page is done loading and isn't doing anything
    Page.PromoteThumbs = function() {
        $('img.promote').each(function() {
            $(this).attr('src', $(this).data('promote') );
        });
    }

    Page.SetTab = function(p) {
        $('.page').fadeOut(0);
        $('#'+p).fadeIn(500, function() {
            /*
            if ( Browser.ScreenSize() === 1 ) {
                $("html, body").animate({ scrollTop: ($('#'+p).offset().top-50) }, 300);
            }
            */
        });

        if ( p == 'about' ) {
            // trigger resize to fix jquery cycle
            $(window).trigger('resize');
        }

        if ( p == 'location' ) {
            MapManager.showMap().done(function (map) {
                var point = {};
                point[Settings.point.id] = Settings.point;
                var markers = MapManager.syncMarkers(point);
                MapManager.showBounds(markers);
            });
        }

        Global.UpdateURLQueryStringParam('p', p.replace('_tab', ''));
        //window.history.replaceState({}, "", p.replace('_tab', ''));
    };

    Page.ShowFeature = function(visibility, $feature) {
        if( typeof(visibility)==='undefined') visibility = true;

        var $content = $feature.find('.feature_content');

        if ( visibility === true ) {

            if ( $feature.attr('id') === 'map_feature' ) {
                $feature.find('.show_feature').fadeOut(0);
                $content.animate({'height':'350px'}, 300, function(){
                    $feature.find('.hide_feature').fadeIn(300);
                });
            } else {
                $feature.find('.show_feature').fadeOut(0);
                Global.animate_height_auto($content, 300, true, function() {
                    $feature.find('.hide_feature').fadeIn(300);
                });
            }
        } else {
            $feature.find('.hide_feature').fadeOut(0);
            $content.animate({'height':'0px'}, 300, function(){
                $feature.find('.show_feature').fadeIn(300);
            });
        }
    }

    Page.InitAddress = function() {
        Global.Google_Places_Autocomplete('directions_from', function(address) {
            if ( address == '' ) {
                return false;
            }
            //return false;
        });
    };

    // Explicitly save/update a url parameter using HTML5's replaceState().

}( window.Page = window.Page || {}, jQuery ));