
// Namespace Template
// http://stackoverflow.com/a/5947280/377205
(function( Page, $, undefined ) {
    //Private Property
    var Settings = {};

    //Public Method
    Page.init = function(args) {
        Settings = $.extend( Settings, args );
        $('.j-profile-toggler').on('click', function () {
            $(this).siblings(".j-profile-point-box").toggle("slow");
        });
    };

}( window.Page = window.Page || {}, jQuery ));