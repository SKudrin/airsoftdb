
// Namespace Template
// http://stackoverflow.com/a/5947280/377205
(function( Page, $, undefined ) {
    //Private Property
    var Settings = {};

    //Public Method
    Page.init = function(args) {
        Settings = $.extend( Settings, args );


        $('#user_name').focus();

        // поиск совподений в названиях уже существующих точек
        // $('#name').on('keyup', function(e) {
        //     Page.ShowMatches();
        // });

        $('.radioStyleGrp').each(function() {
            var id = $(this).find('label').attr('for');
            $(this).wrap("<div id='opt_"+id+"' class='option'></div>")
        });

        // при смене типа точек сменяется подсказки с уникальными именами (возможно у них уникальное не только имя а имя + тип точки)
        // $("input[name='type']:radio").change(function () {

        //     var name = $(this).val().toUpperCase();
        //     $('.type_name').each(function(){
        //         $(this).text(name);
        //     });
        //     Page.ShowMatches();
        // });

        // автокомплит адресов
        // Global.LoadGoogleMap('Page.InitAddress', 'places');
    };

    // Page.HideMatches = function() {
    //     $('#matches_wrap').hide();
    //     $('#matches_type').text($('input:radio[name=type]:checked').val()+"s");
    //     $('#matches').empty();
    // }

    // Page.ShowMatches = function() {
    //     //var type = $('input:radio[name=type]:checked').val();
    //     $('#matches_type').text($('input:radio[name=type]:checked').val()+"s");


    //     var val  = $('#name').val();
    //     if ( val.length < 2 ) {
    //         Page.HideMatches();
    //         return;
    //     }

    //     $.ajax({
    //         url: "/other/addlisting/addlisting.match.ajax.php",
    //         dataType: "json",
    //         type: "POST",
    //         data: {
    //             query: val,
    //             type: $('input:radio[name=type]:checked').val(),
    //             security_token: Settings.security_token
    //         },
    //         success: function(response) {
    //             $('#matches').empty();

    //             if ( !response ) {
    //                 Page.HideMatches();
    //                 return;
    //             }

    //             if ( response.matches.length > 0 ) {
    //                 $('#matches_wrap').show();
    //             } else {
    //                 Page.HideMatches();
    //             }

    //             function escapeRegExChars(value) {
    //                 return value.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
    //             }

    //             var pattern = '(' + escapeRegExChars(val) + ')';

    //             for ( match in response.matches ) {
    //                 name = response.matches[match]['name'];
    //                 name = name.replace(new RegExp(pattern, 'gi'), '<strong>$1<\/strong>')
    //                 $('#matches').append("<div class='match' id='"+response.matches[match]['uid']+"'><div class='unit_info'><a href='"+response.matches[match]['url']+"'>"+name+"</a>  <span class='type'>"+response.matches[match]['type']+" in "+response.matches[match]['city']+", "+response.matches[match]['state']+"</span></div><a target='_blank' href='claim/"+response.matches[match]['uid']+"' class='claim button basic gold'>Claim</a><div class='clear'></div></div>");
    //             }
    //         }
    //     });
    // }

    // Page.InitAddress = function() {
    //     Global.Google_Places_Autocomplete('address', function(address) {
    //         return false;
    //     }, false);
    // }

}( window.Page = window.Page || {}, jQuery ));