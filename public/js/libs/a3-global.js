
// This is legacy.  Needs to be replaced with global namespace and removed.
// function animate_height_auto($element, speed)
// {
//   var curHeight = $element.height();

//   if ( curHeight == 0 )
//   {
//     $element.css('height', 'auto');
//     var autoHeight = $element.height();
//     $element.height(curHeight).stop().animate({height: autoHeight}, speed);
//   }
// }

// http://stackoverflow.com/a/11381730
window.isMobile = function() {
    var check = false;
    (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
    return check;
}

String.prototype.endsWith = function(suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};
String.prototype.capitalizeFirstLetter = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
};
String.prototype.ucfirst = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
};

// Javascript Namespace
// http://stackoverflow.com/a/5947280/377205

(function( Global, $, undefined ) {
    //Private Properties
    var data = {};
    var $window    = $(window);
    var Sticker = {};  // The sticker element (needs to be a property so that the vars are constant)
    var GoogleMapsLoaded = false;

    /* Update a page's url with the give param and value.
     * to give the user feeback about something. Usually it's after a change has been made to a form
     * @param string param name
     * @param string param value
     * @return none
     */
    Global.UpdateURLQueryStringParam = function(key, value) {
        baseUrl = [location.protocol, '//', location.host, location.pathname].join('');
        urlQueryString = document.location.search;
        var newParam = key + '=' + value,
        params = '?' + newParam;

        // If the "search" string exists, then build params from it
        if (urlQueryString) {
          keyRegex = new RegExp('([\?&])' + key + '[^&]*');
          // If param exists already, update it
          if (urlQueryString.match(keyRegex) !== null) {
              params = urlQueryString.replace(keyRegex, "$1" + newParam);
          } else { // Otherwise, add it to end of query string
              params = urlQueryString + '&' + newParam;
          }
        }
        window.history.replaceState({}, "", baseUrl + params);
    }

    Global.RemoveParamFromQueryString = function(key) {
      baseUrl = [location.protocol, '//', location.host, location.pathname].join('');
      urlQueryString = document.location.search;

      // If the "search" string exists, then build params from it
      if (urlQueryString) {
        keyRegex = new RegExp('([\?&])' + key + '[^&]*');

        if (urlQueryString.match(keyRegex) !== null) {
          baseUrl = urlQueryString.replace(keyRegex, "");
        }

        window.history.replaceState({}, "", baseUrl);
      }
    }

    Global.AddURLQueryStringParam = function(key, value) {
      baseUrl = [location.protocol, '//', location.host, location.pathname].join('');
      urlQueryString = document.location.search;
      var newParam = key + '=' + value;

      if (urlQueryString) {
        params = urlQueryString + '&' + newParam;
      } else {
        params = '?' + newParam;
      }

      window.history.replaceState({}, "", baseUrl + params);
    }

    Global.declOfNum = function (number, titles) {
        cases = [2, 0, 1, 1, 1, 2];
        return titles[(number%100>4 && number%100<20) ? 2 : cases[(number%10<5)?number%10:5]];
    }

    Global.capitaliseFirstLetter = function(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    Global.InitMobileMenu = function() {
        var $mmenu = $('nav#mainmenu-panel').mmenu(
        {
            dragOpen: {
                        open: false,
                        threshold : 200
            },
        },{
            hardwareAcceleration: false
        });

        $('#mainmenu-panel').removeAttr('style');

        $('#header-main-mmenu').on('click', function() {
            $("#mainmenu-panel").trigger("open");
        });

        $('#mm-blocker').on('click', function() {
            $("#mainmenu-panel").trigger("close");
        });
    }

    Global.CountryBounds = function() {
        var bounds = new google.maps.LatLngBounds(
            new google.maps.LatLng(24, -128),
            new google.maps.LatLng(52, -64)
        );
        return bounds;
    }

    Global.Google_Places_Autocomplete = function(id, callback, submitOnEnter) {
      submitOnEnter = typeof submitOnEnter !== 'undefined' ? submitOnEnter : true;


        var placeSearch, autocomplete;
        var componentForm = {
          street_number: 'short_name',
          route: 'long_name',
          locality: 'long_name',
          administrative_area_level_1: 'short_name',
          country: 'long_name',
          postal_code: 'short_name'
        };

        // US bounds
        var bounds = Global.CountryBounds();

        var input = document.getElementById(id);

        // Create the autocomplete object, restricting the search to geographical location types.
        autocomplete = new google.maps.places.Autocomplete(
            (input),
            { types: ['geocode'],
              //bounds: bounds,
              componentRestrictions: {country: "ru"} //http://stackoverflow.com/questions/8282026
             }
        );

        // When the user selects an address from the dropdown,
        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            if (typeof(callback) == "function") {
                var address = $('#'+id).val();
                var components = autocomplete.getPlace();
                callback(address, components);
            }
        });

        // Prevent form submission with the enter key ( button is required )
        if ( submitOnEnter == false ) {
          google.maps.event.addDomListener(input, 'keydown', function(e) {
              if (e.keyCode == 13)
              {
                  if (e.preventDefault) {
                      e.preventDefault();
                  } else {
                      e.cancelBubble = true;
                      e.returnValue = false;
                  }
              }
          });
        }

        return autocomplete;
    };

    /* The feedback bar is an html element at the top of the page that can come into view
     * to give the user feeback about something. Usually it's after a change has been made to a form
     * @param string | False message - the message (html) that you want to display, if False then close the feeback bar
     * @param int speed - the animation height speed
     * @param int delay - the delay time before showing the feedbackbar
     * @retrurn none
     */
    Global.ShowFeedbackBar = function(message, speed, delay, auto_close) {
        message = typeof message !== 'undefined' ? message : 'Your changes have been saved.';
        speed = typeof speed !== 'undefined' ? speed : 400;
        delay = typeof delay !== 'undefined' ? delay : 200;
        auto_close = typeof auto_close !== 'undefined' ? auto_close : true;

        if ( message !== false ) {
            setTimeout(function(){
                $('#feedbackbar_msg').html(message);
                Global.animate_height_auto($("#feedbackbar"), speed, true, function() {
                    if ( auto_close ) {
                      setTimeout(function(){
                        $('#feedbackbar').animate({height:0}, speed);
                      }, 2000);
                    }
                });
            }, delay);
        } else {
            $('#feedbackbar').animate({height:0}, 200);
        }
    }

    Global.ShowProgressbar = function(visibility) {
      visibility = typeof visibility !== 'undefined' ? visibility : true;
      if ( visibility === true ) {
        $('#progressbar').show();
      } else {
        $('#progressbar').hide();
      }
    }

    Global.encodeURIComponent = function ( str ) {
        return encodeURIComponent(str).replace(/[!'()*]/g, function(c) {
          return '%' + c.charCodeAt(0).toString(16);
        });
    }

    /* Public
     * Convert a form textarea element into a rich text area (Redactor)
     * @param JQuery element
     * @param object Redactor custom_options
     * @param array Redactor plugins
     */
    Global.Redactor = function($element, custom_optons, plugins) {
        custom_optons = typeof custom_optons !== 'undefined' ? custom_optons : false;
        plugins = typeof plugins !== 'undefined' ? plugins : false;

        var buttons = ['html', 'formatting', 'bold', 'italic', 'deleted', 'unorderedlist', 'orderedlist', 'outdent', 'indent', 'link', 'alignment', 'horizontalrule'];
        if ( $element.hasClass('media') ) {
          buttons = ['html', 'formatting', 'bold', 'italic', 'deleted', 'unorderedlist', 'orderedlist', 'outdent', 'indent', 'image', 'file', 'link', 'alignment', 'horizontalrule', '|', 'image', 'video'];
        }

        var default_settings = {
          minHeight: 200,
          buttons: buttons,
          buttonsHideOnMobile: ['image', 'video'],
          initCallback: function() {
            /*
            var r = this;
            // Not sure why, but I have to put this in setTimeout for it to work.
            setTimeout(function() {
              var name = r.core.getElement().attr('name');
              object.defaults[name] = r.code.get();
            }, 10);
            */
          }
        }

        if ( !plugins ) {
          if ( $element.hasClass('media') ) {
            default_settings.plugins = ['imagemanager', 'video', 'fontsize', 'fontcolor'];
          } else {
            default_settings.plugins = ['fontsize', 'fontcolor'];
          }
        }

        var settings = $.extend( default_settings, custom_optons );
        var redactor = $element.redactor(settings);
        return redactor;
    }

    Global.InitSiteSearch = function() {
        var SS = $('#site_search').SiteSearch({
          filter: ['fields', 'shops', 'teams', 'retailers'], // this should be removed to include places
          placeholder: "Search for anything",
          top: "35px",
          sectionDisplayLimit: 5,
          onShow: function() {

          },
          onClose: function() {

          }
        });

        function close_search() {
            $('#site_search_wrap').fadeOut(100);
            $('#site_search_input_grp').css({'margin-top': '-50px'});
            SS.close();
        }

        $('.site_search_button').on('click', function() {
            $('#site_search_input_grp').css({'margin-top': '-50px'});
            $('#site_search_wrap').fadeIn(0);
            SS.focus();

            if ( Global.MobileDevice() ) {
              $('#site_search_cover').fadeIn(0);
            }

            $('#site_search_input_grp').animate({
                'margin-top': '0px',
                }, 150, function(){
                    $('#site_search').focus();
            });

        });

        $('#site_search_cover').on('click', function() {
            close_search();
        });

        $('.site_search_cancel').on('click', function() {
            close_search();
        });

        SS.$input.on('keypress', function(e) {
            if (e.keyCode == 27) {
                close_search();
            }
        });
    }

    // executeFunctionByName("My.Namespace.functionName", window, arguments);
    Global.executeFunctionByName = function(functionName, context, argument) {
      argument = typeof argument !== 'undefined' ? argument : '';

      var args = [].slice.call(arguments).splice(2);
      var namespaces = functionName.split(".");
      var func = namespaces.pop();
      for(var i = 0; i < namespaces.length; i++) {
        context = context[namespaces[i]];
      }
      return context[func].apply(this, args);
    }

    //Public Property
    //Global.name = "Jacob";

    // Public Method
    /* Load google map api into web page.
     * @param string onload - callback function that should be executed after the maps has loaded.
     * @retrurn none
     */
    Global.LoadGoogleMap = function(onload, libraries, immediately) {
        immediately = typeof immediately !== 'undefined' ? immediately : true;
        onload = typeof onload !== 'undefined' ? onload : false;
        libraries = typeof libraries !== 'undefined' ? libraries : '';

        if ( GoogleMapsLoaded ) {
          console.log('google maps is already loaded');
          //Global.executeFunctionByName(onload, window);
          return;
        }

        GoogleMapsLoaded = true;

        console.log("Should LoadGoogleMap always load visualization?");

        function go_for_it() {

          var script = document.createElement('script');
          script.type = 'text/javascript';
          var src = 'https://maps.googleapis.com/maps/api/js?sensor=true&libraries=visualization,'+libraries;

          if ( onload !== false ) {
              src = src + '&callback=' + onload;
          }
          script.src = src;
          document.body.appendChild(script);
        }

        if ( immediately === true ) {
          go_for_it();
        } else {
          $( window ).load(function() {
            go_for_it();
          });
        }
    };

   /* Animate an html element to its auto height by evaluting the height before the animation.
     * @param jQuerySelector $element
     * @param int speed - the speed at which the element should open
     * @param bool end_as_auto - the height should end as auto when it's done
     * @param function callback - fire this after the animation is done
   * @retrurn none
   */
    Global.animate_height_auto = function($element, speed, end_as_auto, callback) {
      speed = typeof speed !== 'undefined' ? speed : 700;
      end_as_auto = typeof end_as_auto !== 'undefined' ? end_as_auto : true;

      var curHeight = $element.height();

      //if ( curHeight == 0 )
      //{
        $element.css('height', 'auto');
        var autoHeight = $element.height();
        $element.height(curHeight).stop().animate({height: autoHeight}, speed, function() {

                  if ( end_as_auto === true ) {
                      $element.css('height', 'auto');
                  }

                  if (typeof(callback) == "function") {
                      callback($element);
                  }
                  // remove the inline style for height. (not sure if I should do this because it will delete all of the
                  // inline style data and does it really matter if the height is on there (yes, for responsive design it might)

                  // Actually, not a good idea because if an element has height set to 0 in a stylsheet
                  // then the element will revert back to 0 after the animation is done.
                  // $element.removeAttr('style');
              });
      //}
          return autoHeight;
    }

    Global.get_auto_height = function($element) {
        //var inline = $element.prop('style');
        var h = $element.css('height');
        $element.css('height', 'auto');
        var height = $element.height();
        $element.css('height', h);
        return height;
    }


    Global.MobileDevice = function() {
        var isAndroid = function() {
            return navigator.userAgent.match(/Android/i);
        };

        var isBlackBerry = function() {
            return navigator.userAgent.match(/BlackBerry/i);
        };

        var isIOS = function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        };

        var isOpera = function() {
            return navigator.userAgent.match(/Opera Mini/i);
        };

        var isWindows = function() {
            return navigator.userAgent.match(/IEMobile/i);
        };


        if (isAndroid() || isBlackBerry() || isIOS() || isOpera() || isWindows()) {
            return true;
        } else {
            return false;
        }
    }

    Global.InitSubpageMenu = function($element) {
      var speed = 200;
      $element.find('.subpages_mobile_collapse').on('click', function(){
        var $s = $element.find('.subpages');
        var h = $
        if ( $s.height() == 0) {
          Global.animate_height_auto($s, speed);
        } else {
           $s.animate({height: 0}, speed, function(){
              $(this).css('height', '');
           });
        }
      });
    }

    /* Makes an element sticky so that when the page is scrolled past it will lock to the top
     * of the page.
     * @retrurn none
     */
    Global.Sticky = function(element) {
        Sticker.$element = element;
        Sticker.offset = Sticker.$element.offset();

        $window.scroll(function() {
          Global.SetSticky();
        });
    }

    Global.SetSticky = function() {
        if ($window.scrollTop() > Sticker.offset.top)
        {
            // is_sticky class is used to query whether the sticker is already sticky.  If so then
            // no need to continue.
            if ( Sticker.$element.hasClass("is_sticky") ) return;
            // set the sticker_standin height so there is no pop during scroll.
            $('#sticker_standin').height( Sticker.$element.height() );
            // move the sticker element into the sticky element
            Sticker.$element.addClass("is_sticky").appendTo("#sticky");
        } else {
            Global.UnsetSticky();
        }
    }

    Global.UnsetSticky = function() {
        if ( !Sticker.$element.hasClass("is_sticky") ) return;
        $('#sticker_standin').height(0);
        Sticker.$element.appendTo("#unsticky").removeClass("is_sticky");
    }

    // PRIVATE METHODS

    /* Sets the sticker element to be sticky at the top of the page.
     * @retrurn none
     */
    function SetSticky()
    {
        if ($window.scrollTop() > Sticker.offset.top)
        {
            // is_sticky class is used to query whether the sticker is already sticky.  If so then
            // no need to continue.
            if ( Sticker.$element.hasClass("is_sticky") ) return;
            // set the sticker_standin height so there is no pop during scroll.
            $('#sticker_standin').height( Sticker.$element.height() );
            // move the sticker element into the sticky element
            Sticker.$element.addClass("is_sticky").appendTo("#sticky");
        } else {
            UnsetSticky();
        }
    }

    /* Unsets the sticker element when the page is scrolled to the top
     * @retrurn none
     */
    function UnsetSticky()
    {
        if ( !Sticker.$element.hasClass("is_sticky") ) return;
        $('#sticker_standin').height(0);
        Sticker.$element.appendTo("#unsticky").removeClass("is_sticky");
    }
}( window.Global = window.Global || {}, jQuery ));

// Namespace Template
// http://stackoverflow.com/a/5947280/377205
(function( Browser, $, undefined ) {


    Browser.CurrentScreenSize = null;

    //Public Method
    Browser.isWidescreen = function() {
        if ( Browser.Ratio() > 1 ) {
            return true;
        }
        return false;
    }

    Browser.Ratio = function() {
        return $(window).width() / $(window).height();
    }

    Browser.isMobile = function() {
        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
            return true;
        }
        return false;
    }

    Browser.ScreenSize = function() {
        var width = $(window).width();
        if ( width > 1200 ) {
            return 5;
        }

        if ( width > 959 && width < 1200 ) {
            return 4;
            //return 'largest'
        }
        if ( width >= 768 && width < 959 ) {
            return 3;
            //return 'large'
        }
        if ( width >= 480 && width <= 767 ) {
            return 2;
            //return 'medium'
        }
        if ( width <= 479 ) {
            return 1;
            //return 'small'
        }
    }

    // Execute callback whenever the browser has changed screen sizes (responsive steps)
    Browser.OnScreenSizeChange = function(callback) {
      Browser.CurrentScreenSize = Browser.ScreenSize();
      $(window).resize(function() {
        if ( Browser.CurrentScreenSize !== Browser.ScreenSize() ) {
          if (typeof(callback) == "function") {
            callback(Browser.ScreenSize());
          }
          Browser.CurrentScreenSize = Browser.ScreenSize();
          return;
        }
        Browser.CurrentScreenSize = Browser.ScreenSize();
      });
    }

    Browser.ResizeEnd = function(callback, delay) {
        delay = typeof delay !== 'undefined' ? delay : 200;

        $(window).resize(function() {
            if(this.resizerTO) {
                clearTimeout(this.resizerTO);
            }
            this.resizerTO = setTimeout(function() {
                $(window).trigger('resizerEnd');
            }, delay);
        });
        // delay the marker fitting until the resize of the browser has come to a rest.
        $(window).bind('resizerEnd', function() {
            if (typeof(callback) == "function") {
                callback();
            }
        });
    };
}( window.Browser = window.Browser || {}, jQuery ));

(function( MapManager, $, undefined ) {
  var settings =  {'selector' : $('#map'),
                   'center' : [60.2519,102.1420],
                   'zoom' : 4
                  },
                  markers = {},
                  openedMarker = null,
                  map = null;

  MapManager.init = function(args) {
    settings = $.extend(settings,args);
  };

  MapManager.getMap = function() {
    return map;
  };

  MapManager.initMap = function() {
    // Default options for the map.
    var mapSettings = {center: settings.center,
                       map: {zoom: settings.zoom}, // google map options
                          //initOnLoad: false // wait until the user has requested to see the map.
                        };

    // Create the AC3Map
    map = settings.selector.AC3Map(mapSettings);
  };

  MapManager.showMap = function(visibility) {
    var d = $.Deferred();

    // Load the map if it hasn't been already.
    if( map === null ) {
      Global.LoadGoogleMap('MapManager.initMap', 'places');
      setInterval(function () {
        if (map !== null) {
          d.resolve(map);
        };
      }, 300);
    } else {
      d.resolve(map);
    }

    return d.promise();
  };

  MapManager.syncMarkers = function(points) {
    var pointsToAdd = $.extend({}, points);
    for (pointId in markers) {
      if (!keepMarker(pointId, pointsToAdd)) {
        markers[pointId].setMap(null);
        delete markers[pointId];
      } else {
        delete deletePoint(pointId, pointsToAdd);
      };
    };

    for (point in pointsToAdd) {
      MapManager.showMarker(pointsToAdd[point]);
    };

    return markers;
  }

  function deletePoint(pointId, points)
  {
    for (var point in points) {
      if (points[point].id == pointId) {
        delete points[point];
      }
    };
  }

  function keepMarker(pointId, points) {
    for (var point in points) {
      if (points[point].id == pointId) {
        return true;
      };
    };

    return false;
  }

  MapManager.showMarker = function(point) {
    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(point.lat,point.lng),
      map: map.gmap(),
      icon: '/img/icons/marker.png',
      title: point.name
    });

    var infoWindow = new google.maps.InfoWindow({
        content: point.name
    });

    google.maps.event.addListener(marker, 'click', function () {
      if (openedMarker !== null) {
        openedMarker.close();
      };
      openedMarker = infoWindow;
      infoWindow.open(map.gmap(), marker);
    });

    markers[point.id] = marker;

    return marker;
  }

  MapManager.showLocation = function(location)
  {
    map.gmap().setCenter(location);
    map.gmap().setZoom(14);
  }

  MapManager.showBounds = function(customMarkers) {
    var markersToBounds = customMarkers || markers;
    var bounds = new google.maps.LatLngBounds();

    var size = 0;
    for (marker in markersToBounds) {
      if (markersToBounds.hasOwnProperty(marker)) {
        size++;
        bounds.extend(markersToBounds[marker].getPosition());
      }
    }

    if (size > 0) {
      if (bounds.getNorthEast().equals(bounds.getSouthWest())) {
         var extendPoint1 = new google.maps.LatLng(bounds.getNorthEast().lat() + 0.01, bounds.getNorthEast().lng() + 0.01);
         var extendPoint2 = new google.maps.LatLng(bounds.getNorthEast().lat() - 0.01, bounds.getNorthEast().lng() - 0.01);
         bounds.extend(extendPoint1);
         bounds.extend(extendPoint2);
      }
      map.gmap().fitBounds(bounds);
    };
  }
}( window.MapManager = window.MapManager || {}, jQuery ));

// http://stackoverflow.com/a/3552496
var dateFormat=function(){var e=/d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,t=/\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,n=/[^-+\dA-Z]/g,r=function(e,t){e=String(e);t=t||2;while(e.length<t)e="0"+e;return e};return function(i,s,o){var u=dateFormat;if(arguments.length==1&&Object.prototype.toString.call(i)=="[object String]"&&!/\d/.test(i)){s=i;i=undefined}i=i?new Date(i):new Date;if(isNaN(i))throw SyntaxError("invalid date");s=String(u.masks[s]||s||u.masks["default"]);if(s.slice(0,4)=="UTC:"){s=s.slice(4);o=true}var a=o?"getUTC":"get",f=i[a+"Date"](),l=i[a+"Day"](),c=i[a+"Month"](),h=i[a+"FullYear"](),p=i[a+"Hours"](),d=i[a+"Minutes"](),v=i[a+"Seconds"](),m=i[a+"Milliseconds"](),g=o?0:i.getTimezoneOffset(),y={d:f,dd:r(f),ddd:u.i18n.dayNames[l],dddd:u.i18n.dayNames[l+7],m:c+1,mm:r(c+1),mmm:u.i18n.monthNames[c],mmmm:u.i18n.monthNames[c+12],yy:String(h).slice(2),yyyy:h,h:p%12||12,hh:r(p%12||12),H:p,HH:r(p),M:d,MM:r(d),s:v,ss:r(v),l:r(m,3),L:r(m>99?Math.round(m/10):m),t:p<12?"a":"p",tt:p<12?"am":"pm",T:p<12?"A":"P",TT:p<12?"AM":"PM",Z:o?"UTC":(String(i).match(t)||[""]).pop().replace(n,""),o:(g>0?"-":"+")+r(Math.floor(Math.abs(g)/60)*100+Math.abs(g)%60,4),S:["th","st","nd","rd"][f%10>3?0:(f%100-f%10!=10)*f%10]};return s.replace(e,function(e){return e in y?y[e]:e.slice(1,e.length-1)})}}();dateFormat.masks={"default":"ddd mmm dd yyyy HH:MM:ss",shortDate:"m/d/yy",mediumDate:"mmm d, yyyy",longDate:"mmmm d, yyyy",fullDate:"dddd, mmmm d, yyyy",shortTime:"h:MM TT",mediumTime:"h:MM:ss TT",longTime:"h:MM:ss TT Z",isoDate:"yyyy-mm-dd",isoTime:"HH:MM:ss",isoDateTime:"yyyy-mm-dd'T'HH:MM:ss",isoUtcDateTime:"UTC:yyyy-mm-dd'T'HH:MM:ss'Z'"};dateFormat.i18n={dayNames:["Sun","Mon","Tue","Wed","Thu","Fri","Sat","Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],monthNames:["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec","January","February","March","April","May","June","July","August","September","October","November","December"]};Date.prototype.format=function(e,t){return dateFormat(this,e,t)}
