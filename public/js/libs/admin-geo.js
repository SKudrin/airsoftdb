$(function () {
  $('.j-internal_map').click(function () {
    $('.j-internal_map_opened').prop("checked", !$('.j-internal_map_opened').is(':checked'));
    center = map.getCenter();
    if (center.lat() == 0 && center.lng() == 0) {
      var region = $("#region_id").length > 0 ? $("#region_id option:selected").text() : '';
      geocoder.geocode({ address: 'Россия ' + region},
        function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            // fillLatLng(results[0].geometry.location);
            map.setCenter(results[0].geometry.location);
          }
        }
      );
    } else if ($("#lat").val() !== '' && $("#lng").val() !== '') {
      showOnMap({lat: +$("#lat").val(), lng: +$("#lng").val()});
    };
    google.maps.event.trigger(map, "resize");
    map.setCenter(center);
  });
});

// addresses autocomplete
var autocomplete,
    map,
    center = {lat: +$("#lat").val(), lng: +$("#lng").val()},
    marker = undefined,
    geocoder;

function initGoogle() {
  initGeocoder();
  initAutocomplete();
  initMap();
}

function initGeocoder() {
  geocoder = new google.maps.Geocoder();
}

function initMap() {
  var element = document.getElementById('g-map');
  map = new google.maps.Map(element, {
      center: center,
      scrollwheel: false,
      zoom: 12
    });

  map.addListener('rightclick', function(e) {
    fillLatLng(e.latLng);
    showOnMap(e.latLng);
  });
}

function initAutocomplete() {
  var autocompleteId = $("#address").length > 0 ? "address" : undefined;

  if (autocompleteId !== undefined) {
    var element = document.getElementById(autocompleteId);
    autocomplete = new google.maps.places.Autocomplete(
      /** @type {!HTMLInputElement} */ element,
      {types: ['geocode']});

    element.addEventListener('focusout', getLocationByAddressField);
  };
}

function getLocationByAddressField() {
  var address = $(this).val(),
      region = $("#region_id").length > 0 ? $("#region_id option:selected").text() : '';

  geocoder.geocode({ address: 'Россия ' + region + ' ' + address},
    function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        fillLatLng(results[0].geometry.location);
      }
    }
  );
}

function showOnMap(location) {
  if (marker) marker.setMap(null);

  marker = new google.maps.Marker({
      map: map,
      position: location
  });
}

function fillLatLng(location) {
  var latContainer = $("#lat").length > 0 ? $("#lat") : undefined,
      lngContainer = $("#lng").length > 0 ? $("#lng") : undefined;

  if (latContainer && lngContainer && latContainer.val() == '' && lngContainer.val() == '') {
    latContainer.val(location.lat);
    lngContainer.val(location.lng);
    map.setCenter(location);
    showOnMap(location);
  } else if (latContainer && lngContainer) {
    if(confirm('Заменить координаты точки?')) {
      latContainer.val(location.lat);
      lngContainer.val(location.lng);
      map.setCenter(location);
      showOnMap(location);
    }
  };
}