(function( PublicListing, $, undefined ) {
    //Private Property
    var Settings = {toggleCanvas: true};
    var DirectionsService = null;
    var DirectionsDisplay = null;
    var DirectionsMarkers = [];
    PublicListing.Map = null;

    //Public Method
    PublicListing.init = function(args) {
        Settings = $.extend( Settings, args );

        // if ( Settings.page == 'location' ) {
        //     PublicListing.ShowMap();
        // }

        $('#tabs').AC3Tabs({
            onTabClick: function($element) {
                PublicListing.SetTab($element.data('page'));
            }
        });

        /*
        data-cycle-center-horz=true
         data-cycle-center-vert=true
         data-cycle-auto-height=true
         data-cycle-speed='500'
         data-cycle-fx=scrollHorz
         data-cycle-swipe-fx=scrollHorz
         data-cycle-swipe=true
         data-cycle-log="false"
         data-cycle-pause-on-hover="#slideshow_wrap"
         data-cycle-prev="#feature_cycle_prev"
         data-cycle-next="#feature_cycle_next"
         data-cycle-loader="true"
        */

        $('#slideshow').cycle({
            speed: 500,
            loader: true,
            prev: '#feature_cycle_prev',
            next: '#feature_cycle_next',
            swipe: true,
            log: false,
            pauseOnHover: "#slideshow_wrap",
            fx: 'scrollHorz',
            swipefx: 'scrollHorz',
            slides: '> div',
            pause: true,
            centerVert: true,
            centerHorz: true
        });


        // Get directions button click
        $('#get_directions').on('click', function(){
            // PublicListing.GetDirections();
        });

        // Clear directions button click
        $('#directions_clear').on('click', function(){
            // PublicListing.ClearDirections();
        });

        // Get directions on enter
        $('#directions_from').on('keypress', function(e){
            if (e.which == 13) {
                // PublicListing.GetDirections();
            }
        });

        // Show the map when you click on the address
        $('#address, .show_location').on('click', function() {
            PublicListing.SetTab('location');
            $('#tabs').AC3Tabs().SelectTab('location_tab');
        });


        $('#about .show_feature').on('click', function() {
            var $feature = $(this).parents('.feature');
            $feature.find('.show_feature').fadeOut(0);
            var $content = $feature.find('.feature_content');
            var height = 350;
            if ( Browser.ScreenSize() == 2 ) {
                height = 275;
            }
            if ( Browser.ScreenSize() == 1 ) {
                height = 235;
            }
            $content.animate({'height':height+'px'}, 300, function() {
                $feature.find('.hide_feature').fadeIn(300);
                $content.css('height', 'auto');
            });
        });

        $('#about .hide_feature').on('click', function() {
            var $feature = $(this).parents('.feature');
            $feature.find('.hide_feature').fadeOut(0);
            var $content = $feature.find('.feature_content');
            $content.animate({'height':'0px'}, 300, function() {
                $feature.find('.show_feature').fadeIn(300);
            });
        });

        $('#location .show_feature').on('click', function() {
            PublicListing.ShowFeature(true, $(this).parents('.feature'));
        });

        $('#location .hide_feature').on('click', function(){
            PublicListing.ShowFeature(false, $(this).parents('.feature'));
        });

        // // If the window is resized then update the map.
        // $(window).resize( function() {
        //     if ( PublicListing.Map ) {
        //         google.maps.event.trigger(PublicListing.Map.gmap(), "resize");
        //         PublicListing.Map.center();
        //     }
        // });

        if ( $('#slideshow').length != 0) {
            $('#slideshow').on('cycle-initialized', function( event, opts ) {
                $(window).trigger('resize');
            });
        }

        $('#slideshow').on('mouseleave', function() {
            $('#feature_cycle_prev').fadeTo(200, 0);
            $('#feature_cycle_next').fadeTo(200, 0);
        });

        $('#feature_cycle_prev').on('mouseenter', function() {
            $('#feature_cycle_prev').fadeTo(200, .7);
            $('#feature_cycle_next').fadeTo(200, .3);
        });
        $('#feature_cycle_next').on('mouseenter', function() {
            $('#feature_cycle_prev').fadeTo(200, .3);
            $('#feature_cycle_next').fadeTo(200, .7);
        });

        //$('#header-main div').last().before($('#edit_page_wrap'));

        $('a.fresco').on('click', function() {
            setTimeout(function() {
                var text = $('h1').text();
                var $win = $('.fr-window');
                if ( !$win.find('h1.fresco_header').length ) {
                    $('<h1 class="fresco_header">'+text+'</h1>').prependTo($win);
                }
            }, 200);
        });

        if ( typeof Report != 'undefined' ) {
            Report.init();
            $('#inappropriate').on('click', function(){
                Report.Open();
            });
        }

        $(window).load(function() {
            PublicListing.PromoteThumbs();
        });
    };

    // Promote all of the thumbnails to full res after the page is done loading and isn't doing anything
    PublicListing.PromoteThumbs = function() {
        $('img.promote').each(function() {
            $(this).attr('src', $(this).data('promote') );
        });
    }

    PublicListing.SetTab = function(p) {
        $('.page').fadeOut(0);
        $('#'+p).fadeIn(500, function() {
            /*
            if ( Browser.ScreenSize() === 1 ) {
                $("html, body").animate({ scrollTop: ($('#'+p).offset().top-50) }, 300);
            }
            */
        });

        if ( p == 'about' ) {
            // trigger resize to fix jquery cycle
            $(window).trigger('resize');
        }

        if ( p == 'location' ) {
            // MapManager.showBounds({});
            MapManager.showMap().done(function (map) {
                var markers = MapManager.syncMarkers({});
                MapManager.showBounds(markers);
              });
        }

        Global.UpdateURLQueryStringParam('p', p.replace('_tab', ''));
        //window.history.replaceState({}, "", p.replace('_tab', ''));
    };

    PublicListing.ShowFeature = function(visibility, $feature) {
        if( typeof(visibility)==='undefined') visibility = true;

        var $content = $feature.find('.feature_content');

        if ( visibility === true ) {

            if ( $feature.attr('id') === 'map_feature' ) {
                $feature.find('.show_feature').fadeOut(0);
                $content.animate({'height':'350px'}, 300, function(){
                    $feature.find('.hide_feature').fadeIn(300);
                });
            } else {
                $feature.find('.show_feature').fadeOut(0);
                Global.animate_height_auto($content, 300, true, function() {
                    $feature.find('.hide_feature').fadeIn(300);
                });
            }
        } else {
            $feature.find('.hide_feature').fadeOut(0);
            $content.animate({'height':'0px'}, 300, function(){
                $feature.find('.show_feature').fadeIn(300);
            });
        }


        //$('#feature').animate_height_auto
    }


    // PublicListing.ShowMap = function(visibility, callback) {
    //     if( typeof(visibility)==='undefined') visibility = true;
    //     if ( visibility === true ) {
    //         // Load the map if it hasn't been already.
    //         if( PublicListing.Map === null ) {
    //             Global.LoadGoogleMap('PublicListing.InitMap', 'places');
    //             return; // InitMap sets the visibility so don't continue.
    //         }
    //     }
    //     if (typeof(callback) == "function") {
    //         callback();
    //     }
    // }


    // PublicListing.LoadGoogleMap = function() {
    //     Global.LoadGoogleMap('PublicListing.InitMap', 'places');
    // }

    PublicListing.InitAddress = function() {
        Global.Google_Places_Autocomplete('directions_from', function(address) {
            if ( address == '' ) {
                return false;
            }
            // PublicListing.GetDirections();
            //return false;
        });
    }

    // PublicListing.InitMap = function() {

    //     // Initialize google address autocomplete
    //     PublicListing.InitAddress();

    //     // Default options for the map.
    //     var mapSettings = { selector: $('#map'),
    //                           center: false,
    //                           map: {zoom: 10}, // google map options
    //                           //initOnLoad: false // wait until the user has requested to see the map.
    //                         };

    //     var options = $.extend( mapSettings, Settings.map );



    //     // Create the AC3Map
    //     PublicListing.Map = options.selector.AC3Map(options);

    //     DirectionsDisplay = new google.maps.DirectionsRenderer({
    //         map: PublicListing.Map.gmap(),
    //         polylineOptions: new google.maps.Polyline({
    //                             strokeColor: '#e4a317',
    //                             strokeOpacity: .8,
    //                             strokeWeight: 4
    //                         }),
    //         panel: document.getElementById('driving_directions'),
    //         suppressMarkers: true,
    //     });
    //     DirectionsService = new google.maps.DirectionsService();

    //     var title = Settings.unit.name;
    //     if ( Settings.unit.address.street ) {
    //         title+= "<br/>"+Settings.unit.address.street;
    //     }
    //     if ( Settings.unit.address.city_state_zip ) {
    //         title+= "<br/>"+Settings.unit.address.city_state_zip;
    //     }

    //     // Add the marker
    //     var AC3marker = PublicListing.Map.add_marker(options.center, {id: Settings.uid,
    //                                             unit: Settings.unit.type,
    //                                             visible: true,
    //                                             url: '',
    //                                             title: title,
    //                                             //icon: Settings.unit.type
    //             });

    //     google.maps.event.addListenerOnce(PublicListing.Map.gmap(), 'tilesloaded', function() {
    //         if ( Browser.ScreenSize() > 2 ) {
    //             PublicListing.Map.select_marker(Settings.uid);
    //         }
    //         // do something only the first time the map is loaded
    //         google.maps.event.clearListeners(map, 'tilesloaded');
    //     });



    //     //AC3marker.bubble().open(); // disable the info bubble
    //     //AC3marker.bubble().set('map', null); // disable the info bubble

    //     $('#map_loading').remove();


    //     // $('#slideshow_wrap').fadeOut(0);
    //     // $('#map').fadeIn(0);
    // };


    // PublicListing.ClearDirections = function()
    // {
    //     $('#directions_from').val('');

    //     $('#driving_directions').animate({height: '0px'}, 500, function(){
    //         DirectionsDisplay.setDirections({routes: []});
    //         PublicListing.Map.center();
    //         PublicListing.Map.gmap().setZoom(10);

    //         // clear the directions
    //         for (var i = 0; i < DirectionsMarkers.length; i++) {
    //             DirectionsMarkers[i].setMap(null);
    //         }
    //         DirectionsMarkers = [];
    //     });
    //     /*
    //     $('#driving_directions').fadeOut(300, function(){
    //         DirectionsDisplay.setDirections({routes: []});
    //         PublicListing.Map.center();
    //     });
    //     */
    //     $('#directions_clear').fadeOut(300);
    // }

    // PublicListing.GetDirections = function()
    // {
    //     // var start = $('#directions_from').val();
    //     // var end =  Settings.map.center[0] + "," + Settings.map.center[1];
    //     // var request = {
    //     //     origin:start,
    //     //     destination:end,
    //     //     travelMode: google.maps.TravelMode.DRIVING
    //     // };

    //     DirectionsService.route(request, function(response, status) {
    //         if (status == google.maps.DirectionsStatus.OK) {
    //             DirectionsDisplay.setDirections(response);

    //             // http://googlemaps.googlermania.com/google_maps_api_v3/en/map_example_direction_customicon.html
    //             var myRoute = response.routes[0].legs[0];
    //             for (var i = 0; i < myRoute.steps.length; i++) {
    //                 var icon = "https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=" + i + "|E4A317|000000";
    //                 var visible = false;
    //                 if (i == 0) {
    //                     icon = "/resources/icons/map/start.png";
    //                     visible = true;
    //                 }

    //                 var marker = new google.maps.Marker({
    //                     position: myRoute.steps[i].start_point,
    //                     map: PublicListing.Map.gmap(),
    //                     icon: icon,
    //                     visible: visible
    //                 });

    //                 DirectionsMarkers.push(marker);
    //                 //attachInstructionText(marker, myRoute.steps[i].instructions);
    //                 //markerArray.push(marker);
    //             }

    //             google.maps.event.addListenerOnce(PublicListing.Map.gmap(), 'idle', function() {
    //                 //$('#driving_directions').fadeIn(0);
    //                 Global.animate_height_auto($('#driving_directions'), 500);
    //                 $('#directions_clear').fadeIn(500);
    //                 //$('#driving_directions').fadeIn(300);
    //             });

    //             /*
    //             // try forcing the map to refresh and then listen for tilesloaded or something
    //             google.maps.event.addListenerOnce(PublicListing.Map.gmap(), 'center_changed', function(){
    //                 setTimeout(function(){
    //                     //Global.animate_height_auto($('#driving_directions'), 800);
    //                     $('#driving_directions').fadeIn(300);
    //                 }, 100);
    //             });
    //             */
    //         }
    //   });
    // };
}( window.PublicListing = window.PublicListing || {}, jQuery ));