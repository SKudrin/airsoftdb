<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCodeFieldToRegions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('regions', function (Blueprint $table) {
            $table->string('code')->nullable();
        });

        \DB::statement("UPDATE regions SET code = 'RU-AD' WHERE name = 'АДЫГЕЯ'");
        \DB::statement("UPDATE regions SET code = 'RU-AL' WHERE name = 'АЛТАЙ'");
        \DB::statement("UPDATE regions SET code = 'RU-BA' WHERE name = 'БАШКОРТОСТАН'");
        \DB::statement("UPDATE regions SET code = 'RU-BU' WHERE name = 'БУРЯТИЯ'");
        \DB::statement("UPDATE regions SET code = 'RU-DA' WHERE name = 'ДАГЕСТАН'");
        \DB::statement("UPDATE regions SET code = 'RU-SE' WHERE name = 'СЕВЕРНАЯ ОСЕТИЯ - АЛАНИЯ'");
        \DB::statement("UPDATE regions SET code = 'RU-KB' WHERE name = 'КАБАРДИНО-БАЛКАРИЯ'");
        \DB::statement("UPDATE regions SET code = 'RU-KL' WHERE name = 'КАЛМЫКИЯ'");
        \DB::statement("UPDATE regions SET code = 'RU-KC' WHERE name = 'КАРАЧАЕВО-ЧЕРКЕСИЯ'");
        \DB::statement("UPDATE regions SET code = 'RU-KR' WHERE name = 'КАРЕЛИЯ'");
        \DB::statement("UPDATE regions SET code = 'RU-KO' WHERE name = 'КОМИ'");
        \DB::statement("UPDATE regions SET code = 'RU-ME' WHERE name = 'МАРИЙ ЭЛ'");
        \DB::statement("UPDATE regions SET code = 'RU-MO' WHERE name = 'МОРДОВИЯ'");
        \DB::statement("UPDATE regions SET code = 'RU-SA' WHERE name = 'САХА (ЯКУТИЯ)'");
        \DB::statement("UPDATE regions SET code = 'RU-IN' WHERE name = 'ИНГУШЕТИЯ'");
        \DB::statement("UPDATE regions SET code = 'RU-TA' WHERE name = 'ТАТАРСТАН'");
        \DB::statement("UPDATE regions SET code = 'RU-TY' WHERE name = 'ТЫВА'");
        \DB::statement("UPDATE regions SET code = 'RU-UD' WHERE name = 'УДМУРТИЯ'");
        \DB::statement("UPDATE regions SET code = 'RU-KK' WHERE name = 'ХАКАСИЯ'");
        \DB::statement("UPDATE regions SET code = 'RU-CE' WHERE name = 'ЧЕЧНЯ'");
        \DB::statement("UPDATE regions SET code = 'RU-CU' WHERE name = 'ЧУВАШИЯ'");
        \DB::statement("UPDATE regions SET code = 'RU-ALT' WHERE name = 'АЛТАЙСКИЙ КРАЙ'");
        \DB::statement("UPDATE regions SET code = 'RU-ZAB' WHERE name = 'ЗАБАЙКАЛЬСКИЙ КРАЙ'");
        \DB::statement("UPDATE regions SET code = 'RU-KAM' WHERE name = 'КАМЧАТСКИЙ КРАЙ'");
        \DB::statement("UPDATE regions SET code = 'RU-KDA' WHERE name = 'КРАСНОДАРСКИЙ КРАЙ'");
        \DB::statement("UPDATE regions SET code = 'RU-KYA' WHERE name = 'КРАСНОЯРСКИЙ КРАЙ'");
        \DB::statement("UPDATE regions SET code = 'RU-PER' WHERE name = 'ПЕРМСКИЙ КРАЙ'");
        \DB::statement("UPDATE regions SET code = 'RU-PRI' WHERE name = 'ПРИМОРСКИЙ КРАЙ'");
        \DB::statement("UPDATE regions SET code = 'RU-STA' WHERE name = 'СТАВРОПОЛЬСКИЙ КРАЙ'");
        \DB::statement("UPDATE regions SET code = 'RU-KHA' WHERE name = 'ХАБАРОВСКИЙ КРАЙ'");
        \DB::statement("UPDATE regions SET code = 'RU-AMU' WHERE name = 'АМУРСКАЯ ОБЛ.'");
        \DB::statement("UPDATE regions SET code = 'RU-ARK' WHERE name = 'АРХАНГЕЛЬСКАЯ ОБЛ.'");
        \DB::statement("UPDATE regions SET code = 'RU-AST' WHERE name = 'АСТРАХАНСКАЯ ОБЛ.'");
        \DB::statement("UPDATE regions SET code = 'RU-BEL' WHERE name = 'БЕЛГОРОДСКАЯ ОБЛ.'");
        \DB::statement("UPDATE regions SET code = 'RU-BRY' WHERE name = 'БРЯНСКАЯ ОБЛ.'");
        \DB::statement("UPDATE regions SET code = 'RU-VLA' WHERE name = 'ВЛАДИМИРСКАЯ ОБЛ.'");
        \DB::statement("UPDATE regions SET code = 'RU-VGG' WHERE name = 'ВОЛГОГРАДСКАЯ ОБЛ.'");
        \DB::statement("UPDATE regions SET code = 'RU-VLG' WHERE name = 'ВОЛОГОДСКАЯ ОБЛ.'");
        \DB::statement("UPDATE regions SET code = 'RU-VOR' WHERE name = 'ВОРОНЕЖСКАЯ ОБЛ.'");
        \DB::statement("UPDATE regions SET code = 'RU-IVA' WHERE name = 'ИВАНОВСКАЯ ОБЛ.'");
        \DB::statement("UPDATE regions SET code = 'RU-IRK' WHERE name = 'ИРКУТСКАЯ ОБЛ.'");
        \DB::statement("UPDATE regions SET code = 'RU-KGD' WHERE name = 'КАЛИНИНГРАДСКАЯ ОБЛ.'");
        \DB::statement("UPDATE regions SET code = 'RU-KLU' WHERE name = 'КАЛУЖСКАЯ ОБЛ.'");
        \DB::statement("UPDATE regions SET code = 'RU-KEM' WHERE name = 'КЕМЕРОВСКАЯ ОБЛ.'");
        \DB::statement("UPDATE regions SET code = 'RU-KIR' WHERE name = 'КИРОВСКАЯ ОБЛ.'");
        \DB::statement("UPDATE regions SET code = 'RU-KOS' WHERE name = 'КОСТРОМСКАЯ ОБЛ.'");
        \DB::statement("UPDATE regions SET code = 'RU-KGN' WHERE name = 'КУРГАНСКАЯ ОБЛ.'");
        \DB::statement("UPDATE regions SET code = 'RU-KRS' WHERE name = 'КУРСКАЯ ОБЛ.'");
        \DB::statement("UPDATE regions SET code = 'RU-LEN' WHERE name = 'ЛЕНИНГРАДСКАЯ ОБЛ.'");
        \DB::statement("UPDATE regions SET code = 'RU-LIP' WHERE name = 'ЛИПЕЦКАЯ ОБЛ.'");
        \DB::statement("UPDATE regions SET code = 'RU-MAG' WHERE name = 'МАГАДАНСКАЯ ОБЛ.'");
        \DB::statement("UPDATE regions SET code = 'RU-MOS' WHERE name = 'МОСКОВСКАЯ ОБЛ.'");
        \DB::statement("UPDATE regions SET code = 'RU-MUR' WHERE name = 'МУРМАНСКАЯ ОБЛ.'");
        \DB::statement("UPDATE regions SET code = 'RU-NIZ' WHERE name = 'НИЖЕГОРОДСКАЯ ОБЛ.'");
        \DB::statement("UPDATE regions SET code = 'RU-NGR' WHERE name = 'НОВГОРОДСКАЯ ОБЛ.'");
        \DB::statement("UPDATE regions SET code = 'RU-NVS' WHERE name = 'НОВОСИБИРСКАЯ ОБЛ.'");
        \DB::statement("UPDATE regions SET code = 'RU-OMS' WHERE name = 'ОМСКАЯ ОБЛ.'");
        \DB::statement("UPDATE regions SET code = 'RU-ORE' WHERE name = 'ОРЕНБУРГСКАЯ ОБЛ.'");
        \DB::statement("UPDATE regions SET code = 'RU-ORL' WHERE name = 'ОРЛОВСКАЯ ОБЛ.'");
        \DB::statement("UPDATE regions SET code = 'RU-PNZ' WHERE name = 'ПЕНЗЕНСКАЯ ОБЛ.'");
        \DB::statement("UPDATE regions SET code = 'RU-PSK' WHERE name = 'ПСКОВСКАЯ ОБЛ.'");
        \DB::statement("UPDATE regions SET code = 'RU-ROS' WHERE name = 'РОСТОВСКАЯ ОБЛ.'");
        \DB::statement("UPDATE regions SET code = 'RU-RYA' WHERE name = 'РЯЗАНСКАЯ ОБЛ.'");
        \DB::statement("UPDATE regions SET code = 'RU-SAM' WHERE name = 'САМАРСКАЯ ОБЛ.'");
        \DB::statement("UPDATE regions SET code = 'RU-SAR' WHERE name = 'САРАТОВСКАЯ ОБЛ.'");
        \DB::statement("UPDATE regions SET code = 'RU-SAK' WHERE name = 'САХАЛИНСКАЯ ОБЛ.'");
        \DB::statement("UPDATE regions SET code = 'RU-SVE' WHERE name = 'СВЕРДЛОВСКАЯ ОБЛ.'");
        \DB::statement("UPDATE regions SET code = 'RU-SMO' WHERE name = 'СМОЛЕНСКАЯ ОБЛ.'");
        \DB::statement("UPDATE regions SET code = 'RU-TAM' WHERE name = 'ТАМБОВСКАЯ ОБЛ.'");
        \DB::statement("UPDATE regions SET code = 'RU-TVE' WHERE name = 'ТВЕРСКАЯ ОБЛ.'");
        \DB::statement("UPDATE regions SET code = 'RU-TOM' WHERE name = 'ТОМСКАЯ ОБЛ.'");
        \DB::statement("UPDATE regions SET code = 'RU-TUL' WHERE name = 'ТУЛЬСКАЯ ОБЛ.'");
        \DB::statement("UPDATE regions SET code = 'RU-TYU' WHERE name = 'ТЮМЕНСКАЯ ОБЛ.'");
        \DB::statement("UPDATE regions SET code = 'RU-ULY' WHERE name = 'УЛЬЯНОВСКАЯ ОБЛ.'");
        \DB::statement("UPDATE regions SET code = 'RU-CHE' WHERE name = 'ЧЕЛЯБИНСКАЯ ОБЛ.'");
        \DB::statement("UPDATE regions SET code = 'RU-YAR' WHERE name = 'ЯРОСЛАВСКАЯ ОБЛ.'");
        \DB::statement("UPDATE regions SET code = 'RU-MOS' WHERE name = 'МОСКВА'");
        \DB::statement("UPDATE regions SET code = 'RU-SPE' WHERE name = 'САНКТ-ПЕТЕРБУРГ'");
        \DB::statement("UPDATE regions SET code = 'RU-YEV' WHERE name = 'ЕВРЕЙСКАЯ АВТ. ОБЛ.'");
        \DB::statement("UPDATE regions SET code = 'RU-NEN' WHERE name = 'НЕНЕЦКИЙ АО'");
        \DB::statement("UPDATE regions SET code = 'RU-KHM' WHERE name = 'ХАНТЫ-МАНСИЙСКИЙ АО'");
        \DB::statement("UPDATE regions SET code = 'RU-CHU' WHERE name = 'ЧУКОТСКИЙ АО'");
        \DB::statement("UPDATE regions SET code = 'RU-YAN' WHERE name = 'ЯМАЛО-НЕНЕЦКИЙ АО'");
        \DB::statement("UPDATE regions SET code = '' WHERE name = 'РЕСПУБЛИКА КРЫМ'");
        \DB::statement("UPDATE regions SET code = '' WHERE name = 'СЕВАСТОПОЛЬ'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('regions', function (Blueprint $table) {
            $table->dropColumn('code');
        });
    }
}
