<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRevisionsPropsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('revisions_props', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('revision_id')->unsigned();
            $table->foreign('revision_id')->references('id')->on('revisions');
            $table->string('key');
            $table->string('value');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('revisions_props', function (Blueprint $table) {
            $table->dropForeign('revision_id_foreign');
        });

        Schema::drop('revisions_props');
    }
}
