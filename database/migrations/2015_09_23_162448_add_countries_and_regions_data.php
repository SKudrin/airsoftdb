<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCountriesAndRegionsData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("INSERT INTO countries (id, name, created_at, updated_at) VALUES (nextval('countries_id_seq'), 'РОССИЯ', now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'АДЫГЕЯ', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'АЛТАЙ', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'БАШКОРТОСТАН', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'БУРЯТИЯ', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'ДАГЕСТАН', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'СЕВЕРНАЯ ОСЕТИЯ - АЛАНИЯ', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'КАБАРДИНО-БАЛКАРИЯ', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'КАЛМЫКИЯ', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'КАРАЧАЕВО-ЧЕРКЕСИЯ', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'КАРЕЛИЯ', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'КОМИ', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'МАРИЙ ЭЛ', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'МОРДОВИЯ', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'САХА (ЯКУТИЯ)', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'ИНГУШЕТИЯ', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'ТАТАРСТАН', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'ТЫВА', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'УДМУРТИЯ', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'ХАКАСИЯ', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'ЧЕЧНЯ', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'ЧУВАШИЯ', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'АЛТАЙСКИЙ КРАЙ', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'ЗАБАЙКАЛЬСКИЙ КРАЙ', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'КАМЧАТСКИЙ КРАЙ', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'КРАСНОДАРСКИЙ КРАЙ', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'КРАСНОЯРСКИЙ КРАЙ', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'ПЕРМСКИЙ КРАЙ', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'ПРИМОРСКИЙ КРАЙ', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'СТАВРОПОЛЬСКИЙ КРАЙ', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'ХАБАРОВСКИЙ КРАЙ', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'АМУРСКАЯ ОБЛ.', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'АРХАНГЕЛЬСКАЯ ОБЛ.', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'АСТРАХАНСКАЯ ОБЛ.', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'БЕЛГОРОДСКАЯ ОБЛ.', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'БРЯНСКАЯ ОБЛ.', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'ВЛАДИМИРСКАЯ ОБЛ.', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'ВОЛГОГРАДСКАЯ ОБЛ.', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'ВОЛОГОДСКАЯ ОБЛ.', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'ВОРОНЕЖСКАЯ ОБЛ.', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'ИВАНОВСКАЯ ОБЛ.', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'ИРКУТСКАЯ ОБЛ.', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'КАЛИНИНГРАДСКАЯ ОБЛ.', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'КАЛУЖСКАЯ ОБЛ.', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'КЕМЕРОВСКАЯ ОБЛ.', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'КИРОВСКАЯ ОБЛ.', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'КОСТРОМСКАЯ ОБЛ.', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'КУРГАНСКАЯ ОБЛ.', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'КУРСКАЯ ОБЛ.', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'ЛЕНИНГРАДСКАЯ ОБЛ.', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'ЛИПЕЦКАЯ ОБЛ.', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'МАГАДАНСКАЯ ОБЛ.', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'МОСКОВСКАЯ ОБЛ.', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'МУРМАНСКАЯ ОБЛ.', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'НИЖЕГОРОДСКАЯ ОБЛ.', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'НОВГОРОДСКАЯ ОБЛ.', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'НОВОСИБИРСКАЯ ОБЛ.', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'ОМСКАЯ ОБЛ.', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'ОРЕНБУРГСКАЯ ОБЛ.', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'ОРЛОВСКАЯ ОБЛ.', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'ПЕНЗЕНСКАЯ ОБЛ.', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'ПСКОВСКАЯ ОБЛ.', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'РОСТОВСКАЯ ОБЛ.', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'РЯЗАНСКАЯ ОБЛ.', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'САМАРСКАЯ ОБЛ.', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'САРАТОВСКАЯ ОБЛ.', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'САХАЛИНСКАЯ ОБЛ.', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'СВЕРДЛОВСКАЯ ОБЛ.', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'СМОЛЕНСКАЯ ОБЛ.', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'ТАМБОВСКАЯ ОБЛ.', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'ТВЕРСКАЯ ОБЛ.', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'ТОМСКАЯ ОБЛ.', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'ТУЛЬСКАЯ ОБЛ.', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'ТЮМЕНСКАЯ ОБЛ.', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'УЛЬЯНОВСКАЯ ОБЛ.', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'ЧЕЛЯБИНСКАЯ ОБЛ.', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'ЯРОСЛАВСКАЯ ОБЛ.', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'МОСКВА', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'САНКТ-ПЕТЕРБУРГ', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'ЕВРЕЙСКАЯ АВТ. ОБЛ.', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'НЕНЕЦКИЙ АО', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'ХАНТЫ-МАНСИЙСКИЙ АО', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'ЧУКОТСКИЙ АО', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'ЯМАЛО-НЕНЕЦКИЙ АО', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'РЕСПУБЛИКА КРЫМ', (SELECT currval('countries_id_seq')), now(), now())");
        \DB::statement("INSERT INTO regions (id, name, country_id, created_at, updated_at) VALUES (nextval('regions_id_seq'), 'СЕВАСТОПОЛЬ', (SELECT currval('countries_id_seq')), now(), now())");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
