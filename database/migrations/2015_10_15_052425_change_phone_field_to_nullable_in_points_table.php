<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangePhoneFieldToNullableInPointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("ALTER TABLE points ALTER COLUMN phone DROP NOT NULL");
        \DB::statement("ALTER TABLE points ALTER COLUMN description DROP NOT NULL");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement("ALTER TABLE points ALTER COLUMN phone SET NOT NULL");
        \DB::statement("ALTER TABLE points ALTER COLUMN description SET NOT NULL");
    }
}
