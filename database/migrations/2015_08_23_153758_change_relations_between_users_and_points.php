<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeRelationsBetweenUsersAndPoints extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('point_user', function (Blueprint $table) {
            $table->dropForeign('point_user_user_id_foreign');
            $table->dropForeign('point_user_point_id_foreign');
        });

        Schema::drop('point_user');

        Schema::table('points', function (Blueprint $table) {
            $table->tinyInteger('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('point_user', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('point_id')->unsigned();
            $table->foreign('point_id')->references('id')->on('points');
            $table->tinyInteger('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::table('points', function (Blueprint $table) {
            $table->dropForeign('points_user_id_foreign');
            $table->dropColumn('user_id');
        });
    }
}
