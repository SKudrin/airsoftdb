<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

// USERS
$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
        'is_admin' => false,
        'is_active' => true
    ];
});

$factory->defineAs(App\Models\User::class, 'admin', function (Faker\Generator $faker) use ($factory) {
    $user = $factory->raw(App\Models\User::class);

    return array_merge($user, ['is_admin' => true]);
});

$factory->defineAs(App\Models\User::class, 'trust', function (Faker\Generator $faker) use ($factory) {
    $user = $factory->raw(App\Models\User::class);

    return array_merge($user, ['is_trust' => true]);
});

$factory->defineAs(App\Models\User::class, 'inactive_user', function (Faker\Generator $faker) use ($factory) {
    $user = $factory->raw(App\Models\User::class);

    return array_merge($user, ['is_active' => false]);
});

$factory->defineAs(App\Models\User::class, 'inactive_admin', function (Faker\Generator $faker) use ($factory) {
    $user = $factory->raw(App\Models\User::class);

    return array_merge($user, ['is_admin' => true, 'is_active' => false]);
});

// POINTS TYPES
$factory->define(App\Models\PointsType::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
        'description' => $faker->text
    ];
});

// COUNTRIES
$factory->define(App\Models\Country::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word
    ];
});

// REGIONS
$factory->define(App\Models\Region::class, function (Faker\Generator $faker) use ($factory) {
    $country = $factory->create(App\Models\Country::class);

    return [
        'name' => $faker->word,
        'country_id' => $country->id
    ];
});

// POINTS
$factory->define(App\Models\Point::class, function (Faker\Generator $faker) use ($factory) {
    $pointsType = $factory->create(App\Models\PointsType::class);
    $region = $factory->create(App\Models\Region::class);

    return [
        'name' => $faker->word,
        'phone' => $faker->numerify('###########'),
        'address' => $faker->address,
        'description' => $faker->text,
        'lat' => $faker->randomFloat,
        'lng' => $faker->randomFloat,
        'type_id' => $pointsType->id,
        'region_id' => $region->id
    ];
});
