<?php

class TestCase extends Illuminate\Foundation\Testing\TestCase
{
    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost';

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();

        return $app;
    }

    public function seeRelationBelongsTo($to, $object)
    {
        $object = $object->fresh();

        $this->assertNotTrue(null === $object->$to, sprintf(
            'Relation [%s] BelongsTo [%s] not exists.', get_class($object), $to
        ));

        return $this;
    }

    public function notSeeRelationBelongsTo($to, $object)
    {
        $object = $object->fresh();

        $this->assertTrue(null === $object->$to, sprintf(
            'Found unexpected relation [%s] BelongsTo [%s].', get_class($object), $to
        ));

        return $this;
    }
}
