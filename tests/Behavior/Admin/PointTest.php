<?php
namespace Tests\Behavior\Admin;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PointTest extends \TestCase
{
    use DatabaseTransactions;

    /**
     * Point index page
     * Must get 200 response
     *
     * @return void
     */
    public function testPointIndex()
    {
        $admin = factory('App\Models\User', 'admin')->make();

        $this->actingAs($admin)
             ->visit(route('admin.point.index'))
             ->assertResponseStatus(200);
    }

    /**
     * Point show page
     * Must get 200 response
     *
     * @return void
     */
    public function testPointShow()
    {
        $admin = factory('App\Models\User', 'admin')->make();

        $point = factory('App\Models\Point')->make();

        $this->actingAs($admin)
             ->visit(route('admin.point.show', $point))
             ->assertResponseStatus(200);
    }

    /**
     * Point create success
     * Must storage new point
     *
     * @return void
     */
    public function testPointCreate()
    {
        $admin = factory('App\Models\User', 'admin')->make();

        $pointsType = factory('App\Models\PointsType')->create();
        $region = factory('App\Models\Region')->create();

        $pointData = ['name' => 'Shop',
                      'phone' => '87776665544',
                      'address' => 'Pushkins Street Kulotushkins House',
                      'description' => 'Description',
                      'lat' => '8.94444',
                      'lng' => '8.95555',
                      'type_id' => $pointsType->id,
                      'region_id' => $region->id
                      ];

        $this->actingAs($admin)
             ->visit(route('admin.point.create'))
             ->type($pointData['name'], 'name')
             ->type($pointData['phone'], 'phone')
             ->type($pointData['address'], 'address')
             ->type($pointData['description'], 'description')
             ->type($pointData['lat'], 'lat')
             ->type($pointData['lng'], 'lng')
             ->type($pointData['type_id'], 'type_id')
             ->type($pointData['region_id'], 'region_id')
             ->press('Submit')
             ->seeInDatabase('points', $pointData);
    }

    /**
     * Point create fail
     * Must redirect to point create page and don't storage new point
     *
     * @return void
     */
    public function testPointCreateValidationFail()
    {
        $admin = factory('App\Models\User', 'admin')->make();

        $pointsType = factory('App\Models\PointsType')->create();

        // wrong values name, phone and missing type_id
        $pointData = ['name' => '',
                      'phone' => '877476665544',
                      'address' => 'Pushkins Street Kulotushkins House',
                      'description' => 'Description',
                      'lat' => '8.94444',
                      'lng' => '8.95555'
                      ];

        $this->actingAs($admin)
             ->visit(route('admin.point.create'))
             ->type($pointData['name'], 'name')
             ->type($pointData['phone'], 'phone')
             ->type($pointData['address'], 'address')
             ->type($pointData['description'], 'description')
             ->type($pointData['lat'], 'lat')
             ->type($pointData['lng'], 'lng')
             ->press('Submit')
             ->notSeeInDatabase('points', $pointData)
             ->seePageIs(route('admin.point.create'));
    }

    /**
     * Point edit success
     * Must storage changes of point
     *
     * @return void
     */
    public function testPointEdit()
    {
        $admin = factory('App\Models\User', 'admin')->make();

        $user = factory('App\Models\User')->create();
        $pointsType = factory('App\Models\PointsType')->create();
        $region = factory('App\Models\Region')->create();

        $point = factory('App\Models\Point')->create();

        $pointData = ['name' => 'Shop',
                      'phone' => '87776665544',
                      'address' => 'Pushkins Street Kulotushkins House',
                      'description' => 'Description',
                      'lat' => '8.94444',
                      'lng' => '8.95555',
                      'type_id' => $pointsType->id,
                      'user_id' => $user->id,
                      'region_id' => $region->id
                      ];

        $this->actingAs($admin)
             ->visit(route('admin.point.edit', $point))
             ->type($pointData['name'], 'name')
             ->type($pointData['phone'], 'phone')
             ->type($pointData['address'], 'address')
             ->type($pointData['description'], 'description')
             ->type($pointData['lat'], 'lat')
             ->type($pointData['lng'], 'lng')
             ->type($pointData['type_id'], 'type_id')
             ->type($pointData['user_id'], 'user_id')
             ->type($pointData['region_id'], 'region_id')
             ->press('Submit')
             ->seeInDatabase('points', $pointData);
    }

    /**
     * Point edit fail
     * Must redirect to point edit page
     *
     * @return void
     */
    public function testPointEditValidationFail()
    {
        $admin = factory('App\Models\User', 'admin')->make();

        $point = factory('App\Models\Point')->create();

        // wrong values name, phone and missing type_id
        $pointData = ['name' => '',
                      'phone' => '877476665544',
                      'address' => 'Pushkins Street Kulotushkins House',
                      'description' => 'Description',
                      'lat' => '8.94444',
                      'lng' => '8.95555'
                      ];

        $this->actingAs($admin)
             ->visit(route('admin.point.edit', $point))
             ->type($pointData['name'], 'name')
             ->type($pointData['phone'], 'phone')
             ->type($pointData['address'], 'address')
             ->type($pointData['description'], 'description')
             ->type($pointData['lat'], 'lat')
             ->type($pointData['lng'], 'lng')
             ->press('Submit')
             ->notSeeInDatabase('points', $pointData)
             ->seePageIs(route('admin.point.edit', $point));
    }
}