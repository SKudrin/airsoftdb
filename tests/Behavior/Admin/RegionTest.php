<?php
namespace Tests\Behavior\Admin;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RegionTest extends \TestCase
{
    use DatabaseTransactions;

    /**
     * Region index page
     * Must get 200 response
     *
     * @return void
     */
    public function testRegionIndex()
    {
        $admin = factory('App\Models\User', 'admin')->make();

        $this->actingAs($admin)
             ->visit(route('admin.region.index'))
             ->assertResponseStatus(200);
    }

    /**
     * Region show page
     * Must get 200 response
     *
     * @return void
     */
    public function testRegionShow()
    {
        $admin = factory('App\Models\User', 'admin')->make();

        $region = factory('App\Models\Region')->make();

        $this->actingAs($admin)
             ->visit(route('admin.region.show', $region))
             ->assertResponseStatus(200);
    }

    /**
     * Region create success
     * Must storage new region
     *
     * @return void
     */
    public function testRegionCreate()
    {
        $admin = factory('App\Models\User', 'admin')->make();

        $country = factory('App\Models\Country')->create();

        $regionData = ['name' => 'Moscow',
                       'country_id' => $country->id
                      ];

        $this->actingAs($admin)
             ->visit(route('admin.region.create'))
             ->type($regionData['name'], 'name')
             ->type($regionData['country_id'], 'country_id')
             ->press('Submit')
             ->seeInDatabase('regions', $regionData);
    }

    /**
     * Region create fail
     * Must redirect to region create page and don't storage new region
     *
     * @return void
     */
    public function testRegionCreateValidationFail()
    {
        $admin = factory('App\Models\User', 'admin')->make();

        $country = factory('App\Models\Country')->create();

        // wrong name value and missing country_id
        $regionData = ['name' => ''];

        $this->actingAs($admin)
             ->visit(route('admin.region.create'))
             ->type($regionData['name'], 'name')
             ->press('Submit')
             ->notSeeInDatabase('regions', $regionData)
             ->seePageIs(route('admin.region.create'));
    }

    /**
     * Region edit success
     * Must storage changes of region
     *
     * @return void
     */
    public function testRegionEdit()
    {
        $admin = factory('App\Models\User', 'admin')->make();

        $country = factory('App\Models\Country')->create();
        $region = factory('App\Models\Region')->create();

        $regionData = ['name' => 'Moscow',
                       'country_id' => $country->id
                      ];

        $this->actingAs($admin)
             ->visit(route('admin.region.edit', $region))
             ->type($regionData['name'], 'name')
             ->type($regionData['country_id'], 'country_id')
             ->press('Submit')
             ->seeInDatabase('regions', $regionData);
    }

    /**
     * Region edit fail
     * Must redirect to region edit page
     *
     * @return void
     */
    public function testRegionEditValidationFail()
    {
        $admin = factory('App\Models\User', 'admin')->make();

        $region = factory('App\Models\Region')->create();

        // wrong name value missing country_id
        $regionData = ['name' => ''];

        $this->actingAs($admin)
             ->visit(route('admin.region.edit', $region))
             ->type($regionData['name'], 'name')
             ->press('Submit')
             ->notSeeInDatabase('regions', $regionData)
             ->seePageIs(route('admin.region.edit', $region));
    }
}