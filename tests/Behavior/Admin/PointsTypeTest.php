<?php
namespace Tests\Behavior\Admin;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PointsTypeTest extends \TestCase
{
    use DatabaseTransactions;

    /**
     * PointsType index page
     * Must get 200 response
     *
     * @return void
     */
    public function testPointsTypeIndex()
    {
        $admin = factory('App\Models\User', 'admin')->make();

        $this->actingAs($admin)
             ->visit(route('admin.pointsType.index'))
             ->assertResponseStatus(200);
    }

    /**
     * PointsType show page
     * Must get 200 response
     *
     * @return void
     */
    public function testPointsTypeShow()
    {
        $admin = factory('App\Models\User', 'admin')->make();

        $pointsType = factory('App\Models\PointsType')->make();

        $this->actingAs($admin)
             ->visit(route('admin.pointsType.show', $pointsType))
             ->assertResponseStatus(200);
    }

    /**
     * PointsType create success
     * Must storage new pointsType
     *
     * @return void
     */
    public function testPointsTypeCreate()
    {
        $admin = factory('App\Models\User', 'admin')->make();

        $pointsTypeData = ['name' => 'Shops',
                           'description' => 'Description'];

        $this->actingAs($admin)
             ->visit(route('admin.pointsType.create'))
             ->type($pointsTypeData['name'], 'name')
             ->type($pointsTypeData['description'], 'description')
             ->press('Submit')
             ->seeInDatabase('points_types', $pointsTypeData);
    }

    /**
     * PointsType create fail
     * Must redirect to pointsType create page and don't storage new pointsType
     *
     * @return void
     */
    public function testPointsTypeCreateValidationFail()
    {
        $admin = factory('App\Models\User', 'admin')->make();

        // wrong name value
        $pointsTypeData = ['name' => '',
                           'description' => 'Description'];

        $this->actingAs($admin)
             ->visit(route('admin.pointsType.create'))
             ->type($pointsTypeData['name'], 'name')
             ->type($pointsTypeData['description'], 'description')
             ->press('Submit')
             ->notSeeInDatabase('points_types', $pointsTypeData)
             ->seePageIs(route('admin.pointsType.create'));
    }

    /**
     * PointsType edit success
     * Must storage changes of pointsType
     *
     * @return void
     */
    public function testPointsTypeEdit()
    {
        $admin = factory('App\Models\User', 'admin')->make();

        $pointsType = factory('App\Models\PointsType')->create();

        $pointsTypeData = ['name' => 'Shops',
                           'description' => 'Description'];

        $this->actingAs($admin)
             ->visit(route('admin.pointsType.edit', $pointsType))
             ->type($pointsTypeData['name'], 'name')
             ->type($pointsTypeData['description'], 'description')
             ->press('Submit')
             ->seeInDatabase('points_types', $pointsTypeData);
    }

    /**
     * PointsType edit fail
     * Must redirect to pointsType edit page and don't storage new pointsType
     *
     * @return void
     */
    public function testPointsTypeEditValidationFail()
    {
        $admin = factory('App\Models\User', 'admin')->make();

        $pointsType = factory('App\Models\PointsType')->create();

        // wrong name value
        $pointsTypeData = ['name' => '',
                           'description' => 'Description'];

        $this->actingAs($admin)
             ->visit(route('admin.pointsType.edit', $pointsType))
             ->type($pointsTypeData['name'], 'name')
             ->type($pointsTypeData['description'], 'description')
             ->press('Submit')
             ->notSeeInDatabase('points_types', $pointsTypeData)
             ->seePageIs(route('admin.pointsType.edit', $pointsType));
    }
}
