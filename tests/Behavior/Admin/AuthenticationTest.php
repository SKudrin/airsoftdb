<?php
namespace Tests\Behavior\Admin;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AuthenticationTest extends \TestCase
{
    use DatabaseTransactions;

    /**
     * Authenticate admin
     * Must redirect to admin home page
     *
     * @return void
     */
    public function testAdminAuthenticationInAdminArea()
    {
        $email = 'test.admin@mail.ru';
        $password = '12345';

        $admin = factory('App\Models\User', 'admin')->create([
                                            'name' => 'Admin',
                                            'email' => $email,
                                            'password' => bcrypt($password)
                                           ]);

        $this->visit(route('admin.login'))
             ->type(ucfirst($email), 'email')
             ->type($password, 'password')
             ->press('Sign In')
             ->seePageIs(route('admin.home'));

    }

    /**
     * Authenticate admin with bad credentials
     * Must redirect to admin login page
     *
     * @return void
     */
    public function testAdminAuthenticationInAdminAreaWithBadCredentials()
    {
        $email = 'test.admin@mail.ru';
        $password = '12345';
        $wrongPassword = '54321';

        $admin = factory('App\Models\User', 'admin')->create([
                                            'name' => 'Admin',
                                            'email' => $email,
                                            'password' => bcrypt($password)
                                           ]);

        $this->visit(route('admin.login'))
             ->type($email, 'email')
             ->type($wrongPassword, 'password')
             ->press('Sign In')
             ->seePageIs(route('admin.login'));
    }

    /**
     * Authenticate inactive admin
     * Must redirect to admin login page
     *
     * @return void
     */
    public function testInactiveAdminAuthenticationInAdminArea()
    {
        $email = 'test.user@mail.ru';
        $password = '12345';

        $inactiveAdmin = factory('App\Models\User', 'inactive_admin')->create([
                                            'name' => 'Admin',
                                            'email' => $email,
                                            'password' => bcrypt($password)
                                           ]);

        $this->visit(route('admin.login'))
             ->type($email, 'email')
             ->type($password, 'password')
             ->press('Sign In')
             ->seePageIs(route('admin.login'));
    }

    /**
     * Authenticate simple user
     * Must redirect to admin login page
     *
     * @return void
     */
    public function testUserAuthenticationInAdminArea()
    {
        $email = 'test.user@mail.ru';
        $password = '12345';

        $user = factory('App\Models\User')->create([
                                            'name' => 'User',
                                            'email' => $email,
                                            'password' => bcrypt($password)
                                           ]);

        $this->visit(route('admin.login'))
             ->type($email, 'email')
             ->type($password, 'password')
             ->press('Sign In')
             ->seePageIs(route('admin.login'));
    }

    /*
     * Visit admin area as active admin
     * Must get 200 response
     *
     * @return void
    */
    public function testVisitAdminAreaAsAdmin()
    {
        $admin = factory('App\Models\User', 'admin')->make();

        $this->actingAs($admin)
             ->visit(route('admin.home'))
             ->assertResponseStatus(200);
    }

    /**
     * Visit admin area as guest
     * Must be redirect on login page
     *
     * @return void
     */
    public function testVisitAdminAreaAsGuest()
    {
        $this->visit(route('admin.home'))
             ->seePageIs(route('admin.login'));
    }

    /**
     * Visit admin area as inactive admin
     * Must redirect on login page
     *
     * @return void
     */
    public function testVisitAdminAreaAsInactiveAdmin()
    {
        $inactiveAdmin = factory('App\Models\User', 'inactive_admin')->make();

        $this->actingAs($inactiveAdmin)
             ->visit(route('admin.home'))
             ->seePageIs(route('admin.login'));
    }

    /**
     * Visit admin area as user
     * Must redirect on login page
     *
     * @return void
     */
    public function testVisitAdminAreaAsUser()
    {
        $user = factory('App\Models\User')->make();

        $this->actingAs($user)
             ->visit(route('admin.home'))
             ->seePageIs(route('admin.login'));
    }
}
