<?php
namespace Tests\Behavior\Admin;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CountryTest extends \TestCase
{
    use DatabaseTransactions;

    /**
     * Country index page
     * Must get 200 response
     *
     * @return void
     */
    public function testCountryIndex()
    {
        $admin = factory('App\Models\User', 'admin')->make();

        $this->actingAs($admin)
             ->visit(route('admin.country.index'))
             ->assertResponseStatus(200);
    }

    /**
     * Country show page
     * Must get 200 response
     *
     * @return void
     */
    public function testCountryShow()
    {
        $admin = factory('App\Models\User', 'admin')->make();

        $country = factory('App\Models\Country')->make();

        $this->actingAs($admin)
             ->visit(route('admin.country.show', $country))
             ->assertResponseStatus(200);
    }

    /**
     * Country create success
     * Must storage new country
     *
     * @return void
     */
    public function testCountryCreate()
    {
        $admin = factory('App\Models\User', 'admin')->make();

        $countryData = ['name' => 'Russia'];

        $this->actingAs($admin)
             ->visit(route('admin.country.create'))
             ->type($countryData['name'], 'name')
             ->press('Submit')
             ->seeInDatabase('countries', $countryData);
    }

    /**
     * Country create fail
     * Must redirect to country create page and don't storage new country
     *
     * @return void
     */
    public function testCountryCreateValidationFail()
    {
        $admin = factory('App\Models\User', 'admin')->make();

        // wrong name value
        $countryData = ['name' => ''];

        $this->actingAs($admin)
             ->visit(route('admin.country.create'))
             ->type($countryData['name'], 'name')
             ->press('Submit')
             ->notSeeInDatabase('countries', $countryData)
             ->seePageIs(route('admin.country.create'));
    }

    /**
     * Country edit success
     * Must storage changes of country
     *
     * @return void
     */
    public function testCountryEdit()
    {
        $admin = factory('App\Models\User', 'admin')->make();

        $country = factory('App\Models\Country')->create();

        $countryData = ['name' => 'Shops'];

        $this->actingAs($admin)
             ->visit(route('admin.country.edit', $country))
             ->type($countryData['name'], 'name')
             ->press('Submit')
             ->seeInDatabase('countries', $countryData);
    }

    /**
     * Country edit fail
     * Must redirect to country edit page and don't storage new country
     *
     * @return void
     */
    public function testCountryEditValidationFail()
    {
        $admin = factory('App\Models\User', 'admin')->make();

        $country = factory('App\Models\Country')->create();

        // wrong name value
        $countryData = ['name' => ''];

        $this->actingAs($admin)
             ->visit(route('admin.country.edit', $country))
             ->type($countryData['name'], 'name')
             ->press('Submit')
             ->notSeeInDatabase('countries', $countryData)
             ->seePageIs(route('admin.country.edit', $country));
    }
}
