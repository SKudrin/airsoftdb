<?php
namespace Tests\Behavior\Admin;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserTest extends \TestCase
{
    use DatabaseTransactions;

    /**
     * User index page
     * Must get 200 response
     *
     * @return void
     */
    public function testUserIndex()
    {
        $admin = factory('App\Models\User', 'admin')->make();

        $this->actingAs($admin)
             ->visit(route('admin.user.index'))
             ->assertResponseStatus(200);
    }

    /**
     * User show page
     * Must get 200 response
     *
     * @return void
     */
    public function testUserShow()
    {
        $admin = factory('App\Models\User', 'admin')->make();

        $user = factory('App\Models\User')->make();

        $this->actingAs($admin)
             ->visit(route('admin.user.show', $user))
             ->assertResponseStatus(200);
    }

    /**
     * User create success
     * Must storage new user and add relation point belongsTo user
     *
     * @return void
     */
    public function testUserCreate()
    {
        $admin = factory('App\Models\User', 'admin')->make();

        $point = factory('App\Models\Point')->create();
        $pointTwo = factory('App\Models\Point')->create();

        $userData = ['name' => 'User',
                     'email' => 'test@mail.ru',
                     'is_admin' => true,
                     'is_active' => true,
                     'is_checked' => true];

        $this->actingAs($admin)
             ->visit(route('admin.user.create'))
             ->type($userData['name'], 'name')
             ->type($userData['email'], 'email')
             ->type('123456', 'password')
             ->select([$point->id, $pointTwo->id], 'point_id')
             ->check('is_admin')
             ->check('is_active')
             ->press('Submit')
             ->seeInDatabase('users', $userData)
             ->seeRelationBelongsTo('user', $point)
             ->seeRelationBelongsTo('user', $pointTwo);
    }

    /**
     * User create fail
     * Must redirect to user create page and don't storage new user
     *
     * @return void
     */
    public function testUserCreateValidationFail()
    {
        $admin = factory('App\Models\User', 'admin')->make();

        // wrong name value
        $userData = ['name' => '',
                     'email' => 'test@mail.ru',
                     'is_admin' => true,
                     'is_active' => true,
                     'is_checked' => true];

        $this->actingAs($admin)
             ->visit(route('admin.user.create'))
             ->type($userData['name'], 'name')
             ->type($userData['email'], 'email')
             ->type('123456', 'password')
             ->check('is_admin')
             ->check('is_active')
             ->press('Submit')
             ->notSeeInDatabase('users', $userData)
             ->seePageIs(route('admin.user.create'));
    }

    /**
     * User edit success
     * Must storage changes of user
     *
     * @return void
     */
    public function testUserEdit()
    {
        $admin = factory('App\Models\User', 'admin')->make();

        $user = factory('App\Models\User')->create();
        $point = factory('App\Models\Point')->create();

        $userData = ['name' => 'test',
                     'email' => 'test@mail.ru',
                     'is_admin' => true,
                     'is_active' => true,
                     'is_checked' => true];

        $this->actingAs($admin)
             ->visit(route('admin.user.edit', $user))
             ->type($userData['name'], 'name')
             ->type($userData['email'], 'email')
             ->check('is_admin')
             ->check('is_active')
             ->press('Submit')
             ->seeInDatabase('users', $userData)
             ->notSeeRelationBelongsTo('user', $point);
    }

    /**
     * User edit success with select two points
     * Must storage changes of user
     *
     * @return void
     */
    public function testUserEditWithSelectTwoPoints()
    {
        $admin = factory('App\Models\User', 'admin')->make();

        $user = factory('App\Models\User')->create();
        $point = factory('App\Models\Point')->create();
        $pointTwo = factory('App\Models\Point')->create();

        $userData = ['name' => 'test',
                     'email' => 'test@mail.ru',
                     'is_admin' => true,
                     'is_active' => true,
                     'is_checked' => true];

        $this->actingAs($admin)
             ->visit(route('admin.user.edit', $user))
             ->type($userData['name'], 'name')
             ->type($userData['email'], 'email')
             ->select([$point->id, $pointTwo->id], 'point_id')
             ->check('is_admin')
             ->check('is_active')
             ->press('Submit')
             ->seeInDatabase('users', $userData)
             ->seeRelationBelongsTo('user', $point)
             ->seeRelationBelongsTo('user', $pointTwo);
    }

    /**
     * User edit fail
     * Must redirect to user edit page
     *
     * @return void
     */
    public function testUserEditValidationFail()
    {
        $admin = factory('App\Models\User', 'admin')->make();

        $user = factory('App\Models\User')->create();

        // wrong name and email value
        $userData = ['name' => '',
                     'email' => '',
                     'is_admin' => true,
                     'is_active' => true,
                     'is_checked' => true];

        $this->actingAs($admin)
             ->visit(route('admin.user.edit', $user))
             ->type($userData['name'], 'name')
             ->type($userData['email'], 'email')
             ->check('is_admin')
             ->check('is_active')
             ->press('Submit')
             ->notSeeInDatabase('users', $userData)
             ->seePageIs(route('admin.user.edit', $user));
    }
}
