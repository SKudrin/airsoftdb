<?php
namespace Tests\Behavior\Main;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\User;

class AuthenticationTest extends \TestCase
{
    use DatabaseTransactions;

    /**
     * Authenticate admin
     * Must redirect to home page
     *
     * @return void
     */
    public function testAdminAuthentication()
    {
        $name = 'Admin';
        $email = 'test@mail.ru';
        $password = '123456';

        $admin = factory('App\Models\User', 'admin')->create([
                                                        'name' => $name,
                                                        'email' => $email,
                                                        'password' => bcrypt($password)
                                                       ]);

        $this->visit(route('login'))
             ->type(ucfirst($email), 'email')
             ->type($password, 'password')
             ->press(trans('layout.sign in'))
             ->seePageIs(route('home'));
    }

    /**
     * Authenticate user
     * Must redirect to home page
     *
     * @return void
     */
    public function testUserAuthentication()
    {
        $name = 'User';
        $email = 'test@mail.ru';
        $password = '123456';

        $user = factory('App\Models\User')->create([
                                            'name' => $name,
                                            'email' => $email,
                                            'password' => bcrypt($password)
                                           ]);

        $this->visit(route('login'))
             ->type(ucfirst($email), 'email')
             ->type($password, 'password')
             ->press(trans('layout.sign in'))
             ->seePageIs(route('home'));
    }

    /**
     * Authenticate user with bad credentials
     * Must redirect to login page
     *
     * @return void
     */
    public function testUserAuthenticationWithBadCredentials()
    {
        $name = 'User';
        $email = 'test@mail.ru';
        $password = '123456';
        $wrongPassword = '654321';

        $user = factory('App\Models\User')->create([
                                            'name' => $name,
                                            'email' => $email,
                                            'password' => bcrypt($password)
                                           ]);

        $this->visit(route('login'))
             ->type($email, 'email')
             ->type($wrongPassword, 'password')
             ->press(trans('layout.sign in'))
             ->seePageIs(route('login'));
    }

    /**
     * Authenticate inactive user
     * Must redirect to login page
     *
     * @return void
     */
    public function testInactiveUserAuthentication()
    {
        $name = 'User';
        $email = 'test@mail.ru';
        $password = '123456';

        $inactiveUser = factory('App\Models\User', 'inactive_user')->create([
                                            'name' => $name,
                                            'email' => $email,
                                            'password' => bcrypt($password)
                                           ]);

        $this->visit(route('login'))
             ->type($email, 'email')
             ->type($password, 'password')
             ->press(trans('layout.sign in'))
             ->seePageIs(route('login'));
    }

    /**
     * Visit profile as guest
     * Must redirect on login page
     *
     * @return void
     */
    public function testGuestVisitProfile()
    {
        $this->visit(route('user.profile'))
             ->seePageIs(route('login'));
    }

    /**
     * Visit profile as user
     * Must get 200 response
     *
     * @return void
     */
    public function testUserVisitProfile()
    {
        $user = factory('App\Models\User')->make();

        $this->actingAs($user)
             ->visit(route('user.profile'))
             ->assertResponseStatus(200);
    }

    /**
     * Visit profile as inactive user
     * Must redirect on login page
     *
     * @return void
     */
    public function testInactiveUserVisitProfile()
    {
        $inactiveUser = factory('App\Models\User', 'inactive_user')->make();

        $this->actingAs($inactiveUser)
             ->visit(route('user.profile'))
             ->seePageIs(route('login'));
    }

    /**
     * Visit profile as admin
     * Must get 200 response
     *
     * @return void
     */
    public function testAdminVisitProfile()
    {
        $admin = factory('App\Models\User', 'admin')->make();

        $this->actingAs($admin)
             ->visit(route('user.profile'))
             ->assertResponseStatus(200);
    }

    /**
     * Visit registration page as user
     * Must redirect to home page
     *
     * @return void
     */
    public function testUserVisitRegister()
    {
        $user = factory('App\Models\User')->make();

        $this->actingAs($user)
             ->visit(route('register'))
             ->seePageIs(route('home'));
    }

    /**
     * Visit registration page as admin
     * Must redirect to home page
     *
     * @return void
     */
    public function testAdminVisitRegister()
    {
        $admin = factory('App\Models\User', 'admin')->make();

        $this->actingAs($admin)
             ->visit(route('register'))
             ->seePageIs(route('home'));
    }

    /**
     * Visit registration page as guest
     * Must get 200 response
     *
     * @return void
     */
    public function testGuestVisitRegister()
    {
        $this->visit(route('register'))
             ->assertResponseStatus(200);
    }

    /**
     * Register user success
     * Must make new user with inactive flag and redirect to register success page
     *
     * @return void
     */
    public function testUserRegister()
    {
        $pointsType = factory('App\Models\PointsType')->create();
        $region = factory('App\Models\Region')->create();

        $name = 'testUser';
        $email = 'test@mail.ru';
        $password = '123456';
        $passwordConfirmation = '123456';
        $pointName = 'Shop';
        $pointPointsType = $pointsType->id;
        $pointAddress = 'Москва, улица ленина дом 5';
        $pointRegion = $region->id;

        $this->visit(route('register'))
             ->type($name, 'name')
             ->type($email, 'email')
             ->type($password, 'password')
             ->type($passwordConfirmation, 'password_confirmation')
             ->type($pointName, 'point[name]')
             ->type($pointPointsType, 'point[type_id]')
             ->type($pointAddress, 'point[address]')
             ->type($pointRegion, 'point[region_id]')
             ->press(trans('register.submit'))
             ->seeInDatabase('users', ['email' => $email, 'is_active' => false])
             ->seePageIs(route('register.success'));
    }

    /**
     * Register user with duplicate email fail
     * Must redirect to register
     *
     * @return void
     */
    public function testUserRegisterWithDuplicateEmail()
    {
        $name = 'user';
        $email = 'test@mail.ru';
        $password = '123456';
        $passwordConfirmation = '123456';

        $oldUser = factory('App\Models\User')->create([
                                                'name' => $name,
                                                'email' => $email,
                                                'password' => bcrypt($password)
                                               ]);

        $this->visit(route('register'))
             ->type($name, 'name')
             ->type(ucfirst($email), 'email')
             ->type($password, 'password')
             ->type($passwordConfirmation, 'password_confirmation')
             ->press(trans('register.submit'));

        $newUser = User::whereRaw('id <> ? and email = ?', [$oldUser->id, ucfirst($email)])->exists();

        if ($newUser)
            $this->fail('User with duplicated email created', 1);

        $this->seePageIs(route('register.page'));
    }

    /**
     * Register user with not valid password confirmation fail
     * Must redirect to register
     *
     * @return void
     */
    public function testUserRegisterWithNotValidPasswordConfirmation()
    {
        $name = 'user';
        $email = 'test@mail.ru';
        $password = '123456';
        $passwordConfirmation = '654321';

        $this->visit(route('register'))
             ->type($name, 'name')
             ->type($email, 'email')
             ->type($password, 'password')
             ->type($passwordConfirmation, 'password_confirmation')
             ->press(trans('register.submit'))
             ->notSeeInDatabase('users', ['email' => $email])
             ->seePageIs(route('register.page'));
    }

    /**
     * Register user with not valid password fail
     * Must redirect to register
     *
     * @return void
     */
    public function testUserRegisterWithNotValidPassword()
    {
        $name = 'user';
        $email = 'test@mail.ru';
        $password = '1';
        $passwordConfirmation = '1';

        $this->visit(route('register'))
             ->type($name, 'name')
             ->type($email, 'email')
             ->type($password, 'password')
             ->type($passwordConfirmation, 'password_confirmation')
             ->press(trans('register.submit'))
             ->notSeeInDatabase('users', ['email' => $email])
             ->seePageIs(route('register.page'));
    }
}
