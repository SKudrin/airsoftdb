<?php

return [

  'about' => 'About',
  'location' => 'Location',
  'contact' => 'Contact',
  'photos' => 'Photos',
  'address' => 'Address',
  'show map' => 'Show map',
  'hide map' => 'Hide map',
  'show photos' => 'Show photos',
  'hide photos' => 'Hide photos',
  'phone' => 'Phone',
  'email' => 'Email',
  'menu' => 'Menu',
  'loading map' => 'Loading map'

];