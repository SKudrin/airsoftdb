<?php

return [

  'copyright' => ':year Airsoft Lands, All rights reserved.',
  'sign in' => 'Sign in',
  'create account' => 'Create account',
  'browse' => 'Browse',
  'finder' => 'Finder',
  'account' => 'Account',
  'logout' => 'Logout',
  'profile' => 'Profile'

];