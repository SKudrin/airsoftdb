<?php

return [

  'select a region' => 'Select a region',
  'search' => 'search by address, city, state or zip',
  'filter' => 'FILTER',
  'sort by' => 'SORT BY',
  'keyword filter' => 'filter by name',
  'sort direction' => 'SORT DIRECTION',
  'descending' => 'Descending',
  'ascending' => 'Ascending',
  'type' => 'Type',
  'name' => 'Name'

];