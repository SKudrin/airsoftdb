<?php

return [
  'sign up' => 'Sign up',
  'sign up message' => 'Welcome, keep calm and register',
  'password' => 'Password',
  'retype password' => 'Retype password',
  'your contact info' => 'Your contact info',
  'your name' => 'Your name',
  'email' => 'Your email',
  'your phone' => 'Your phone',
  'your listing info' => 'Your listing info',
  'type' => 'Type',
  'point name' => 'Name',
  'point phone' => 'Phone',
  'point address' => 'Address',
  'point description' => 'Description',
  'comments to admin' => 'Comments to admin',
  'optional' => 'optional',
  'submit' => 'submit',
  'region' => 'Region',
  'your profile' => 'Your profile',
  'your points' => 'Your points',
  'avatar' => 'Avatar',
  'delete' => 'Delete',
  'change password' => 'Change password',
  'old password' => 'Old password',
  'photos' => 'Photos',
  'security' => 'Security'

];