<?php

return [

  'copyright' => ':year Airsoft Lands, Все права защищены.',
  'sign in' => 'Войти',
  'create account' => 'Регистрация',
  'browse' => 'Обзор',
  'finder' => 'Поиск',
  'account' => 'Аккаунт',
  'logout' => 'Выйти',
  'profile' => 'Личный кабинет'
];