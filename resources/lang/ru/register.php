<?php

return [

  'sign up' => 'Присоединиться',
  'sign up message' => 'Добро пожаловать! Заполните поля формы и нажмите зарегистрироваться, после регистрации вы сможете управлять своим аккаунтом.',
  'password' => 'Пароль',
  'retype password' => 'Пароль еще раз',
  'your contact info' => 'Ваши контактные данные',
  'your name' => 'Ваше имя',
  'email' => 'Почта',
  'your phone' => 'Ваш телефон',
  'your listing info' => 'Ваше объявление',
  'type' => 'Тип',
  'point name' => 'Название',
  'point phone' => 'Телефон',
  'point address' => 'Адрес',
  'point description' => 'Описание',
  'comments to admin' => 'Комментарий для администратора',
  'optional' => 'по желанию',
  'submit' => 'Отправить',
  'region' => 'Регион'

];