<?php

return [

  'about' => 'Информация',
  'location' => 'Гео',
  'contact' => 'Контакты',
  'photos' => 'Фото',
  'address' => 'Адрес',
  'show map' => 'Показать карту',
  'hide map' => 'Спрятать карту',
  'show photos' => 'Показать галерею',
  'hide photos' => 'Спрятать галерею',
  'phone' => 'Телефон',
  'email' => 'Почта',
  'menu' => 'Навигация',
  'loading map' => 'загрузка карты'
];