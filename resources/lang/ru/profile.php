<?php

return [

  'sign up' => 'Присоединиться',
  'sign up message' => 'Добро пожаловать! Заполните поля формы и нажмите зарегистрироваться, после регистрации вы сможете управлять своим аккаунтом.',
  'password' => 'Пароль',
  'retype password' => 'Пароль еще раз',
  'your contact info' => 'Ваши контактные данные',
  'your name' => 'Имя',
  'email' => 'Почта',
  'your phone' => 'Телефон',
  'your listing info' => 'Ваше объявление',
  'type' => 'Тип',
  'point name' => 'Название',
  'point phone' => 'Телефон',
  'point address' => 'Адрес',
  'point description' => 'Описание',
  'comments to admin' => 'Комментарий для администратора',
  'optional' => 'по желанию',
  'submit' => 'Сохранить',
  'region' => 'Регион',
  'your profile' => 'Личный кабинет',
  'your points' => 'Ваши объекты',
  'avatar' => 'Аватар',
  'delete' => 'Удалить',
  'change password' => 'Сменить пароль',
  'old password' => 'Старый пароль',
  'photos' => 'Фотографии',
  'security' => 'Безопасность'

];