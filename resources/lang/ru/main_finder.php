<?php

return [

  'select a region' => 'Регион',
  'search' => 'поиск по адресу',
  'filter' => 'ФИЛЬТР',
  'sort by' => 'СОРТИРОВКА',
  'keyword filter' => 'название',
  'sort direction' => 'ПОРЯДОК СОРТИРОВКИ',
  'descending' => 'Убывание',
  'ascending' => 'Возрастание',
  'type' => 'Тип',
  'name' => 'Название'

];