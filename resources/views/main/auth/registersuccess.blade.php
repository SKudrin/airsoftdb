@extends('main.layout')

@section('htmlheader_title')
    {{ 'Airsoftlands ' }}
@endsection

@section('htmlheader_description')
    {{ 'Airsoftlands ' }}
@endsection

@section('htmlheader_css_before')
  <link href="/plugins/select/bootstrap.min.css" rel="stylesheet"/>s
@endsection

@section('htmlheader_css_after')
  <link href="/css/pages/login.css" rel="stylesheet"/>
@endsection

@section('htmlheader')
@endsection

@section('topcontent')
  <div class='container'>
    <div class='sixteen columns'>
      <div class='center_block'>
        <div class='center_block_padding'>
          <h2>{{ trans('registersuccess.success') }}</h2>
          <p>{{ trans('registersuccess.success message') }}</p>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scripts_after')
@endsection