@extends('main.layout')

@section('htmlheader_title')
    {{ 'Airsoftlands ' }}
@endsection

@section('htmlheader_description')
    {{ 'Airsoftlands ' }}
@endsection

@section('htmlheader_css_before')
  <link href="/plugins/select/bootstrap.min.css" rel="stylesheet"/>s
@endsection

@section('htmlheader_css_after')
  <link href="/css/pages/login.css" rel="stylesheet"/>
  <link href="/css/form.css" rel="stylesheet"/>
@endsection

@section('htmlheader_js_after')
  <script src="/js/pages/login.js"></script>
@endsection

@section('htmlheader')
@endsection

@section('topcontent')
<div class='container'>
  <div class='sixteen columns'>
    <div class='center_block'>
      <div class='center_block_padding'>
        <h1>{{ trans('layout.sign in') }}</h1>
        <hr>
        <form id='login' action="{{ route('login') }}" method="post">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <br>
          <div class='inputgrp'>
            <h4>{{ trans('register.email') }}</h4>
            <input class='user_input' name='email' type='text' value="{{ old('email')}}" required />
          </div>
          <div class='inputgrp'>
            <h4>{{ trans('register.password') }}</h4>
            <input class='user_input' name='password' type="password" value="{{ old('password')}}" autocomplete="off" />
          </div>
          @if (count($errors) > 0)
              <div id='login_error' class='login_error'>
                @foreach ($errors->all() as $error)
                    {{ $error }}<br/>
                @endforeach
              </div>
          @endif
          <br>
          <button type='submit' id='submit' class='basic gold'>{{ trans('layout.sign in') }}</button>
          {{-- <div id='forgot'><a href='http://www.airsoftc3.com/forgot'>Forgot Password?</a></div> --}}
        </form>
        <div class='clear'></div>
        <br>
        <div id='signup'>{{ trans('login.Need an account?') }} <a class='underline' href="{{ route('register') }}">{{ trans('layout.create account') }}</a>.</div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('scripts_after')
<script type="text/javascript">
  $(function () {
    Page.init({});
  });
</script>
@endsection