@extends('main.layout')

@section('htmlheader_title')
    {{ 'Airsoftlands ' }}
@endsection

@section('htmlheader_description')
    {{ 'Airsoftlands ' }}
@endsection

@section('htmlheader_css_before')
  <link href="/plugins/select/bootstrap.min.css" rel="stylesheet"/>s
  <link href="/plugins/select/bootstrap-select.min.css" rel="stylesheet"/>s
@endsection

@section('htmlheader_css_after')
  <link href="/css/pages/register.css" rel="stylesheet"/>
@endsection

@section('htmlheader')
@endsection

@section('htmlheader_js_after')
    <script src="/plugins/select/bootstrap.min.js"></script>
    <script src="/plugins/select/bootstrap-select.min.js"></script>
    <script src="/js/pages/register.js"></script>
    <script type='text/javascript'>
      $(function() {
        $('.selectpicker').selectpicker();
      });
    </script>
@endsection

@section('topcontent')
<div class='container'>
    <div class='sixteen columns'>
        <div class='slim'>
            <h1>{{ trans('register.sign up') }}</h1>
            <div class='read'>
                {{ trans('register.sign up message') }}
            </div>
            <hr>
            <form id='form' action="{{ route('register') }}" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <h3>{{ trans('register.your contact info') }}</h3>
                <label>{{ trans('register.email') }}</label>
                <div class='inputgrp'>
                  <input placeholder='' class="$errors->has('email') ? 'error' : ''" id='email' name='email' type="text" value="{{ old('email') }}" autocomplete="off" />
                  @if ($errors->has('email'))
                    @foreach ($errors->get('email') as $error)
                      <label for="email" class="error">{{ $error }}</label>
                    @endforeach
                  @endif
                </div>
                <br>
                <label>{{ trans('register.password') }}</label>
                <div class='inputgrp'>
                  <input placeholder='' class="$errors->has('email') ? 'error' : ''" id='password' name='password' type="password" value="{{ old('password') }}" autocomplete="off" />
                  @if ($errors->has('password'))
                    @foreach ($errors->get('password') as $error)
                      <label for="password" class="error">{{ $error }}</label>
                    @endforeach
                  @endif
                </div>
                <br>
                <label>{{ trans('register.retype password') }}</label>
                <div class='inputgrp'>
                  <input placeholder='' class="$errors->has('password_confirmation') ? 'error' : ''" id='password_confirmation' name='password_confirmation' type="password" value="{{ old('password_confirmation') }}" autocomplete="off" />
                  @if ($errors->has('password_confirmation'))
                    @foreach ($errors->get('password_confirmation') as $error)
                      <label for="password_confirmation" class="error">{{ $error }}</label>
                    @endforeach
                  @endif
                </div>
                <br>
                <label>{{ trans('register.your name') }}</label>
                <div class='inputgrp'>
                  <input placeholder='' class="$errors->has('name') ? 'error' : ''" id='name' name='name' type="text" value="{{ old('name') }}" autocomplete="off" />
                  @if ($errors->has('name'))
                    @foreach ($errors->get('name') as $error)
                      <label for="name" class="error">{{ $error }}</label>
                    @endforeach
                  @endif
                </div>
                <br>
                <h3>{{ trans('register.your listing info') }}</h3>
                <label>{{ trans('register.type') }}</label>
                <div id="" class="">
                  @foreach ($pointsTypes as $key => $pointsType)
                    @if (empty(old('point.type_id')) && $key == 0)
                      @define $checked = 'checked'
                    @elseif (old('point.type_id') == $pointsType->id)
                      @define $checked = 'checked'
                    @else
                      @define $checked = ''
                    @endif
                    <div class='radioStyleGrp'>
                      <input class='radioStyle' type="radio" name="point[type_id]" id="type_id_{{ $pointsType->id }}" value="{{ $pointsType->id }}" {{$checked}}/>
                      <label for="type_id_{{ $pointsType->id }}">{{ $pointsType->name }}</label>
                      <div class='radioStyleSublabel'>{!! $pointsType->description !!}</div>
                    </div>
                  @endforeach
                </div>
                <br style="clear: both;"/>
                <label>{{ trans('register.point name') }}</label>
                <div class='inputgrp'>
                  <input placeholder='' class="$errors->has('point.name') ? 'error' : ''" id='name' name='point[name]' type="text" value="{{ old('point.name') }}" autocomplete="off" />
                  @if ($errors->has('point.name'))
                    @foreach ($errors->get('point.name') as $error)
                      <label for="point[name]" class="error">{{ $error }}</label>
                    @endforeach
                  @endif
                </div>
                <br>
                <div id='continue'>
                    <label>{{ trans('register.point phone') }} ({{ trans('register.optional') }})</label>
                    <div class='inputgrp'>
                      <input placeholder='' class="$errors->has('point.phone') ? 'error' : ''" id='phone' name='point[phone]' type="text" value="{{ old('point.phone') }}" autocomplete="off" />
                      @if ($errors->has('point.phone'))
                        @foreach ($errors->get('point.phone') as $error)
                          <label for="point[phone]" class="error">{{ $error }}</label>
                        @endforeach
                      @endif
                    </div>
                    <br>
                    <label>{{ trans('register.region') }}</label>
                    <div class='inputgrp'>
                      <select class="selectpicker" data-live-search="true" name="point[region_id]" >
                        @foreach ($regions as $region)
                          <option {{ old('point.region_id') == $region->id ? 'selected' : '' }} value="{{$region->id}}">{{$region->name}}</option>
                        @endforeach
                      </select>
                    </div>
                    <br>
                    <label>{{ trans('register.point address') }}</label>
                    <div class='inputgrp'>
                      <input placeholder='' class="$errors->has('point.address') ? 'error' : ''" id='address' name='point[address]' type="text" value="{{ old('point.address') }}" autocomplete="off" />
                      @if ($errors->has('point.address'))
                        @foreach ($errors->get('point.address') as $error)
                          <label for="point[address]" class="error">{{ $error }}</label>
                        @endforeach
                      @endif
                    </div>
                    <br>
                    <label>{{ trans('register.point description') }} ({{ trans('register.optional') }})</label>
                    <textarea id='description' name='point[description]'>{{ old('point.description') }}</textarea>
                    <br>
                    <br>
                    <label>{{ trans('register.comments to admin') }} ({{ trans('register.optional') }})</label>
                    <textarea id='comment' name='point[comment]'>{{ old('point.comment') }}</textarea>
                    <br>
                    <br>
                    <button id='submit' type='submit' class='basic gold'>{{ trans('register.submit') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts_after')
  <script type='text/javascript'>
    $(function() {
      Page.init({});
    });
  </script>
@endsection
