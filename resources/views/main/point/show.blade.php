@extends('main.layout')

@section('htmlheader_title')
    {{ 'Airsoftlands ' . $point->name }}
@endsection

@section('htmlheader_description')
    {{ 'Airsoftlands ' . $point->name }}
@endsection

@section('htmlheader_css_before')
  <link href="/plugins/select/bootstrap.min.css" rel="stylesheet"/ type="text/css">
@endsection

@section('htmlheader_css_after')
  <link href="/plugins/a3-we-sorry/tabs/tabs.ac3.css" rel="stylesheet" type="text/css">
  <link href="/plugins/fresco2/fresco.css" rel="stylesheet" type="text/css" />
  <link href="/plugins/jquery.tooltipster/tooltipster.css" rel="stylesheet" type="text/css" />
  <link href="/css/pages/point-show.css" rel="stylesheet" />
@endsection

@section('htmlheader_js_after')
  <script src="/plugins/a3-we-sorry/map.ac3.js" type="text/javascript"></script>
  <script src="/plugins/a3-we-sorry/tabs/tabs.ac3.js" type="text/javascript"></script>
  <script src="/plugins/fresco2/fresco.js" type="text/javascript"></script>
  <script src="/plugins/google/maps.infobubble.js" type="text/javascript"></script>
  <script src="/plugins/jquery.tooltipster/jquery.tooltipster.min.js" type="text/javascript"></script>
  <script src="/plugins/jquery.cycle/jquery.cycle2.js" type="text/javascript"></script>
  <script src="/plugins/jquery.cycle/jquery.cycle2.center.min.js" type="text/javascript"></script>
  <script src="/plugins/jquery.cycle/jquery.cycle2.swipe.min.js" type="text/javascript"></script>
  <script src="/plugins/jquery.cycle/jquery.cycle2.swipe.min.js" type="text/javascript"></script>
  <script src="/js/pages/point-show.js" type="text/javascript"></script>
@endsection

@section('htmlheader')
@endsection

@section('topcontent')
<div id='unit_header_wrap'>
    <div class='container'>
        <div class='twelve columns'>
            <div id="breadcrumbs">
                <span>
                  <span class='breadcrumb'> <a href="{{url(route('point.index', $point->type->id))}}">{{$point->type->name}}</a></span>
                  <span class='breadcrumb'> &#8594; <a href="{{url(route('point.index', [$point->type->id, $point->region->id]))}}">{{$point->region->name}}</a></span>
                  <span class='breadcrumb'> &#8594; <a href="{{url(route('point.show', $point->id))}}">{{ $point->name }}</a></span>
                </span>
            </div>
            <h1>{{ $point->name }}</h1>
            <div id='address'>{{ $point->region->name }}</div>
            {{--<a target='_blank' rel='nofollow' href='' id='website'></a>--}}
            {{--<div id='social_links'>
                <a target='_blank' href='https://www.facebook.com/'>
                    <img src='/img/icons/social/64/FacebookSquare.png' />
                </a>
                <a target='_blank' href='https://twitter.com/'>
                    <img src='/img/icons/social/64/TwitterSquare.png' />
                </a>
            </div>--}}
        </div>
        <div id='unit_header_right' class='four columns'>
            <div id='unit_logo_wrap'>
                {{--<img id='unit_logo' class='scale-with-grid' src='/data/fields/f53f6152f77171/logo/Thumbs/f254i5511f9273b0df.jpg'>--}}
            </div>
            {{--<div id='inappropriate'>Report inappropriate content</div>--}}
        </div>
        <div class='clear'></div>
        <div class='tabs sixteen columns'>
            <div id='tabs' class='ac3tabs_wrap'>
                <div class='ac3tabs_mobile'>
                    <label class='ac3tabs_menu_label'>{{trans('main_point_show.menu')}}</label>
                    <div class='ac3tabs_norgie_wrap'>
                        <div class='ac3tabs_norgie'>
                            <div class='ac3tabs_icon'>
                                <div class='ac3tabs_pointerRight'></div>
                            </div>
                        </div>
                    </div>
                    <div class='clear'></div>
                </div>
                <div class='ac3tabs'>
                    <div id='about_tab' class='ac3tab selected ' data-page="about">
                        <label class='ac3tab_label'>{{trans('main_point_show.about')}}</label>
                    </div>
                    <div id='location_tab' class='ac3tab  ' data-page="location">
                        <label class='ac3tab_label'>{{trans('main_point_show.location')}}</label>
                    </div>
                    @if ($point->photos()->get())
                    <div id='photos_tab' class='ac3tab  ' data-page="photos">
                        <label class='ac3tab_label'>{{trans('main_point_show.photos')}}</label>
                    </div>
                    @endif
                    <div id='contact_tab' class='ac3tab  ' data-page="contact">
                        <label class='ac3tab_label'>{{trans('main_point_show.contact')}}</label>
                    </div>
                    <div class='clear'></div>
                </div>
            </div>
        </div>
        <div class='clear'></div>
    </div>
</div>
<div id='pages'>
    <div id='about' class='page' style="display:block">
        <div class='feature' id='map_feature'>
            @if ($point->photos()->get())
                <div class='feature_icons'>
                    <div class='hide_feature'>
                        {{ trans('main_point_show.hide photos')}}
                    </div>
                    <div class='show_feature'>
                        {{ trans('main_point_show.show photos')}}
                    </div>
                </div>
                <div class='clear'></div>
                <div class='feature_content'>
                    <div id='slideshow_wrap'>
                        <div id='pauser'>
                        </div>
                        <div id='slideshow'>
                            @foreach ($point->photos()->get() as $photo)
                            <div>
                                <img src='/img/photos/o/{{$photo->name}}.{{$photo->ext}}'>
                            </div>
                            @endforeach
                        </div>
                        <div id='feature_cycle_prev' class="cycle-prev"></div>
                        <div id='feature_cycle_next' class="cycle-next"></div>
                    </div>
                </div>
            @endif
        </div>
        <div class='container' style='margin-top:15px;'>
            <div class='sixteen columns'>
                <div id='unit_logo_wrap_mobile'>
                    <img id='unit_logo_mobile' class='scale-with-grid' src=''>
                </div>
                <br />
                <div class='redactored'>
                    {!! strip_tags($point->description, '<p><a><br><b>') !!}
                </div>
            </div>
        </div>
    </div>
    @if ($point->photos()->get())
    <div id='photos' class='page'>
        <div class='container'>
            <div class='sixteen columns'>
                <br/>
                <div class='album_thumbnails'>
                    @foreach ($point->photos()->get() as $photo)
                        <a href="/img/photos/o/{{$photo->name}}.{{$photo->ext}}" class="media_photo fresco" data-fresco-group="featured_album" data-fresco-options="thumbnail: '/img/photos/o/{{$photo->name}}.{{$photo->ext}}'">
                            <img class='promote' data-promote="/img/photos/s/{{$photo->name}}.{{$photo->ext}}" src="/img/photos/s/{{$photo->name}}.{{$photo->ext}}" />
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    @endif
    <div id='location' class='page no_top_pad'>
        <div class='feature' id='map_feature'>
            <div class='feature_content'>
                <div id='map_loading' class="vcenter_wrap ">
                    <div class="vcenter_out">
                        <div class="vcenter_mid">
                            <div class="vcenter_inn">
                                {{ trans('main_point_show.loading map')}} <img style='margin-left: 5px;' src='/img/icons/loader2.gif'>
                            </div>
                        </div>
                    </div>
                </div>
                <div class='gmap' id='map'></div>
            </div>
            <div class='feature_icons'>
                <div class='hide_feature'>
                    {{ trans('main_point_show.hide map')}}
                </div>
                <div class='show_feature'>
                    {{ trans('main_point_show.show map')}}
                </div>
            </div>
            <div class='clear'></div>
        </div>
        <div class='container'>
            {{--<div class='sixteen columns'>
                <h5 class='inputlabel'>GET DIRECTIONS</h5>
                <div style="width: 100%;">
                    <table style="width: 100%;">
                        <tr>
                            <td>
                                <input placeholder='Type in any address information' type='text' id='directions_from' value='' />
                            </td>
                            <td style="width:50px; padding-left:10px">
                                <button id='get_directions' style="height:35px;" class='basic dark'>GO</button>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id='directions_clear'>Clear Directions</div>
                <div id='driving_directions'></div>
                <div class='clear'></div>
                <br>
            </div>--}}
            <div class='clear'></div>
        </div>
    </div>
    <div id='contact' class='page'>
        <div class='container'>
            <div class='sixteen columns'>
                <br/>
                @if ($point->address)
                    <div class='contact_info'>
                        <div class='contact_label'>{{ trans('main_point_show.address')}}</div>
                        <div class='contact_value show_location'>{{ $point->address }}</div>
                    </div>
                @endif
                @if ($point->user)
                    <div class='contact_info'>
                        <div class='contact_label'>{{ trans('main_point_show.email')}}</div>
                        <div class='contact_value'><a href='mailto:{{ $point->user->email }}'>{{ $point->user->email }}</span></a></div>
                    </div>
                @endif
                @if ($point->phone)
                    <div class='contact_info'>
                        <div class='contact_label'>{{ trans('main_point_show.phone')}}</div>
                        <div class='contact_value'>{{ $point->phone }}</div>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts_after')
  <script type="text/javascript">
    $(function () {
      MapManager.init({'selector' : $('#map'),
                       'center' : [60.2519,102.1420],
                       'zoom' : 4
                       });

      Page.init({
         point: {!! $point->toJson() !!}
      });
    })
  </script>
@endsection
