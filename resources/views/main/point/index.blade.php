@extends('main.layout')

@section('htmlheader_title')
    {{ 'Airsoftlands ' . $pointsType->name }}
@endsection

@section('htmlheader_description')
    {{ 'Airsoftlands ' . $pointsType->name }}
@endsection

@section('htmlheader_css_before')
  <link href="/plugins/select/bootstrap.min.css" rel="stylesheet"/>
  <link href="/plugins/collapser/collapser.css" rel="stylesheet"/>
@endsection

@section('htmlheader_css_after')
  <link href="/css/pages/point-index.css" rel="stylesheet"/>
@endsection

@section('htmlheader_js_after')
  <script src="/plugins/jquery.pin/jquery.pin.min.js" type="text/javascript"></script>
  <script src="/plugins/jquery.scrollup/jquery.scrollup.js" type="text/javascript"></script>
  <script src="/plugins/collapser/collapser.js" type="text/javascript"></script>
  <script src="/js/pages/point-index.js" type="text/javascript"></script>

@endsection

@section('htmlheader')
@endsection

@section('topcontent')
<div class='container'>
  <div class='sixteen columns'>
    <br/>
    <h1 class='gold'>{{ $pointsType->name }} {{ !empty($region) ? $region->name : ''}}</h1>
    {{--
    <div id='search_wrap'>
      <label>Search</label>
      <div id='search_by_location'></div>
    </div>
    --}}
    <div class='clear'></div>

    <div id='tools'>
      <div id='results_count'>{{$pointsCount}} {{ trans_choice('main_point_index.results', $pointsCount) }}</div>

      <div id='tools_right'>
        <div class='collapser_sizers collapser_icons' data-selector=".unit"><div class='collapser_small collapser_icon'><div class='collapser_icon_row'><div class='collapser_icon_bullet'></div><div class='collapser_icon_item'></div></div><div class='collapser_icon_row'><div class='collapser_icon_bullet'></div><div class='collapser_icon_item'></div></div><div class='collapser_icon_row'><div class='collapser_icon_bullet'></div><div class='collapser_icon_item'></div></div><div class='collapser_icon_row'><div class='collapser_icon_bullet'></div><div class='collapser_icon_item'></div></div></div><div class='collapser_medium collapser_icon'><div class='collapser_icon_row'><div class='collapser_icon_bullet'></div><div class='collapser_icon_item'></div></div><div class='collapser_icon_row'><div class='collapser_icon_bullet'></div><div class='collapser_icon_item'></div></div></div><div class='clear'></div></div>
      </div>
      <div class='clear'></div>
    </div>
    <hr style="margin: 5px 0px;">
    <div id='states'>
      @if (empty($region))
        @foreach ($pointsType->regions as $region)
          <div id="{{ $region->id }}" class='state'>
            <h3><a class='state_link' href="{{ route('finder', ['region' => $region]) }}">{{ $region->name }}</a></h3>
            @foreach ($region->points()->ofType($pointsType)->ofRegion($region)->active()->get() as $point)
              @include('main.partials.pointindexelement', ['point' => $point])
            @endforeach
          </div>
        @endforeach
      @else
        @foreach ($region->points()->ofType($pointsType)->active()->get() as $point)
          @include('main.partials.pointindexelement', ['point' => $point])
        @endforeach
      @endif
    </div>
  </div>
</div>
@endsection

@section('scripts_after')
  <script type="text/javascript">
    $(function () {
      Page.init({});
    });
  </script>

@endsection