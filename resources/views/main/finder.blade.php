@extends('main.layout')

@section('htmlheader_title')
    {{ 'Airsoftlands ' }}
@endsection

@section('htmlheader_description')
    {{ 'Airsoftlands ' }}
@endsection

@section('htmlheader_css_before')
    <link href="/plugins/select/bootstrap.min.css" rel="stylesheet" />
    <link href="/plugins/select/bootstrap-select.min.css" rel="stylesheet" />
@endsection

@section('htmlheader_css_after')
    <link href="/plugins/jqueryui2/jqueryui.min.css" rel="stylesheet" type="text/css">

    <link href="/plugins/jquery.tooltipster/tooltipster.css" rel="stylesheet" />
    <link href="http://malihu.github.io/custom-scrollbar/jquery.mCustomScrollbar.min.css" rel="stylesheet" />
    <link href="/css/pages/finder.css" rel="stylesheet" />
@endsection

@section('htmlheader_js_after')
    <script src='/plugins/jqueryui2/jqueryui.min.js'></script>
    <script src="/plugins/a3-we-sorry/map.ac3.js" type="text/javascript"></script>
    <script src="/plugins/a3-we-sorry/form.ac3.js" type="text/javascript"></script>
    <script src="/plugins/google/maps.infobubble.js" type="text/javascript"></script>
    <script src="/plugins/select/bootstrap.min.js" type="text/javascript"></script>
    <script src="/plugins/select/bootstrap-select.min.js" type="text/javascript"></script>
    <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <script src="/plugins/jquery.notifications/jquery.notifications.js" type="text/javascript"></script>
    <script src="/plugins/jquery.cookie/jquery.cookie.js"></script>
    <script src="/plugins/a3-we-sorry/sticky.ac3.js" type="text/javascript"></script>
    <script src="/plugins/jquery.tooltipster/jquery.tooltipster.min.js" type="text/javascript"></script>
    <script src="http://malihu.github.io/custom-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="/plugins/jquery.scrollup/jquery.scrollup.js" type="text/javascript"></script>
    <script src="/plugins/fastclick/fastclick.js" type="text/javascript"></script>
@endsection

@section('htmlheader')
@endsection

@section('topcontent')
<div id='mapside' style='display:block'>
    <div id='mapside_left'>
        <div id='mapside_filters'>
            <div id="finder_placeholder" class="mapside_placeholder"></div>
            <div id='finder'>
            <div id='location_pickers_wrap' class='container'>
                <div class='sixteen columns' id='location_pickers_collapsed'>
                </div>
                <div class='sixteen columns' id='location_pickers'>
                    <div id='unit_checkbox_wide'>
                        <div id='unit_checkboxes_old' style="display:block;">
                            <div id='find_near_label' class='find_near_label'>FIND:</div>
                            @foreach ($pointsTypes as $pointsType)
                                <div class='customCheckboxGrp'>
                                    <div class="customCheckbox">
                                        <input id='points_type_{{$pointsType->id}}' {{ (!isset($appliedFilters['ftype']) || in_array($pointsType->id, $appliedFilters['ftype'])) ? 'checked="checked"' : ''}} class='unit_picker_cbox' data-type-id='{{$pointsType->id}}' name="type_filter[{{$pointsType->id}}]" type="checkbox" />
                                        <label for="points_type_{{$pointsType->id}}"></label>
                                    </div>
                                    <div class='customCheckboxLabel'>
                                        {{$pointsType->name}}
                                    </div>
                                    <div class='clear'></div>
                                </div>
                            @endforeach
                            <div class='clear'></div>
                        </div>
                    </div>
                    <div class='clear'></div>
                    <div class='pickercol' id='findcol'>
                        <div class='filtergrp'></div>
                    </div>
                    <div class='pickercol' id='addresscol'>
                        <div class='inputgrp'>
                            <input placeholder="{{ trans('main_finder.search') }}" class='' id='address' name='address' type="text" value="" autocomplete="off" ng-model='Address' />
                        </div>
                    </div>
                    <div class='pickercol' id='statecol'>
                        <select title="{{ trans('main_finder.select a region') }}" name='region' class="selectpicker needsclick" data-live-search="true" data-width="100%">
                            <option {{ empty($currentRegion) ? "selected='selected'":"" }} value="nil">{{ trans('main_finder.select a region') }}</option>
                            @foreach ($regions as $region)
                                <option {{ (!empty($currentRegion) && $currentRegion->id == $region->id) ? "selected='selected'":"" }} class='needsclick' value="{{$region->id}}">{{$region->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>

            <div class='container' id='result_filters'>
                <div id='unit_pickers'>
                    <div class='sixteen columns'>
                        <div class='hrsmall' style='margin-bottom:10px;'></div>
                        <div style="float:left">
                            <div id='results_count'>

                                <div id='results'>
                                    {{ count($points) }} {{ trans_choice('main_point_index.results', count($points)) }}
                                </div>
                            </div>
                        </div>
                        <div id='tooloptions'>
                            <div id='sizers'>
                                <div id='small' class='sizer'>
                                    <div class='sizer_row'>
                                        <div class='sizer_bullet'></div>
                                        <div class='sizer_item'></div>
                                    </div>
                                    <div class='sizer_row'>
                                        <div class='sizer_bullet'></div>
                                        <div class='sizer_item'></div>
                                    </div>
                                    <div class='sizer_row'>
                                        <div class='sizer_bullet'></div>
                                        <div class='sizer_item'></div>
                                    </div>
                                    <div class='sizer_row'>
                                        <div class='sizer_bullet'></div>
                                        <div class='sizer_item'></div>
                                    </div>
                                </div>
                                <div id='medium' class='sizer'>
                                    <div class='sizer_row'>
                                        <div class='sizer_bullet'></div>
                                        <div class='sizer_item'></div>
                                    </div>
                                    <div class='sizer_row'>
                                        <div class='sizer_bullet'></div>
                                        <div class='sizer_item'></div>
                                    </div>
                                </div>
                                <div class='clear'></div>
                            </div>
                            <div id='gear' class='rotate toolbtn'>
                                <img src='/img/icons/gear.png' width='20'>
                            </div>
                            <div id='unisolate' class='toolbtn'>
                                <img src='/img/icons/marker.png' width='13'>
                            </div>
                        </div>
                    </div>
                    <div class='clear'></div>
                </div>
                <div class='sixteen columns' id='morefilters' style="height: 0px">
                    <div id='unit_checkbox_narrow'></div>
                    <div id='keywordcol' class='pickercol'>
                        <div class='filterlabel'>{{ trans('main_finder.filter') }}</div>
                        <input id='filter' type='text' ng-model='query' placeholder="{{ trans('main_finder.keyword filter') }}" />
                        <div ng-click='FilterQuery()' id='clear_keyword'>&#215;</div>
                    </div>
                    <div id='sortcol' class='pickercol'>
                        <div class='filterlabel'>{{ trans('main_finder.sort by') }}</div>
                        <div id='sort' class='bootstrap selectpickerwrap ac3select needsclick' style="position: relative">
                            <select ng-model='unitOrder' name='sort' class="selectpicker needsclick" data-width="100%">
                                <option class='needsclick' value="name" selected='selected'>{{ trans('main_finder.name') }}</option></option>
                                <option class='needsclick' value="type">{{ trans('main_finder.type') }}</option>
                                {{-- <option class='needsclick' value="city">City</option>
                                <option class='needsclick' value="distance">Distance</option> --}}
                            </select>
                        </div>
                    </div>
                    {{-- <div class='pickercol' id='distancecol'>
                        <div id='distancewrap' class='disabled'>
                            <div id='distancelabel' class='filterlabel disabled'>DISTANCE</div>
                            <div id='distance' class='bootstrap selectpickerwrap ac3select needsclick' style="position: relative">
                                <select title="" name='distance' data-live-search="true"class="selectpicker needsclick" data-width="100%" disabled='disabled'>
                                    <option class='needsclick' value="10">10 Miles</option>
                                    <option class='needsclick' value="20">20 Miles</option>
                                    <option class='needsclick' value="50">50 Miles</option>
                                    <option class='needsclick' value="100" selected='selected'>100 Miles</option>
                                    <option class='needsclick' value="250">250 Miles</option>
                                </select>
                            </div>
                        </div>
                    </div> --}}
                    <div id='sortdircol' class='pickercol'>
                        <div class='filterlabel'>{{ trans('main_finder.sort direction') }}</div>
                        <div id="sortdir" class="">
                            <div class='radioStyleGrp'>
                                <input value="desc"  class='radioStyle' type="radio" name="sortdir" id="desc" checked="checked" />
                                <label for="desc">{{ trans('main_finder.descending') }}</label>
                            </div>
                            <div class='radioStyleGrp'>
                                <input value="asc" class='radioStyle' type="radio" name="sortdir" id="asc"/>
                                <label for="asc">{{ trans('main_finder.ascending') }}</label>
                            </div>
                        </div>
                    </div>
                    <div id='map_options_narrow'>
                        <div class='clear'></div>
                    </div>
                    <div class='clear'></div>
                </div>
                <div class='clear'></div>
            </div>
        </div>
        </div>
        <!-- <div class='clear'></div> -->
        <div id='mapside_results_wrap' style="top: {{ 158 + (ceil(count($pointsTypes) / 3) * 31)}}px">
            <div ng-view="" id="units" class="ng-scope">
            <div id="myAppView" class="ng-scope">
                <div class="container">
                    <div class="sixteen columns" style="position:relative;">
                        @foreach ($points as $point)
                            <div class="point unit" id="point_{{$point->id}}" data-sort-type="{{$point->type->type}}" data-sort-name="{{$point->name}}">
                                <table class="header">
                                  <tbody>
                                    <tr>
                                      <td class="widget">
                                        <div class="norgie">
                                          <div class="icon">
                                            <div class="pointerRight"></div>
                                          </div>
                                        </div>
                                      </td>
                                      <td class="view">
                                        <div class="unitname ng-scope">
                                            <img class="unit_icon" src="{{$point->type->icon}}" width="16" style="opacity:.4;">
                                          <a class="name" target="_self" href="{{ url(route('point.show', $point))}}">{{$point->name}}</a>
                                        </div>
                                        <div class="citystate">
                                          <span class="location">{{$point->region->name}}</span>
                                        </div>
                                      </td>
                                      <td class="options">
                                        <div class="option">
                                          <img class="marker iconbutton tooltipstered" src="/img/icons/marker.svg" width="14">
                                        </div>
                                        <img class="mobile_unit_icon ng-scope" src="/img/icons/menu/field.svg" width="16" style="opacity:.4;">
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>

                                <div class="content" style="height: 0px;">
                                  <hr>
                                  <div class="contentpad">
                                    <div class="ng-scope">
                                        @if ($point->hasAvatar)
                                            <div class="coverwrap ng-scope">
                                                <a target="_self" href="{{ url(route('point.show', $point))}}">
                                                    <img class="cover" src='/img/pavatars/p_{{ $point->id }}.png'>
                                                </a>
                                            </div>
                                        @endif
                                      <div class="citystate">{{$point->region->name}}</div>
                                      <div class="directions">
                                        <a target="_self" href="{{ url(route('point.show', $point))}}">Map &amp; Directions</a>
                                      </div>
                                      <div class="description">{{$point->description}}</div>
                                    </div>
                                  </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
              </div>
            </div>
        </div>
    </div>
    <div id='mapside_right'>
        <div id='feature' class='open'>
            <div id='canvas' style="height:100%;">
                <!--div id='map_tool_bar_wrap'>
                    <div class='container'>
                        <div id='map_tool_bar' class='sixteen columns'>
                            <div id='fit_visible_markers' class='map_tool_btn'>
                                <img src='/img/icons/refresh.svg' width='22' style='margin-top:4px;'>
                            </div>
                        </div>
                        <div class='clear'></div>
                    </div>
                </div-->
                <div class='gmap' id='map'></div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts_after')
  <script type="text/javascript" src="/js/pages/finder.js"></script>
  <script type='text/javascript'>
    MapManager.init({'selector' : $('#map'),
                   'center' : [60.2519,102.1420],
                   'zoom' : 4
                  });

    Finder.init({
        ftypeApplied : {!! isset($appliedFilters['ftype']) ? json_encode($appliedFilters['ftype']) : 'undefined' !!},
        points : {!! $pointsJson !!},
        region : {!! $currentRegion ? $currentRegion->id : '"nil"' !!}
    });

    Page.init({});
  </script>

@endsection