<!DOCTYPE html>
<html>

  @include('main.partials.htmlheader')

  <body>
    <div id="page">
      <div id='page_footer_push'>
        <!-- top -->
        <div id='page_content'>
          <!-- header -->
          @include('main.partials.topheader')
          <!-- content -->
          @include('main.partials.topcontent')
          <!-- menu -->
          @include('main.partials.topmenu')
          <div id='page_footer_margin'></div>
        </div>
        <!-- footer -->
        <div id='page_footer'>
          <div id='page_footer_padding'>
            <div class="container">
              <!-- copyright -->
              @include('main.partials.footercopyright')
              <!-- menu -->
              @include('main.partials.footermenu')
              <div class='clear'></div>
            </div>
          </div>
        </div>
      </div>
      <div class='clear'></div>
    </div>
  </body>
</html>
@yield('scripts_after')
