<div class='five columns' id='page_footer_left'>
  <a id='page_footer_logo' href='/'>AIRSOFT<span style='color:#b57b00; opacity:.7;'>LANDS</span></a>
  <div id='page_footer_copywrite' style="">
    <span>&#169;</span>{{ trans('layout.copyright', ['year' => date('Y')] ) }}
  </div>
</div>