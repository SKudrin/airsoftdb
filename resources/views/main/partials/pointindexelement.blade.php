<div id='point_{{ $point->id }}' class='collapser unit' >
  <table class='header'>
    <tr>
      <td class='widget'>
        <div class='norgie'>
          <div class='icon'>
            <div class='pointerRight'></div>
          </div>
        </div>
      </td>
      <td class='view'>
        <a class='unit_link' href="{{ url(route('point.show', $point))}}">{{ $point->name }}</a>
        <span class='unit_city_state'>{{ $point->region->name }}</span>
      </td>
      <td class='options'>
        {{-- <a class='unit_marker' href="{{ url(route('point.show', [$point, 'p' => 'location']))}}">
          <img src='/img/icons/marker.svg'>
        </a> --}}
      </td>
    </tr>
  </table>
  <div class='content'>
    <div class='content_padding'>
      <hr/>
      <div class='logo_wrap'>
        <a href='{{ url(route('point.show', $point))}}'>
           <img data-src="{{ $point->hasAvatar ? '/img/pavatars/p_' . $point->id . '.png' : ''}}" src="{{ $point->hasAvatar ? '/img/pavatars/p_' . $point->id . '.png' : ''}}" class='logo'>
        </a>
      </div>
      <a class='unit_address' href='{{ url(route('point.show', $point))}}'>{{ $point->address }}</a>
      <br />
        {!! str_limit(strip_tags($point->description, '<p><a><br><b>'), $limit = 150, $end = '...') !!}
          <a class='gold' href="{{ url(route('point.show', $point))}}">{{ trans('main_point_index.see more')}}</a>
    </div>
    <div class='clear'></div>
    <div class='collapser_bottom_padding'></div>
  </div>
</div>
