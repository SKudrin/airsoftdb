<div id="menubar-mobile-wrap">
    <div id="menubar-mobile">
        <a id='menubar-mobile-mmenu' href="#mainmenu-panel">
            <div class='menubar_mobile_button_bars' id='menubar_mobile_button'>
                <div class='menubar_mobile_button_bar'></div>
                <div class='menubar_mobile_button_bar'></div>
                <div class='menubar_mobile_button_bar'></div>
            </div>
        </a>
        <div id='menubar-mobile-title'>
            <a href='/'>
                <img src='/img/icons/logo/logo_wide_grey_gold_icon.png' height="26">
            </a>
        </div>
        <div id='mmenu-site-search'>
            <img class='site_search_button' style="width:22px;" src='/img/icons/search.png'>
        </div>
    </div>
</div>
<nav id="mainmenu-panel" style="display: none;">
    <ul>
        <li class="Label">{{ trans('layout.browse') }}</li>
        <li>
            <a class='mobile_menu_item' href="{{ route('finder')}}"><img src='/img/icons/menu/marker.png' /> {{ trans('layout.finder') }}
            </a>
            <ul>
                @foreach ($regions as $region)
                <li><a href="{{ route('finder', ['region' => $region])}}">{{$region->name}}</a></li>
                @endforeach
            </ul>
        </li>
        @foreach ($pointsTypes as $pointsType)
        <li>
            <a class='mobile_menu_item' href="{{ route('point.index', ['pointsType' => $pointsType]) }}"><img src='/img/icons/menu/marker.png' /> {{ $pointsType->name }}</a>
            <ul>
                @foreach ($pointsType->regions as $region)
                    <li><a href="{{ route('point.index', ['pointsType' => $pointsType, 'region' => $region]) }}">{{$region->name}}</a></li>
                @endforeach
            </ul>
        </li>
        @endforeach
        <li class="Label">{{ trans('layout.account') }}</li>
        @if (Auth::guest())
            <li>
                <a class='mobile_menu_item' href="{{ route('login') }}"><img src='/img/icons/menu/account.png' /> {{ trans('layout.sign in') }}
                </a>
            </li>
            <li>
                <a class='mobile_menu_item' href="{{ route('register') }}"><img src='/img/icons/menu/add.png' /> {{ trans('layout.create account') }}
                </a>
            </li>
        @else
            <li>
                <a class='mobile_menu_item' href="{{ route('user.profile') }}"><img src='/img/icons/menu/account.png' /> {{ trans('layout.profile') }}
                </a>
            </li>
            <li>
                <a class='mobile_menu_item' href="{{ route('logout') }}"><img src='/img/icons/arrow_goto_white.png' /> {{ trans('layout.logout') }}
                </a>
            </li>
        @endif
    </ul>
</nav>