<div class="j-profile-point">
<hr />
<h5 style="display: inline; margin-left: 5px; cursor: pointer;" class="j-profile-toggler">{{ $point->name }}</h5>
  <div class="j-profile-point-box" style="display: {{ (old('id') == $point->id || session('pointUpdate')['id'] == $point->id) ? 'block' : 'none' }}">
    <hr>
    <form id='form' action="{{ route('profile.update_point') }}" method="post" enctype="multipart/form-data">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <input type="hidden" name="id" value="{{ $point->id }}">
      <label>{{ trans('profile.avatar') }}</label>
      <br />
      <div style="border: 1px solid #444; width: 200px; height: 200px; margin-bottom: 5px;"><img src="{{ $point->hasAvatar ? '/img/pavatars/p_' . $point->id . '.png' : '/img/blank_avatar.png' }}" width="200px" height="200px" /></div>
      <div class='inputgrp'>
        <input type="file" class='gold' name="point[{{$point->id}}][avatar]" id="avatar">
      </div>
      <div style="width: 120px; margin-top: 3px; display: none;"><button id='submit' type='submit' class='small gold'>{{ trans('profile.delete') }}</button></div>
      <br />
      <br />
      <label>{{ trans('profile.point name') }}</label>
      <div class='inputgrp'>
        <input placeholder='' class="$errors->has('point.name') ? 'error' : ''" id='name' name='point[{{$point->id}}][name]' type="text" value="{{ old('point.' . $point->id . '.name', $point->name ) }}" autocomplete="off" />
        @if ($errors->has('point.' . $point->id . '.name'))
          @foreach ($errors->get('point.' . $point->id . '.name') as $error)
            <label for="name" class="error">{{ $error }}</label>
          @endforeach
        @endif
      </div>
      <br>
      <label>{{ trans('profile.type') }}</label>
      <div class='inputgrp'>
        <select class="selectpicker" data-live-search="true" name="point[{{$point->id}}][type_id]" >
          @foreach ($pointsTypes as $pointsType)
            <option {{ old('point.' . $point->id . '.type_id', $point->type->id) == $pointsType->id ? 'selected' : '' }} value="{{$pointsType->id}}">{{$pointsType->name}}</option>
          @endforeach
        </select>
        @if ($errors->has('point.' . $point->id . '.type_id'))
          @foreach ($errors->get('point.' . $point->id . '.type_id') as $error)
            <label for="type_id" class="error">{{ $error }}</label>
          @endforeach
        @endif
      </div>
      <br>
      <label>{{ trans('profile.point phone') }}</label>
      <div class='inputgrp'>
        <input placeholder='' class="$errors->has('point.phone') ? 'error' : ''" id='phone' name='point[{{$point->id}}][phone]' type="text" value="{{ old('point.' . $point->id . '.phone', $point->phone ) }}" autocomplete="off" />
        @if ($errors->has('point.' . $point->id . '.phone'))
          @foreach ($errors->get('point.' . $point->id . '.phone') as $error)
            <label for="phone" class="error">{{ $error }}</label>
          @endforeach
        @endif
      </div>
      <br>
      <label>{{ trans('profile.region') }}</label>
      <div class='inputgrp'>
        <select class="selectpicker" data-live-search="true" name="point[{{$point->id}}][region_id]" >
          @foreach ($regions as $region)
            <option {{ old('point.' . $point->id . '.region_id', $point->region_id ) == $region->id ? 'selected' : '' }} value="{{$region->id}}">{{$region->name}}</option>
          @endforeach
        </select>
        @if ($errors->has('point.' . $point->id . '.region_id'))
          @foreach ($errors->get('point.' . $point->id . '.region_id') as $error)
            <label for="region_id" class="error">{{ $error }}</label>
          @endforeach
        @endif
      </div>
      <br>
      <label>{{ trans('profile.point address') }}</label>
      <div class='inputgrp'>
        <input placeholder='' class="$errors->has('point.address') ? 'error' : ''" id='address' name='point[{{$point->id}}][address]' type="text" value="{{ old('point.' . $point->id . '.address', $point->address) }}" autocomplete="off" />
        @if ($errors->has('point.' . $point->id . '.address'))
          @foreach ($errors->get('point.' . $point->id . '.address') as $error)
            <label for="address" class="error">{{ $error }}</label>
          @endforeach
        @endif
      </div>
      <br>
      <label>{{ trans('profile.photos') }}</label>
      <br />
      @foreach ($point->photos()->get() as $photo)
        <img style="padding: 5px" src="/img/photos/s/{{$photo->name}}.{{$photo->ext}}" />
      @endforeach
      <div class='inputgrp'>
        <input type="file" class='gold' name="point[{{$point->id}}][photos][]" multiple id="photos">
      </div>
      <button id='submit' type='submit' class='basic gold'>{{ trans('profile.submit') }}</button>
      <br />
      <br />
    </form>
  </div>
</div>