<div id="header-main" class='header-bg'>
  <div id='header-main-mmenu'>
    <a href="#mainmenu-panel">
      <div class='menubar_mobile_button_bars' id='header-main-mmenu-icon'>
        <div class='menubar_mobile_button_bar'></div>
        <div class='menubar_mobile_button_bar'></div>
        <div class='menubar_mobile_button_bar'></div>
      </div>
    </a>
  </div>
  <div id='header-main-logo'>
    <a href="/" style="text-decoration:none;">
      <img width="130px" height="20px" src='/img/logo.png' >
    </a>
  </div>
  <div id='header-main-signin-buttons'>
    @if (Auth::guest())
      <a id='header-main-signup' href="{{ route('register') }}">{{ trans('layout.create account') }}</a> |
      <a href="{{ route('auth') }}">{{ trans('layout.sign in') }}</a>
    @else
    <a href="{{ route('user.profile') }}">{{ trans('layout.profile') }}</a> |
    <a href="{{ route('logout') }}">{{ trans('layout.logout') }}</a>
    @endif
  </div>
  <div class='clear'></div>
</div>
<div id="header-main-push"></div>