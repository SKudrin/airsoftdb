<head>
  <meta charset="utf-8">
  <title>@yield('htmlheader_title')</title>
  <meta name="description" content="@yield('htmlheader_description')">
  <meta http-equiv="content-type" content="text/html;charset=utf-8" />
  @yield('htmlheader_css_before')
  <link href="/img/icons/favicon.ico" rel="shortcut icon" type="image/x-icon" />
  <link href="/plugins/skeleton/base.css" rel="stylesheet">
  <link href="/plugins/skeleton/skeleton.css" rel="stylesheet">
  <link href="/plugins/skeleton/layout.css" rel="stylesheet">
  <link href="/plugins/jquery.mmenu/mmenu.css" type="text/css" rel="stylesheet" />
  <link href="/plugins/jquery.mmenu/mmenu-positioning.css" type="text/css" rel="stylesheet" />
  <link href="/css/global.css" rel="stylesheet">
  @yield('htmlheader_css_after')
  @yield('htmlheader_js_before')
  <script src="/plugins/jquery/jquery-2.1.4.min.js" type="text/javascript" ></script>
  <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
  <script src="/plugins/jquery.mmenu/jquery.mmenu.js" type="text/javascript"></script>
  <script src="/js/libs/a3-global.js" type="text/javascript" ></script>
  <script type="text/javascript">
    $(function() {
      Global.InitMobileMenu();
    });
  </script>
  @yield('htmlheader_js_after')
</head>