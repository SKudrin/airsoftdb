@extends('main.layout')

@section('htmlheader_title')
    {{ 'Airsoftlands' }}
@endsection

@section('htmlheader_description')
    {{ 'Airsoftlands' }}
@endsection

@section('htmlheader_css_before')
  <link href="/plugins/select/bootstrap.min.css" rel="stylesheet"/>s
  <link href="/plugins/select/bootstrap-select.min.css" rel="stylesheet"/>s
@endsection

@section('htmlheader_css_after')
  <link href="/css/pages/register.css" rel="stylesheet"/>
@endsection

@section('htmlheader')
@endsection

@section('htmlheader_js_after')
    <script src="/plugins/select/bootstrap.min.js"></script>
    <script src="/plugins/select/bootstrap-select.min.js"></script>
    <script src="/js/pages/profile.js"></script>
    <script type='text/javascript'>
      $(function() {
        $('.selectpicker').selectpicker();
      });
    </script>
@endsection
@section('topcontent')
<div class='container'>
    <div class='sixteen columns'>
        <div class='slim'>
        <br />
            <h3>{{ trans('profile.your profile') }}</h3>
            <hr>
            <form id='form' action="{{ route('profile.update_user') }}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <h3>{{ trans('profile.your contact info') }}</h3>
                <label>{{ trans('profile.avatar') }}</label>
                <br />
                <div style="border: 1px solid #444; width: 200px; height: 200px; margin-bottom: 5px;"><img src="{{ $user->hasAvatar ? '/img/uavatars/u_' . $user->id . '.png' : '/img/blank_avatar.png' }}" width="200px" height="200px" /></div>
                <div class='inputgrp'>
                  <input type="file" class='gold' name="avatar" id="avatar">
                </div>
                <div style="width: 120px; margin-top: 3px; display: none;"><button id='submit' type='submit' class='small gold'>{{ trans('profile.delete') }}</button></div>
                <br />
                <br />
                <label>{{ trans('profile.email') }}</label>
                <div class='inputgrp'>
                  <input placeholder='' class="$errors->has('email') ? 'error' : ''" id='email' name='email' type="text" value="{{ old('email', $user->email) }}" autocomplete="off" />
                  @if ($errors->has('email'))
                    @foreach ($errors->get('email') as $error)
                      <label for="email" class="error">{{ $error }}</label>
                    @endforeach
                  @endif
                </div>
                <br>
                <label>{{ trans('profile.your name') }}</label>
                <div class='inputgrp'>
                  <input placeholder='' class="$errors->has('name') ? 'error' : ''" id='name' name='name' type="text" value="{{ old('name', $user->name) }}" autocomplete="off" />
                  @if ($errors->has('name'))
                    @foreach ($errors->get('name') as $error)
                      <label for="name" class="error">{{ $error }}</label>
                    @endforeach
                  @endif
                </div>
                <br>
                <button id='submit' type='submit' class='basic gold j-update-user'>{{ trans('profile.submit') }}</button>
              </form>
              <br />
              <br />
              <br />
              <h3>{{ trans('profile.your points') }}</h3>
              @foreach ($user->points()->get() as $point)
                @include('main.partials.profilepointelement', ['point' => $point])
              @endforeach
              <br />
              <h3>{{ trans('profile.security') }}</h3>
              <div class="j-profile-point">
                <hr />
                <h5 style="display: inline; margin-left: 5px; cursor: pointer;" class="j-profile-toggler">{{ trans('profile.change password') }}</h5>
                <div class="j-profile-point-box" style="display: none">
                  <hr />
                  <form id='form' action="{{ route('profile.change_password') }}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <label>{{ trans('profile.old password') }}</label>
                    <div class='inputgrp'>
                      <input placeholder='' class="$errors->has('email') ? 'error' : ''" id='old_password' name='old_password' type="password" value="{{ old('old_password') }}" autocomplete="off" />
                      @if ($errors->has('old_password'))
                        @foreach ($errors->get('old_password') as $error)
                          <label for="old_password" class="error">{{ $error }}</label>
                        @endforeach
                      @endif
                    </div>
                    <br>
                    <label>{{ trans('profile.password') }}</label>
                    <div class='inputgrp'>
                      <input placeholder='' class="$errors->has('email') ? 'error' : ''" id='password' name='password' type="password" value="{{ old('password') }}" autocomplete="off" />
                      @if ($errors->has('password'))
                        @foreach ($errors->get('password') as $error)
                          <label for="password" class="error">{{ $error }}</label>
                        @endforeach
                      @endif
                    </div>
                    <br>
                    <label>{{ trans('profile.retype password') }}</label>
                    <div class='inputgrp'>
                      <input placeholder='' class="$errors->has('password_confirmation') ? 'error' : ''" id='password_confirmation' name='password_confirmation' type="password" value="{{ old('password_confirmation') }}" autocomplete="off" />
                      @if ($errors->has('password_confirmation'))
                        @foreach ($errors->get('password_confirmation') as $error)
                          <label for="password_confirmation" class="error">{{ $error }}</label>
                        @endforeach
                      @endif
                    </div>
                    <br>
                  </form>
                </div>
              </div>
        </div>
    </div>
</div>
@endsection

@section('scripts_after')
  <script type='text/javascript'>
    $(function() {
      Page.init({});
    });
  </script>
@endsection
