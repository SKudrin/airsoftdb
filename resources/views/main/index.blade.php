@extends('main.layout')

@section('htmlheader_title')
    {{ 'Airsoftlands ' }}
@endsection

@section('htmlheader_description')
    {{ 'Airsoftlands ' }}
@endsection

@section('htmlheader_css_before')
@endsection

@section('htmlheader_css_after')
  <link href="/fonts/BebasNeue/stylesheet.css" rel="stylesheet">
  <link href="/css/pages/index.css" rel="stylesheet"/>
@endsection

@section('htmlheader_js_after')
  <script type="text/javascript" src="/plugins/jquery.hammer/jquery.hammer.js"></script>
  <script type="text/javascript" src="http://d3js.org/d3.v3.min.js"></script>
  <script type="text/javascript" src="http://d3js.org/queue.v1.min.js"></script>
  <script type="text/javascript" src="http://d3js.org/topojson.v0.min.js"></script>
  <!-- <script type="text/javascript" src="http://d3js.org/topojson.v1.min.js"></script> -->
@endsection

@section('htmlheader')
@endsection

@section('topcontent')
  <div id="header-main-push"></div>
  <div id='intro' style="" class="">
      <div class='container'>
          <div class='sixteen columns'>
              <!-- <h1>AirsoftC3 Finder</h1> -->
              <div id='main_logo'>
                  <a href="/">
                      <img width="400px" height="109px" src="/img/logo.png" />
                  </a>
              </div>
              <div style="margin:0px auto 0px auto;">
                  <div id='finderlabel'>
                      <h1>{{trans('main_index.welcome')}}</h1>
                  </div>
              </div>
              <div class='unit_pages'>
                @foreach ($pointsTypes as $pointsType)
                  <a class='unit_page' href="{{ route('point.index', ['pointsType' => $pointsType]) }}">{{$pointsType->name}}</a>
                @endforeach
              </div>
              <div id='finder' class='round'>

              </div>
          </div>
      </div>
  </div>
  <div class='container'>
      <div class='sixteen columns'>
          <div id='state_label'>{{ $country ? $country->name : 'Страна' }}</div>
          <hr id='state_label_hr'>
          <div id='unit_picker'>
              <table>
                  <tr>
                      @foreach ($pointsTypes as $index => $pointsType)
                          <td>
                              <div id='field' class='unit'>
                                  <a class='unit_count' href="{{ route('point.index', ['pointsType' => $pointsType]) }}">
                                      <span class='unit_name'>{{$pointsType->name}}</span>
                                      <span class='count'>{{count($pointsType->points()->active()->get())}}</span>
                                  </a>
                                  <div class='switcher' data-action='hide' data-type='{{$pointsType->id}}'>{{trans('main_index.hide')}}</div>
                              </div>
                          </td>
                          @if ($index % 4 === 3)
                                </tr>
                            </table>
                            <hr id="state_label_hr">
                            <table>
                                <tr>
                          @endif
                      @endforeach
                  </tr>
              </table>
          </div>
          <div id='hide_plot_map'><span class="open"><nobr>{{trans('main_index.hide map')}}</nobr></span></div>
          <div id='plot_map_wrap'>
              <div id='map_loading' class="vcenter_wrap ">
                    <div class="vcenter_out">
                        <div class="vcenter_mid">
                            <div class="vcenter_inn">
                                <br />{{ trans('main_point_show.loading map')}} <img style='margin-left: 5px;' src='/img/icons/loader2.gif'><br /><br />
                            </div>
                        </div>
                    </div>
                </div>
              <div id="plot_map" style="display: none;"></div>
          </div>
          <div class='clear'></div>
          <div id='byname'>
              <span>{{trans('main_index.regions')}}</span>
          </div>
      </div>
      <div class='clear'></div>
      <div id='state_picker'>
          <div id='state_selector_names'>
              {{-- move to helper --}}
              {{-- */$regionsCount = count($regions)/* --}}
              {{-- */$regionsInColumn = floor($regionsCount / 4)/* --}}
              {{-- */$regionsNotInColumn = $regionsCount % 4/* --}}
              {{-- */$index = 0/* --}}
              @for ($i = 0; $i < 4; $i++)
                  <div class='four columns'>
                      @if ($regionsNotInColumn !== 0)
                          {{-- */$regionsNotInColumn -= 1/* --}}
                          <a class='state statesel' id='state_selector_IL' href="{{ route('finder', ['region' => $regions[$index]]) }}">{{ $regions[$index]->name }}</a>
                          {{-- */$index += 1/* --}}
                      @endif
                      @for ($j = 0; $j < $regionsInColumn; $j++)
                          <a class='state statesel' id='state_selector_IL' href="{{ route('finder', ['region' => $regions[$index]]) }}">{{ $regions[$index]->name }}</a>
                          {{-- */$index += 1/* --}}
                      @endfor
                  </div>
              @endfor
          </div>
      </div>
      <div class='clear'></div>
  </div>
  <div style=''>
      <div class='container'>
          <div class='sixteen columns'>
              <hr>
              {{-- <h2 id='retailers_label' class='gold section'>Информативнцый текстовый блок</h2>
              <div id='retailers_sub'>
                  Текст о чем то кст о чем ткст о чем ткст о чем ткст о чем ткст о чем ткст о чем ткст о чем ткст о чем ткст о чем ткст о чем ткст о чем ткст о чем ткст о чем ткст о чем ткст о чем ткст о чем ткст о чем ткст о чем ткст о чем ткст о чем ткст о чем ткст о чем ткст о чем ткст о чем ткст о чем ткст о чем ткст о чем ткст о чем ткст о чем ткст о чем ткст о чем ткст о чем ткст о чем т
              </div> --}}
              {{--<div id='retailers'>
                  <a href='retailer/1/xtreme-airsoft.html' class='retailer'>
                      <div class='tmp'>
                          <img alt='Xtreme Airsoft' src='retail/retailers/logos/xtreme_airsoft_white.png'>
                          <div class='retailer_name'>Xtreme Airsoft</div>
                      </div>
                  </a>
                  <a href='retailer/5/evike.html' class='retailer'>
                      <div class='tmp'>
                          <img alt='Evike' src='retail/retailers/logos/evike_white.png'>
                          <div class='retailer_name'>Evike</div>
                      </div>
                  </a>
                  <a href='retailer/7/airsoft-extreme.html' class='retailer'>
                      <div class='tmp'>
                          <img alt='Airsoft Extreme' src='retail/retailers/logos/airsoft_extreme_white.png'>
                          <div class='retailer_name'>Airsoft Extreme</div>
                      </div>
                  </a>
              </div>--}}
          </div>
          {{-- <div class='sixteen columns'>
              <hr>
              <h2 class='gold section'>Coming Soon</h2>
              <div class='read' id='upcoming'>
                  Our next major update will be events. You will be able to find events by date, type, and other criteria. Fields and Teams will be able to create one time events as well as recurring events. Shops and Retailers will be able to list events that they sponsor or support. Searching by event will make AirsoftC3's finder even more valuable to the airsoft community.
              </div>
          </div>--}}
      </div>
  </div>
@endsection

@section('scripts_after')
  <script type="text/javascript" src="/plugins/fastclick/fastclick.js"></script>
  <script type="text/javascript" src="/js/pages/index.js"></script>
  <script type="text/javascript">
  Page.init({});
  </script>
@endsection