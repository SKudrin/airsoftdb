<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        @yield('contentheader_title', 'Page Header here')
        <small>@yield('contentheader_description')</small>
    </h1>
    <ol class="breadcrumb">
        @section('breadcrumb')
        <li><a href="{{{ route('admin.home') }}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        @show
    </ol>
</section>