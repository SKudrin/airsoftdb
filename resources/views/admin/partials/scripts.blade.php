<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.1.4 -->
<script src="{{ asset('/plugins/jquery/jquery-2.1.4.min.js') }}"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="{{ asset('/js/bootstrap.min.js') }}" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="{{ asset('/js/app.min.js') }}" type="text/javascript"></script>
<!-- wysihtml5 -->
<script src="{{ asset('/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}" type="text/javascript"></script>
<!-- select2 -->
<script src="{{ asset('/plugins/select2/select2.min.js') }}" type="text/javascript"></script>
<!-- masked input -->
<script src="{{ asset('/plugins/jquery.maskedinput/jquery.maskedinput.min.js') }}" type="text/javascript"></script>
<!-- google autocomplete -->
<script src="https://maps.googleapis.com/maps/api/js?sensor=true&libraries=geometry,places,visualization&callback=initGoogle" async defer></script>
<script src="{{ asset('/js/libs/admin-geo.js') }}"></script>

<!-- multiselect -->
<script type="text/javascript" src="/plugins/bootstrap-multiselect/js/bootstrap-multiselect.js"></script>
<link rel="stylesheet" href="/plugins/bootstrap-multiselect/css/bootstrap-multiselect.css" type="text/css"/>

<script type="text/javascript">
  $(function() {
      $('textarea').wysihtml5();
      $('select.select2').select2();
      $('#point_id').multiselect({
        enableFiltering: true,
        buttonWidth: '100%',
        nonSelectedText: 'Select Related Record(s)'
      });
      $('#subscribe_types_id').multiselect({
        enableFiltering: true,
        buttonWidth: '100%',
        nonSelectedText: 'Select Groups Of Recipients'
      });
      $('.j-internal_point_add').click(function () {
        $('#point_id').multiselect($('.j-internal_point_select').is(':disabled') ? 'enable' : 'disable');
        $('.j-internal_point_create').prop("checked", !$('.j-internal_point_create').is(':checked'));
      });
      $("#phone").mask("+7 (999) 999-9999");
      $("#point_phone").mask("+7 (999) 999-9999");
  });
</script>