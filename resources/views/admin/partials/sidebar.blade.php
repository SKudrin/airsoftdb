<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ Auth::user()->hasAvatar ? '/img/uavatars/u_' . Auth::user()->id . '.png' : '/img/blank_avatar.png'}}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        {{--<!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
        </form>
        <!-- /.search form -->--}}

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">MAIN MENU</li>
            <!-- Optionally, you can add icons to the links -->
            <li class={{ Route::is('admin.home') ? 'active' : '' }}><a href="{{ route('admin.home') }}"><i class='fa fa-link'></i> <span>Dashboard</span></a></li>
            <li class="treeview {{ Route::is('admin.user.*') ? 'active' : '' }}">
                <a href="#"><i class='fa fa-user'></i> <span>Users</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.user.create')}}">Add user</a></li>
                    <li><a href="{{ route('admin.user.index') }}">List users</a></li>
                </ul>
            </li>
            <li class="treeview {{ Route::is('admin.point.*') || Route::is('admin.pointsType.*') ? 'active' : '' }}">
                <a href="#"><i class='fa fa-map-marker'></i> <span>Points</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.pointsType.create')}}">Add pointsType</a></li>
                    <li><a href="{{ route('admin.pointsType.index')}}">List pointsTypes</a></li>
                    <li><a href="{{ route('admin.point.create')}}">Add point</a></li>
                    <li><a href="{{ route('admin.point.index')}}">List points</a></li>
                </ul>
            </li>
            <li class="treeview {{ (Route::is('admin.country.*') || Route::is('admin.region.*')) ? 'active' : '' }}">
                <a href="#"><i class='fa fa-road'></i> <span>Geo</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.country.create')}}">Add Country</a></li>
                    <li><a href="{{ route('admin.country.index')}}">List Countries</a></li>
                    <li><a href="{{ route('admin.region.create')}}">Add region</a></li>
                    <li><a href="{{ route('admin.region.index')}}">List regions</a></li>
                </ul>
            </li>
            <li class="treeview {{ Route::is('admin.subscribe.*') ? 'active' : '' }}">
                <a href="#"><i class='fa fa-road'></i> <span>Subscribe</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.subscribe.create')}}">Subscribe</a></li>
                </ul>
            </li>
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
