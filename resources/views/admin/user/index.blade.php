@extends('admin.layout')

@section('htmlheader_title', 'Users list')
@section('contentheader_title', 'Users')
@section('contentheader_description', 'list')

@section('breadcrumb')
  @parent
  <li class="active">Users list</li>
@stop

@section('main-content')
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">Users <a class="btn btn-xs btn-info pull-right" href="{{ route('admin.user.create')}}">Add User</a></div>

      <div class="panel-body">
        <div class="box-body no-padding">
            @if (session('delete'))
              <div class="alert {{ session('delete')['success'] ? 'alert-success': 'alert-error'}}">
                  {{ session('delete')['message'] }}
              </div>
            @endif
            <table class="table table-hover table-bordered">
              <tbody>
                <tr>
                  <th style="width: 30px">Id</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th style="width: 20px">Active</th>
                  <th style="width: 20px">Admin</th>
                  <th style="width: 20px">Checked</th>
                  <th style="width: 80px">Created</th>
                  <th style="width: 80px">Actions</th>
                </tr>
                @if (count($users) > 0)
                  @foreach ($users as $user)
                  <tr>
                    <td>{{ $user->id }}</td>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>
                      <span class="label label-{{ $user->isActive ? 'success' : 'warning' }}">{{ $user->isActive ? 'Yes' : 'No' }}</span>
                    </td>
                    <td>
                      <span class="label label-{{ $user->isAdmin ? 'success' : 'warning' }}">{{ $user->isAdmin ? 'Yes' : 'No' }}</span>
                    </td>
                    <td>
                      <span class="label label-{{ $user->isChecked ? 'success' : 'warning' }}">{{ $user->isChecked ? 'Yes' : 'No' }}</span>
                    </td>
                    <td>{{ date('d.m.Y', strtotime($user->createdAt)) }}</td>
                    <td>
                      <a href="{{ route('admin.user.show', $user) }}"><i class='fa fa-eye'>&nbsp;</i></a>
                      <a href="{{ route('admin.user.edit', $user) }}"><i class='fa fa-edit'>&nbsp;</i></a>
                      <form id="delete_user_{{$user->id}}" style="display:none" action="{{ route('admin.user.destroy', $user) }}" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="_method" value="DELETE">
                      </form>
                      <a href="#" onclick="document.getElementById('delete_user_{{$user->id}}').submit();"><i class='fa fa-trash'>&nbsp;</i></a>
                    </td>
                  </tr>
                  @endforeach
                @else
                  <tr>
                    <td colspan='6'>Nothing here.. But you can <a href="{{ route('admin.user.create')}}">create ;)</a></td>
                  <tr>
                @endif
              </tbody>
            </table>
        </div>

        {!! $users->render() !!}
      </div>
    </div>
  </div>
</div>
@endsection
