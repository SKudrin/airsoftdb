@extends('admin.layout')

@section('htmlheader_title', 'User create')
@section('contentheader_title', 'User')
@section('contentheader_description', 'create')

@section('breadcrumb')
  @parent
  <li><a href="{{{ route('admin.user.index') }}}">Users list</a></li>
  <li class="active">User create</li>
@stop
@section('main-content')
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">New user</div>
      <div class="panel-body">
        <div class="box-body no-padding">
          <form autocomplete="off" method="POST" name="create_user" action="{{ route('admin.user.store') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="_method" value="POST">
            <input type="checkbox" {{ old('create_point') ? 'checked' : ''}} name="create_point" class="j-internal_point_create" type="checkbox" style="display: none">
            <input style="display:none" type="email" name="fakeusernameremembered">
            <input style="display:none" type="password" name="fakepasswordremembered">
            <div class="box-body">
              <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                <label for="name">Name</label>
                <input value="{{ old('name') }}" autocomplete="off" type="text" name="name" class="form-control" id="name" placeholder="Name">
                @if ($errors->has('name'))
                  <div class="alert-danger">
                    <ul>
                  @foreach ($errors->get('name') as $error)
                      <li>{{ $error }}</li>
                  @endforeach
                    </ul>
                  </div>
                @endif
              </div>
              <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                <label for="email">Email</label>
                <input value="{{ old('email') }}" autocomplete="off" type="email" name="email" class="form-control" id="email" placeholder="Email">
                @if ($errors->has('email'))
                  <div class="alert-danger">
                    <ul>
                  @foreach ($errors->get('email') as $error)
                      <li>{{ $error }}</li>
                  @endforeach
                    </ul>
                  </div>
                @endif
              </div>
              <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
                <label for="password">Password</label>
                <input value="{{ old('password') }}" autocomplete="off" type="password" name="password" class="form-control" id="password" placeholder="Password">
                @if ($errors->has('password'))
                  <div class="alert-danger">
                    <ul>
                  @foreach ($errors->get('password') as $error)
                      <li>{{ $error }}</li>
                  @endforeach
                    </ul>
                  </div>
                @endif
              </div>
              <div class="form-group {{ $errors->has('point_id') ? 'has-error' : ''}}">
                <label for="point_id">Select point</label><br />
                <select {{ old('create_point') ? 'disabled' : '' }} style="display:none;" name="point_id[]" class="form-control j-internal_point_select" style="display:block" id="point_id" multiple="multiple">
                  @foreach ($points as $point)
                      <option {{ in_array($point->id, old('point_id', [])) ? 'selected' : ''}} value="{{ $point->id }}">{{ $point->name }}</option>
                  @endforeach
                </select>
                @if ($errors->has('point_id'))
                  <div class="alert-danger">
                    <ul>
                  @foreach ($errors->get('point_id') as $error)
                      <li>{{ $error }}</li>
                  @endforeach
                    </ul>
                  </div>
                @endif
              </div>
              <div class="box box-default {{ old('create_point') ? '' : 'collapsed-box' }}">
                <div class="box-header with-border">
                  <h3 class="box-title">Add new point</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool j-internal_point_add" data-widget="collapse"><i class="fa fa-{{ old('create_point') ? 'minus' : 'plus' }}">.</i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body" style="display: {{ old('create_point') ? 'block' : 'none' }};">
                  <div class="form-group {{ $errors->has('point.name') ? 'has-error' : ''}}">
                    <label for="point_name">Name</label>
                    <input value="{{ old('point.name') }}" autocomplete="off" type="text" name="point[name]" class="form-control" id="point_name" placeholder="Name">
                    @if ($errors->has('point.name'))
                      <div class="alert-danger">
                        <ul>
                      @foreach ($errors->get('point.name') as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                        </ul>
                      </div>
                    @endif
                  </div>
                  <div class="form-group {{ $errors->has('point.phone') ? 'has-error' : ''}}">
                    <label for="point_phone">Phone</label>
                    <input value="{{ old('point.phone') }}" autocomplete="off" type="text" name="point[phone]" class="form-control" id="point_phone" placeholder="+7 (999) 999-9999">
                    @if ($errors->has('point.phone'))
                      <div class="alert-danger">
                        <ul>
                      @foreach ($errors->get('point.phone') as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                        </ul>
                      </div>
                    @endif
                  </div>
                  <div class="form-group {{ $errors->has('point.region_id') ? 'has-error' : ''}}">
                    <label for="point_region_id">Region</label>
                    <select name="point[region_id]" class="form-control" id="region_id">
                      @foreach ($regions as $region)
                          <option {{ old('point.region_id') == $region->id ? 'selected' : ''}} value="{{ $region->id }}">{{ $region->name }}</option>
                      @endforeach
                    </select>
                    @if ($errors->has('point.region_id'))
                      <div class="alert-danger">
                        <ul>
                      @foreach ($errors->get('point.region_id') as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                        </ul>
                      </div>
                    @endif
                  </div>
                  <div class="form-group {{ $errors->has('point.address') ? 'has-error' : ''}}">
                    <label for="point_address">Address</label>
                    <input value="{{ old('point.address') }}" autocomplete="off" type="text" name="point[address]" class="form-control" id="address" placeholder="Address">
                    @if ($errors->has('point.address'))
                      <div class="alert-danger">
                        <ul>
                      @foreach ($errors->get('point.address') as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                        </ul>
                      </div>
                    @endif
                  </div>
                  <div class="form-group {{ $errors->has('point.lat') ? 'has-error' : ''}}">
                    <label for="point_lat">Lat</label>
                    <input value="{{ old('point.lat') }}" autocomplete="off" type="text" name="point[lat]" class="form-control" id="lat" placeholder="55.7522200">
                    @if ($errors->has('point.lat'))
                      <div class="alert-danger">
                        <ul>
                      @foreach ($errors->get('point.lat') as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                        </ul>
                      </div>
                    @endif
                  </div>
                  <div class="form-group {{ $errors->has('point.lng') ? 'has-error' : ''}}">
                    <label for="point_lng">Lng</label>
                    <input value="{{ old('point.lng') }}" autocomplete="off" type="text" name="point[lng]" class="form-control" id="lng" placeholder="37.6155600">
                    @if ($errors->has('point.lng'))
                      <div class="alert-danger">
                        <ul>
                          @foreach ($errors->get('point.lng') as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                        </ul>
                      </div>
                    @endif
                  </div>
                  <div class="box box-default {{ old('map_opened') ? '' : 'collapsed-box' }}">
                    <div class="box-header with-border">
                      <h3 class="box-title">map</h3>
                      <div class="box-tools pull-right">
                        <button class="btn btn-box-tool j-internal_map" data-widget="collapse"><i class="fa fa-{{ old('map_opened') ? 'minus' : 'plus' }}">.</i></button>
                      </div><!-- /.box-tools -->
                    </div><!-- /.box-header -->
                    <div class="box-body" style="display: {{ old('map_opened') ? 'block' : 'none' }};">
                      <div id="g-map"style="height: 400px"></div>
                    </div>
                  </div>
                  <div class="form-group {{ $errors->has('point.description') ? 'has-error' : ''}}">
                    <label for="point_description">Description</label>
                    <textarea name="point[description]" class="form-control textarea-wysilhtml" id="point_description" placeholder="Description">{{ old('point.description') }}</textarea>
                    @if ($errors->has('point.description'))
                      <div class="alert-danger">
                        <ul>
                      @foreach ($errors->get('point.description') as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                        </ul>
                      </div>
                    @endif
                  </div>

                  <div class="form-group {{ $errors->has('point.type_id') ? 'has-error' : ''}}">
                    <label for="point_type_id">Type</label>
                    <select name="point[type_id]" class="form-control" id="point_type_id">
                      @foreach ($pointsTypes as $pointsType)
                          <option {{ old('point.type_id') == $pointsType->id ? 'selected' : ''}} value="{{ $pointsType->id }}">{{ $pointsType->name }}</option>
                      @endforeach
                    </select>
                    @if ($errors->has('point.type_id'))
                      <div class="alert-danger">
                        <ul>
                      @foreach ($errors->get('point.type_id') as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                        </ul>
                      </div>
                    @endif
                  </div>
                </div><!-- /.box-body -->
              </div>
              <div class="checkbox">
                <label>
                  <input {{ old('is_admin') ? 'checked' : ''}} type="checkbox" name="is_admin"> Admin
                </label>
              </div>
              <div class="checkbox">
                <label>
                  <input {{ old('is_active') ? 'checked' : ''}} type="checkbox" name="is_active"> Active
                </label>
              </div>
            </div><!-- /.box-body -->

            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
