@extends('admin.layout')

@section('htmlheader_title', 'User ' .  $user->name  . ' show')
@section('contentheader_title', 'User')
@section('contentheader_description', 'show')

@section('breadcrumb')
  @parent
  <li><a href="{{{ route('admin.user.index') }}}">Users list</a></li>
  <li class="active">User {{ $user->name }}</li>
@stop

@section('main-content')
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">{{ $user->name }}
        <a class="btn btn-xs btn-info pull-right" href="{{ route('admin.user.edit', $user) }}">Edit User</a>
        @if (!$user->isChecked)
          {{-- ALLOW user --}}
          <form id="allow_user_{{$user->id}}" style="display:none" action="{{ route('admin.user.active', $user) }}" method="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="_method" value="POST">
            <input type="hidden" name="is_active" value="true">
          </form>
          <a class="btn btn-xs btn-success pull-right" onclick="document.getElementById('allow_user_{{$user->id}}').submit();" style="margin-right: 10px" href="#">Allow User</a>
          {{-- DISALLOW user --}}
          <form id="disallow_user_{{$user->id}}" style="display:none" action="{{ route('admin.user.active', $user) }}" method="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="_method" value="POST">
          </form>
          <a class="btn btn-xs btn-danger pull-right" onclick="document.getElementById('disallow_user_{{$user->id}}').submit();" style="margin-right: 10px" href="#">Disallow User</a>
        @endif
      </div>

      <div class="panel-body">
        <div class="box-body no-padding">
            @if (session('create'))
              <div class="alert alert-success">
                  {{ session('create')['message'] }}
              </div>
            @endif
            @if (session('active'))
              <div class="alert {{ session('active')['active'] ? 'alert-success': 'alert-warning'}}">
                  {{ session('active')['message'] }}
              </div>
            @endif
            <table class="table table-hover table-bordered">
              <tbody>
                  <tr>
                    <th style="width: 60px">Property</th>
                    <th>Value</th>
                  </tr>
                  <tr>
                    <td>ID</td>
                    <td>{{ $user->id }}</td>
                  </tr>
                  <tr>
                    <td>Name</td>
                    <td>{{ $user->name }}</td>
                  </tr>
                  <tr>
                    <td>Email</td>
                    <td>{{ $user->email }}</td>
                  </tr>
                  <tr>
                    <td>Admin</td>
                    <td><span class="label label-{{ $user->isAdmin ? 'success' : 'warning' }}">{{ $user->isAdmin ? 'Yes' : 'No' }}</span></td>
                  </tr>
                  <tr>
                    <td>Active</td>
                    <td><span class="label label-{{ $user->isActive ? 'success' : 'warning' }}">{{ $user->isActive ? 'Yes' : 'No' }}</span></td>
                  </tr>
                  <tr>
                    <td>Checked</td>
                    <td><span class="label label-{{ $user->isChecked ? 'success' : 'warning' }}">{{ $user->isChecked ? 'Yes' : 'No' }}</span></td>
                  </tr>
                  </tr>
                    <td>Point</td>
                    <td>
                      @if ($user->points)
                        @foreach ($user->points as $key => $point)
                          <a href="{{ route('admin.point.show', $point) }}">{{ $point->name }}</a> {{ $key == count($user->points) - 1 ? "" : "|" }}
                        @endforeach
                      @else
                        &mdash;
                      @endif
                    </td>
                  <tr>
              </tbody>
            </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
