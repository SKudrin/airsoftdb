@extends('admin.layout')

@section('htmlheader_title', 'User ' .  $user->name  . ' edit')
@section('contentheader_title', 'User')
@section('contentheader_description', 'edit')

@section('breadcrumb')
  @parent
  <li><a href="{{{ route('admin.user.index') }}}">Users list</a></li>
  <li class="active">User {{ $user->name }} edit</li>
@stop

@section('main-content')
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">{{ $user->name }}</div>
      <div class="panel-body">
        <div class="box-body no-padding">
          @if (session('update'))
            <div class="alert alert-success">
                {{ session('update')['message'] }}
            </div>
          @endif
          <form autocomplete="off" method="POST" name="create_user" action="{{ route('admin.user.update', $user) }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="_method" value="PUT">
            <input style="display:none" type="email" name="fakeusernameremembered">
            <input style="display:none" type="password" name="fakepasswordremembered">
            <div class="box-body">
              <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                <label for="name">Name</label>
                <input value="{{ old('name', $user->name) }}" autocomplete="off" type="text" name="name" class="form-control" id="name" placeholder="Name">
                @if ($errors->has('name'))
                  <div class="alert-danger">
                    <ul>
                  @foreach ($errors->get('name') as $error)
                      <li>{{ $error }}</li>
                  @endforeach
                    </ul>
                  </div>
                @endif
              </div>
              <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                <label for="email">Email</label>
                <input value="{{ old('email', $user->email) }}" autocomplete="off" type="email" name="email" class="form-control" id="email" placeholder="Email">
                @if ($errors->has('email'))
                  <div class="alert-danger">
                    <ul>
                  @foreach ($errors->get('email') as $error)
                      <li>{{ $error }}</li>
                  @endforeach
                    </ul>
                  </div>
                @endif
              </div>
              <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
                <label for="password">Password</label>
                <input value="{{ old('password', $user->password) }}" autocomplete="off" type="password" name="password" class="form-control" id="password" placeholder="Password">
                @if ($errors->has('password'))
                  <div class="alert-danger">
                    <ul>
                  @foreach ($errors->get('password') as $error)
                      <li>{{ $error }}</li>
                  @endforeach
                    </ul>
                  </div>
                @endif
              </div>
              <div class="form-group {{ $errors->has('point_id') ? 'has-error' : ''}}">
                <label for="point_id">Select point</label><br />
                <select name="point_id[]" style="display: none;" multiple="multiple" class="form-control j-internal_point_select" id="point_id">
                  @foreach ($points as $point)
                      <option {{ in_array($point->id, old('point_id', array_fetch($user->points->toArray(), 'id') ? array_fetch($user->points->toArray(), 'id') : [])) ? 'selected' : ''}} value="{{ $point->id }}">{{ $point->name }}</option>
                  @endforeach
                </select>
                @if ($errors->has('point_id'))
                  <div class="alert-danger">
                    <ul>
                  @foreach ($errors->get('point_id') as $error)
                      <li>{{ $error }}</li>
                  @endforeach
                    </ul>
                  </div>
                @endif
              </div>
              <div class="checkbox">
                <label>
                  <input {{ old('is_admin', $user->is_admin) ? 'checked' : ''}} type="checkbox" name="is_admin"> Admin
                </label>
              </div>
              <div class="checkbox">
                <label>
                  <input {{ old('is_active', $user->is_active) ? 'checked' : ''}} type="checkbox" name="is_active"> Active
                </label>
              </div>
            </div><!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
