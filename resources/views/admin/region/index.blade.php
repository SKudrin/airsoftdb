@extends('admin.layout')

@section('htmlheader_title', 'Regions list')
@section('contentheader_title', 'Regions')
@section('contentheader_description', 'list')

@section('breadcrumb')
  @parent
  <li class="active">Regions list</li>
@stop

@section('main-content')
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">Regions <a class="btn btn-xs btn-info pull-right" href="{{ route('admin.region.create')}}">Add Region</a></div>

      <div class="panel-body">
        <div class="box-body no-padding">
            @if (session('delete'))
              <div class="alert {{ session('delete')['success'] ? 'alert-success': 'alert-error'}}">
                  {{ session('delete')['message'] }}
              </div>
            @endif
            <table class="table table-hover table-bordered">
              <tbody>
                <tr>
                  <th style="width: 30px">Id</th>
                  <th>Country</th>
                  <th>Name</th>
                  <th style="width: 80px">Created</th>
                  <th style="width: 80px">Actions</th>
                </tr>
                @if (count($regions) > 0)
                  @foreach ($regions as $region)
                  <tr>
                    <td>{{ $region->id }}</td>
                    <td>{{ $region->country->name }}</td>
                    <td>{{ $region->name }}</td>
                    <td>{{ date('d.m.Y', strtotime($region->createdAt)) }}</td>
                    <td>
                      <a href="{{ route('admin.region.show', $region) }}"><i class='fa fa-eye'>&nbsp;</i></a>
                      <a href="{{ route('admin.region.edit', $region) }}"><i class='fa fa-edit'>&nbsp;</i></a>
                      <form id="delete_region_{{$region->id}}" style="display:none" action="{{ route('admin.region.destroy', $region) }}" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="_method" value="DELETE">
                      </form>
                      <a href="#" onclick="document.getElementById('delete_region_{{$region->id}}').submit();"><i class='fa fa-trash'>&nbsp;</i></a>
                    </td>
                  </tr>
                  @endforeach
                @else
                  <tr>
                    <td colspan='9'>Nothing here.. But you can <a href="{{ route('admin.region.create')}}">create ;)</a></td>
                  <tr>
                @endif
              </tbody>
            </table>
        </div>

        {!! $regions->render() !!}
      </div>
    </div>
  </div>
</div>
@endsection
