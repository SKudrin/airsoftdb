@extends('admin.layout')

@section('htmlheader_title', 'Region ' . $region->name . ' show')
@section('contentheader_title', 'Region')
@section('contentheader_description', $region->name)

@section('breadcrumb')
  @parent
  <li><a href="{{{ route('admin.region.index') }}}">Regions list</a></li>
  <li class="active">Region  {{ $region->name }}</li>
@stop

@section('main-content')
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">Region {{ $region->name }}
        <a class="btn btn-xs btn-info pull-right" href="{{ route('admin.region.edit', $region) }}">Edit Region</a>
      </div>

      <div class="panel-body">
        <div class="box-body no-padding">
            @if (session('create'))
              <div class="alert alert-success">
                  {{ session('create')['message'] }}
              </div>
            @endif
            <table class="table table-hover table-bordered">
              <tbody>
                  <tr>
                    <th style="width: 60px">Property</th>
                    <th>Value</th>
                  </tr>
                  <tr>
                    <td>ID</td>
                    <td>{{ $region->id }}</td>
                  </tr>
                  <tr>
                    <td>Name</td>
                    <td>{{ $region->name }}</td>
                  </tr>
                  <tr>
                    <td>Country</td>
                    <td>{{ $region->country->name }}</td>
                  </tr>
              </tbody>
            </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
