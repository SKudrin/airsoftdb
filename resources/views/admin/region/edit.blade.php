@extends('admin.layout')

@section('htmlheader_title', 'Region ' . $region->name . ' edit')
@section('contentheader_title', 'Region')
@section('contentheader_description', 'edit')

@section('breadcrumb')
  @parent
  <li><a href="{{{ route('admin.region.index') }}}">Regions list</a></li>
  <li class="active">Region {{ $region->name }} edit </li>
@stop
@section('main-content')
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">{{ $region->name }}</div>
      <div class="panel-body">
        <div class="box-body no-padding">
          @if (session('update'))
            <div class="alert alert-success">
                {{ session('update')['message'] }}
            </div>
          @endif
          <form autocomplete="off" method="POST" name="edit_region" action="{{ route('admin.region.update', $region) }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="_method" value="PUT">
            <div class="box-body">
              <div class="box-body">
              <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                <label for="name">Name</label>
                <input value="{{ old('name', $region->name) }}" autocomplete="off" type="text" name="name" class="form-control" id="name" placeholder="Name">
                @if ($errors->has('name'))
                  <div class="alert-danger">
                    <ul>
                  @foreach ($errors->get('name') as $error)
                      <li>{{ $error }}</li>
                  @endforeach
                    </ul>
                  </div>
                @endif
              </div>
              <div class="form-group {{ $errors->has('country_id') ? 'has-error' : ''}}">
                <label for="country_id">Country</label>
                <select name="country_id" class="form-control select2" id="country_id">
                  @foreach ($countries as $country)
                      <option {{ old('country_id', $region->country->id) == $country->id ? 'selected' : ''}} value="{{ $country->id }}">{{ $country->name }}</option>
                  @endforeach
                </select>
                @if ($errors->has('country_id'))
                  <div class="alert-danger">
                    <ul>
                  @foreach ($errors->get('country_id') as $error)
                      <li>{{ $error }}</li>
                  @endforeach
                    </ul>
                  </div>
                @endif
              </div>
            </div><!-- /.box-body -->

            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
