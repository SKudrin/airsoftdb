@extends('admin.layout')

@section('htmlheader_title', 'Region create')
@section('contentheader_title', 'Region')
@section('contentheader_description', 'create')

@section('breadcrumb')
  @parent
  <li><a href="{{{ route('admin.region.index') }}}">Regions list</a></li>
  <li class="active">Region create</li>
@stop

@section('main-content')
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">New region</div>
      <div class="panel-body">
        <div class="box-body no-padding">
          <form autocomplete="off" method="POST" name="create_region" action="{{ route('admin.region.store') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="_method" value="POST">
            <div class="box-body">
              <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                <label for="name">Name</label>
                <input value="{{ old('name') }}" autocomplete="off" type="text" name="name" class="form-control" id="name" placeholder="Name">
                @if ($errors->has('name'))
                  <div class="alert-danger">
                    <ul>
                  @foreach ($errors->get('name') as $error)
                      <li>{{ $error }}</li>
                  @endforeach
                    </ul>
                  </div>
                @endif
              </div>
              <div class="form-group {{ $errors->has('country_id') ? 'has-error' : ''}}">
                <label for="country_id">Country</label>
                <select name="country_id" class="form-control select2" id="country_id">
                  @foreach ($countries as $country)
                      <option {{ old('country_id' == $country->id ? 'selected' : '')}} value="{{ $country->id }}">{{ $country->name }}</option>
                  @endforeach
                </select>
                @if ($errors->has('country_id'))
                  <div class="alert-danger">
                    <ul>
                  @foreach ($errors->get('country_id') as $error)
                      <li>{{ $error }}</li>
                  @endforeach
                    </ul>
                  </div>
                @endif
              </div>
            </div><!-- /.box-body -->

            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
