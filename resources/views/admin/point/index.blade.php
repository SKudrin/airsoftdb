@extends('admin.layout')

@section('htmlheader_title', 'Points list')
@section('contentheader_title', 'Points')
@section('contentheader_description', 'list')

@section('breadcrumb')
  @parent
  <li class="active">Points list</li>
@stop

@section('main-content')
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">Points <a class="btn btn-xs btn-info pull-right" href="{{ route('admin.point.create')}}">Add Point</a></div>

      <div class="panel-body">
        <div class="box-body no-padding">
            @if (session('delete'))
              <div class="alert {{ session('delete')['success'] ? 'alert-success': 'alert-error'}}">
                  {{ session('delete')['message'] }}
              </div>
            @endif
            <table class="table table-hover table-bordered">
              <tbody>
                <tr>
                  <th style="width: 30px">Id</th>
                  <th>Name</th>
                  <th>Phone</th>
                  <th style="width: 80px">Address</th>
                  <th style="width: 20px">Lat</th>
                  <th style="width: 20px">Lng</th>
                  <th style="width: 80px">Type</th>
                  <th style="width: 80px">Created</th>
                  <th style="width: 80px">Actions</th>
                </tr>
                @if (count($points) > 0)
                  @foreach ($points as $point)
                  <tr>
                    <td>{{ $point->id }}</td>
                    <td>{{ $point->name }}</td>
                    <td>{{ $point->phone }}</td>
                    <td>{{ $point->address }}</td>
                    <td>{{ $point->lat }}</td>
                    <td>{{ $point->lng }}</td>
                    <td>{{ $point->type->name }}</td>
                    <td>{{ date('d.m.Y', strtotime($point->createdAt)) }}</td>
                    <td>
                      <a href="{{ route('admin.point.show', $point) }}"><i class='fa fa-eye'>&nbsp;</i></a>
                      <a href="{{ route('admin.point.edit', $point) }}"><i class='fa fa-edit'>&nbsp;</i></a>
                      <form id="delete_point_{{$point->id}}" style="display:none" action="{{ route('admin.point.destroy', $point) }}" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="_method" value="DELETE">
                      </form>
                      <a href="#" onclick="document.getElementById('delete_point_{{$point->id}}').submit();"><i class='fa fa-trash'>&nbsp;</i></a>
                    </td>
                  </tr>
                  @endforeach
                @else
                  <tr>
                    <td colspan='9'>Nothing here.. But you can <a href="{{ route('admin.point.create')}}">create ;)</a></td>
                  <tr>
                @endif
              </tbody>
            </table>
        </div>

        {!! $points->render() !!}
      </div>
    </div>
  </div>
</div>
@endsection
