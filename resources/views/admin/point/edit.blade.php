@extends('admin.layout')

@section('htmlheader_title', 'Point ' . $point->name . ' edit')
@section('contentheader_title', 'Point')
@section('contentheader_description', 'edit')

@section('breadcrumb')
  @parent
  <li><a href="{{{ route('admin.point.index') }}}">Points list</a></li>
  <li class="active">Point {{ $point->name }} edit </li>
@stop
@section('main-content')
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">{{ $point->name }}</div>
      <div class="panel-body">
        <div class="box-body no-padding">
          @if (session('update'))
            <div class="alert alert-success">
                {{ session('update')['message'] }}
            </div>
          @endif
          <form autocomplete="off" method="POST" name="create_point" action="{{ route('admin.point.update', $point) }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="_method" value="PUT">
            <div class="box-body">
              <div class="box-body">
              <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                <label for="name">Name</label>
                <input value="{{ old('name', $point->name) }}" autocomplete="off" type="text" name="name" class="form-control" id="name" placeholder="Name">
                @if ($errors->has('name'))
                  <div class="alert-danger">
                    <ul>
                  @foreach ($errors->get('name') as $error)
                      <li>{{ $error }}</li>
                  @endforeach
                    </ul>
                  </div>
                @endif
              </div>
              <div class="form-group {{ $errors->has('phone') ? 'has-error' : ''}}">
                <label for="phone">Phone</label>
                <input value="{{ old('phone', $point->phone) }}" autocomplete="off" type="text" name="phone" class="form-control" id="phone" placeholder="+7 (999) 999-9999">
                @if ($errors->has('phone'))
                  <div class="alert-danger">
                    <ul>
                  @foreach ($errors->get('phone') as $error)
                      <li>{{ $error }}</li>
                  @endforeach
                    </ul>
                  </div>
                @endif
              </div>
              <div class="form-group {{ $errors->has('region_id') ? 'has-error' : ''}}">
                <label for="region_id">Region</label>
                <select name="region_id" class="form-control select2" id="region_id">
                  @foreach ($regions as $regions)
                      <option {{ old('region_id', $point->region->id) == $regions->id ? 'selected' : ''}} value="{{ $regions->id }}">{{ $regions->name }}</option>
                  @endforeach
                </select>
                @if ($errors->has('region_id'))
                  <div class="alert-danger">
                    <ul>
                  @foreach ($errors->get('region_id') as $error)
                      <li>{{ $error }}</li>
                  @endforeach
                    </ul>
                  </div>
                @endif
              </div>
              <div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
                <label for="address">Address</label>
                <input value="{{ old('address', $point->address) }}" autocomplete="off" type="text" name="address" class="form-control" id="address" placeholder="Address">
                @if ($errors->has('address'))
                  <div class="alert-danger">
                    <ul>
                  @foreach ($errors->get('address') as $error)
                      <li>{{ $error }}</li>
                  @endforeach
                    </ul>
                  </div>
                @endif
              </div>
              <div class="form-group {{ $errors->has('lat') ? 'has-error' : ''}}">
                <label for="lat">Lat</label>
                <input value="{{ old('lat', $point->lat) }}" autocomplete="off" type="text" name="lat" class="form-control" id="lat" placeholder="55.7522200">
                @if ($errors->has('lat'))
                  <div class="alert-danger">
                    <ul>
                  @foreach ($errors->get('lat') as $error)
                      <li>{{ $error }}</li>
                  @endforeach
                    </ul>
                  </div>
                @endif
              </div>
              <div class="form-group {{ $errors->has('lng') ? 'has-error' : ''}}">
                <label for="lng">Lng</label>
                <input value="{{ old('lng', $point->lng) }}" autocomplete="off" type="text" name="lng" class="form-control" id="lng" placeholder="37.6155600">
                @if ($errors->has('lng'))
                  <div class="alert-danger">
                    <ul>
                  @foreach ($errors->get('lng') as $error)
                      <li>{{ $error }}</li>
                  @endforeach
                    </ul>
                  </div>
                @endif
              </div>
              <div class="box box-default {{ old('map_opened') ? '' : 'collapsed-box' }}">
                <div class="box-header with-border">
                  <h3 class="box-title">map</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool j-internal_map" data-widget="collapse"><i class="fa fa-{{ old('map_opened') ? 'minus' : 'plus' }}">.</i></button>
                  </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body" style="display: {{ old('map_opened') ? 'block' : 'none' }};">
                  <div id="g-map"style="height: 400px"></div>
                </div>
              </div>
              <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
                <label for="description">Description</label>
                <textarea name="description" class="form-control textarea-wysilhtml" id="description" placeholder="Description">{{ old('description', $point->description) }}</textarea>
                @if ($errors->has('description'))
                  <div class="alert-danger">
                    <ul>
                  @foreach ($errors->get('email') as $error)
                      <li>{{ $error }}</li>
                  @endforeach
                    </ul>
                  </div>
                @endif
              </div>
              <div class="form-group {{ $errors->has('type_id') ? 'has-error' : ''}}">
                <label for="type_id">Type</label>
                <select name="type_id" class="form-control select2" id="type_id">
                  @foreach ($pointsTypes as $pointsType)
                      <option {{ old('type_id', $point->type->id) == $pointsType->id ? 'selected' : ''}} value="{{ $pointsType->id }}">{{ $pointsType->name }}</option>
                  @endforeach
                </select>
                @if ($errors->has('type_id'))
                  <div class="alert-danger">
                    <ul>
                  @foreach ($errors->get('type_id') as $error)
                      <li>{{ $error }}</li>
                  @endforeach
                    </ul>
                  </div>
                @endif
              </div>
              <div class="form-group {{ $errors->has('user_id') ? 'has-error' : ''}}">
                <label for="user_id">User</label>

                <select name="user_id" class="form-control select2" id="user_id">
                    <option value="">&mdash;</option>
                    @foreach ($users as $user)
                      <option {{ old('user_id', $point->user ? $point->user->id : '') == $user->id ? 'selected' : ''}} value="{{ $user->id }}">{{ $user->email }}</option>
                    @endforeach
                </select>
                @if ($errors->has('user_id'))
                  <div class="alert-danger">
                    <ul>
                  @foreach ($errors->get('user_id') as $error)
                      <li>{{ $error }}</li>
                  @endforeach
                    </ul>
                  </div>
                @endif
              </div>
            </div><!-- /.box-body -->

            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
