@extends('admin.layout')

@section('htmlheader_title', 'Point ' . $point->name . ' show')
@section('contentheader_title', 'Point')
@section('contentheader_description', $point->name)

@section('breadcrumb')
  @parent
  <li><a href="{{{ route('admin.point.index') }}}">Points list</a></li>
  <li class="active">Point  {{ $point->name }}</li>
@stop

@section('main-content')
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">Point {{ $point->name }}
        <a class="btn btn-xs btn-info pull-right" href="{{ route('admin.point.edit', $point) }}">Edit Point</a>
      </div>

      <div class="panel-body">
        <div class="box-body no-padding">
            @if (session('create'))
              <div class="alert alert-success">
                  {{ session('create')['message'] }}
              </div>
            @endif
            <table class="table table-hover table-bordered">
              <tbody>
                  <tr>
                    <th style="width: 60px">Property</th>
                    <th>Value</th>
                  </tr>
                  <tr>
                    <td>ID</td>
                    <td>{{ $point->id }}</td>
                  </tr>
                  <tr>
                    <td>Name</td>
                    <td>{{ $point->name }}</td>
                  </tr>
                  <tr>
                    <td>Phone</td>
                    <td>{{ $point->phone }}</td>
                  </tr>
                  <tr>
                    <td>Adsress</td>
                    <td>{{ $point->address }}</td>
                  </tr>
                  <tr>
                    <td>Description</td>
                    <td>{{ $point->description }}</td>
                  </tr>
                  <tr>
                    <td>lat</td>
                    <td>{{ $point->lat }}</td>
                  </tr>
                  <tr>
                    <td>lng</td>
                    <td>{{ $point->lng }}</td>
                  </tr>
                  <tr>
                    <td>Type</td>
                    <td>{{ $point->type->name }}</td>
                  </tr>
                  <tr>
                    <td>VIP</td>
                    <td><span class="label label-{{ $point->is_vip ? 'success' : 'warning' }}">{{ $point->is_vip ? 'Yes' : 'No' }}</span></td>
                  </tr>
                  <tr>
                    <td>Залет</td>
                    <td><span class="label label-{{ $point->is_bad ? 'success' : 'warning' }}">{{ $point->is_bad ? 'Yes' : 'No' }}</span></td>
                  </tr>
                  <tr>
                    <td>User</td>
                    <td>
                      @if ($point->user)
                        <a href="{{ route('admin.user.show', $point->user) }}">{{ $point->user->email }}</a>
                      @else
                        &mdash;
                      @endif
                    </td>
                  </tr>
              </tbody>
            </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
