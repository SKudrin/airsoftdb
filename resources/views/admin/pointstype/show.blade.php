@extends('admin.layout')

@section('htmlheader_title', 'Points Type ' .  $pointsType->name . ' show')
@section('contentheader_title', 'Points Type')
@section('contentheader_description', 'show')

@section('breadcrumb')
  @parent
  <li><a href="{{{ route('admin.pointsType.index') }}}">Points types list</a></li>
  <li class="active">Points type {{ $pointsType->name }}</li>
@stop

@section('main-content')
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">{{ $pointsType->name }}
        <a class="btn btn-xs btn-info pull-right" href="{{ route('admin.pointsType.edit', $pointsType) }}">Edit Points Type</a>
      </div>

      <div class="panel-body">
        <div class="box-body no-padding">
            @if (session('create'))
              <div class="alert alert-success">
                  {{ session('create')['message'] }}
              </div>
            @endif
            <table class="table table-hover table-bordered">
              <tbody>
                  <tr>
                    <th style="width: 60px">Property</th>
                    <th>Value</th>
                  </tr>
                  <tr>
                    <td>ID</td>
                    <td>{{ $pointsType->id }}</td>
                  </tr>
                  <tr>
                    <td>Name</td>
                    <td>{{ $pointsType->name }}</td>
                  </tr>
                  <tr>
                    <td>Description</td>
                    <td>{{ $pointsType->description }}</td>
                  </tr>
              </tbody>
            </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
