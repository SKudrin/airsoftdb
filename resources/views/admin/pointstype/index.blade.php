@extends('admin.layout')

@section('htmlheader_title', 'Points types list')
@section('contentheader_title', 'Points types')
@section('contentheader_description', 'list')

@section('breadcrumb')
  @parent
  <li class="active">Points types list</li>
@stop

@section('main-content')
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">Points types <a class="btn btn-xs btn-info pull-right" href="{{ route('admin.pointsType.create')}}">Add Points Type</a></div>

      <div class="panel-body">
        <div class="box-body no-padding">
            @if (session('delete'))
              <div class="alert {{ session('delete')['success'] ? 'alert-success': 'alert-error'}}">
                  {{ session('delete')['message'] }}
              </div>
            @endif
            <table class="table table-hover table-bordered">
              <tbody>
                <tr>
                  <th style="width: 30px">Id</th>
                  <th>Name</th>
                  <th>Description</th>
                  <th style="width: 80px">Created</th>
                  <th style="width: 80px">Actions</th>
                </tr>
                @if (count($pointsTypes) > 0)
                  @foreach ($pointsTypes as $pointsType)
                  <tr>
                    <td>{{ $pointsType->id }}</td>
                    <td>{{ $pointsType->name }}</td>
                    <td>{{ $pointsType->description }}</td>
                    <td>{{ date('d.m.Y', strtotime($pointsType->createdAt)) }}</td>
                    <td>
                      <a href="{{ route('admin.pointsType.show', $pointsType) }}"><i class='fa fa-eye'>&nbsp;</i></a>
                      <a href="{{ route('admin.pointsType.edit', $pointsType) }}"><i class='fa fa-edit'>&nbsp;</i></a>
                      <form id="delete_pointsType_{{$pointsType->id}}" style="display:none" action="{{ route('admin.pointsType.destroy', $pointsType) }}" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="_method" value="DELETE">
                      </form>
                      <a href="#" onclick="document.getElementById('delete_pointsType_{{$pointsType->id}}').submit();"><i class='fa fa-trash'>&nbsp;</i></a>
                    </td>
                  </tr>
                  @endforeach
                @else
                  <tr>
                    <td colspan='6'>Nothing here.. But you can <a href="{{ route('admin.pointsType.create')}}">create ;)</a></td>
                  <tr>
                @endif
              </tbody>
            </table>
        </div>

        {!! $pointsTypes->render() !!}
      </div>
    </div>
  </div>
</div>
@endsection
