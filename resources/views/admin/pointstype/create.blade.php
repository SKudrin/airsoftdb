@extends('admin.layout')

@section('htmlheader_title', 'Points type create')
@section('contentheader_title', 'Points type')
@section('contentheader_description', 'create')

@section('breadcrumb')
  @parent
  <li><a href="{{{ route('admin.pointsType.index') }}}">Points types list</a></li>
  <li class="active">Points Type create</li>
@stop

@section('main-content')
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">New points type</div>
      <div class="panel-body">
        <div class="box-body no-padding">
          <form autocomplete="off" method="POST" name="create_points_type" action="{{ route('admin.pointsType.store') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="_method" value="POST">

            <div class="box-body">
              <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                <label for="name">Name</label>
                <input value="{{ old('name') }}" autocomplete="off" type="text" name="name" class="form-control" id="name" placeholder="Name">
                @if ($errors->has('name'))
                  <div class="alert-danger">
                    <ul>
                  @foreach ($errors->get('name') as $error)
                      <li>{{ $error }}</li>
                  @endforeach
                    </ul>
                  </div>
                @endif
              </div>
              <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
                <label for="description">Description</label>
                <textarea name="description" class="form-control textarea-wysilhtml" id="description" placeholder="description">{{ old('description') }}</textarea>
                @if ($errors->has('description'))
                  <div class="alert-danger">
                    <ul>
                  @foreach ($errors->get('description') as $error)
                      <li>{{ $error }}</li>
                  @endforeach
                    </ul>
                  </div>
                @endif
              </div>
            </div><!-- /.box-body -->

            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
