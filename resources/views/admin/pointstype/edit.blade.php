@extends('admin.layout')

@section('htmlheader_title', 'Points type ' .  $pointsType->name  . ' edit')
@section('contentheader_title', 'Points type')
@section('contentheader_description', 'edit')

@section('breadcrumb')
  @parent
  <li><a href="{{{ route('admin.pointsType.index') }}}">Points types list</a></li>
  <li class="active">Points type {{ $pointsType->name }} edit </li>
@stop

@section('main-content')
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">{{ $pointsType->name }}</div>
      <div class="panel-body">
        <div class="box-body no-padding">
          @if (session('update'))
            <div class="alert alert-success">
                {{ session('update')['message'] }}
            </div>
          @endif
          <form autocomplete="off" method="POST" name="create_points_type" action="{{ route('admin.pointsType.update', $pointsType) }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="_method" value="PUT">
            <div class="box-body">
              <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                <label for="name">Name</label>
                <input value="{{ old('name', $pointsType->name) }}" autocomplete="off" type="text" name="name" class="form-control" id="name" placeholder="Name">
                @if ($errors->has('name'))
                  <div class="alert-danger">
                    <ul>
                  @foreach ($errors->get('name') as $error)
                      <li>{{ $error }}</li>
                  @endforeach
                    </ul>
                  </div>
                @endif
              </div>
              <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
                <label for="description">Description</label>
                <textarea name="description" class="form-control textarea-wysilhtml" id="description" placeholder="Description">{{ old('description', $pointsType->description) }}</textarea>
                @if ($errors->has('description'))
                  <div class="alert-danger">
                    <ul>
                  @foreach ($errors->get('description') as $error)
                      <li>{{ $error }}</li>
                  @endforeach
                    </ul>
                  </div>
                @endif
              </div>
            </div><!-- /.box-body -->

            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
