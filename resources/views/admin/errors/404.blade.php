@extends('admin.layout')

@section('htmlheader_title')
    Page not found
@endsection

@section('contentheader_title')
    404 Error Page
@endsection

@section('$contentheader_description')
@endsection

@section('main-content')

<div class="error-page">
    <h2 class="headline text-yellow">404</h2>
    <div class="error-content">
        <img src="{{asset('/img/admin/doh.png')}}" width="90px" height="120px" />

        <h3><i class="fa fa-warning text-yellow"></i> Oops! Page not found.</h3>
        <p>
            This is realy unpleasant situation, we sorry.<br />
            May be <a href='{{ route('admin.home') }}'>return to dashboard</a>?
        </p>
    </div><!-- /.error-content -->
</div><!-- /.error-page -->
@endsection