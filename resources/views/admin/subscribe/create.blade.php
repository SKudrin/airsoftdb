@extends('admin.layout')

@section('htmlheader_title', 'Subscribe')
@section('contentheader_title', 'Subscribe')
@section('contentheader_description', 'Subscribe')

@section('breadcrumb')
  @parent
  <li class="active">Subscribe</li>
@stop
@section('main-content')
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">New subscribe</div>
      <div class="panel-body">
        <div class="box-body no-padding">
          @if (session('process'))
            <div class="alert alert-success">
                {{ session('process')['message'] }}
            </div>
          @endif
          <form autocomplete="off" method="POST" name="create_subscribe" action="{{ route('admin.subscribe.store') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="_method" value="POST">

            <div class="form-group {{ $errors->has('type_id') ? 'has-error' : ''}}">
              <label for="type_id">Groups of recipients</label>
              <select name="type_id[]" class="form-control j-internal_point_select" id="subscribe_types_id" multiple="multiple">
                @foreach ($pointsTypes as $pointsType)
                  <option {{ old('type_id') == $pointsType->id ? 'selected' : ''}} value="{{ $pointsType->id }}">{{ $pointsType->name }}</option>
                @endforeach
              </select>
              @if ($errors->has('type_id'))
                <div class="alert-danger">
                  <ul>
                    @foreach ($errors->get('type_id') as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                  </ul>
                </div>
              @endif
            </div>
            <div class="box-body">
              <div class="form-group {{ $errors->has('subject') ? 'has-error' : ''}}">
                <label for="subject">Subject</label>
                <input value="{{ old('subject') }}" autocomplete="off" type="text" name="subject" class="form-control" id="subject" placeholder="subject">
                @if ($errors->has('subject'))
                  <div class="alert-danger">
                    <ul>
                  @foreach ($errors->get('subject') as $error)
                      <li>{{ $error }}</li>
                  @endforeach
                    </ul>
                  </div>
                @endif
              </div>
              <div class="form-group {{ $errors->has('subject') ? 'has-error' : ''}}">
                <label for="subject">Body</label>
                <textarea name="body" class="form-control textarea-wysilhtml" id="body" placeholder="body">{{ old('body') }}</textarea>
                @if ($errors->has('body'))
                  <div class="alert-danger">
                    <ul>
                  @foreach ($errors->get('body') as $error)
                      <li>{{ $error }}</li>
                  @endforeach
                    </ul>
                  </div>
                @endif
              </div>
              <div class="form-group {{ $errors->has('test_mail') ? 'has-error' : ''}}">
                <label for="test_mail">Test mode (send only one e-mail)</label>
                <input value="{{ old('test_mail') }}" autocomplete="off" type="text" name="test_mail" class="form-control" id="test_mail" placeholder="test_mail">
                @if ($errors->has('test_mail'))
                  <div class="alert-danger">
                    <ul>
                  @foreach ($errors->get('test_mail') as $error)
                      <li>{{ $error }}</li>
                  @endforeach
                    </ul>
                  </div>
                @endif
              </div>
              {{-- <div class="checkbox">
                <label>
                  <input {{ old('is_admin') ? 'checked' : ''}} type="checkbox" name="is_admin"> Admin
                </label>
              </div>
              <div class="checkbox">
                <label>
                  <input {{ old('is_active') ? 'checked' : ''}} type="checkbox" name="is_active"> Active
                </label>
              </div> --}}
            </div><!-- /.box-body -->

            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
