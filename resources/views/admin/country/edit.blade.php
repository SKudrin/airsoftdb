@extends('admin.layout')

@section('htmlheader_title', 'Country ' .  $country->name  . ' edit')
@section('contentheader_title', 'Country')
@section('contentheader_description', 'edit')

@section('breadcrumb')
  @parent
  <li><a href="{{{ route('admin.country.index') }}}">Countries list</a></li>
  <li class="active">Country {{ $country->name }} edit </li>
@stop

@section('main-content')
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">{{ $country->name }}</div>
      <div class="panel-body">
        <div class="box-body no-padding">
          @if (session('update'))
            <div class="alert alert-success">
                {{ session('update')['message'] }}
            </div>
          @endif
          <form autocomplete="off" method="POST" name="create_country" action="{{ route('admin.country.update', $country) }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="_method" value="PUT">
            <div class="box-body">
              <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                <label for="name">Name</label>
                <input value="{{ old('name', $country->name) }}" autocomplete="off" type="text" name="name" class="form-control" id="name" placeholder="Name">
                @if ($errors->has('name'))
                  <div class="alert-danger">
                    <ul>
                  @foreach ($errors->get('name') as $error)
                      <li>{{ $error }}</li>
                  @endforeach
                    </ul>
                  </div>
                @endif
              </div>
            </div><!-- /.box-body -->

            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
