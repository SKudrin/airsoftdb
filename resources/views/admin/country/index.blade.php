@extends('admin.layout')

@section('htmlheader_title', 'Countries list')
@section('contentheader_title', 'Countries')
@section('contentheader_description', 'list')

@section('breadcrumb')
  @parent
  <li class="active">Countries list</li>
@stop

@section('main-content')
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">Countries <a class="btn btn-xs btn-info pull-right" href="{{ route('admin.country.create')}}">Add Country</a></div>

      <div class="panel-body">
        <div class="box-body no-padding">
            @if (session('delete'))
              <div class="alert {{ session('delete')['success'] ? 'alert-success': 'alert-error'}}">
                  {{ session('delete')['message'] }}
              </div>
            @endif
            <table class="table table-hover table-bordered">
              <tbody>
                <tr>
                  <th style="width: 30px">Id</th>
                  <th>Name</th>
                  <th style="width: 80px">Created</th>
                  <th style="width: 80px">Actions</th>
                </tr>
                @if (count($countries) > 0)
                  @foreach ($countries as $country)
                  <tr>
                    <td>{{ $country->id }}</td>
                    <td>{{ $country->name }}</td>
                    <td>{{ date('d.m.Y', strtotime($country->createdAt)) }}</td>
                    <td>
                      <a href="{{ route('admin.country.show', $country) }}"><i class='fa fa-eye'>&nbsp;</i></a>
                      <a href="{{ route('admin.country.edit', $country) }}"><i class='fa fa-edit'>&nbsp;</i></a>
                      <form id="delete_country_{{$country->id}}" style="display:none" action="{{ route('admin.country.destroy', $country) }}" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="_method" value="DELETE">
                      </form>
                      <a href="#" onclick="document.getElementById('delete_country_{{$country->id}}').submit();"><i class='fa fa-trash'>&nbsp;</i></a>
                    </td>
                  </tr>
                  @endforeach
                @else
                  <tr>
                    <td colspan='6'>Nothing here.. But you can <a href="{{ route('admin.country.create')}}">create ;)</a></td>
                  <tr>
                @endif
              </tbody>
            </table>
        </div>

        {!! $countries->render() !!}
      </div>
    </div>
  </div>
</div>
@endsection
