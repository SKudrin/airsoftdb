@extends('admin.layout')

@section('htmlheader_title', 'Country ' .  $country->name . ' show')
@section('contentheader_title', 'Country')
@section('contentheader_description', 'show')

@section('breadcrumb')
  @parent
  <li><a href="{{{ route('admin.country.index') }}}">Countries list</a></li>
  <li class="active">Country {{ $country->name }}</li>
@stop

@section('main-content')
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">{{ $country->name }}
        <a class="btn btn-xs btn-info pull-right" href="{{ route('admin.country.edit', $country) }}">Edit Country</a>
      </div>

      <div class="panel-body">
        <div class="box-body no-padding">
            @if (session('create'))
              <div class="alert alert-success">
                  {{ session('create')['message'] }}
              </div>
            @endif
            <table class="table table-hover table-bordered">
              <tbody>
                  <tr>
                    <th style="width: 60px">Property</th>
                    <th>Value</th>
                  </tr>
                  <tr>
                    <td>ID</td>
                    <td>{{ $country->id }}</td>
                  </tr>
                  <tr>
                    <td>Name</td>
                    <td>{{ $country->name }}</td>
                  </tr>
              </tbody>
            </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
